var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var globalDocument = document;
var globalWindow = window;
var System;
(function (System) {
    System.document = globalDocument;
    System.window = globalWindow;
    System.isTouchSupported = "ontouchstart" in System.document.documentElement;
    System.isLikelyNode = false;
    function DefaultValue(value) {
        return (function (target, key) {
            target[key] = value;
        });
    }
    System.DefaultValue = DefaultValue;
    function Browsable(target, key) {
        var t = target;
        if (t.hasOwnProperty('stateProperties') === false) {
            target.stateProperties = [];
        }
        target.stateProperties.push(key);
    }
    System.Browsable = Browsable;
})(System || (System = {}));
var NxtControl;
(function (NxtControl) {
    var Util;
    (function (Util) {
        function enlivenObjects(objects, callback, namespace, reviver) {
            objects = objects || [];
            function onLoaded() {
                if (++numLoadedObjects === numTotalObjects) {
                    callback && callback(enlivenedObjects);
                }
            }
            var enlivenedObjects = [], numLoadedObjects = 0, numTotalObjects = objects.length;
            if (!numTotalObjects) {
                callback && callback(enlivenedObjects);
                return;
            }
            objects.forEach(function (o, index) {
                if (!o || !o.type) {
                    onLoaded();
                    return;
                }
                var klass = NxtControl.Util.Parser.getKlass(o.type, namespace);
                if (klass.async) {
                    klass.fromObject(o, function (obj, error) {
                        if (!error) {
                            enlivenedObjects[index] = obj;
                            reviver && reviver(o, enlivenedObjects[index]);
                        }
                        onLoaded();
                    });
                }
                else {
                    enlivenedObjects[index] = klass.fromObject(o);
                    reviver && reviver(o, enlivenedObjects[index]);
                    onLoaded();
                }
            });
        }
        Util.enlivenObjects = enlivenObjects;
        function falseFunction() {
            return false;
        }
        Util.falseFunction = falseFunction;
        var _slice = Array.prototype.slice;
        function getById(id) {
            return typeof id === 'string' ? System.document.getElementById(id) : id;
        }
        Util.getById = getById;
        var toArray = function (arrayLike) {
            return _slice.call(arrayLike, 0);
        };
        var sliceCanConvertNodelists;
        try {
            sliceCanConvertNodelists = toArray(System.document.childNodes) instanceof Array;
        }
        catch (err) { }
        if (!sliceCanConvertNodelists) {
            toArray = function (arrayLike) {
                var arr = new Array(arrayLike.length), i = arrayLike.length;
                while (i--) {
                    arr[i] = arrayLike[i];
                }
                return arr;
            };
        }
        function makeElement(tagName, attributes) {
            var el = System.document.createElement(tagName);
            for (var prop in attributes) {
                if (prop === 'class') {
                    el.className = attributes[prop];
                }
                else if (prop === 'for') {
                    if (prop instanceof HTMLLabelElement)
                        el.htmlFor = attributes[prop];
                }
                else {
                    el.setAttribute(prop, attributes[prop]);
                }
            }
            return el;
        }
        Util.makeElement = makeElement;
        function addClass(element, className) {
            if ((' ' + element.className + ' ').indexOf(' ' + className + ' ') === -1) {
                element.className += (element.className ? ' ' : '') + className;
            }
        }
        Util.addClass = addClass;
        function wrapElement(element, wrapper, attributes) {
            if (typeof wrapper === 'string') {
                wrapper = makeElement(wrapper, attributes);
            }
            if (element.parentNode) {
                element.parentNode.replaceChild(wrapper, element);
            }
            wrapper.appendChild(element);
            return wrapper;
        }
        Util.wrapElement = wrapElement;
        function getScrollLeftTop(element, upperCanvasEl) {
            var firstFixedAncestor, origElement, left = 0, top = 0, docElement = System.document.documentElement, body = System.document.body || {
                scrollLeft: 0, scrollTop: 0
            };
            origElement = element;
            while (element && element.parentNode && !firstFixedAncestor) {
                element = element.parentNode;
                if (element !== System.document &&
                    NxtControl.Util.getElementStyle(element, 'position') === 'fixed') {
                    firstFixedAncestor = element;
                }
                if (element !== System.document &&
                    origElement !== upperCanvasEl &&
                    NxtControl.Util.getElementStyle(element, 'position') === 'absolute') {
                    left = 0;
                    top = 0;
                }
                else if (element === System.document) {
                    left = body.scrollLeft || docElement.scrollLeft || 0;
                    top = body.scrollTop || docElement.scrollTop || 0;
                }
                else {
                    left += element.scrollLeft || 0;
                    top += element.scrollTop || 0;
                }
            }
            return { left: left, top: top };
        }
        Util.getScrollLeftTop = getScrollLeftTop;
        function getElementOffset(element) {
            var docElem, box = { left: 0, top: 0 }, doc = element && element.ownerDocument, offset = { left: 0, top: 0 }, scrollLeftTop, offsetAttributes = {
                'borderLeftWidth': 'left',
                'borderTopWidth': 'top',
                'paddingLeft': 'left',
                'paddingTop': 'top'
            };
            if (!doc) {
                return { left: 0, top: 0 };
            }
            for (var attr in offsetAttributes) {
                offset[offsetAttributes[attr]] += parseInt(getElementStyle(element, attr), 10) || 0;
            }
            docElem = doc.documentElement;
            if (typeof element.getBoundingClientRect !== "undefined") {
                box = element.getBoundingClientRect();
            }
            scrollLeftTop = NxtControl.Util.getScrollLeftTop(element, null);
            return {
                left: box.left + scrollLeftTop.left - (docElem.clientLeft || 0) + offset.left,
                top: box.top + scrollLeftTop.top - (docElem.clientTop || 0) + offset.top
            };
        }
        Util.getElementOffset = getElementOffset;
        function getElementStyle(element, attr) {
            if (!element.style) {
                element.style = {};
            }
            if (System.document.defaultView && System.document.defaultView.getComputedStyle) {
                return System.document.defaultView.getComputedStyle(element, null)[attr];
            }
            else {
                var value = element.style[attr];
                if (!value && element.currentStyle)
                    value = element.currentStyle[attr];
                return value;
            }
        }
        Util.getElementStyle = getElementStyle;
        var style = System.document.documentElement.style;
        var selectProp = 'userSelect' in style
            ? 'userSelect'
            : 'MozUserSelect' in style
                ? 'MozUserSelect'
                : 'WebkitUserSelect' in style
                    ? 'WebkitUserSelect'
                    : 'KhtmlUserSelect' in style
                        ? 'KhtmlUserSelect'
                        : '';
        function makeElementUnselectable(element) {
            if (typeof element.onselectstart !== 'undefined') {
                element.onselectstart = NxtControl.Util.falseFunction;
            }
            if (selectProp) {
                element.style[selectProp] = 'none';
            }
            else if (typeof element.unselectable === 'string') {
                element.unselectable = 'on';
            }
            return element;
        }
        Util.makeElementUnselectable = makeElementUnselectable;
        function makeElementSelectable(element) {
            if (typeof element.onselectstart !== 'undefined') {
                element.onselectstart = null;
            }
            if (selectProp) {
                element.style[selectProp] = '';
            }
            else if (typeof element.unselectable === 'string') {
                element.unselectable = '';
            }
            return element;
        }
        Util.makeElementSelectable = makeElementSelectable;
        function getScript(url, callback) {
            var headEl = System.document.getElementsByTagName("head")[0], scriptEl = System.document.createElement('script'), loading = true;
            scriptEl.onreadystatechange = scriptEl.onload = function (e) {
                if (loading) {
                    if (typeof this.readyState === 'string' &&
                        this.readyState !== 'loaded' &&
                        this.readyState !== 'complete')
                        return;
                    loading = false;
                    callback(e || System.window.event);
                    scriptEl = scriptEl.onload = scriptEl.onreadystatechange = null;
                }
            };
            scriptEl.src = url;
            headEl.appendChild(scriptEl);
        }
        Util.getScript = getScript;
        var unknown = 'unknown';
        function areHostMethods(object) {
            var methodNames = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                methodNames[_i - 1] = arguments[_i];
            }
            var t, i, len = methodNames.length;
            for (i = 0; i < len; i++) {
                t = typeof object[methodNames[i]];
                if (!(/^(?:function|object|unknown)$/).test(t))
                    return false;
            }
            return true;
        }
        var getUniqueId = (function () {
            var uid = 0;
            return function (element) {
                return element.__uniqueID || (element.__uniqueID = 'uniqueID__' + uid++);
            };
        })();
        var getElement, setElement;
        (function () {
            var elements = {};
            getElement = function (uid) {
                return elements[uid];
            };
            setElement = function (uid, element) {
                elements[uid] = element;
            };
        })();
        function createListener(uid, handler) {
            return {
                handler: handler,
                wrappedHandler: createWrappedHandler(uid, handler)
            };
        }
        function createWrappedHandler(uid, handler) {
            return function (e) {
                handler.call(getElement(uid), e || System.window.event);
            };
        }
        function createDispatcher(uid, eventName) {
            return function (e) {
                if (handlers[uid] && handlers[uid][eventName]) {
                    var handlersForEvent = handlers[uid][eventName];
                    for (var i = 0, len = handlersForEvent.length; i < len; i++) {
                        handlersForEvent[i].call(this, e || System.window.event);
                    }
                }
            };
        }
        var shouldUseAddListenerRemoveListener = (areHostMethods(System.document.documentElement, 'addEventListener', 'removeEventListener') &&
            areHostMethods(System.window, 'addEventListener', 'removeEventListener')), shouldUseAttachEventDetachEvent = (areHostMethods(System.document.documentElement, 'attachEvent', 'detachEvent') &&
            areHostMethods(System.window, 'attachEvent', 'detachEvent')), listeners = {}, handlers = {}, localAddListener, localRemoveListener;
        if (shouldUseAddListenerRemoveListener) {
            localAddListener = function (element, eventName, handler) {
                element.addEventListener(eventName, handler, false);
            };
            localRemoveListener = function (element, eventName, handler) {
                element.removeEventListener(eventName, handler, false);
            };
        }
        else if (shouldUseAttachEventDetachEvent) {
            localAddListener = function (element, eventName, handler) {
                var uid = getUniqueId(element);
                setElement(uid, element);
                if (!listeners[uid]) {
                    listeners[uid] = {};
                }
                if (!listeners[uid][eventName]) {
                    listeners[uid][eventName] = [];
                }
                var listener = createListener(uid, handler);
                listeners[uid][eventName].push(listener);
                element.attachEvent('on' + eventName, listener.wrappedHandler);
            };
            localRemoveListener = function (element, eventName, handler) {
                var uid = getUniqueId(element), listener;
                if (listeners[uid] && listeners[uid][eventName]) {
                    for (var i = 0, len = listeners[uid][eventName].length; i < len; i++) {
                        listener = listeners[uid][eventName][i];
                        if (listener && listener.handler === handler) {
                            element.detachEvent('on' + eventName, listener.wrappedHandler);
                            listeners[uid][eventName][i] = null;
                        }
                    }
                }
            };
        }
        else {
            localAddListener = function (element, eventName, handler) {
                var uid = getUniqueId(element);
                if (!handlers[uid]) {
                    handlers[uid] = {};
                }
                if (!handlers[uid][eventName]) {
                    handlers[uid][eventName] = [];
                    var existingHandler = element['on' + eventName];
                    if (existingHandler) {
                        handlers[uid][eventName].push(existingHandler);
                    }
                    element['on' + eventName] = createDispatcher(uid, eventName);
                }
                handlers[uid][eventName].push(handler);
            };
            localRemoveListener = function (element, eventName, handler) {
                var uid = getUniqueId(element);
                if (handlers[uid] && handlers[uid][eventName]) {
                    var handlersForEvent = handlers[uid][eventName];
                    for (var i = 0, len = handlersForEvent.length; i < len; i++) {
                        if (handlersForEvent[i] === handler) {
                            handlersForEvent.splice(i, 1);
                        }
                    }
                }
            };
        }
        function addListener(element, eventName, handler) {
            localAddListener && localAddListener(element, eventName, handler);
        }
        Util.addListener = addListener;
        function removeListener(element, eventName, handler) {
            localRemoveListener && localRemoveListener(element, eventName, handler);
        }
        Util.removeListener = removeListener;
        var parseEl = System.document.createElement('div'), supportsOpacity = typeof parseEl.style.opacity === 'string', supportsFilters = typeof parseEl.style.filter === 'string', reOpacity = /alpha\s*\(\s*opacity\s*=\s*([^\)]+)\)/, setOpacity = function (element, value) { return element; };
        if (supportsOpacity) {
            setOpacity = function (element, value) {
                element.style.opacity = value;
                return element;
            };
        }
        else if (supportsFilters) {
            setOpacity = function (element, value) {
                var es = element.style;
                if (element.currentStyle && !element.currentStyle.hasLayout) {
                    es.zoom = 1;
                }
                if (reOpacity.test(es.filter)) {
                    value = value >= 0.9999 ? '' : ('alpha(opacity=' + (value * 100) + ')');
                    es.filter = es.filter.replace(reOpacity, value);
                }
                else {
                    es.filter += ' alpha(opacity=' + (value * 100) + ')';
                }
                return element;
            };
        }
        function setStyle(element, styles) {
            var elementStyle = element.style;
            if (!elementStyle) {
                return element;
            }
            if (typeof styles === 'string') {
                element.style.cssText += ';' + styles;
                return styles.indexOf('opacity') > -1
                    ? setOpacity(element, styles.match(/opacity:\s*(\d?\.?\d*)/)[1])
                    : element;
            }
            for (var property in styles) {
                if (property === 'opacity') {
                    setOpacity(element, styles[property]);
                }
                else {
                    var normalizedProperty = (property === 'float' || property === 'cssFloat')
                        ? (typeof elementStyle.styleFloat === 'undefined' ? 'cssFloat' : 'styleFloat')
                        : property;
                    elementStyle[normalizedProperty] = styles[property];
                }
            }
            return element;
        }
        Util.setStyle = setStyle;
    })(Util = NxtControl.Util || (NxtControl.Util = {}));
})(NxtControl || (NxtControl = {}));
if (!Array.prototype.injectArray)
    Array.prototype.injectArray = function (idx, arr) { return this.slice(0, idx).concat(arr).concat(this.slice(idx)); };
if (!Array.prototype.unique)
    Array.prototype.unique = function () {
        var a = this.concat();
        for (var i = 0; i < a.length; ++i) {
            for (var j = i + 1; j < a.length; ++j) {
                if (a[i] === a[j])
                    a.splice(j--, 1);
            }
        }
        return a;
    };
String.prototype.lpad = function (padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
};
var NxtControl;
(function (NxtControl) {
    var Util;
    (function (Util) {
        var unknown = 'unknown';
        var ismobile = null;
        function isMobile() {
            if (ismobile === null)
                ismobile = (/Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Windows Phone/i.test(navigator.userAgent)) ? true : false;
            return ismobile;
        }
        Util.isMobile = isMobile;
        function removeFromArray(array, value) {
            var idx = array.indexOf(value);
            if (idx !== -1) {
                array.splice(idx, 1);
            }
            return array;
        }
        Util.removeFromArray = removeFromArray;
        function populateWithProperties(source, destination, properties) {
            if (properties && Object.prototype.toString.call(properties) === '[object Array]') {
                for (var i = 0, len = properties.length; i < len; i++) {
                    if (properties[i] in source) {
                        destination[properties[i]] = source[properties[i]];
                    }
                }
            }
        }
        Util.populateWithProperties = populateWithProperties;
        function createCanvasElement(canvasEl) {
            canvasEl || (canvasEl = System.document.createElement('canvas'));
            return canvasEl;
        }
        Util.createCanvasElement = createCanvasElement;
        var pointerX = function (event) {
            return (typeof event.clientX !== unknown ? event.clientX : 0);
        };
        var pointerY = function (event) {
            return (typeof event.clientY !== unknown ? event.clientY : 0);
        };
        function _getPointer(event, pageProp, clientProp) {
            var touchProp = event.type === 'touchend' ? 'changedTouches' : 'touches';
            return (event[touchProp] && event[touchProp][0]
                ? (event[touchProp][0][pageProp] - (event[touchProp][0][pageProp] - event[touchProp][0][clientProp]))
                    || event[clientProp]
                : event[clientProp]);
        }
        if (System.isTouchSupported) {
            pointerX = function (event) {
                return _getPointer(event, 'pageX', 'clientX');
            };
            pointerY = function (event) {
                return _getPointer(event, 'pageY', 'clientY');
            };
        }
        function getPointer(event, upperCanvasEl) {
            event || (event = System.window.event);
            var element = event.target ||
                (typeof event.srcElement !== unknown ? event.srcElement : null);
            var scroll = NxtControl.Util.getScrollLeftTop(element, upperCanvasEl);
            return {
                x: pointerX(event) + scroll.left,
                y: pointerY(event) + scroll.top
            };
        }
        Util.getPointer = getPointer;
        function getFunctionBody(fn) {
            return (String(fn).match(/function[^{]*\{([\s\S]*)\}/) || {})[1];
        }
        Util.getFunctionBody = getFunctionBody;
        function isTransparent(ctx, x, y, tolerance) {
            if (tolerance > 0) {
                if (x > tolerance) {
                    x -= tolerance;
                }
                else {
                    x = 0;
                }
                if (y > tolerance) {
                    y -= tolerance;
                }
                else {
                    y = 0;
                }
            }
            var _isTransparent = true;
            var imageData = ctx.getImageData(x, y, (tolerance * 2) || 1, (tolerance * 2) || 1);
            for (var i = 3, l = imageData.data.length; i < l; i += 4) {
                var temp = imageData.data[i];
                _isTransparent = temp <= 0;
                if (_isTransparent === false)
                    break;
            }
            imageData = null;
            return _isTransparent;
        }
        Util.isTransparent = isTransparent;
        function drawDashedLine(ctx, x, y, x2, y2, da) {
            var dx = x2 - x, dy = y2 - y, len = Math.sqrt(dx * dx + dy * dy), rot = Math.atan2(dy, dx), dc = da.length, di = 0, draw = true;
            ctx.save();
            ctx.translate(x, y);
            ctx.moveTo(0, 0);
            ctx.rotate(rot);
            x = 0;
            while (len > x) {
                x += da[di++ % dc];
                if (x > len) {
                    x = len;
                }
                ctx[draw ? 'lineTo' : 'moveTo'](x, 0);
                draw = !draw;
            }
            ctx.restore();
        }
        Util.drawDashedLine = drawDashedLine;
        var Clip = (function () {
            function Clip() {
            }
            Clip.clipContext = function (receiver, ctx) {
                ctx.save();
                ctx.beginPath();
                receiver.clipTo(ctx);
                ctx.clip();
            };
            return Clip;
        }());
        Util.Clip = Clip;
        var Parser = (function () {
            function Parser() {
            }
            Parser.camelize = function (value) {
                return value.replace(/-+(.)?/g, function (match, character) {
                    return character ? character.toUpperCase() : '';
                });
            };
            Parser.capitalize = function (value, firstLetterOnly) {
                return value.charAt(0).toUpperCase() +
                    (firstLetterOnly ? value.slice(1) : value.slice(1).toLowerCase());
            };
            Parser.escapeXml = function (string) {
                return string.replace(/&/g, '&amp;')
                    .replace(/"/g, '&quot;')
                    .replace(/'/g, '&apos;')
                    .replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;');
            };
            Parser.resolveNamespace = function (nmsp) {
                if (!nmsp || nmsp === '')
                    return NxtControl;
                var parts = nmsp.split('.'), len = parts.length;
                var obj = window;
                for (var i = 0; i < len; ++i) {
                    obj = obj[parts[i]];
                }
                return obj;
            };
            Parser.getKlass = function (type, namespace) {
                if (typeof namespace === 'undefined' || namespace === null) {
                    if (type === 'group')
                        return NxtControl.GuiFramework.Group;
                    var index = type.lastIndexOf('.');
                    if (index !== -1) {
                        var ns = type.substr(0, index);
                        var nstype = type.substr(index + 1, type.length - index);
                        return Parser.resolveNamespace(ns)[nstype];
                    }
                }
                type = Parser.camelize(type.charAt(0).toUpperCase() + type.slice(1));
                return Parser.resolveNamespace(namespace)[type];
            };
            return Parser;
        }());
        Util.Parser = Parser;
        var slice = Array.prototype.slice;
        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function (searchElement) {
                if (this === void 0 || this === null) {
                    throw new TypeError();
                }
                var t = Object(this), len = t.length >>> 0;
                if (len === 0) {
                    return -1;
                }
                var n = 0;
                if (arguments.length > 0) {
                    n = Number(arguments[1]);
                    if (n !== n) {
                        n = 0;
                    }
                    else if (n !== 0 && n !== Number.POSITIVE_INFINITY && n !== Number.NEGATIVE_INFINITY) {
                        n = n > 0 ? n : -1 * Math.floor(Math.abs(n));
                    }
                }
                if (n >= len) {
                    return -1;
                }
                var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
                for (; k < len; k++) {
                    if (k in t && t[k] === searchElement) {
                        return k;
                    }
                }
                return -1;
            };
        }
        if (!Array.prototype.forEach) {
            Array.prototype.forEach = function (fn, context) {
                for (var i = 0, len = this.length >>> 0; i < len; i++) {
                    if (i in this) {
                        fn.call(context, this[i], i, this);
                    }
                }
            };
        }
        if (!Array.prototype.map) {
            Array.prototype.map = function (fn, context) {
                var result = [];
                for (var i = 0, len = this.length >>> 0; i < len; i++) {
                    if (i in this) {
                        result[i] = fn.call(context, this[i], i, this);
                    }
                }
                return result;
            };
        }
        if (!Array.prototype.every) {
            Array.prototype.every = function (fn, context) {
                for (var i = 0, len = this.length >>> 0; i < len; i++) {
                    if (i in this && !fn.call(context, this[i], i, this)) {
                        return false;
                    }
                }
                return true;
            };
        }
        if (!Array.prototype.some) {
            Array.prototype.some = function (fn, context) {
                for (var i = 0, len = this.length >>> 0; i < len; i++) {
                    if (i in this && fn.call(context, this[i], i, this)) {
                        return true;
                    }
                }
                return false;
            };
        }
        if (!Array.prototype.filter) {
            Array.prototype.filter = function (fn, context) {
                var result = [], val;
                for (var i = 0, len = this.length >>> 0; i < len; i++) {
                    if (i in this) {
                        val = this[i];
                        if (fn.call(context, val, i, this)) {
                            result.push(val);
                        }
                    }
                }
                return result;
            };
        }
        if (!Array.prototype.reduce) {
            Array.prototype.reduce = function (fn) {
                var len = this.length >>> 0, i = 0, rv;
                if (arguments.length > 1) {
                    rv = arguments[1];
                }
                else {
                    do {
                        if (i in this) {
                            rv = this[i++];
                            break;
                        }
                        if (++i >= len) {
                            throw new TypeError();
                        }
                    } while (true);
                }
                for (; i < len; i++) {
                    if (i in this) {
                        rv = fn.call(null, rv, this[i], i, this);
                    }
                }
                return rv;
            };
        }
        var array = (function () {
            function array() {
            }
            array.invoke = function (array, method) {
                var args = [];
                for (var _i = 2; _i < arguments.length; _i++) {
                    args[_i - 2] = arguments[_i];
                }
                var result = [];
                for (var i = 0, len = array.length; i < len; i++) {
                    result[i] = args.length ? array[i][method].apply(array[i], args) : array[i][method].call(array[i]);
                }
                return result;
            };
            array.min = function (array, byProperty) {
                return NxtControl.Util.array.find(array, byProperty, function (value1, value2) {
                    return value1 < value2;
                });
            };
            array.max = function (array, byProperty) {
                return NxtControl.Util.array.find(array, byProperty, function (value1, value2) {
                    return value1 >= value2;
                });
            };
            array.find = function (array, byProperty, condition) {
                if (!array || array.length === 0)
                    return undefined;
                var i = array.length - 1, result = byProperty ? array[i][byProperty] : array[i];
                if (byProperty) {
                    while (i--) {
                        if (condition(array[i][byProperty], result)) {
                            result = array[i][byProperty];
                        }
                    }
                }
                else {
                    while (i--) {
                        if (condition(array[i], result)) {
                            result = array[i];
                        }
                    }
                }
                return result;
            };
            return array;
        }());
        Util.array = array;
        var object = (function () {
            function object() {
            }
            object.extend = function (destination, source) {
                for (var property in source) {
                    destination[property] = source[property];
                }
                return destination;
            };
            object.clone = function (object) {
                return NxtControl.Util.object.extend({}, object);
            };
            return object;
        }());
        Util.object = object;
        function loadImage(url, callback, context, crossOrigin) {
            if (!url) {
                callback && callback.call(context, url);
                return;
            }
            var img = System.document.createElement('img');
            img.onload = function () {
                callback && callback.call(context, img);
                img = img.onload = img.onerror = null;
            };
            img.onerror = function () {
                console.error('Error loading ' + img.src);
                callback && callback.call(context, null, true);
                img = img.onload = img.onerror = null;
            };
            if (url.indexOf('data') !== 0 && typeof crossOrigin !== 'undefined') {
                img.crossOrigin = crossOrigin;
            }
            img.src = url;
        }
        Util.loadImage = loadImage;
    })(Util = NxtControl.Util || (NxtControl.Util = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var Util;
    (function (Util) {
        var Convert = (function () {
            function Convert() {
            }
            Convert.toFixed = function (number, fractionDigits) {
                return parseFloat(Number(number).toFixed(fractionDigits));
            };
            Convert.degreesToRadians = function (degrees) {
                return degrees * (Math.PI / 180);
                ;
            };
            Convert.radiansToDegrees = function (radians) {
                return radians / (Math.PI / 180);
            };
            Convert.tryFromStringToValueType = function (typeCode, text, numberBase) {
                var result = { result: false, value: text };
                if (text === null || text.length === 0)
                    return result;
                if (typeCode === NxtControl.GuiFramework.TypeCode.DateAndTime)
                    return Convert.tryFromStringToDateAndTime(text);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Date)
                    return Convert.tryFromStringToDate(text);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Time)
                    return Convert.tryFromStringToTime(text);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.TimeOfDay)
                    return Convert.tryFromStringToTimeOfDay(text);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Byte)
                    return Convert.tryFromStringToInt(text, numberBase, typeCode);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.SByte)
                    return Convert.tryFromStringToInt(text, numberBase, typeCode);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Int16)
                    return Convert.tryFromStringToInt(text, numberBase, typeCode);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.UInt16)
                    return Convert.tryFromStringToInt(text, numberBase, typeCode);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Int32)
                    return Convert.tryFromStringToInt(text, numberBase, typeCode);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.UInt32)
                    return Convert.tryFromStringToInt(text, numberBase, typeCode);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Int64)
                    return Convert.tryFromStringToInt(text, numberBase, typeCode);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.UInt64)
                    return Convert.tryFromStringToInt(text, numberBase, typeCode);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Single)
                    return Convert.tryFromStringToSingle(text, numberBase);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Double)
                    return Convert.tryFromStringToDouble(text, numberBase);
                else if (typeCode === NxtControl.GuiFramework.TypeCode.String)
                    result.result = true;
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Boolean)
                    return Convert.tryFromStringToBoolean(text);
                return result;
            };
            Convert.tryFromStringToBoolean = function (s) {
                switch (String(s).toLowerCase()) {
                    case "true":
                    case "1":
                    case "yes":
                    case "y":
                        return { result: true, value: true };
                    case "false":
                    case "0":
                    case "no":
                    case "n":
                        return { result: true, value: false };
                    default:
                        return { result: false, value: undefined };
                }
            };
            Convert.tryFromStringToDouble = function (text, numberBase) {
                var result = { result: true, value: 0 };
                result.value = Number(text);
                if (isNaN(result.value))
                    result.result = false;
                return result;
            };
            Convert.tryFromStringToSingle = function (text, numberBase) {
                var result = Convert.tryFromStringToDouble(text, numberBase);
                if (result.result === true) {
                    if (result.value > 3.40282347E+38 || result.value < -3.40282347E+38)
                        result.result = false;
                }
                return result;
            };
            Convert.tryFromStringToInt = function (text, numberBase, typeCode) {
                var result = { result: true, value: 0 };
                var radix = numberBase === NxtControl.GuiFramework.NumberBase.Decimal ? 10 :
                    numberBase === NxtControl.GuiFramework.NumberBase.Binary ? 2 :
                        numberBase === NxtControl.GuiFramework.NumberBase.Octal ? 8 :
                            numberBase === NxtControl.GuiFramework.NumberBase.Hexadecimal ? 16 : 10;
                var min = -9223372036854775808, max = +9223372036854775807;
                if (typeCode === NxtControl.GuiFramework.TypeCode.Byte) {
                    min = 0;
                    max = 255;
                }
                else if (typeCode === NxtControl.GuiFramework.TypeCode.SByte) {
                    min = -128;
                    max = 127;
                }
                if (typeCode === NxtControl.GuiFramework.TypeCode.Int16) {
                    min = -32768;
                    max = 32787;
                }
                else if (typeCode === NxtControl.GuiFramework.TypeCode.UInt16) {
                    min = 0;
                    max = 65535;
                }
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Int32) {
                    min = -2147483684;
                    max = 2147483683;
                }
                else if (typeCode === NxtControl.GuiFramework.TypeCode.UInt32) {
                    min = 0;
                    max = 4294967295;
                }
                else if (typeCode === NxtControl.GuiFramework.TypeCode.Int64) {
                    min = -9223372036854775808;
                    max = +9223372036854775807;
                }
                else if (typeCode === NxtControl.GuiFramework.TypeCode.UInt64) {
                    min = 0;
                    max = 18446744073709551615;
                }
                result.value = parseInt(text, radix);
                if (isNaN(result.value) || result.value < min || result.value > max)
                    result.result = false;
                return result;
            };
            Convert.fromDoubleToString = function (value, numberBase) {
                return Convert.fromIntToString(Math.round(value), numberBase);
            };
            Convert.fromIntToString = function (value, numberBase) {
                var radix = 10, pad = 10;
                switch (numberBase) {
                    case NxtControl.GuiFramework.NumberBase.Binary:
                        radix = 2;
                        pad = 8;
                        break;
                    case NxtControl.GuiFramework.NumberBase.Octal:
                        radix = 8;
                        pad = 3;
                        break;
                    case NxtControl.GuiFramework.NumberBase.Hexadecimal:
                        radix = 16;
                        pad = 2;
                        break;
                }
                var result = '';
                for (var index = 0; index < 8; index++) {
                    var theByte = value & 0xff;
                    result += theByte.toString(radix).lpad('0', pad);
                    value = (value - theByte) / 256;
                }
                return result;
            };
            Convert.tryFromStringToTime = function (text) {
                var result = { result: false, value: 0 };
                return result;
            };
            Convert.tryFromStringToTimeOfDay = function (text) {
                var result = { result: false, value: 0 };
                return result;
            };
            Convert.tryFromStringToDateAndTime = function (text) {
                var result = { result: false, value: 0 };
                return result;
            };
            Convert.tryFromStringToDate = function (text) {
                var result = { result: false, value: 0 };
                return result;
            };
            Convert.fromValueTypeToString = function (typeCode, value, numberBase) {
                var valueString = '';
                switch (numberBase) {
                    case NxtControl.GuiFramework.NumberBase.Binary:
                    case NxtControl.GuiFramework.NumberBase.Octal:
                    case NxtControl.GuiFramework.NumberBase.Hexadecimal:
                        valueString = Convert.fromIntToString(Number(value), numberBase);
                        break;
                    default:
                        valueString = String(value);
                        break;
                }
                return valueString;
            };
            Convert.fromTimeToString = function (value, format) {
                if (!format || format.length === 0)
                    format = "%s%f";
                return '';
            };
            Convert.fromTimeOfDayToString = function (value) {
                return '';
            };
            Convert.fromDateToString = function (value, format) {
                return '';
            };
            Convert.fromDateAndTimeToString = function (value, format) {
                return '';
            };
            return Convert;
        }());
        Util.Convert = Convert;
    })(Util = NxtControl.Util || (NxtControl.Util = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var Util;
    (function (Util) {
        var Intersection = (function () {
            function Intersection(status) {
                this.status = status;
                this.points = [];
            }
            Intersection.prototype.appendPoint = function (point) {
                this.points.push(point);
            };
            Intersection.prototype.appendPoints = function (points) {
                this.points = this.points.concat(points);
            };
            Intersection.intersectLineLine = function (a1, a2, b1, b2) {
                var result, ua_t = (b2.x - b1.x) * (a1.y - b1.y) - (b2.y - b1.y) * (a1.x - b1.x), ub_t = (a2.x - a1.x) * (a1.y - b1.y) - (a2.y - a1.y) * (a1.x - b1.x), u_b = (b2.y - b1.y) * (a2.x - a1.x) - (b2.x - b1.x) * (a2.y - a1.y);
                if (u_b !== 0) {
                    var ua = ua_t / u_b, ub = ub_t / u_b;
                    if (0 <= ua && ua <= 1 && 0 <= ub && ub <= 1) {
                        result = new Intersection("Intersection");
                        result.points.push(new NxtControl.Drawing.Point(a1.x + ua * (a2.x - a1.x), a1.y + ua * (a2.y - a1.y)));
                    }
                    else {
                        result = new Intersection();
                    }
                }
                else {
                    if (ua_t === 0 || ub_t === 0) {
                        result = new Intersection("Coincident");
                    }
                    else {
                        result = new Intersection("Parallel");
                    }
                }
                return result;
            };
            Intersection.intersectLinePolygon = function (a1, a2, points) {
                var result = new Intersection(), length = points.length;
                for (var i = 0; i < length; i++) {
                    var b1 = points[i], b2 = points[(i + 1) % length], inter = Intersection.intersectLineLine(a1, a2, b1, b2);
                    result.appendPoints(inter.points);
                }
                if (result.points.length > 0) {
                    result.status = "Intersection";
                }
                return result;
            };
            Intersection.intersectPolygonPolygon = function (points1, points2) {
                var result = new Intersection(), length = points1.length;
                for (var i = 0; i < length; i++) {
                    var a1 = points1[i], a2 = points1[(i + 1) % length], inter = Intersection.intersectLinePolygon(a1, a2, points2);
                    result.appendPoints(inter.points);
                }
                if (result.points.length > 0) {
                    result.status = "Intersection";
                }
                return result;
            };
            Intersection.intersectPolygonRectangle = function (points, r1, r2) {
                var min = r1.min(r2), max = r1.max(r2), topRight = new NxtControl.Drawing.Point(max.x, min.y), bottomLeft = new NxtControl.Drawing.Point(min.x, max.y), inter1 = Intersection.intersectLinePolygon(min, topRight, points), inter2 = Intersection.intersectLinePolygon(topRight, max, points), inter3 = Intersection.intersectLinePolygon(max, bottomLeft, points), inter4 = Intersection.intersectLinePolygon(bottomLeft, min, points), result = new Intersection();
                result.appendPoints(inter1.points);
                result.appendPoints(inter2.points);
                result.appendPoints(inter3.points);
                result.appendPoints(inter4.points);
                if (result.points.length > 0) {
                    result.status = "Intersection";
                }
                return result;
            };
            return Intersection;
        }());
        Util.Intersection = Intersection;
    })(Util = NxtControl.Util || (NxtControl.Util = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    NxtControl.EmptyEventArgs = {};
    var event = (function () {
        function event() {
            this.callees = [];
        }
        event.prototype.trigger = function (sender, parameter) {
            for (var i = 0; i < this.callees.length; i++) {
                var callee = this.callees[i];
                if (callee)
                    callee(sender, parameter);
            }
        };
        event.prototype.contains = function (callee) {
            if (!callee)
                return false;
            return this.callees.indexOf(callee) >= 0;
        };
        event.prototype.add = function (callee) {
            if (!callee)
                return this;
            if (!this.contains(callee))
                this.callees.push(callee);
            return this;
        };
        event.prototype.remove = function (callee) {
            if (!callee)
                return this;
            var index = this.callees.indexOf(callee);
            if (index >= 0)
                this.callees.splice(index, 1);
            return this;
        };
        event.prototype.toDelegate = function () {
            var _this = this;
            return function (sender, parameter) { return _this.trigger(sender, parameter); };
        };
        return event;
    }());
    NxtControl.event = event;
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        (function (SendKey) {
            SendKey[SendKey["ValueChanged"] = 0] = "ValueChanged";
            SendKey[SendKey["History"] = 1] = "History";
            SendKey[SendKey["ArchiveAnswer"] = 2] = "ArchiveAnswer";
            SendKey[SendKey["FaceplateSubscribeData"] = 3] = "FaceplateSubscribeData";
            SendKey[SendKey["AlarmUpdate"] = 4] = "AlarmUpdate";
            SendKey[SendKey["AlarmUpdateClear"] = 5] = "AlarmUpdateClear";
            SendKey[SendKey["AlarmUpdateColor"] = 6] = "AlarmUpdateColor";
            SendKey[SendKey["ServerUpdate"] = 7] = "ServerUpdate";
            SendKey[SendKey["AlarmUpdate2"] = 8] = "AlarmUpdate2";
        })(GuiFramework.SendKey || (GuiFramework.SendKey = {}));
        var SendKey = GuiFramework.SendKey;
        (function (MaximumPosition) {
            MaximumPosition[MaximumPosition["leftBottom"] = 0] = "leftBottom";
            MaximumPosition[MaximumPosition["rightTop"] = 1] = "rightTop";
        })(GuiFramework.MaximumPosition || (GuiFramework.MaximumPosition = {}));
        var MaximumPosition = GuiFramework.MaximumPosition;
        (function (AnchorStyles) {
            AnchorStyles[AnchorStyles["bottom"] = 2] = "bottom";
            AnchorStyles[AnchorStyles["left"] = 4] = "left";
            AnchorStyles[AnchorStyles["none"] = 0] = "none";
            AnchorStyles[AnchorStyles["right"] = 8] = "right";
            AnchorStyles[AnchorStyles["top"] = 1] = "top";
        })(GuiFramework.AnchorStyles || (GuiFramework.AnchorStyles = {}));
        var AnchorStyles = GuiFramework.AnchorStyles;
        (function (MouseButtonType) {
            MouseButtonType[MouseButtonType["None"] = 0] = "None";
            MouseButtonType[MouseButtonType["Click"] = 1] = "Click";
            MouseButtonType[MouseButtonType["DoubleClick"] = 2] = "DoubleClick";
            MouseButtonType[MouseButtonType["MouseDownLeft"] = 4] = "MouseDownLeft";
            MouseButtonType[MouseButtonType["MouseDownRight"] = 8] = "MouseDownRight";
            MouseButtonType[MouseButtonType["MouseUpLeft"] = 16] = "MouseUpLeft";
            MouseButtonType[MouseButtonType["MouseUpRight"] = 32] = "MouseUpRight";
        })(GuiFramework.MouseButtonType || (GuiFramework.MouseButtonType = {}));
        var MouseButtonType = GuiFramework.MouseButtonType;
        (function (TypeCode) {
            TypeCode[TypeCode["Boolean"] = 3] = "Boolean";
            TypeCode[TypeCode["Byte"] = 6] = "Byte";
            TypeCode[TypeCode["Char"] = 4] = "Char";
            TypeCode[TypeCode["DateTime"] = 16] = "DateTime";
            TypeCode[TypeCode["DBNull"] = 2] = "DBNull";
            TypeCode[TypeCode["Decimal"] = 15] = "Decimal";
            TypeCode[TypeCode["Double"] = 14] = "Double";
            TypeCode[TypeCode["Empty"] = 0] = "Empty";
            TypeCode[TypeCode["Int16"] = 7] = "Int16";
            TypeCode[TypeCode["Int32"] = 9] = "Int32";
            TypeCode[TypeCode["Int64"] = 11] = "Int64";
            TypeCode[TypeCode["Object"] = 1] = "Object";
            TypeCode[TypeCode["SByte"] = 5] = "SByte";
            TypeCode[TypeCode["Single"] = 13] = "Single";
            TypeCode[TypeCode["String"] = 18] = "String";
            TypeCode[TypeCode["UInt16"] = 8] = "UInt16";
            TypeCode[TypeCode["UInt32"] = 10] = "UInt32";
            TypeCode[TypeCode["UInt64"] = 12] = "UInt64";
            TypeCode[TypeCode["Time"] = 2147483646] = "Time";
            TypeCode[TypeCode["Date"] = 2147483645] = "Date";
            TypeCode[TypeCode["TimeOfDay"] = 2147483644] = "TimeOfDay";
            TypeCode[TypeCode["DateAndTime"] = 2147483643] = "DateAndTime";
        })(GuiFramework.TypeCode || (GuiFramework.TypeCode = {}));
        var TypeCode = GuiFramework.TypeCode;
        var TypeCode;
        (function (TypeCode) {
            function fromTypeToTypeCode(type) {
                switch (type) {
                    case 'bool':
                        return TypeCode.Boolean;
                    case 'byte':
                        return TypeCode.Byte;
                    case 'sbyte':
                        return TypeCode.SByte;
                    case 'DateTime':
                        return TypeCode.DateTime;
                    case 'decimal':
                        return TypeCode.Decimal;
                    case 'double':
                        return TypeCode.Double;
                    case 'short':
                        return TypeCode.Int16;
                    case 'ushort':
                        return TypeCode.UInt16;
                    case 'int':
                        return TypeCode.Int32;
                    case 'uint':
                        return TypeCode.UInt32;
                    case 'long':
                        return TypeCode.Int64;
                    case 'ulong':
                        return TypeCode.UInt64;
                    case 'float':
                        return TypeCode.Single;
                    case 'string':
                        return TypeCode.String;
                    case 'Time':
                        return TypeCode.Time;
                    case 'Date':
                        return TypeCode.Date;
                    case 'TimeOfDay':
                        return TypeCode.TimeOfDay;
                    case 'DateAndTime':
                        return TypeCode.DateAndTime;
                }
                return TypeCode.Empty;
            }
            TypeCode.fromTypeToTypeCode = fromTypeToTypeCode;
        })(TypeCode = GuiFramework.TypeCode || (GuiFramework.TypeCode = {}));
        (function (NumberBase) {
            NumberBase[NumberBase["Decimal"] = 0] = "Decimal";
            NumberBase[NumberBase["Binary"] = 1] = "Binary";
            NumberBase[NumberBase["Octal"] = 2] = "Octal";
            NumberBase[NumberBase["Hexadecimal"] = 3] = "Hexadecimal";
        })(GuiFramework.NumberBase || (GuiFramework.NumberBase = {}));
        var NumberBase = GuiFramework.NumberBase;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var Services;
    (function (Services) {
        var ColorService = (function () {
            function ColorService() {
                this.isDesign = false;
                this.clients = {};
                this.needRedraw = false;
                this.count = 0;
                this.phase = 0;
                this.inTick = false;
                this.timer = null;
                this.startColorChange = new NxtControl.event();
                this.stopColorChange = new NxtControl.event();
            }
            ColorService.prototype.getNeedRedraw = function () { return this.needRedraw; };
            ColorService.prototype.attach = function (color) {
                if (this.isDesign)
                    return;
                for (var clrName in this.clients) {
                    if (clrName === color.name) {
                        color.referenceBlinkColor = this.clients[clrName].color;
                        this.clients[clrName].count++;
                        return;
                    }
                }
                this.clients[color.name] = { color: color, count: 1 };
                this.count++;
                if (this.count === 1)
                    this.startTicker();
            };
            ColorService.prototype.detach = function (color) {
                if (this.isDesign)
                    return;
                for (var clrName in this.clients) {
                    if (clrName === color.name) {
                        color.referenceBlinkColor = null;
                        this.clients[clrName].count--;
                        if (this.clients[clrName].count === 0) {
                            delete this.clients[clrName];
                            this.count--;
                            if (this.count === 0)
                                this.stopTicker();
                        }
                        return;
                    }
                }
            };
            ColorService.prototype.tick = function () {
                this.phase++;
                if (this.inTick === true)
                    return;
                this.inTick = true;
                var needRedraw = false;
                try {
                    if (this.count !== 0) {
                        this.startColorChange.trigger(this, NxtControl.EmptyEventArgs);
                        for (var colorName in this.clients) {
                            if (this.clients[colorName].color.tick(this.phase)) {
                                needRedraw = true;
                            }
                        }
                        this.stopColorChange.trigger(this, { value: true });
                    }
                }
                catch (err) {
                    console.error('Exception in ColorService Tick' + err);
                }
                finally {
                    this.inTick = false;
                }
            };
            ColorService.prototype.startTicker = function () {
                if (this.timer === null)
                    this.timer = setInterval(this.tick.bind(this), 125);
            };
            ColorService.prototype.stopTicker = function () {
                if (this.timer !== null) {
                    clearInterval(this.timer);
                    this.timer = null;
                }
            };
            return ColorService;
        }());
        Services.ColorService = ColorService;
        NxtControl.Services.ColorService.instance = new NxtControl.Services.ColorService();
    })(Services = NxtControl.Services || (NxtControl.Services = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var Drawing;
    (function (Drawing) {
        var Point = (function () {
            function Point(x, y) {
                this.x = x;
                this.y = y;
            }
            Point.prototype.rotatePoint = function (origin, radians) {
                var sin = Math.sin(radians), cos = Math.cos(radians);
                var point = this.subtract(origin);
                var rx = point.x * cos - point.y * sin, ry = point.x * sin + point.y * cos;
                return new Point(rx, ry).addEquals(origin);
            };
            Point.prototype.add = function (that) {
                return new Point(this.x + that.x, this.y + that.y);
            };
            Point.prototype.addEquals = function (that) {
                this.x += that.x;
                this.y += that.y;
                return this;
            };
            Point.prototype.scalarAdd = function (scalar) {
                return new Point(this.x + scalar, this.y + scalar);
            };
            Point.prototype.scalarAddEqual = function (scalar) {
                this.x += scalar;
                this.y += scalar;
                return this;
            };
            Point.prototype.subtract = function (that) {
                return new Point(this.x - that.x, this.y - that.y);
            };
            Point.prototype.subtractEquals = function (that) {
                this.x -= that.x;
                this.y -= that.y;
                return this;
            };
            Point.prototype.scalarSubtract = function (scalar) {
                return new Point(this.x - scalar, this.y - scalar);
            };
            Point.prototype.scalarSubtractEquals = function (scalar) {
                this.x -= scalar;
                this.y -= scalar;
                return this;
            };
            Point.prototype.multiply = function (scalar) {
                return new Point(this.x * scalar, this.y * scalar);
            };
            Point.prototype.multiplyEquals = function (scalar) {
                this.x *= scalar;
                this.y *= scalar;
                return this;
            };
            Point.prototype.divide = function (scalar) {
                return new Point(this.x / scalar, this.y / scalar);
            };
            Point.prototype.divideEquals = function (scalar) {
                this.x /= scalar;
                this.y /= scalar;
                return this;
            };
            Point.prototype.eq = function (that) {
                return (this.x === that.x && this.y === that.y);
            };
            Point.prototype.lt = function (that) {
                return (this.x < that.x && this.y < that.y);
            };
            Point.prototype.lte = function (that) {
                return (this.x <= that.x && this.y <= that.y);
            };
            Point.prototype.gt = function (that) {
                return (this.x > that.x && this.y > that.y);
            };
            Point.prototype.gte = function (that) {
                return (this.x >= that.x && this.y >= that.y);
            };
            Point.prototype.lerp = function (that, t) {
                return new Point(this.x + (that.x - this.x) * t, this.y + (that.y - this.y) * t);
            };
            Point.prototype.distanceFrom = function (that) {
                var dx = this.x - that.x, dy = this.y - that.y;
                return Math.sqrt(dx * dx + dy * dy);
            };
            Point.prototype.midPointFrom = function (that) {
                return new Point(this.x + (that.x - this.x) / 2, this.y + (that.y - this.y) / 2);
            };
            Point.prototype.min = function (that) {
                return new Point(Math.min(this.x, that.x), Math.min(this.y, that.y));
            };
            Point.prototype.max = function (that) {
                return new Point(Math.max(this.x, that.x), Math.max(this.y, that.y));
            };
            Point.prototype.toString = function () {
                return this.x + "," + this.y;
            };
            Point.prototype.setXY = function (x, y) {
                this.x = x;
                this.y = y;
            };
            Point.prototype.setFromPoint = function (that) {
                this.x = that.x;
                this.y = that.y;
            };
            Point.prototype.swap = function (that) {
                var x = this.x, y = this.y;
                this.x = that.x;
                this.y = that.y;
                that.x = x;
                that.y = y;
            };
            return Point;
        }());
        Drawing.Point = Point;
        (function (FillDirection) {
            FillDirection[FillDirection["TopToDown"] = 0] = "TopToDown";
            FillDirection[FillDirection["DownToTop"] = 1] = "DownToTop";
            FillDirection[FillDirection["LeftToRight"] = 2] = "LeftToRight";
            FillDirection[FillDirection["RightToLeft"] = 3] = "RightToLeft";
        })(Drawing.FillDirection || (Drawing.FillDirection = {}));
        var FillDirection = Drawing.FillDirection;
        (function (FillOrientation) {
            FillOrientation[FillOrientation["None"] = 0] = "None";
            FillOrientation[FillOrientation["DiagonalLeftTop"] = 1] = "DiagonalLeftTop";
            FillOrientation[FillOrientation["DiagonalLeftBottom"] = 2] = "DiagonalLeftBottom";
            FillOrientation[FillOrientation["DiagonalRightBottom"] = 3] = "DiagonalRightBottom";
            FillOrientation[FillOrientation["DiagonalRightTop"] = 4] = "DiagonalRightTop";
            FillOrientation[FillOrientation["VerticalTop"] = 5] = "VerticalTop";
            FillOrientation[FillOrientation["VerticalBottom"] = 6] = "VerticalBottom";
            FillOrientation[FillOrientation["HorizontalLeft"] = 7] = "HorizontalLeft";
            FillOrientation[FillOrientation["HorizontalRight"] = 8] = "HorizontalRight";
            FillOrientation[FillOrientation["Center"] = 9] = "Center";
            FillOrientation[FillOrientation["HorizontalCenter"] = 10] = "HorizontalCenter";
            FillOrientation[FillOrientation["VerticalCenter"] = 11] = "VerticalCenter";
            FillOrientation[FillOrientation["InvertedHorizontalCenter"] = 12] = "InvertedHorizontalCenter";
            FillOrientation[FillOrientation["InvertedVerticalCenter"] = 13] = "InvertedVerticalCenter";
        })(Drawing.FillOrientation || (Drawing.FillOrientation = {}));
        var FillOrientation = Drawing.FillOrientation;
        (function (Orientation) {
            Orientation[Orientation["Horizontal"] = 0] = "Horizontal";
            Orientation[Orientation["Vertical"] = 1] = "Vertical";
        })(Drawing.Orientation || (Drawing.Orientation = {}));
        var Orientation = Drawing.Orientation;
        var Matrix = (function () {
            function Matrix(config) {
                this._rounder = 100000;
                this._defaults = {
                    a: 1,
                    b: 0,
                    c: 0,
                    d: 1,
                    dx: 0,
                    dy: 0
                };
                var defaults = this._defaults, prop;
                config = config || {};
                for (prop in defaults) {
                    if (defaults.hasOwnProperty(prop)) {
                        this[prop] = (prop in config) ? config[prop] : defaults[prop];
                    }
                }
                this._config = config;
            }
            Matrix.prototype.multiply = function (a, b, c, d, dx, dy) {
                var matrix = this, matrix_a = matrix.a * a + matrix.c * b, matrix_b = matrix.b * a + matrix.d * b, matrix_c = matrix.a * c + matrix.c * d, matrix_d = matrix.b * c + matrix.d * d, matrix_dx = matrix.a * dx + matrix.c * dy + matrix.dx, matrix_dy = matrix.b * dx + matrix.d * dy + matrix.dy;
                matrix.a = this._round(matrix_a);
                matrix.b = this._round(matrix_b);
                matrix.c = this._round(matrix_c);
                matrix.d = this._round(matrix_d);
                matrix.dx = this._round(matrix_dx);
                matrix.dy = this._round(matrix_dy);
                return this;
            };
            Matrix.prototype.applyCSSText = function (val) {
                var re = /\s*([a-z]*)\(([\w,\.,\-,\s]*)\)/gi, args, m;
                val = val.replace(/matrix/g, "multiply");
                while ((m = re.exec(val))) {
                    if (typeof this[m[1]] === 'function') {
                        args = m[2].split(',');
                        this[m[1]].apply(this, args);
                    }
                }
            };
            Matrix.prototype.getTransformArray = function (val) {
                var re = /\s*([a-z]*)\(([\w,\.,\-,\s]*)\)/gi, transforms = [], args, m;
                val = val.replace(/matrix/g, "multiply");
                while ((m = re.exec(val))) {
                    if (typeof this[m[1]] === 'function') {
                        args = m[2].split(',');
                        args.unshift(m[1]);
                        transforms.push(args);
                    }
                }
                return transforms;
            };
            Matrix.prototype._round = function (val) {
                val = Math.round(val * this._rounder) / this._rounder;
                return val;
            };
            Matrix.prototype.scale = function (x, y) {
                this.multiply(x, 0, 0, y, 0, 0);
                return this;
            };
            Matrix.prototype.skew = function (x, y) {
                x = x || 0;
                y = y || 0;
                if (typeof x !== 'undefined') {
                    x = Math.tan(this.angle2rad(x));
                }
                if (typeof y !== 'undefined') {
                    y = Math.tan(this.angle2rad(y));
                }
                this.multiply(1, y, x, 1, 0, 0);
                return this;
            };
            Matrix.prototype.skewX = function (x) {
                this.skew(x);
                return this;
            };
            Matrix.prototype.skewY = function (y) {
                this.skew(null, y);
                return this;
            };
            Matrix.prototype.toCSSText = function () {
                var matrix = this, text = 'matrix(' +
                    matrix.a + ',' +
                    matrix.b + ',' +
                    matrix.c + ',' +
                    matrix.d + ',' +
                    matrix.dx + ',' +
                    matrix.dy + ')';
                return text;
            };
            Matrix.prototype.toFilterText = function () {
                var matrix = this, text = 'progid:DXImageTransform.Microsoft.Matrix(';
                text += 'M11=' + matrix.a + ',' +
                    'M21=' + matrix.b + ',' +
                    'M12=' + matrix.c + ',' +
                    'M22=' + matrix.d + ',' +
                    'sizingMethod="auto expand")';
                text += '';
                return text;
            };
            Matrix.prototype.rad2deg = function (rad) {
                var deg = rad * (180 / Math.PI);
                return deg;
            };
            Matrix.prototype.deg2rad = function (deg) {
                var rad = deg * (Math.PI / 180);
                return rad;
            };
            Matrix.prototype.angle2rad = function (val) {
                var res;
                if (typeof val === 'string') {
                    if (val.indexOf('rad') > -1)
                        res = parseFloat(val);
                    else
                        res = this.deg2rad(parseFloat(val));
                }
                else {
                    res = this.deg2rad(val);
                }
                return res;
            };
            Matrix.prototype.rotate = function (deg, x, y) {
                var rad = this.angle2rad(deg), sin = Math.sin(rad), cos = Math.cos(rad);
                this.multiply(cos, sin, 0 - sin, cos, 0, 0);
                return this;
            };
            Matrix.prototype.translate = function (x, y) {
                var _dx;
                var _dy;
                if (typeof x === 'number')
                    _dx = x;
                else
                    _dx = parseFloat(x) || 0;
                if (typeof y === 'number')
                    _dy = y;
                else
                    _dy = parseFloat(y) || 0;
                this.multiply(1, 0, 0, 1, _dx, _dy);
                return this;
            };
            Matrix.prototype.translateX = function (x) {
                this.translate(x);
                return this;
            };
            Matrix.prototype.translateY = function (y) {
                this.translate(null, y);
                return this;
            };
            Matrix.prototype.identity = function () {
                var config = this._config, defaults = this._defaults, prop;
                for (prop in config) {
                    if (prop in defaults) {
                        this[prop] = defaults[prop];
                    }
                }
                return this;
            };
            Matrix.prototype.getMatrixArray = function () {
                var matrix = this, matrixArray = [
                    [matrix.a, matrix.c, matrix.dx],
                    [matrix.b, matrix.d, matrix.dy],
                    [0, 0, 1]
                ];
                return matrixArray;
            };
            Matrix.prototype.getTransformedPoint = function (point) {
                var tmpx = point.x * this.a + point.y * this.b + this.dx;
                return { x: tmpx, y: point.x * this.c + point.y * this.d + this.dy };
            };
            Matrix.prototype.getContentRect = function (width, height, x, y) {
                var left = !isNaN(x) ? x : 0, top = !isNaN(y) ? y : 0, right = left + width, bottom = top + height, matrix = this, a = matrix.a, b = matrix.b, c = matrix.c, d = matrix.d, dx = matrix.dx, dy = matrix.dy, x1 = (a * left + c * top + dx), y1 = (b * left + d * top + dy), x2 = (a * right + c * top + dx), y2 = (b * right + d * top + dy), x3 = (a * left + c * bottom + dx), y3 = (b * left + d * bottom + dy), x4 = (a * right + c * bottom + dx), y4 = (b * right + d * bottom + dy);
                return {
                    left: Math.min(x3, Math.min(x1, Math.min(x2, x4))),
                    right: Math.max(x3, Math.max(x1, Math.max(x2, x4))),
                    top: Math.min(y2, Math.min(y4, Math.min(y3, y1))),
                    bottom: Math.max(y2, Math.max(y4, Math.max(y3, y1)))
                };
            };
            Matrix.prototype.getDeterminant = function () {
                return MatrixUtil.getDeterminant(this.getMatrixArray());
            };
            Matrix.prototype.inverse = function () {
                var denom = this.a * this.d - this.c * this.b;
                return new Matrix({ a: this.d / denom, c: -this.c / denom, b: -this.b / denom, d: this.a / denom, dx: (this.b * this.dy - this.dx * this.d) / denom, dy: (this.dx * this.c - this.a * this.dy) / denom });
            };
            Matrix.prototype.transpose = function () {
                return MatrixUtil.transpose(this.getMatrixArray());
            };
            Matrix.prototype.decompose = function () {
                return MatrixUtil.decompose(this.getMatrixArray());
            };
            return Matrix;
        }());
        Drawing.Matrix = Matrix;
        var MatrixUtil = (function () {
            function MatrixUtil() {
            }
            MatrixUtil._round = function (val) {
                val = Math.round(val * MatrixUtil._rounder) / MatrixUtil._rounder;
                return val;
            };
            MatrixUtil.rad2deg = function (rad) {
                var deg = rad * (180 / Math.PI);
                return deg;
            };
            MatrixUtil.deg2rad = function (deg) {
                var rad = deg * (Math.PI / 180);
                return rad;
            };
            MatrixUtil.angle2rad = function (val) {
                var res;
                if (typeof val === 'string') {
                    if (val.indexOf('rad') > -1)
                        res = parseFloat(val);
                    else
                        res = MatrixUtil.deg2rad(parseFloat(val));
                }
                else {
                    res = MatrixUtil.deg2rad(val);
                }
                return res;
            };
            MatrixUtil.convertTransformToArray = function (matrix) {
                var matrixArray = [
                    [matrix.a, matrix.c, matrix.dx],
                    [matrix.b, matrix.d, matrix.dy],
                    [0, 0, 1]
                ];
                return matrixArray;
            };
            MatrixUtil.getDeterminant = function (matrix) {
                var determinant = 0, len = matrix.length, i = 0, multiplier;
                if (len === 2) {
                    return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
                }
                for (; i < len; ++i) {
                    multiplier = matrix[i][0];
                    if (i % 2 === 0 || i === 0) {
                        determinant += multiplier * MatrixUtil.getDeterminant(MatrixUtil.getMinors(matrix, i, 0));
                    }
                    else {
                        determinant -= multiplier * MatrixUtil.getDeterminant(MatrixUtil.getMinors(matrix, i, 0));
                    }
                }
                return determinant;
            };
            MatrixUtil.inverse = function (matrix) {
                var determinant = 0, len = matrix.length, i = 0, j, inverse, adjunct = [], minor = [];
                if (len === 2) {
                    determinant = matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
                    inverse = [
                        [matrix[1][1] * determinant, -matrix[1][0] * determinant],
                        [-matrix[0][1] * determinant, matrix[0][0] * determinant]
                    ];
                }
                else {
                    determinant = MatrixUtil.getDeterminant(matrix);
                    for (; i < len; ++i) {
                        adjunct[i] = [];
                        for (j = 0; j < len; ++j) {
                            minor = MatrixUtil.getMinors(matrix, j, i);
                            adjunct[i][j] = MatrixUtil.getDeterminant(minor);
                            if ((i + j) % 2 !== 0 && (i + j) !== 0) {
                                adjunct[i][j] *= -1;
                            }
                        }
                    }
                    inverse = MatrixUtil.scalarMultiply(adjunct, 1 / determinant);
                }
                return new Matrix(inverse);
            };
            MatrixUtil.scalarMultiply = function (matrix, multiplier) {
                var i = 0, j, len = matrix.length;
                for (; i < len; ++i) {
                    for (j = 0; j < len; ++j) {
                        matrix[i][j] = MatrixUtil._round(matrix[i][j] * multiplier);
                    }
                }
                return matrix;
            };
            MatrixUtil.transpose = function (matrix) {
                var len = matrix.length, i = 0, j = 0, transpose = [];
                for (; i < len; ++i) {
                    transpose[i] = [];
                    for (j = 0; j < len; ++j) {
                        transpose[i].push(matrix[j][i]);
                    }
                }
                return transpose;
            };
            MatrixUtil.getMinors = function (matrix, columnIndex, rowIndex) {
                var minors = [], len = matrix.length, i = 0, j, column;
                for (; i < len; ++i) {
                    if (i !== columnIndex) {
                        column = [];
                        for (j = 0; j < len; ++j) {
                            if (j !== rowIndex) {
                                column.push(matrix[i][j]);
                            }
                        }
                        minors.push(column);
                    }
                }
                return minors;
            };
            MatrixUtil.sign = function (val) {
                return val === 0 ? 1 : val / Math.abs(val);
            };
            MatrixUtil.vectorMatrixProduct = function (vector, matrix) {
                var i, j, len = vector.length, product = [], rowProduct;
                for (i = 0; i < len; ++i) {
                    rowProduct = 0;
                    for (j = 0; j < len; ++j) {
                        rowProduct += vector[i] * matrix[i][j];
                    }
                    product[i] = rowProduct;
                }
                return product;
            };
            MatrixUtil.decompose = function (matrix) {
                var a = matrix[0][0], b = matrix[1][0], c = matrix[0][1], d = matrix[1][1], dx = matrix[0][2], dy = matrix[1][2], rotate, sx, sy, shear;
                if ((a * d - b * c) === 0) {
                    return null;
                }
                sx = MatrixUtil._round(Math.sqrt(a * a + b * b));
                a /= sx;
                b /= sx;
                shear = MatrixUtil._round(a * c + b * d);
                c -= a * shear;
                d -= b * shear;
                sy = MatrixUtil._round(Math.sqrt(c * c + d * d));
                c /= sy;
                d /= sy;
                shear /= sy;
                shear = MatrixUtil._round(MatrixUtil.rad2deg(Math.atan(shear)));
                rotate = MatrixUtil._round(MatrixUtil.rad2deg(Math.atan2(matrix[1][0], matrix[0][0])));
                return [
                    ["translate", dx, dy],
                    ["rotate", rotate],
                    ["skewX", shear],
                    ["scale", sx, sy]
                ];
            };
            MatrixUtil.prototype.getTransformArray = function (transform) {
                var re = /\s*([a-z]*)\(([\w,\.,\-,\s]*)\)/gi, transforms = [], args, m, decomp, methods = MatrixUtil.transformMethods;
                while ((m = re.exec(transform))) {
                    if (methods.hasOwnProperty(m[1])) {
                        args = m[2].split(',');
                        args.unshift(m[1]);
                        transforms.push(args);
                    }
                    else if (m[1] === "matrix") {
                        args = m[2].split(',');
                        decomp = MatrixUtil.decompose([
                            [args[0], args[2], args[4]],
                            [args[1], args[3], args[5]],
                            [0, 0, 1]
                        ]);
                        transforms.push(decomp[0]);
                        transforms.push(decomp[1]);
                        transforms.push(decomp[2]);
                        transforms.push(decomp[3]);
                    }
                }
                return transforms;
            };
            MatrixUtil.prototype.getTransformFunctionArray = function (transform) {
                var list;
                switch (transform) {
                    case "skew":
                        list = [transform, 0, 0];
                        break;
                    case "scale":
                        list = [transform, 1, 1];
                        break;
                    case "scaleX":
                        list = [transform, 1];
                        break;
                    case "scaleY":
                        list = [transform, 1];
                        break;
                    case "translate":
                        list = [transform, 0, 0];
                        break;
                    default:
                        list = [transform, 0];
                        break;
                }
                return list;
            };
            MatrixUtil.prototype.compareTransformSequence = function (list1, list2) {
                var i = 0, len = list1.length, len2 = list2.length, isEqual = len === len2;
                if (isEqual) {
                    for (; i < len; ++i) {
                        if (list1[i][0] !== list2[i][0]) {
                            isEqual = false;
                            break;
                        }
                    }
                }
                return isEqual;
            };
            MatrixUtil._rounder = 100000;
            MatrixUtil.transformMethods = {
                rotate: "rotate",
                skew: "skew",
                skewX: "skewX",
                skewY: "skewY",
                translate: "translate",
                translateX: "translateX",
                translateY: "tranlsateY",
                scale: "scale",
                scaleX: "scaleX",
                scaleY: "scaleY"
            };
            return MatrixUtil;
        }());
        var MyPredicateImpl = (function () {
            function MyPredicateImpl() {
            }
            MyPredicateImpl.IsLess = function (x1, x2) {
                return x1 < x2;
            };
            MyPredicateImpl.IsGreaterOrEqual = function (x1, x2) {
                return x1 >= x2;
            };
            return MyPredicateImpl;
        }());
        var OutputStage = (function () {
            function OutputStage() {
                this.m_pDest = null;
            }
            OutputStage.prototype.SetDestination = function (pDest) {
                this.m_pDest = pDest;
            };
            OutputStage.prototype.HandleVertex = function (pnt) {
                this.m_pDest.push({ x: pnt.x, y: pnt.y });
            };
            OutputStage.prototype.Finalizer = function () {
            };
            return OutputStage;
        }());
        var ClipStage = (function () {
            function ClipStage(nextStage, boundary) {
                this.m_bFirst = false;
                this.m_pntFirst = null;
                this.m_pntPrevious = null;
                this.m_bPreviousInside = false;
                this.boundary = boundary;
                this.m_NextStage = nextStage;
                this.m_bFirst = true;
            }
            ClipStage.prototype.HandleVertex = function (pntCurrent) {
                var bCurrentInside = this.boundary.IsInside(pntCurrent);
                if (this.m_bFirst) {
                    this.m_pntFirst = pntCurrent;
                    this.m_bFirst = false;
                }
                else {
                    if (bCurrentInside) {
                        if (!this.m_bPreviousInside)
                            this.m_NextStage.HandleVertex(this.boundary.Intersect(this.m_pntPrevious, pntCurrent));
                        this.m_NextStage.HandleVertex(pntCurrent);
                    }
                    else if (this.m_bPreviousInside)
                        this.m_NextStage.HandleVertex(this.boundary.Intersect(this.m_pntPrevious, pntCurrent));
                }
                this.m_pntPrevious = pntCurrent;
                this.m_bPreviousInside = bCurrentInside;
            };
            ClipStage.prototype.Finalizer = function () {
                this.HandleVertex(this.m_pntFirst);
                this.m_NextStage.Finalizer();
            };
            return ClipStage;
        }());
        var BoundaryHor = (function () {
            function BoundaryHor(y, comp) {
                this.coord = 0;
                this.comp = null;
                this.coord = y;
                this.comp = comp;
            }
            BoundaryHor.prototype.IsInside = function (pnt) {
                return this.comp(pnt.y, this.coord);
            };
            BoundaryHor.prototype.Intersect = function (p0, p1) {
                var d = { x: p1.x - p0.x, y: p1.y - p0.y };
                var xslope = d.x / d.y;
                return { x: p0.x + xslope * (this.coord - p0.y), y: this.coord };
            };
            return BoundaryHor;
        }());
        var BoundaryVert = (function () {
            function BoundaryVert(x, comp) {
                this.coord = 0;
                this.comp = null;
                this.coord = x;
                this.comp = comp;
            }
            BoundaryVert.prototype.IsInside = function (pnt) {
                return this.comp(pnt.x, this.coord);
            };
            BoundaryVert.prototype.Intersect = function (p0, p1) {
                var d = { x: p1.x - p0.x, y: p1.y - p0.y };
                var yslope = d.y / d.x;
                return { y: p0.y + yslope * (this.coord - p0.x), x: this.coord };
            };
            return BoundaryVert;
        }());
        var SutherlandHodgman = (function () {
            function SutherlandHodgman() {
            }
            SutherlandHodgman.clip = function (input, direction, fillPercent) {
                var i, minX = Number.MAX_VALUE, minY = Number.MAX_VALUE, maxX = Number.MIN_VALUE, maxY = Number.MIN_VALUE;
                for (i = 0; i < input.length; i++) {
                    var p = input[i];
                    if (p.x < minX)
                        minX = p.x;
                    if (p.x > maxX)
                        maxX = p.x;
                    if (p.y < minY)
                        minY = p.y;
                    if (p.y > maxY)
                        maxY = p.y;
                }
                var clipped = [], stageOut = new OutputStage(), stage;
                switch (direction) {
                    case FillDirection.DownToTop:
                        stage = new ClipStage(stageOut, new BoundaryHor(maxY - (maxY - minY) / 100 * fillPercent, MyPredicateImpl.IsGreaterOrEqual));
                        break;
                    case FillDirection.TopToDown:
                        stage = new ClipStage(stageOut, new BoundaryHor(minY + (maxY - minY) / 100 * fillPercent, MyPredicateImpl.IsLess));
                        break;
                    case FillDirection.LeftToRight:
                        stage = new ClipStage(stageOut, new BoundaryVert(minX + (maxX - minX) / 100 * fillPercent, MyPredicateImpl.IsLess));
                        break;
                    case FillDirection.RightToLeft:
                        stage = new ClipStage(stageOut, new BoundaryVert(maxX - (maxX - minX) / 100 * fillPercent, MyPredicateImpl.IsGreaterOrEqual));
                        break;
                }
                stageOut.SetDestination(clipped);
                for (i = 0; i < input.length; i++)
                    stage.HandleVertex(input[i]);
                stage.Finalizer();
                return clipped;
            };
            return SutherlandHodgman;
        }());
        Drawing.SutherlandHodgman = SutherlandHodgman;
    })(Drawing = NxtControl.Drawing || (NxtControl.Drawing = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var Drawing;
    (function (Drawing) {
        var Color = (function () {
            function Color(color) {
                color = color || {};
                this.name = color.name || null;
                this.rgba = null;
                if (this.name != null) {
                    this.setFromName(this.name);
                }
                if (this.rgba === null) {
                    if (color.htmlString && color.htmlString.length > 0) {
                        this.tryParsingColor(color.htmlString);
                    }
                    if (this.rgba === null) {
                        if (typeof color.rgba !== 'undefined') {
                            this.setSource(color.rgba);
                        }
                        else {
                            this.setSource([color.r || 0, color.g || 0, color.b || 0, color.a || 0]);
                        }
                    }
                }
            }
            Color.prototype.currentColor = function () {
                return this;
            };
            Color.prototype.getR = function () {
                return this.rgba[0];
            };
            Color.prototype.getG = function () {
                return this.rgba[1];
            };
            Color.prototype.getB = function () {
                return this.rgba[2];
            };
            Color.prototype.getA = function () {
                return this.rgba[3];
            };
            Color.prototype.setA = function (alpha) {
                if (alpha > 1)
                    this.rgba[3] = 1;
                else
                    this.rgba[3] = alpha;
                return this;
            };
            Color.prototype.isTransparent = function () {
                return this.getA() === 0;
            };
            Color.fromName = function (name) {
                if (name in Color.blinkColorNameMap)
                    return new BlinkColor({ name: name });
                else if (name in Color.colorNameMap)
                    return new Color({ name: name });
                else {
                    console.error('Color ' + name + 'not found, setting to black');
                    return new Color({ rgba: [0, 0, 0, 1] });
                }
            };
            Color.fromString = function (htmlString) {
                if (htmlString in Color.blinkColorNameMap)
                    return new BlinkColor({ name: htmlString });
                else if (htmlString in Color.colorNameMap)
                    return new Color({ name: htmlString });
                return new Color({ htmlString: htmlString });
            };
            Color.prototype.toLive = function () {
                return this.toRgba();
            };
            Color.prototype.toObject = function () {
                if (this.name)
                    return this.name;
                return this.rgba;
            };
            Color.prototype.toJSON = function () {
                return this.toObject();
            };
            Color.prototype.setFromName = function (name) {
                if (name in Color.colorNameMap) {
                    var clr = Color.colorNameMap[name] || null;
                    if (clr instanceof Array) {
                        this.setSource(clr);
                    }
                    else
                        console.error(this.name + 'is not simple color, setting to black');
                }
                else
                    console.error('Color ' + this.name + 'not found, setting to black');
            };
            Color.prototype.tryParsingColor = function (color) {
                var source = Color.sourceFromHex(color);
                if (!source) {
                    source = Color.sourceFromRgb(color);
                }
                if (!source) {
                    source = Color.sourceFromHsl(color);
                }
                if (source) {
                    this.setSource(source);
                }
            };
            Color.prototype.rgbToHsl = function (r, g, b) {
                r /= 255, g /= 255, b /= 255;
                var h, s, l, max = Math.max.apply(Math, [r, g, b]), min = Math.min.apply(Math, [r, g, b]);
                l = (max + min) / 2;
                if (max === min) {
                    h = s = 0;
                }
                else {
                    var d = max - min;
                    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
                    switch (max) {
                        case r:
                            h = (g - b) / d + (g < b ? 6 : 0);
                            break;
                        case g:
                            h = (b - r) / d + 2;
                            break;
                        case b:
                            h = (r - g) / d + 4;
                            break;
                    }
                    h /= 6;
                }
                return [
                    Math.round(h * 360),
                    Math.round(s * 100),
                    Math.round(l * 100)
                ];
            };
            Color.prototype.getSource = function () {
                return this.rgba;
            };
            Color.prototype.setSource = function (source) {
                this.rgba = source;
                if (this.rgba.length == 3)
                    this.rgba.push(1);
                else if (this.rgba[3] > 1)
                    this.rgba[3] = 1;
            };
            Color.prototype.toRgb = function () {
                var source = this.getSource();
                return 'rgb(' + source[0] + ',' + source[1] + ',' + source[2] + ')';
            };
            Color.prototype.toRgba = function () {
                var source = this.getSource();
                return 'rgba(' + source[0] + ',' + source[1] + ',' + source[2] + ',' + source[3] + ')';
            };
            Color.prototype.toHsl = function () {
                var source = this.getSource(), hsl = this.rgbToHsl(source[0], source[1], source[2]);
                return 'hsl(' + hsl[0] + ',' + hsl[1] + '%,' + hsl[2] + '%)';
            };
            Color.prototype.toHsla = function () {
                var source = this.getSource(), hsl = this.rgbToHsl(source[0], source[1], source[2]);
                return 'hsla(' + hsl[0] + ',' + hsl[1] + '%,' + hsl[2] + '%,' + source[3] + ')';
            };
            Color.prototype.toHex = function () {
                var source = this.getSource();
                var r = source[0].toString(16);
                r = (r.length === 1) ? ('0' + r) : r;
                var g = source[1].toString(16);
                g = (g.length === 1) ? ('0' + g) : g;
                var b = source[2].toString(16);
                b = (b.length === 1) ? ('0' + b) : b;
                return r.toUpperCase() + g.toUpperCase() + b.toUpperCase();
            };
            Color.prototype.toGrayscale = function () {
                var source = this.getSource(), average = parseInt((source[0] * 0.3 + source[1] * 0.59 + source[2] * 0.11).toFixed(0), 10), currentAlpha = source[3];
                this.setSource([average, average, average, currentAlpha]);
                return this;
            };
            Color.prototype.toBlackWhite = function (threshold) {
                var source = this.getSource(), average = source[0] * 0.3 + source[1] * 0.59 + source[2] * 0.11, currentAlpha = source[3];
                threshold = threshold || 127;
                average = (Number(average) < Number(threshold)) ? 0 : 255;
                this.setSource([average, average, average, currentAlpha]);
                return this;
            };
            Color.prototype.lightenColor = function (lightenBy) {
                var source = this.rgba, hsl = this.rgbToHsl(source[0], source[1], source[2]);
                if (hsl[2] == 0)
                    hsl[2] = lightenBy * 100;
                else
                    hsl[2] *= (1.0 + lightenBy);
                if (hsl[2] > 100)
                    hsl[2] = 100;
                hsl[2] = Math.round(hsl[2]);
                return 'hsl(' + hsl[0] + ',' + hsl[1] + '%,' + hsl[2] + '%)';
            };
            Color.hue2rgb = function (p, q, t) {
                if (t < 0)
                    t += 1;
                if (t > 1)
                    t -= 1;
                if (t < 1 / 6)
                    return p + (q - p) * 6 * t;
                if (t < 1 / 2)
                    return q;
                if (t < 2 / 3)
                    return p + (q - p) * (2 / 3 - t) * 6;
                return p;
            };
            Color.fromRgb = function (color) {
                return Color.fromSource(Color.sourceFromRgb(color));
            };
            ;
            Color.sourceFromRgb = function (color) {
                var match = color.match(Color.reRGBa);
                if (match) {
                    var r = parseInt(match[1], 10) / (/%$/.test(match[1]) ? 100 : 1) * (/%$/.test(match[1]) ? 255 : 1), g = parseInt(match[2], 10) / (/%$/.test(match[2]) ? 100 : 1) * (/%$/.test(match[2]) ? 255 : 1), b = parseInt(match[3], 10) / (/%$/.test(match[3]) ? 100 : 1) * (/%$/.test(match[3]) ? 255 : 1);
                    return [r, g, b, match[4] ? parseFloat(match[4]) : 1];
                }
            };
            Color.fromHsl = function (color) {
                return Color.fromSource(Color.sourceFromHsl(color));
            };
            Color.fromObject = function (object) {
                try {
                    object = object || {};
                    if (object instanceof Color)
                        return object;
                    else if (object instanceof BlinkColor)
                        return object;
                    else if (typeof object === 'string')
                        return Color.fromString(object);
                    if (object.name)
                        return NxtControl.Drawing.Color.fromName(object.name);
                    if (object instanceof Array)
                        return new NxtControl.Drawing.Color({ rgba: object });
                    return new NxtControl.Drawing.BlinkColor(object);
                }
                catch (err) {
                    return NxtControl.Drawing.Color.Black;
                }
            };
            Color.sourceFromHsl = function (color) {
                var match = color.match(Color.reHSLa);
                if (!match)
                    return;
                var h = (((parseFloat(match[1]) % 360) + 360) % 360) / 360, s = parseFloat(match[2]) / (/%$/.test(match[2]) ? 100 : 1), l = parseFloat(match[3]) / (/%$/.test(match[3]) ? 100 : 1), r, g, b;
                if (s === 0) {
                    r = g = b = l;
                }
                else {
                    var q = l <= 0.5 ? l * (s + 1) : l + s - l * s;
                    var p = l * 2 - q;
                    r = Color.hue2rgb(p, q, h + 1 / 3);
                    g = Color.hue2rgb(p, q, h);
                    b = Color.hue2rgb(p, q, h - 1 / 3);
                }
                return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255), match[4] ? parseFloat(match[4]) : 1];
            };
            Color.fromHex = function (color) {
                return Color.fromSource(Color.sourceFromHex(color));
            };
            Color.sourceFromHex = function (color) {
                if (color.match(Color.reHex)) {
                    var value = color.slice(color.indexOf('#') + 1), isShortNotation = (value.length === 3), r = isShortNotation ? (value.charAt(0) + value.charAt(0)) : value.substring(0, 2), g = isShortNotation ? (value.charAt(1) + value.charAt(1)) : value.substring(2, 4), b = isShortNotation ? (value.charAt(2) + value.charAt(2)) : value.substring(4, 6);
                    return [parseInt(r, 16), parseInt(g, 16), parseInt(b, 16), 1];
                }
            };
            Color.fromSource = function (source) {
                var oColor = new Color();
                oColor.setSource(source);
                return oColor;
            };
            Color.replaceNamedColorWithBlinkColor = function (name, options) {
                if (Color.colorNameMap.hasOwnProperty(name) === true)
                    delete Color.colorNameMap[name];
                Color.blinkColorNameMap[name] = options;
            };
            Color.replaceNamedColorWithColor = function (name, options) {
                if (Color.blinkColorNameMap.hasOwnProperty(name) === true)
                    delete Color.blinkColorNameMap[name];
                Color.colorNameMap[name] = options;
            };
            Color.reRGBa = /^rgba?\(\s*(\d{1,3}\%?)\s*,\s*(\d{1,3}\%?)\s*,\s*(\d{1,3}\%?)\s*(?:\s*,\s*(\d+(?:\.\d+)?)\s*)?\)$/;
            Color.reHSLa = /^hsla?\(\s*(\d{1,3})\s*,\s*(\d{1,3}\%)\s*,\s*(\d{1,3}\%)\s*(?:\s*,\s*(\d+(?:\.\d+)?)\s*)?\)$/;
            Color.reHex = /^#?([0-9a-f]{6}|[0-9a-f]{3})$/i;
            Color.fromRgba = Color.fromRgb;
            Color.fromHsla = Color.fromHsl;
            Color.colorNameMap = {
                'White': [255, 255, 255],
                'Black': [0, 0, 0],
                'Transparent': [0, 0, 0, 0],
                'Red': [255, 0, 0],
                'Green': [0, 255, 0],
                'Blue': [0, 0, 255],
                'Yellow': [255, 255, 0],
                'Magenta': [255, 0, 255],
                'Cyan': [0, 255, 255],
                'Grey50': [128, 128, 128],
                'DarkGrey': [64, 64, 64],
                'LightGrey': [196, 196, 196],
                'DevPassive': [255, 255, 255],
                'DevActive1': [0, 255, 0],
                'DevActive2': [0, 255, 0],
                'DevAnalogSp': [0, 255, 0],
                'DevAnalogPv': [255, 255, 255],
                'DevAnalogOut': [0, 255, 0],
                'MedAcid': [255, 170, 0],
                'MedAir': [0, 0, 255],
                'MedAlcali': [77, 77, 77],
                'MedCoal': [153, 51, 255],
                'MedBurnGas': [255, 255, 0],
                'MedFlueGas': [255, 255, 153],
                'MedBurnOil': [128, 101, 64],
                'MedOxygen': [170, 153, 255],
                'MedOil': [128, 64, 0],
                'MedSteam': [179, 54, 54],
                'MedSteamMP': [255, 90, 95],
                'MedSteamLP': [255, 172, 89],
                'Water': [135, 205, 122],
                'CoolWater': [0, 102, 0],
                'NatWater': [90, 140, 105],
                'Ele110kV': [255, 0, 0],
                'Ele30kV': [0, 204, 0],
                'Ele10_20kV': [204, 102, 51],
                'Ele1_10kV': [255, 153, 0],
                'EleNV': [255, 255, 0],
                'Ele24dc': [53, 53, 53],
                'EleNVme': [255, 255, 153],
                'AlarmCame': [255, 0, 0],
                'WarningCame': [255, 255, 0],
                'ButtonPenColor': [0, 0, 0],
                'ButtonTextColor': [230, 230, 230],
                'ButtonTextDisabledColor': [80, 80, 80],
                'ButtonTextColorMouseDown': [2, 178, 238],
                'ButtonTextColorTrue': [2, 178, 238],
                'ButtonTextColorFalse': [230, 230, 230],
                'ButtonInnerBorderColor': [70, 70, 70],
                'TrackerTextColor': [140, 140, 140],
                'TrackerValueColor': [2, 178, 238],
                'TrackerLineBackColor': [10, 10, 10],
                'TrackerTextDisabledColor': [80, 80, 80],
                'TrackerValueDisabledColor': [80, 80, 80],
                'TrackerScaleColor': [0, 0, 0],
                'TrackerScaleDisabledColor': [80, 80, 80],
                'RoundKnobTextColor': [230, 230, 230],
                'RoundKnobTextDisabledColor': [80, 80, 80],
                'RoundKnobKnobColor': [0, 0, 0],
                'RoundKnobNeedleColor': [0, 0, 0],
                'RoundKnobInnerCircleColor': [2, 178, 238],
                'RoundKnobInnerCircleDisabledColor': [120, 120, 120],
                'RoundKnobScaleColor': [0, 0, 0],
                'RoundKnobScaleDisabledColor': [30, 30, 30],
                'GroupBoxBackColor': [32, 36, 40],
                'LabelBackColor': [22, 22, 22],
                'LabelPenColor': [22, 22, 22],
                'LabelTextColor': [140, 140, 140],
                'TextBoxTextColor': [230, 230, 230],
                'TextBoxTextDisabledColor': [80, 80, 80],
                'TextBoxBackColor': [5, 5, 5],
                'TextBoxPenColor': [5, 5, 5],
                'CanvasBackColor': [62, 65, 75],
                'FaceplateBackColor': [62, 65, 75],
                'ComboBoxTextColor': [230, 230, 230],
                'ComboBoxTextDisabledColor': [80, 80, 80],
                'ComboBoxBackColor': [5, 5, 5],
                'ComboBoxArrowBackColor': [5, 5, 5],
                'ComboBoxArrowColor': [255, 255, 255],
                'ComboBoxArrowDisabledColor': [70, 70, 70],
                'ComboBoxPenColor': [5, 5, 5],
                'CanvasTopologyButtonColor': [255, 255, 255],
                'CanvasTopologyButtonTextColor': [255, 255, 255],
                'CanvasTopologyButtonCurrentTextColor': [2, 178, 238],
                'FpStyleRectangularWindowBackground': [166, 166, 166],
                'FpStyleRectangularWindowOuterBorder': [0, 0, 0],
                'FpStyleRectangularWindowInnerBorder': [240, 240, 240],
                'FpStyleRectangularHeaderBackground': [0, 0, 0],
                'FpStyleRectangularTitle': [255, 255, 255],
                'FpStyleRectangularHeader3DLineDark': [0, 0, 0],
                'FpStyleRectangularHeader3DLineBright': [87, 87, 87],
                'FpStyleRectangularHeaderLine1': [0, 0, 0],
                'FpStyleRectangularHeaderLine2': [198, 198, 198],
                'FpStyleRectangularLineClose': [255, 255, 255],
                'FpStyleRoundWindowBackground': [166, 166, 166],
                'FpStyleRoundWindowBorder': [255, 255, 255],
                'FpStyleRoundHeaderBackground': [0, 0, 0],
                'FpStyleRoundTitle': [255, 255, 255],
                'FpStyleRoundHeaderLine': [186, 186, 186],
                'FpStyleRoundLineClose': [255, 255, 255],
                'RuntimeConnectionConnected': [255, 255, 255],
                'RuntimeConnectionNotConnected': [70, 70, 70],
                'LogStateNone': [255, 255, 255],
                'LogStateWarning': [255, 255, 0],
                'LogStateError': [255, 0, 0],
                'TagValueEditorDarkBackCellColor': [219, 224, 228],
                'TagValueEditorLightBackCellColor': [239, 239, 239],
                'TagValueEditorForeCellColor': [47, 48, 50],
                'TagValueEditorColumnHeaderBackColor': [47, 48, 50],
                'TagValueEditorColumnHeaderForeColor': [255, 255, 255],
                'TagValueEditorStatusStripBackColor': [100, 105, 110],
                'TagValueEditorToolStripBackColor': [100, 105, 110],
                'AlarmControlDarkBackCellColor': [219, 224, 228],
                'AlarmControlLightBackCellColor': [239, 239, 239],
                'AlarmControlForeCellColor': [47, 48, 50],
                'AlarmControlHeaderBackColor': [47, 48, 50],
                'AlarmControlHeaderForeColor': [255, 255, 255],
                'LedFrameColor': [0, 0, 0],
                'LedDefaultColor': [90, 90, 90],
                'LedFalseColor': [90, 90, 90],
                'LedTrueColor': [0, 255, 0],
            };
            Color.blinkColorNameMap = {
                'BlackWhite': ['Black', 4, 'White', 4],
                'YellowWhite': ['Yellow', 4, 'White', 4],
                'RedTransparent': ['Red', 4, 'Transparent', 4],
                'DevGoToPas': [[170, 170, 170], 4, [255, 255, 255], 4],
                'DevGoToAct1': [[170, 170, 170], 4, [0, 255, 0], 4],
                'DevGoToAct2': [[170, 170, 170], 4, [0, 255, 0], 4],
                'DevError': [[170, 170, 170], 4, [255, 0, 0], 4],
                'WarningCameNotAcked': ['Yellow', 4, 'Transparent', 4],
                'AlarmGoneNotAcked': ['Red', 8, 'Transparent', 8],
                'AlarmCameNotAcked': ['Red', 4, 'Transparent', 4],
                'WarningGoneNotAcked': ['Yellow', 8, 'Transparent', 8],
                'ButtonNotMatchingTextColor': ['Red', 8, 'Yellow', 8],
            };
            return Color;
        }());
        Drawing.Color = Color;
        Color.Transparent = new Color({ name: 'Transparent' });
        Color.Black = new Color({ name: 'Black' });
        Color.White = new Color({ name: 'White' });
        var BlinkColor = (function () {
            function BlinkColor(options) {
                this.showFirstColor = true;
                this.referenceBlinkColor = null;
                if (options.hasOwnProperty('name') && options.name !== null) {
                    this.name = options.name;
                    this.setFromName(this.name);
                }
                if (!options.hasOwnProperty('name') || options.name === null) {
                    this.color1 = new Color(options.color1);
                    this.duration1 = options.duration1 || 8;
                    this.color2 = new Color(options.color2);
                    this.duration2 = options.duration2 || 8;
                }
            }
            BlinkColor.prototype.currentColor = function () {
                if (this.showFirstColor)
                    return this.color1;
                return this.color2;
            };
            BlinkColor.prototype.setFromName = function (name) {
                if (name in Color.blinkColorNameMap) {
                    var clr = Color.blinkColorNameMap[name];
                    if (clr && clr instanceof Array) {
                        if (typeof clr[0] === 'string')
                            this.color1 = new Color({ name: clr[0] });
                        else
                            this.color1 = new Color({ rgba: clr[0] });
                        this.duration1 = clr[1];
                        if (typeof clr[2] === 'string')
                            this.color2 = new Color({ name: clr[2] });
                        else
                            this.color2 = new Color({ rgba: clr[2] });
                        this.duration2 = clr[3];
                    }
                    else {
                        this.color1 = new Color({ rgba: [0, 0, 0, 1] });
                        this.color2 = new Color({ rgba: [255, 255, 255, 1] });
                        this.duration1 = this.duration2 = 8;
                    }
                }
                else {
                    console.error('Color ' + this.name + 'not found, setting to black and white');
                    this.color1 = new Color({ rgba: [0, 0, 0, 1] });
                    this.color2 = new Color({ rgba: [255, 255, 255, 1] });
                    this.duration1 = this.duration2 = 8;
                }
            };
            BlinkColor.prototype.tick = function (tick) {
                if (this.duration1 + this.duration2 === 0)
                    return false;
                var phase = tick % (this.duration1 + this.duration2);
                phase -= this.duration1;
                if (phase < 0) {
                    if (this.showFirstColor === true) {
                        return false;
                    }
                    this.showFirstColor = true;
                    return true;
                }
                if (this.showFirstColor === true) {
                    this.showFirstColor = false;
                    return true;
                }
                return false;
            };
            BlinkColor.prototype.toJSON = function () {
                return this.name;
            };
            BlinkColor.prototype.toObject = function () {
                if (this.name)
                    return this.name;
                var arr = [];
                arr.push(this.color1.toObject());
                arr.push(this.duration1);
                arr.push(this.color2.toObject());
                arr.push(this.duration2);
                return arr;
            };
            BlinkColor.prototype.isTransparent = function () {
                return this.referenceBlinkColor !== null ? this.referenceBlinkColor.isTransparent() : this.showFirstColor ? this.color1.isTransparent() : this.color2.isTransparent();
            };
            BlinkColor.prototype.toLive = function (ctx) {
                return this.referenceBlinkColor !== null ? this.referenceBlinkColor.toLive(ctx) : this.showFirstColor ? this.color1.toRgba() : this.color2.toRgba();
            };
            return BlinkColor;
        }());
        Drawing.BlinkColor = BlinkColor;
    })(Drawing = NxtControl.Drawing || (NxtControl.Drawing = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var Drawing;
    (function (Drawing) {
        var toFixed = NxtControl.Util.Convert.toFixed;
        var Pen = (function () {
            function Pen(options) {
                this.color = null;
                this.width = 1;
                this.dashArray = null;
                this.lineCap = 'butt';
                this.lineJoin = 'miter';
                this.miterLimit = 10;
                options = options || {};
                options.color = options.color || Drawing.Color.Black;
                this.color = Drawing.Color.fromObject(options.color);
                this.width = options.width || this.width;
                this.lineCap = options.lineCap || this.lineCap;
                this.lineJoin = options.lineJoin || this.lineJoin;
                this.dashArray = options.dashArray || this.dashArray;
                this.miterLimit = options.miterLimit || this.miterLimit;
            }
            Pen.prototype.toLive = function () {
                return this.color.toLive();
            };
            Pen.prototype.toObject = function () {
                var o = {
                    color: this.color.toObject()
                };
                if (this.width != 1)
                    o.width = toFixed(this.width, 2);
                if (this.dashArray != null)
                    o.dashArray = this.dashArray;
                if (this.lineCap != 'butt')
                    o.lineCap = this.lineCap;
                if (this.lineJoin != 'miter')
                    o.lineJoin = this.lineJoin;
                if (this.miterLimit != 10)
                    o.miterLimit = toFixed(this.miterLimit, 2);
                return o;
            };
            Pen.prototype.toJSON = function () {
                return this.toObject();
            };
            Pen.prototype.clone = function () {
                return NxtControl.Util.object.extend(new NxtControl.Drawing.Pen(), this);
            };
            Pen.fromString = function (text) {
                return new NxtControl.Drawing.Pen({ color: NxtControl.Drawing.Color.fromString(text) });
            };
            Pen.fromObject = function (object) {
                object = object || Pen.Black;
                if (typeof object === 'string') {
                    return NxtControl.Drawing.Pen.fromString(object);
                }
                else if (object instanceof NxtControl.Drawing.Pen) {
                    return object;
                }
                else {
                    try {
                        return new NxtControl.Drawing.Pen(object);
                    }
                    catch (err) {
                        return Pen.Black;
                    }
                }
            };
            return Pen;
        }());
        Drawing.Pen = Pen;
        Pen.White = new Pen({ color: Drawing.Color.White });
        Pen.Black = new Pen({ color: Drawing.Color.Black });
        var Brush = (function () {
            function Brush() {
                this.name = null;
            }
            Brush.fromName = function (name) {
                if (name in Brush.solidBrushNameMap) {
                    return SolidBrush.fromNameMap(name, Brush.solidBrushNameMap[name]);
                }
                if (name in Brush.gradientBrushNameMap) {
                    return GradientBrush.fromNameMap(name, Brush.gradientBrushNameMap[name]);
                }
                return NullBrush.NullBrush;
            };
            Brush.fromObject = function (object) {
                object = object || SolidBrush.Black;
                if (typeof object === 'string') {
                    if (object === '' || object === '(none)')
                        return NullBrush.NullBrush;
                    var b = Brush.fromName(object);
                    if (b === NullBrush.NullBrush)
                        return new NxtControl.Drawing.SolidBrush({ color: object });
                    return b;
                }
                else if (object instanceof Brush) {
                    return object;
                }
                else {
                    try {
                        if (object.hasOwnProperty('name'))
                            return Brush.fromName(object.name);
                        if (object.hasOwnProperty('color'))
                            return new SolidBrush(object);
                        return new GradientBrush(object);
                    }
                    catch (err) {
                        return SolidBrush.Black;
                    }
                }
            };
            Brush.replaceNamedBrushWithSolidBrush = function (name, options) {
                if (Brush.gradientBrushNameMap.hasOwnProperty(name) === true)
                    delete Brush.gradientBrushNameMap[name];
                Brush.solidBrushNameMap[name] = options;
            };
            Brush.replaceNamedBrushWithGradientBrush = function (name, options) {
                if (Brush.solidBrushNameMap.hasOwnProperty(name) === true)
                    delete Brush.solidBrushNameMap[name];
                Brush.gradientBrushNameMap[name] = options;
            };
            Brush.solidBrushNameMap = {
                'Transparent': { "color": "Transparent" },
                'Black': { "color": "Black" },
                'White': { "color": "White" },
                'ButtonPushedBrush': { "color": [0, 0, 0] },
                'TrackerBrush': { "color": "Transparent" },
                'TrackerLineBackgroundBrush': { "color": "rgb(10, 10, 10)" },
                'TrackerLineBrush': { "color": "TrackerValueColor" },
                'TrackerLineDisabledBrush': { "color": "TrackerValueDisabledColor" },
                'RoundKnobBrush': { "color": "Transparent" },
                'GroupBoxBrush': { "color": "GroupBoxBackColor" },
                'LabelBrush': { "color": "LabelBackColor" },
                'TextBoxBrush': { "color": "TextBoxBackColor" },
                'TextBoxReadOnlyBrush': { "color": "rgb(20, 20, 20)" },
                'CanvasBrush': { "color": "CanvasBackColor" },
                'FaceplateBrush': { "color": "FaceplateBackColor" },
                'ComboBoxBrush': { "color": "ComboBoxBackColor" },
                'ComboBoxDisabledBrush': { "color": "rgb(80, 80, 80)" },
                'ComboBoxArrowBackDisabledBrush': { "color": "rgb(80, 80, 80)" },
                'FpStyleRectangularHandleBrush': { "color": "rgb(90, 90, 90)" },
            };
            Brush.gradientBrushNameMap = {
                'ButtonBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(10, 10, 10)" },
                        { "offset": "0.24", "color": "rgb(40,40,40)" },
                        { "offset": "0.49", "color": "rgb(65,65,65)" },
                        { "offset": "0.52", "color": "rgb(20,20,20)" },
                        { "offset": "1", "color": "rgb(40,40,40)" }]
                },
                'ButtonFalseBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(10, 10, 10)" },
                        { "offset": "0.24", "color": "rgb(40,40,40)" },
                        { "offset": "0.49", "color": "rgb(65,65,65)" },
                        { "offset": "0.52", "color": "rgb(20,20,20)" },
                        { "offset": "1", "color": "rgb(40,40,40)" }]
                },
                'ButtonTrueBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(0, 0, 0)" },
                        { "offset": "0.04", "color": "rgb(100,100,100)" },
                        { "offset": "0.07", "color": "rgb(30,30,30)" },
                        { "offset": "0.1", "color": "rgb(0,0,0)" },
                        { "offset": "1", "color": "rgb(30,30,30)" }]
                },
                'ButtonSwitchBackgroundBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(0, 0, 0)" },
                        { "offset": "0.15", "color": "rgb(10,10,10)" },
                        { "offset": "0.92", "color": "rgb(10,10,10)" },
                        { "offset": "1", "color": "rgb(150,150,150)" }]
                },
                'ButtonSwitchBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(10, 10, 10)" },
                        { "offset": "0.24", "color": "rgb(40,40,40)" },
                        { "offset": "0.49", "color": "rgb(65,65,65)" },
                        { "offset": "0.52", "color": "rgb(20,20,20)" },
                        { "offset": "1", "color": "rgb(40,40,40)" }]
                },
                'CanvasTopologyButtonBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop, "coords": { "x1": 0, "y1": 0, "x2": 0, "y2": 40 },
                    "colorStops": [{ "offset": "0", "color": "rgb(10, 10, 10)" },
                        { "offset": "0.24", "color": "rgb(40,40,40)" },
                        { "offset": "0.49", "color": "rgb(65,65,65)" },
                        { "offset": "0.52", "color": "rgb(20,20,20)" },
                        { "offset": "1", "color": "rgb(40,40,40)" }]
                },
                'TrackerHandleBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(120, 120, 120)" },
                        { "offset": "0.44", "color": "rgb(30,30,30)" },
                        { "offset": "0.9", "color": "rgb(30,30,30)" },
                        { "offset": "1", "color": "rgb(0,0,0)" }]
                },
                'TrackerHandleDisabledBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(20, 20, 20)" },
                        { "offset": "0.49", "color": "rgb(20,20,20)" },
                        { "offset": "0.51", "color": "rgb(35,35,35)" },
                        { "offset": "0.76", "color": "rgb(40,40,40)" },
                        { "offset": "1", "color": "rgb(10,10,10)" }]
                },
                'RuntimeConnectionBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(10, 10, 10)" },
                        { "offset": "0.24", "color": "rgb(40,40,40)" },
                        { "offset": "0.49", "color": "rgb(65,65,65)" },
                        { "offset": "0.52", "color": "rgb(20,20,20)" },
                        { "offset": "1", "color": "rgb(40,40,40)" }]
                },
                'LogStateBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(10, 10, 10)" },
                        { "offset": "0.24", "color": "rgb(40,40,40)" },
                        { "offset": "0.49", "color": "rgb(65,65,65)" },
                        { "offset": "0.52", "color": "rgb(20,20,20)" },
                        { "offset": "1", "color": "rgb(40,40,40)" }]
                },
                'ComboBoxArrowBackBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(80, 80, 80)" },
                        { "offset": "0.44", "color": "rgb(50,50,50)" },
                        { "offset": "0.9", "color": "rgb(50,50,50)" },
                        { "offset": "1", "color": "rgb(5,5,5)" }]
                },
                'CanvasTopologyRoseBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(10, 10, 10)" },
                        { "offset": "0.24", "color": "rgb(40,40,40)" },
                        { "offset": "0.49", "color": "rgb(65,65,65)" },
                        { "offset": "0.52", "color": "rgb(20,20,20)" },
                        { "offset": "1", "color": "rgb(40,40,40)" }]
                },
                'CanvasTopologySeparatorBrush': {
                    "orient": Drawing.FillOrientation.HorizontalLeft,
                    "colorStops": [{ "offset": "0", "color": "rgb(30, 30, 30)" },
                        { "offset": "0.49", "color": "rgb(30,30,30)" },
                        { "offset": "0.5", "color": "rgb(100,100,100)" },
                        { "offset": "1", "color": "rgb(5,5,5)" }]
                },
                'CanvasTopologyButtonCurrentBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(10, 10, 10)" },
                        { "offset": "0.24", "color": "rgb(40,40,40)" },
                        { "offset": "0.49", "color": "rgb(65,65,65)" },
                        { "offset": "0.52", "color": "rgb(20,20,20)" },
                        { "offset": "1", "color": "rgb(40,40,40)" }]
                },
                'CanvasTopologyHeaderBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(140, 140, 140)" },
                        { "offset": "0.48", "color": "rgb(67,70,75)" },
                        { "offset": "0.49", "color": "rgb(47,48,50)" },
                        { "offset": "0.96", "color": "rgb(47,48,50)" },
                        { "offset": "1", "color": "rgb(0,0,0)" }]
                },
                'FpStyleRoundHandleBrush': {
                    "orient": Drawing.FillOrientation.VerticalTop,
                    "colorStops": [{ "offset": "0", "color": "rgb(114, 114, 114)" },
                        { "offset": "1", "color": "rgb(0,0,0)" }]
                }
            };
            return Brush;
        }());
        Drawing.Brush = Brush;
        var NullBrush = (function (_super) {
            __extends(NullBrush, _super);
            function NullBrush() {
                _super.apply(this, arguments);
            }
            NullBrush.prototype.attach = function () { };
            NullBrush.prototype.detach = function () { };
            NullBrush.prototype.toObject = function () {
                return '(none)';
            };
            NullBrush.prototype.toJSON = function () {
                return this.toObject();
            };
            NullBrush.prototype.toLive = function (ctx) {
                return Drawing.Color.Transparent.toLive();
            };
            return NullBrush;
        }(Brush));
        Drawing.NullBrush = NullBrush;
        Brush.NullBrush = new NullBrush;
        var SolidBrush = (function (_super) {
            __extends(SolidBrush, _super);
            function SolidBrush(options) {
                _super.call(this);
                options = options || { color: Drawing.Color.Black };
                var color = options.color;
                this.color = Drawing.Color.fromObject(color);
            }
            SolidBrush.prototype.attach = function () {
                if (this.color && this.color instanceof NxtControl.Drawing.BlinkColor)
                    NxtControl.Services.ColorService.instance.attach(this.color);
            };
            SolidBrush.prototype.detach = function () {
                if (this.color && this.color instanceof NxtControl.Drawing.BlinkColor)
                    NxtControl.Services.ColorService.instance.detach(this.color);
            };
            SolidBrush.prototype.toObject = function () {
                if (this.name)
                    return this.name;
                return { color: this.color.toObject() };
            };
            SolidBrush.prototype.toJSON = function () {
                return this.toObject();
            };
            SolidBrush.prototype.toLive = function (ctx) {
                return this.color.toLive();
            };
            SolidBrush.fromNameMap = function (name, options) {
                var solidBrush = new SolidBrush(options);
                solidBrush.name = name;
                return solidBrush;
            };
            return SolidBrush;
        }(Brush));
        Drawing.SolidBrush = SolidBrush;
        Brush.White = new SolidBrush({ color: Drawing.Color.White });
        Brush.Black = new SolidBrush({ color: Drawing.Color.Black });
        var GradientBrush = (function (_super) {
            __extends(GradientBrush, _super);
            function GradientBrush(options) {
                _super.call(this);
                options = options || {};
                this.type = options.type || 'linear';
                options.coords = options.coords || {};
                this.orient = options.orient || Drawing.FillOrientation.DiagonalLeftTop;
                var coords = {
                    x1: options.coords.x1 || 0,
                    y1: options.coords.y1 || 0,
                    x2: options.coords.x2 || 0,
                    y2: options.coords.y2 || 0
                };
                if (this.type === 'radial') {
                    coords.r1 = options.coords.r1 || 0;
                    coords.r2 = options.coords.r2 || 0;
                }
                this.coords = coords;
                this.colorStops = options.colorStops || [];
                for (var i = this.colorStops.length - 1; i >= 0; i--) {
                    this.colorStops[i].color = Drawing.Color.fromObject(this.colorStops[i].color);
                    if (typeof this.colorStops[i].offset === 'string')
                        this.colorStops[i].offset = parseFloat(this.colorStops[i].offset);
                }
            }
            GradientBrush.prototype.attach = function () {
                if (this.colorStops)
                    for (var i = this.colorStops.length - 1; i >= 0; i--) {
                        if (this.colorStops[i].color && this.colorStops[i].color instanceof NxtControl.Drawing.BlinkColor)
                            NxtControl.Services.ColorService.instance.attach(this.colorStops[i].color);
                    }
            };
            GradientBrush.prototype.detach = function () {
                if (this.colorStops)
                    for (var i = this.colorStops.length - 1; i >= 0; i--) {
                        if (this.colorStops[i].color && this.colorStops[i].color instanceof NxtControl.Drawing.BlinkColor)
                            NxtControl.Services.ColorService.instance.detach(this.colorStops[i].color);
                    }
            };
            GradientBrush.prototype.colorStopsToObject = function () {
                var cs = [];
                for (var i = 0; i < this.colorStops.length; i++) {
                    cs.push({ color: this.colorStops[i].color.toObject(), offset: this.colorStops[i].offset });
                }
                return cs;
            };
            GradientBrush.prototype.toObject = function () {
                if (this.name)
                    return this.name;
                else if (this.orient)
                    return {
                        orient: this.orient,
                        colorStops: this.colorStopsToObject()
                    };
                else
                    return {
                        type: this.type,
                        coords: this.coords,
                        colorStops: this.colorStopsToObject()
                    };
            };
            GradientBrush.prototype.toJSON = function () {
                return this.toObject();
            };
            GradientBrush.prototype.toLive = function (ctx) {
                var i, len, gradient, color, offset;
                if (!this.type)
                    return;
                if (this.type === 'linear') {
                    if (this.orient !== Drawing.FillOrientation.Center)
                        gradient = ctx.createLinearGradient(this.coords.x1, this.coords.y1, this.coords.x2, this.coords.y2);
                    else
                        gradient = ctx.createRadialGradient(Math.abs(this.coords.x2 + this.coords.x1) / 2, Math.abs(this.coords.y2 + this.coords.y1) / 2, 0, Math.abs(this.coords.x2 + this.coords.x1) / 2, Math.abs(this.coords.y2 + this.coords.y1) / 2, Math.max(Math.abs(this.coords.x2 - this.coords.x1) / 2, Math.abs(this.coords.y2 - this.coords.y1) / 2));
                }
                else if (this.type === 'radial') {
                    gradient = ctx.createRadialGradient(this.coords.x1, this.coords.y1, this.coords.r1, this.coords.x2, this.coords.y2, this.coords.r2);
                }
                if (this.orient) {
                    switch (this.orient) {
                        case Drawing.FillOrientation.HorizontalRight:
                        case Drawing.FillOrientation.VerticalBottom:
                            for (i = this.colorStops.length - 1; i >= 0; i--) {
                                color = this.colorStops[i].color;
                                offset = this.colorStops[i].offset;
                                gradient.addColorStop(1 - offset, color.toLive());
                            }
                            break;
                        case Drawing.FillOrientation.InvertedHorizontalCenter:
                        case Drawing.FillOrientation.InvertedVerticalCenter:
                            for (i = 0, len = this.colorStops.length; i < len; i++) {
                                color = this.colorStops[i].color;
                                offset = this.colorStops[i].offset;
                                gradient.addColorStop(offset / 2, color.toLive());
                                gradient.addColorStop(1 - offset / 2, color.toLive());
                            }
                            break;
                        case Drawing.FillOrientation.HorizontalCenter:
                        case Drawing.FillOrientation.VerticalCenter:
                            for (i = this.colorStops.length - 1; i >= 0; i--) {
                                color = this.colorStops[i].color;
                                offset = 1 - this.colorStops[i].offset;
                                gradient.addColorStop(offset / 2, color.toLive());
                                gradient.addColorStop(1 - offset / 2, color.toLive());
                            }
                            break;
                        default:
                            for (i = 0, len = this.colorStops.length; i < len; i++) {
                                color = this.colorStops[i].color;
                                offset = this.colorStops[i].offset;
                                gradient.addColorStop(offset, color.toLive());
                            }
                            break;
                    }
                }
                else {
                    for (i = 0, len = this.colorStops.length; i < len; i++) {
                        color = this.colorStops[i].color;
                        offset = this.colorStops[i].offset;
                        gradient.addColorStop(offset, color.toLive());
                    }
                }
                return gradient;
            };
            GradientBrush.fromNameMap = function (name, options) {
                var gradientBrush = new GradientBrush(options);
                gradientBrush.name = name;
                return gradientBrush;
            };
            return GradientBrush;
        }(Brush));
        Drawing.GradientBrush = GradientBrush;
    })(Drawing = NxtControl.Drawing || (NxtControl.Drawing = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var toFixed = NxtControl.Util.Convert.toFixed, FillOrientation = NxtControl.Drawing.FillOrientation;
        var Shape = (function () {
            function Shape() {
                this.designer = null;
                this._controlsVisibility = null;
                this.selected = new NxtControl.event();
                this.mousedown = new NxtControl.event();
                this.mousemove = new NxtControl.event();
                this.mouseup = new NxtControl.event();
                this.click = new NxtControl.event();
                this.added = new NxtControl.event();
                this.modified = new NxtControl.event();
                this.removed = new NxtControl.event();
                this.rotating = new NxtControl.event();
                this.scaling = new NxtControl.event();
                this.moving = new NxtControl.event();
            }
            Shape.prototype.dispose = function () {
                if (this.brush)
                    this.brush.detach();
                if (this.pen && this.pen.color && this.pen.color instanceof NxtControl.Drawing.BlinkColor)
                    NxtControl.Services.ColorService.instance.detach(this.pen.color);
                return this;
            };
            Shape.prototype.initEvents = function () {
            };
            Shape.prototype.disposeEvents = function () {
            };
            Shape.prototype.getType = function () { return this.type; };
            Shape.prototype.getCsClass = function () { return this.csClass; };
            Shape.prototype.getTop = function () {
                return this.top;
            };
            Shape.prototype.setTop = function (value) {
                this.top = value;
                return this;
            };
            Shape.prototype.getLeft = function () {
                return this.left;
            };
            Shape.prototype.setLeft = function (value) {
                this.left = value;
                return this;
            };
            Shape.prototype.getWidth = function () {
                return this.width * this.scaleX;
            };
            Shape.prototype.getUnscaledWidth = function () {
                return this.width;
            };
            Shape.prototype.setWidth = function (value) {
                this.width = value / this.scaleY;
                return this;
            };
            Shape.prototype.getHeight = function () {
                return this.height * this.scaleY;
            };
            Shape.prototype.getUnscaledHeight = function () {
                return this.height;
            };
            Shape.prototype.setHeight = function (value) {
                this.height = value / this.scaleY;
                return this;
            };
            Shape.prototype.getScaleX = function () {
                return this.scaleX;
            };
            Shape.prototype.getScaleY = function () {
                return this.scaleY;
            };
            Shape.prototype.getFlipX = function () {
                return this.flipX;
            };
            Shape.prototype.setFlipX = function (value) {
                this.flipX = value;
                return this;
            };
            Shape.prototype.getFlipY = function () {
                return this.flipY;
            };
            Shape.prototype.setFlipY = function (value) {
                this.flipY = value;
                return this;
            };
            Shape.prototype.getAngle = function () {
                return this.angle;
            };
            Shape.prototype.setAngle = function (value) {
                this.angle = value;
                return this;
            };
            Shape.prototype.getAnchor = function () {
                return this.anchor;
            };
            Shape.prototype.setAnchor = function (anchor) {
                this.anchor = anchor;
                return this;
            };
            Shape.prototype.getAnchorOffsets = function () {
                return this.anchorOffsets || { x: 0, y: 0, w: 0, h: 0 };
            };
            Shape.prototype.setAnchorOffsets = function (ao) {
                this.anchorOffsets = ao;
                return this;
            };
            Shape.prototype.getBrush = function () {
                return this.brush;
            };
            Shape.prototype.setBrush = function (value) {
                this._set('brush', value || NxtControl.Drawing.NullBrush.NullBrush, true);
                return this;
            };
            Shape.prototype.getPen = function () {
                return this.pen;
            };
            Shape.prototype.setPen = function (value) {
                this._set('pen', value, true);
                return this;
            };
            Shape.prototype.getDrawable = function () {
                return this.drawable;
            };
            Shape.prototype.getVisible = function () {
                return this.visible && this.drawable;
            };
            Shape.prototype.setVisible = function (visible) {
                this._set('visible', visible, true);
            };
            Shape.prototype.getName = function () {
                return this.name;
            };
            Shape.prototype.setName = function (name) {
                this.name = name;
            };
            Shape.prototype.getGroup = function () {
                return this.group;
            };
            Shape.prototype.getCanvas = function () {
                if (this.canvas)
                    return this.canvas;
                if (this.group)
                    return this.group.getCanvas();
                return null;
            };
            Shape.prototype.getOriginalState = function () { return this.originalState; };
            Shape.prototype.setDesigner = function (designer, canvas) {
                this.designer = designer;
                this.designer.initialize({ shape: this, canvas: canvas });
            };
            Shape.prototype.load = function (options) {
                this.setOptions(options);
                return this;
            };
            Shape.prototype.invalidate = function () {
                if (this.canvas)
                    this.canvas.invalidate();
                else if (this.group)
                    this.group.invalidate();
            };
            Shape.prototype.invalidateImmediately = function () {
                if (this.canvas)
                    this.canvas.renderAll();
                else if (this.group)
                    this.group.invalidate();
            };
            Shape.prototype.getTransformation = function () {
                var matrix = new NxtControl.Drawing.Matrix();
                if (this.hasOwnProperty('group') && this.group !== null)
                    matrix = this.group.getTransformation();
                var centerPoint = this.getCenterPoint();
                matrix.translate(centerPoint.x, centerPoint.y);
                matrix.scale(this.scaleX, this.scaleY);
                return matrix;
            };
            Shape.prototype.getInverseTransformation = function () {
                var matrix = this.getTransformation();
                matrix = matrix.inverse();
                return matrix;
            };
            Shape.prototype.captureMouse = function (object) {
                if (this.canvas && this.canvas instanceof GuiFramework.InteractiveCanvas)
                    this.canvas.captureMouse(object);
                else if (this.group)
                    this.group.captureMouse(object);
            };
            Shape.prototype.getCurrentWidth = function () {
                return this.currentWidth;
            };
            Shape.prototype.getCurrentHeight = function () {
                return this.currentHeight;
            };
            Shape.prototype.getBorderColor = function () {
                return this.borderColor;
            };
            Shape.prototype.setBorderColor = function (value) {
                this.borderColor = value;
                return this;
            };
            Shape.prototype.getCornerSize = function () {
                return this.cornerSize;
            };
            Shape.prototype.setCornerSize = function (value) {
                this.cornerSize = value;
                return this;
            };
            Shape.prototype.getFill = function () {
                return this.brush;
            };
            Shape.prototype.setFill = function (value) {
                this.brush = value;
                return this;
            };
            Shape.prototype.setOptions = function (options) {
                for (var prop in options) {
                    this.set(prop, options[prop]);
                }
                this._initClipping(options);
            };
            Shape.prototype._getLeftTopCoords = function () {
                return this.translateToOriginPoint(this.getCenterPoint(), 'left', 'center');
            };
            Shape.prototype.transform = function (ctx, fromLeft) {
                var center = fromLeft ? this._getLeftTopCoords() : this.getCenterPoint();
                ctx.translate(center.x, center.y);
                ctx.rotate(NxtControl.Util.Convert.degreesToRadians(this.angle));
                ctx.scale(this.scaleX * (this.flipX ? -1 : 1), this.scaleY * (this.flipY ? -1 : 1));
            };
            Shape.prototype._removeDefaultValues = function (object) {
                var prototype = NxtControl.Util.Parser.getKlass(object.type).prototype;
                var stateProperties = prototype.getStateProperties();
                stateProperties.forEach(function (prop) {
                    if (object[prop] === prototype[prop]) {
                        delete object[prop];
                    }
                });
                return object;
            };
            Shape.prototype.toObject = function (propertiesToInclude) {
                var NUM_FRACTION_DIGITS = Shape.NUM_FRACTION_DIGITS;
                var object = {
                    type: this.type,
                    name: this.name,
                    originX: this.originX,
                    originY: this.originY,
                    left: toFixed(this.left, NUM_FRACTION_DIGITS),
                    top: toFixed(this.top, NUM_FRACTION_DIGITS),
                    width: toFixed(this.width, NUM_FRACTION_DIGITS),
                    height: toFixed(this.height, NUM_FRACTION_DIGITS),
                    brush: this.brush,
                    pen: this.pen,
                    scaleX: toFixed(this.scaleX, NUM_FRACTION_DIGITS),
                    scaleY: toFixed(this.scaleY, NUM_FRACTION_DIGITS),
                    angle: toFixed(this.getAngle(), NUM_FRACTION_DIGITS),
                    flipX: this.flipX,
                    flipY: this.flipY,
                    visible: this.visible,
                    drawable: this.drawable,
                    clipTo: this.clipTo && String(this.clipTo),
                };
                if (!this.includeDefaultValues) {
                    object = this._removeDefaultValues(object);
                }
                if (this.anchor !== GuiFramework.AnchorStyles.none)
                    object.anchor = this.anchor;
                if (this.openFaceplates !== null && this.openFaceplates.length !== 0)
                    object.openFaceplates = this.openFaceplates;
                if (this._events_ != null && this._events_.length != 0)
                    object._events_ = this._events_;
                NxtControl.Util.populateWithProperties(this, object, propertiesToInclude);
                return object;
            };
            Shape.prototype.toDatalessObject = function (propertiesToInclude) {
                return this.toObject(propertiesToInclude);
            };
            Shape.prototype.get = function (property) {
                return this[property];
            };
            Shape.prototype.set = function (key, value) {
                if (typeof key === 'string') {
                    if (typeof value === 'function' && key !== 'clipTo') {
                        this._set(key, value(this.get(key)));
                    }
                    else {
                        this._set(key, value);
                    }
                }
                else {
                    for (var prop in key) {
                        this._set(prop, key[prop]);
                    }
                }
                return this;
            };
            Shape.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                var shouldConstrainValue = (key === 'scaleX' || key === 'scaleY');
                if (this[key]) {
                    if (this[key] instanceof NxtControl.Drawing.BlinkColor)
                        NxtControl.Services.ColorService.instance.detach(this[key]);
                    if (this[key] instanceof NxtControl.Drawing.SolidBrush)
                        this[key].detach();
                    if (this[key] instanceof NxtControl.Drawing.GradientBrush)
                        this[key].detach();
                }
                if (shouldConstrainValue) {
                    value = this._constrainScale(value);
                }
                if (key === 'scaleX' && value < 0) {
                    this.flipX = !this.flipX;
                    value *= -1;
                }
                else if (key === 'scaleY' && value < 0) {
                    this.flipY = !this.flipY;
                    value *= -1;
                }
                else if (key === 'width' || key === 'height') {
                    this.minScaleLimit = toFixed(Math.min(0.1, 1 / Math.max(this.width, this.height)), 2);
                }
                if (key == 'pen')
                    value = this._initPen({ pen: value });
                if (key == 'brush')
                    value = this._initBrush({ brush: value });
                this[key] = value;
                if (value && value instanceof NxtControl.Drawing.BlinkColor)
                    NxtControl.Services.ColorService.instance.attach(value);
                if (value && value instanceof NxtControl.Drawing.SolidBrush)
                    this[key].attach();
                if (value && value instanceof NxtControl.Drawing.GradientBrush)
                    this[key].attach();
                invalidate && this.invalidate();
                return this;
            };
            Shape.prototype.toggle = function (property) {
                var value = this.get(property);
                if (typeof value === 'boolean') {
                    this.set(property, !value);
                }
                return this;
            };
            Shape.prototype.setSourcePath = function (value) {
                this.sourcePath = value;
                return this;
            };
            Shape.prototype.render = function (ctx, noTransform) {
                if (this.width === 0 || this.height === 0 || !this.visible)
                    return;
                ctx.save();
                this._transform(ctx, noTransform);
                this._setStrokeStyles(ctx);
                this._setFillStyles(ctx);
                var m = this.transformMatrix;
                if (m && this.group) {
                    ctx.translate(-this.group.width / 2, -this.group.height / 2);
                    ctx.transform(m[0], m[1], m[2], m[3], m[4], m[5]);
                }
                this.clipTo && NxtControl.Util.Clip.clipContext(this, ctx);
                this._render(ctx, noTransform);
                this.clipTo && ctx.restore();
                if (this.active && !noTransform) {
                    this.drawBorders(ctx);
                    this.drawControls(ctx);
                }
                ctx.restore();
            };
            Shape.prototype.renderBordersAndControls = function (ctx) {
                if (this.width === 0 || this.height === 0 || !this.visible)
                    return;
                ctx.save();
                this._transform(ctx);
                var m = this.transformMatrix;
                if (m && this.group) {
                    ctx.translate(-this.group.width / 2, -this.group.height / 2);
                    ctx.transform(m[0], m[1], m[2], m[3], m[4], m[5]);
                }
                this.clipTo && NxtControl.Util.Clip.clipContext(this, ctx);
                this.clipTo && ctx.restore();
                if (this.active) {
                    this.drawBorders(ctx);
                    this.drawControls(ctx);
                }
                ctx.restore();
            };
            Shape.prototype._render = function (ctx, noTransform) {
            };
            Shape.prototype._transform = function (ctx, noTransform) {
                var m = this.transformMatrix;
                if (m && !this.group) {
                    ctx.setTransform(m[0], m[1], m[2], m[3], m[4], m[5]);
                }
                if (!noTransform) {
                    this.transform(ctx, false);
                }
            };
            Shape.prototype._setStrokeStyles = function (ctx) {
                if (this.pen) {
                    ctx.lineWidth = this.pen.width;
                    ctx.lineCap = this.pen.lineCap;
                    ctx.lineJoin = this.pen.lineJoin;
                    ctx.miterLimit = this.pen.miterLimit;
                    ctx.strokeStyle = this.pen.toLive();
                }
            };
            Shape.prototype._setFillStyles = function (ctx) {
                if (this.brush) {
                    ctx.fillStyle = this.brush.toLive(ctx);
                }
            };
            Shape.prototype._renderFill = function (ctx, brush, x, y, w, h) {
                brush = brush || this.brush;
                if (!brush)
                    return;
                if (brush instanceof NxtControl.Drawing.GradientBrush) {
                    if (brush.type == 'radial') {
                        brush.coords.x1 = 0;
                        brush.coords.x2 = 0;
                        brush.coords.y1 = 0;
                        brush.coords.y2 = 0;
                    }
                    else if (brush.orient) {
                        x = x || -this.width / 2;
                        y = y || -this.height / 2;
                        w = w || this.width;
                        h = h || this.height;
                        switch (brush.orient) {
                            case FillOrientation.VerticalTop:
                            case FillOrientation.VerticalBottom:
                            case FillOrientation.VerticalCenter:
                            case FillOrientation.InvertedVerticalCenter:
                                brush.coords.x1 = 0;
                                brush.coords.x2 = 0;
                                brush.coords.y1 = y;
                                brush.coords.y2 = y + h;
                                break;
                            case FillOrientation.HorizontalLeft:
                            case FillOrientation.HorizontalRight:
                            case FillOrientation.HorizontalCenter:
                            case FillOrientation.InvertedHorizontalCenter:
                                brush.coords.x1 = x;
                                brush.coords.x2 = x + w;
                                brush.coords.y1 = 0;
                                brush.coords.y2 = 0;
                                break;
                            case FillOrientation.DiagonalLeftTop:
                            case FillOrientation.Center:
                                brush.coords.x1 = x;
                                brush.coords.x2 = x + w;
                                brush.coords.y1 = y;
                                brush.coords.y2 = y + h;
                                break;
                            case FillOrientation.DiagonalLeftBottom:
                                brush.coords.x1 = x;
                                brush.coords.x2 = x + w;
                                brush.coords.y1 = y + h;
                                brush.coords.y2 = y;
                                break;
                            case FillOrientation.DiagonalRightTop:
                                brush.coords.x1 = x + w;
                                brush.coords.x2 = x;
                                brush.coords.y1 = y;
                                brush.coords.y2 = y + h;
                                break;
                            case FillOrientation.DiagonalRightBottom:
                                brush.coords.x1 = x + w;
                                brush.coords.x2 = x;
                                brush.coords.y1 = y + h;
                                brush.coords.y2 = y;
                                break;
                        }
                    }
                    ctx.save();
                    ctx.translate(-this.width / 2 + brush.offsetX || 0, -this.height / 2 + brush.offsetY || 0);
                }
                ctx.fillStyle = brush.toLive(ctx);
                ctx.fill();
                if (brush instanceof NxtControl.Drawing.GradientBrush) {
                    ctx.restore();
                }
            };
            Shape.prototype._stroke = function (ctx) { };
            Shape.prototype._renderDashedStroke = function (ctx) { };
            Shape.prototype._renderStroke = function (ctx, pen) {
                pen = pen || this.pen;
                if (!pen)
                    return;
                ctx.save();
                if (pen.dashArray) {
                    if (1 & pen.dashArray.length) {
                        pen.dashArray.push.apply(pen.dashArray, pen.dashArray);
                    }
                    if (GuiFramework.StaticCanvas.supports('setLineDash')) {
                        ctx.setLineDash(pen.dashArray);
                        this._stroke && this._stroke(ctx);
                    }
                    else {
                        this._renderDashedStroke(ctx);
                    }
                    ctx.stroke();
                }
                else {
                    ctx.translate(-this.width / 2 + pen.offsetX || 0, -this.height / 2 + pen.offsetY || 0);
                    ctx.strokeStyle = pen.toLive();
                    ctx.stroke();
                }
                ctx.restore();
            };
            Shape.prototype.clone = function (callback, propertiesToInclude) {
                if (this.constructor.fromObject) {
                    return this.constructor.fromObject(this.toObject(propertiesToInclude), callback);
                }
                return null;
            };
            Shape.prototype.isType = function (type) {
                return this.type === type;
            };
            Shape.prototype.toJSON = function (propertiesToInclude) {
                return this.toObject(propertiesToInclude);
            };
            Shape.prototype.centerH = function () {
                this.canvas.centerObjectH(this);
                return this;
            };
            Shape.prototype.centerV = function () {
                this.canvas.centerObjectV(this);
                return this;
            };
            Shape.prototype.center = function () {
                this.canvas.centerObject(this);
                return this;
            };
            Shape.prototype.remove = function () {
                return this.canvas.remove(this);
            };
            Shape.prototype.getLocalPointer = function (e, pointer) {
                pointer = pointer || this.canvas.getPointer(e);
                var objectLeftTop = this.translateToOriginPoint(this.getCenterPoint(), 'left', 'top');
                return {
                    x: pointer.x - objectLeftTop.x,
                    y: pointer.y - objectLeftTop.y
                };
            };
            Shape.prototype.getStateProperties = function () {
                var props = [];
                var prototype = NxtControl.Util.Parser.getKlass(this.type).prototype;
                while (true) {
                    if (!prototype)
                        break;
                    if (prototype.hasOwnProperty('stateProperties'))
                        props = props.concat(prototype.stateProperties);
                    prototype = Object.getPrototypeOf(prototype);
                }
                return props;
            };
            Shape.prototype.hasStateChanged = function () {
                return this.getStateProperties().some(function (prop) {
                    return this.get(prop) !== this.originalState[prop];
                }, this);
            };
            Shape.prototype.saveState = function (options) {
                this.getStateProperties().forEach(function (prop) {
                    this.originalState[prop] = this.get(prop);
                }, this);
                if (options && options.stateProperties) {
                    options.stateProperties.forEach(function (prop) {
                        this.originalState[prop] = this.get(prop);
                    }, this);
                }
                return this;
            };
            Shape.prototype.setupState = function () {
                this.originalState = {};
                this.saveState();
                return this;
            };
            Shape.prototype._getAngleValueForStraighten = function () {
                var angle = this.getAngle() % 360;
                if (angle > 0) {
                    return Math.round((angle - 1) / 90) * 90;
                }
                return Math.round(angle / 90) * 90;
            };
            Shape.prototype.straighten = function () {
                this.setAngle(this._getAngleValueForStraighten());
                return this;
            };
            Shape.prototype.bringForward = function (intersecting) {
                if (this.group) {
                    GuiFramework.StaticCanvas.prototype.bringForward.call(this.group, this, intersecting);
                }
                else {
                    this.canvas.bringForward(this, intersecting);
                }
                return this;
            };
            Shape.prototype.bringToFront = function () {
                if (this.group) {
                    GuiFramework.StaticCanvas.prototype.bringToFront.call(this.group, this);
                }
                else {
                    this.canvas.bringToFront(this);
                }
                return this;
            };
            Shape.prototype.sendBackwards = function (intersecting) {
                if (this.group) {
                    GuiFramework.StaticCanvas.prototype.sendBackwards.call(this.group, this, intersecting);
                }
                else {
                    this.canvas.sendBackwards(this, intersecting);
                }
                return this;
            };
            Shape.prototype.sendToBack = function () {
                if (this.group) {
                    GuiFramework.StaticCanvas.prototype.sendToBack.call(this.group, this);
                }
                else {
                    this.canvas.sendToBack(this);
                }
                return this;
            };
            Shape.prototype.moveTo = function (index) {
                if (this.group) {
                    GuiFramework.StaticCanvas.prototype.moveTo.call(this.group, this, index);
                }
                else {
                    this.canvas.moveTo(this, index);
                }
                return this;
            };
            Shape.prototype.translateToCenterPoint = function (point, originX, originY) {
                var cx = point.x, cy = point.y, strokeWidth = this.pen ? this.pen.width : 0;
                if (originX === "left") {
                    cx = point.x + (this.getWidth() + strokeWidth * this.scaleX) / 2;
                }
                else if (originX === "right") {
                    cx = point.x - (this.getWidth() + strokeWidth * this.scaleX) / 2;
                }
                if (originY === "top") {
                    cy = point.y + (this.getHeight() + strokeWidth * this.scaleY) / 2;
                }
                else if (originY === "bottom") {
                    cy = point.y - (this.getHeight() + strokeWidth * this.scaleY) / 2;
                }
                return new NxtControl.Drawing.Point(cx, cy).rotatePoint(point, NxtControl.Util.Convert.degreesToRadians(this.angle));
            };
            Shape.prototype.translateToOriginPoint = function (center, originX, originY) {
                var x = center.x, y = center.y, strokeWidth = this.pen ? this.pen.width : 0;
                if (originX === "left") {
                    x = center.x - (this.getWidth() + strokeWidth * this.scaleX) / 2;
                }
                else if (originX === "right") {
                    x = center.x + (this.getWidth() + strokeWidth * this.scaleX) / 2;
                }
                if (originY === "top") {
                    y = center.y - (this.getHeight() + strokeWidth * this.scaleY) / 2;
                }
                else if (originY === "bottom") {
                    y = center.y + (this.getHeight() + strokeWidth * this.scaleY) / 2;
                }
                return new NxtControl.Drawing.Point(x, y).rotatePoint(center, NxtControl.Util.Convert.degreesToRadians(this.angle));
            };
            Shape.prototype.getCenterPoint = function () {
                var leftTop = new NxtControl.Drawing.Point(this.left, this.top);
                return this.translateToCenterPoint(leftTop, this.originX, this.originY);
            };
            Shape.prototype.getPointByOrigin = function (originX, originY) {
                var center = this.getCenterPoint();
                return this.translateToOriginPoint(center, originX, originY);
            };
            Shape.prototype.toLocalPoint = function (point, originX, originY) {
                var center = this.getCenterPoint(), strokeWidth = this.pen ? this.pen.width : 0, x, y;
                if (originX && originY) {
                    if (originX === "left") {
                        x = center.x - (this.getWidth() + strokeWidth * this.scaleX) / 2;
                    }
                    else if (originX === "right") {
                        x = center.x + (this.getWidth() + strokeWidth * this.scaleX) / 2;
                    }
                    else {
                        x = center.x;
                    }
                    if (originY === "top") {
                        y = center.y - (this.getHeight() + strokeWidth * this.scaleY) / 2;
                    }
                    else if (originY === "bottom") {
                        y = center.y + (this.getHeight() + strokeWidth * this.scaleY) / 2;
                    }
                    else {
                        y = center.y;
                    }
                }
                else {
                    x = this.left;
                    y = this.top;
                }
                return new NxtControl.Drawing.Point(point.x, point.y).rotatePoint(center, -NxtControl.Util.Convert.degreesToRadians(this.angle)).subtractEquals(new NxtControl.Drawing.Point(x, y));
            };
            Shape.prototype.setPositionByOrigin = function (pos, originX, originY) {
                var center = this.translateToCenterPoint(pos, originX, originY);
                var position = this.translateToOriginPoint(center, this.originX, this.originY);
                this.set('left', position.x);
                this.set('top', position.y);
            };
            Shape.prototype.adjustPosition = function (to) {
                var angle = NxtControl.Util.Convert.degreesToRadians(this.angle);
                var hypotHalf = this.getWidth() / 2;
                var xHalf = Math.cos(angle) * hypotHalf;
                var yHalf = Math.sin(angle) * hypotHalf;
                var hypotFull = this.getWidth();
                var xFull = Math.cos(angle) * hypotFull;
                var yFull = Math.sin(angle) * hypotFull;
                if (this.originX === 'center' && to === 'left' ||
                    this.originX === 'right' && to === 'center') {
                    this.left -= xHalf;
                    this.top -= yHalf;
                }
                else if (this.originX === 'left' && to === 'center' ||
                    this.originX === 'center' && to === 'right') {
                    this.left += xHalf;
                    this.top += yHalf;
                }
                else if (this.originX === 'left' && to === 'right') {
                    this.left += xFull;
                    this.top += yFull;
                }
                else if (this.originX === 'right' && to === 'left') {
                    this.left -= xFull;
                    this.top -= yFull;
                }
                this.setCoords();
                this.originX = to;
            };
            Shape.prototype.findTargetCorner = function (e, offset) {
                if (!this.hasControls || !this.active)
                    return null;
                var pointer = NxtControl.Util.getPointer(e, this.canvas.upperCanvasEl), ex = pointer.x - offset.left, ey = pointer.y - offset.top, xPoints, lines;
                for (var i in this.oCoords) {
                    if (!this.isControlVisible(i)) {
                        continue;
                    }
                    if (i === 'mtr' && !this.hasRotatingPoint) {
                        continue;
                    }
                    if (this.get('lockUniScaling') && (i === 'mt' || i === 'mr' || i === 'mb' || i === 'ml')) {
                        continue;
                    }
                    lines = this._getImageLines(this.oCoords[i].corner);
                    xPoints = this._findCrossPoints({ x: ex, y: ey }, lines);
                    if (xPoints !== 0 && xPoints % 2 === 1) {
                        return i;
                    }
                }
                return null;
            };
            Shape.prototype._setCornerCoords = function () {
                var coords = this.oCoords, theta = NxtControl.Util.Convert.degreesToRadians(this.angle), newTheta = NxtControl.Util.Convert.degreesToRadians(45 - this.angle), cornerHypotenuse = Math.sqrt(2 * Math.pow(this.cornerSize, 2)) / 2, cosHalfOffset = cornerHypotenuse * Math.cos(newTheta), sinHalfOffset = cornerHypotenuse * Math.sin(newTheta), sinTh = Math.sin(theta), cosTh = Math.cos(theta);
                coords.tl.corner = {
                    tl: {
                        x: coords.tl.x - sinHalfOffset,
                        y: coords.tl.y - cosHalfOffset
                    },
                    tr: {
                        x: coords.tl.x + cosHalfOffset,
                        y: coords.tl.y - sinHalfOffset
                    },
                    bl: {
                        x: coords.tl.x - cosHalfOffset,
                        y: coords.tl.y + sinHalfOffset
                    },
                    br: {
                        x: coords.tl.x + sinHalfOffset,
                        y: coords.tl.y + cosHalfOffset
                    }
                };
                coords.tr.corner = {
                    tl: {
                        x: coords.tr.x - sinHalfOffset,
                        y: coords.tr.y - cosHalfOffset
                    },
                    tr: {
                        x: coords.tr.x + cosHalfOffset,
                        y: coords.tr.y - sinHalfOffset
                    },
                    br: {
                        x: coords.tr.x + sinHalfOffset,
                        y: coords.tr.y + cosHalfOffset
                    },
                    bl: {
                        x: coords.tr.x - cosHalfOffset,
                        y: coords.tr.y + sinHalfOffset
                    }
                };
                coords.bl.corner = {
                    tl: {
                        x: coords.bl.x - sinHalfOffset,
                        y: coords.bl.y - cosHalfOffset
                    },
                    bl: {
                        x: coords.bl.x - cosHalfOffset,
                        y: coords.bl.y + sinHalfOffset
                    },
                    br: {
                        x: coords.bl.x + sinHalfOffset,
                        y: coords.bl.y + cosHalfOffset
                    },
                    tr: {
                        x: coords.bl.x + cosHalfOffset,
                        y: coords.bl.y - sinHalfOffset
                    }
                };
                coords.br.corner = {
                    tr: {
                        x: coords.br.x + cosHalfOffset,
                        y: coords.br.y - sinHalfOffset
                    },
                    bl: {
                        x: coords.br.x - cosHalfOffset,
                        y: coords.br.y + sinHalfOffset
                    },
                    br: {
                        x: coords.br.x + sinHalfOffset,
                        y: coords.br.y + cosHalfOffset
                    },
                    tl: {
                        x: coords.br.x - sinHalfOffset,
                        y: coords.br.y - cosHalfOffset
                    }
                };
                coords.ml.corner = {
                    tl: {
                        x: coords.ml.x - sinHalfOffset,
                        y: coords.ml.y - cosHalfOffset
                    },
                    tr: {
                        x: coords.ml.x + cosHalfOffset,
                        y: coords.ml.y - sinHalfOffset
                    },
                    bl: {
                        x: coords.ml.x - cosHalfOffset,
                        y: coords.ml.y + sinHalfOffset
                    },
                    br: {
                        x: coords.ml.x + sinHalfOffset,
                        y: coords.ml.y + cosHalfOffset
                    }
                };
                coords.mt.corner = {
                    tl: {
                        x: coords.mt.x - sinHalfOffset,
                        y: coords.mt.y - cosHalfOffset
                    },
                    tr: {
                        x: coords.mt.x + cosHalfOffset,
                        y: coords.mt.y - sinHalfOffset
                    },
                    bl: {
                        x: coords.mt.x - cosHalfOffset,
                        y: coords.mt.y + sinHalfOffset
                    },
                    br: {
                        x: coords.mt.x + sinHalfOffset,
                        y: coords.mt.y + cosHalfOffset
                    }
                };
                coords.mr.corner = {
                    tl: {
                        x: coords.mr.x - sinHalfOffset,
                        y: coords.mr.y - cosHalfOffset
                    },
                    tr: {
                        x: coords.mr.x + cosHalfOffset,
                        y: coords.mr.y - sinHalfOffset
                    },
                    bl: {
                        x: coords.mr.x - cosHalfOffset,
                        y: coords.mr.y + sinHalfOffset
                    },
                    br: {
                        x: coords.mr.x + sinHalfOffset,
                        y: coords.mr.y + cosHalfOffset
                    }
                };
                coords.mb.corner = {
                    tl: {
                        x: coords.mb.x - sinHalfOffset,
                        y: coords.mb.y - cosHalfOffset
                    },
                    tr: {
                        x: coords.mb.x + cosHalfOffset,
                        y: coords.mb.y - sinHalfOffset
                    },
                    bl: {
                        x: coords.mb.x - cosHalfOffset,
                        y: coords.mb.y + sinHalfOffset
                    },
                    br: {
                        x: coords.mb.x + sinHalfOffset,
                        y: coords.mb.y + cosHalfOffset
                    }
                };
                coords.mtr.corner = {
                    tl: {
                        x: coords.mtr.x - sinHalfOffset + (sinTh * this.rotatingPointOffset),
                        y: coords.mtr.y - cosHalfOffset - (cosTh * this.rotatingPointOffset)
                    },
                    tr: {
                        x: coords.mtr.x + cosHalfOffset + (sinTh * this.rotatingPointOffset),
                        y: coords.mtr.y - sinHalfOffset - (cosTh * this.rotatingPointOffset)
                    },
                    bl: {
                        x: coords.mtr.x - cosHalfOffset + (sinTh * this.rotatingPointOffset),
                        y: coords.mtr.y + sinHalfOffset - (cosTh * this.rotatingPointOffset)
                    },
                    br: {
                        x: coords.mtr.x + sinHalfOffset + (sinTh * this.rotatingPointOffset),
                        y: coords.mtr.y + cosHalfOffset - (cosTh * this.rotatingPointOffset)
                    }
                };
            };
            Shape.prototype.drawBorders = function (ctx) {
                if (!this.hasBorders)
                    return this;
                var padding = this.padding, padding2 = padding * 2, strokeWidth = this.pen ? ~~(this.pen.width / 2) * 2 : 0;
                ctx.save();
                ctx.globalAlpha = this.isMoving ? this.borderOpacityWhenMoving : 1;
                ctx.strokeStyle = this.borderColor ? this.borderColor.toLive() : NxtControl.Drawing.Color.Black.toLive();
                var scaleX = 1 / this._constrainScale(this.scaleX), scaleY = 1 / this._constrainScale(this.scaleY);
                ctx.lineWidth = 1 / this.borderScaleFactor;
                ctx.scale(scaleX, scaleY);
                var w = this.getWidth(), h = this.getHeight();
                ctx.strokeRect(~~(-(w / 2) - padding - strokeWidth / 2 * this.scaleX) - 0.5, ~~(-(h / 2) - padding - strokeWidth / 2 * this.scaleY) - 0.5, ~~(w + padding2 + strokeWidth * this.scaleX) + 1, ~~(h + padding2 + strokeWidth * this.scaleY) + 1);
                if (this.hasRotatingPoint && this.isControlVisible('mtr') && !this.lockRotation && this.hasControls) {
                    var rotateHeight = (this.flipY
                        ? h + (strokeWidth * this.scaleY) + (padding * 2)
                        : -h - (strokeWidth * this.scaleY) - (padding * 2)) / 2;
                    ctx.beginPath();
                    ctx.moveTo(0, rotateHeight);
                    ctx.lineTo(0, rotateHeight + (this.flipY ? this.rotatingPointOffset : -this.rotatingPointOffset));
                    ctx.closePath();
                    ctx.stroke();
                }
                ctx.restore();
                return this;
            };
            Shape.prototype._getControlsVisibility = function () {
                if (!this._controlsVisibility) {
                    this._controlsVisibility = {
                        tl: true,
                        tr: true,
                        br: true,
                        bl: true,
                        ml: true,
                        mt: true,
                        mr: true,
                        mb: true,
                        mtr: true
                    };
                }
                return this._controlsVisibility;
            };
            Shape.prototype.drawControls = function (ctx) {
                if (!this.hasControls)
                    return this;
                var size = this.cornerSize, size2 = size / 2, strokeWidth2 = this.pen ? ~~(this.pen.width / 2) : 0, left = -(this.width / 2), top = -(this.height / 2), paddingX = this.padding / this.scaleX, paddingY = this.padding / this.scaleY, scaleOffsetY = size2 / this.scaleY, scaleOffsetX = size2 / this.scaleX, scaleOffsetSizeX = (size2 - size) / this.scaleX, scaleOffsetSizeY = (size2 - size) / this.scaleY, height = this.height, width = this.width, methodName = this.transparentCorners ? 'strokeRect' : 'fillRect';
                ctx.save();
                ctx.lineWidth = 1 / Math.max(this.scaleX, this.scaleY);
                ctx.globalAlpha = this.isMoving ? this.borderOpacityWhenMoving : 1;
                ctx.strokeStyle = ctx.fillStyle = this.cornerColor ? this.cornerColor.toLive() : NxtControl.Drawing.Color.Black.toLive();
                ;
                this._drawControl('tl', ctx, methodName, left - scaleOffsetX - strokeWidth2 - paddingX, top - scaleOffsetY - strokeWidth2 - paddingY);
                this._drawControl('tr', ctx, methodName, left + width - scaleOffsetX + strokeWidth2 + paddingX, top - scaleOffsetY - strokeWidth2 - paddingY);
                this._drawControl('tr', ctx, methodName, left - scaleOffsetX - strokeWidth2 - paddingX, top + height + scaleOffsetSizeY + strokeWidth2 + paddingY);
                this._drawControl('br', ctx, methodName, left + width + scaleOffsetSizeX + strokeWidth2 + paddingX, top + height + scaleOffsetSizeY + strokeWidth2 + paddingY);
                if (!this.get('lockUniScaling')) {
                    this._drawControl('mt', ctx, methodName, left + width / 2 - scaleOffsetX, top - scaleOffsetY - strokeWidth2 - paddingY);
                    this._drawControl('mb', ctx, methodName, left + width / 2 - scaleOffsetX, top + height + scaleOffsetSizeY + strokeWidth2 + paddingY);
                    this._drawControl('mb', ctx, methodName, left + width + scaleOffsetSizeX + strokeWidth2 + paddingX, top + height / 2 - scaleOffsetY);
                    this._drawControl('ml', ctx, methodName, left - scaleOffsetX - strokeWidth2 - paddingX, top + height / 2 - scaleOffsetY);
                }
                if (this.hasRotatingPoint) {
                    this._drawControl('mtr', ctx, methodName, left + width / 2 - scaleOffsetX, this.flipY
                        ? (top + height + (this.rotatingPointOffset / this.scaleY) - this.cornerSize / this.scaleX / 2 + strokeWidth2 + paddingY)
                        : (top - (this.rotatingPointOffset / this.scaleY) - this.cornerSize / this.scaleY / 2 - strokeWidth2 - paddingY));
                }
                ctx.restore();
                return this;
            };
            Shape.prototype._drawControl = function (control, ctx, methodName, left, top) {
                var sizeX = this.cornerSize / this.scaleX, sizeY = this.cornerSize / this.scaleY;
                if (this.isControlVisible(control)) {
                    this.transparentCorners && ctx.clearRect(left, top, sizeX, sizeY);
                    ctx[methodName](left, top, sizeX, sizeY);
                }
            };
            Shape.prototype.isControlVisible = function (controlName) {
                return this._getControlsVisibility()[controlName];
            };
            Shape.prototype.setControlVisible = function (controlName, visible) {
                this._getControlsVisibility()[controlName] = visible;
                return this;
            };
            Shape.prototype.setControlsVisibility = function (options) {
                options || (options = {});
                for (var p in options) {
                    this.setControlVisible(p, options[p]);
                }
                return this;
            };
            Shape.prototype.setCoords = function () {
                var strokeWidth = (this.pen && this.pen.width > 1) ? this.pen.width : 0, padding = this.padding, theta = NxtControl.Util.Convert.degreesToRadians(this.angle);
                this.currentWidth = (this.width + strokeWidth) * this.scaleX + padding * 2;
                this.currentHeight = (this.height + strokeWidth) * this.scaleY + padding * 2;
                if (this.currentWidth < 0) {
                    this.currentWidth = Math.abs(this.currentWidth);
                }
                var _hypotenuse = Math.sqrt(Math.pow(this.currentWidth / 2, 2) +
                    Math.pow(this.currentHeight / 2, 2));
                var _angle = Math.atan(isFinite(this.currentHeight / this.currentWidth) ? this.currentHeight / this.currentWidth : 0);
                var offsetX = Math.cos(_angle + theta) * _hypotenuse, offsetY = Math.sin(_angle + theta) * _hypotenuse, sinTh = Math.sin(theta), cosTh = Math.cos(theta);
                var coords = this.getCenterPoint();
                var tl = {
                    x: coords.x - offsetX,
                    y: coords.y - offsetY
                };
                var tr = {
                    x: tl.x + (this.currentWidth * cosTh),
                    y: tl.y + (this.currentWidth * sinTh)
                };
                var br = {
                    x: tr.x - (this.currentHeight * sinTh),
                    y: tr.y + (this.currentHeight * cosTh)
                };
                var bl = {
                    x: tl.x - (this.currentHeight * sinTh),
                    y: tl.y + (this.currentHeight * cosTh)
                };
                var ml = {
                    x: tl.x - (this.currentHeight / 2 * sinTh),
                    y: tl.y + (this.currentHeight / 2 * cosTh)
                };
                var mt = {
                    x: tl.x + (this.currentWidth / 2 * cosTh),
                    y: tl.y + (this.currentWidth / 2 * sinTh)
                };
                var mr = {
                    x: tr.x - (this.currentHeight / 2 * sinTh),
                    y: tr.y + (this.currentHeight / 2 * cosTh)
                };
                var mb = {
                    x: bl.x + (this.currentWidth / 2 * cosTh),
                    y: bl.y + (this.currentWidth / 2 * sinTh)
                };
                var mtr = {
                    x: mt.x,
                    y: mt.y
                };
                this.oCoords = {
                    tl: tl, tr: tr, br: br, bl: bl,
                    ml: ml, mt: mt, mr: mr, mb: mb,
                    mtr: mtr
                };
                this._setCornerCoords && this._setCornerCoords();
                return this;
            };
            Shape.prototype.getBoundingRect = function () {
                this.oCoords || this.setCoords();
                var xCoords = [this.oCoords.tl.x, this.oCoords.tr.x, this.oCoords.br.x, this.oCoords.bl.x];
                var minX = Math.min.apply(Math, xCoords);
                var maxX = Math.max.apply(Math, xCoords);
                var width = Math.abs(minX - maxX);
                var yCoords = [this.oCoords.tl.y, this.oCoords.tr.y, this.oCoords.br.y, this.oCoords.bl.y];
                var minY = Math.min.apply(Math, yCoords);
                var maxY = Math.max.apply(Math, yCoords);
                var height = Math.abs(minY - maxY);
                return {
                    left: minX,
                    top: minY,
                    width: width,
                    height: height
                };
            };
            Shape.prototype.isContainedWithinObject = function (other) {
                var boundingRect = other.getBoundingRect(), point1 = new NxtControl.Drawing.Point(boundingRect.left, boundingRect.top), point2 = new NxtControl.Drawing.Point(boundingRect.left + boundingRect.width, boundingRect.top + boundingRect.height);
                return this.isContainedWithinRect(point1, point2);
            };
            Shape.prototype.isContainedWithinRect = function (pointTL, pointBR) {
                var boundingRect = this.getBoundingRect();
                return (boundingRect.left > pointTL.x &&
                    boundingRect.left + boundingRect.width < pointBR.x &&
                    boundingRect.top > pointTL.y &&
                    boundingRect.top + boundingRect.height < pointBR.y);
            };
            Shape.prototype._getImageLines = function (oCoords) {
                return {
                    topline: {
                        o: oCoords.tl,
                        d: oCoords.tr
                    },
                    rightline: {
                        o: oCoords.tr,
                        d: oCoords.br
                    },
                    bottomline: {
                        o: oCoords.br,
                        d: oCoords.bl
                    },
                    leftline: {
                        o: oCoords.bl,
                        d: oCoords.tl
                    }
                };
            };
            Shape.prototype._findCrossPoints = function (point, oCoords) {
                var b1, b2, a1, a2, xi, yi, xcount = 0, iLine;
                for (var lineKey in oCoords) {
                    iLine = oCoords[lineKey];
                    if ((iLine.o.y < point.y) && (iLine.d.y < point.y)) {
                        continue;
                    }
                    if ((iLine.o.y >= point.y) && (iLine.d.y >= point.y)) {
                        continue;
                    }
                    if ((iLine.o.x === iLine.d.x) && (iLine.o.x >= point.x)) {
                        xi = iLine.o.x;
                        yi = point.y;
                    }
                    else {
                        b1 = 0;
                        b2 = (iLine.d.y - iLine.o.y) / (iLine.d.x - iLine.o.x);
                        a1 = point.y - b1 * point.x;
                        a2 = iLine.o.y - b2 * iLine.o.x;
                        xi = -(a1 - a2) / (b1 - b2);
                        yi = a1 + b1 * xi;
                    }
                    if (xi >= point.x) {
                        xcount += 1;
                    }
                    if (xcount === 2) {
                        break;
                    }
                }
                return xcount;
            };
            Shape.prototype.containsPoint = function (point, touchPoints) {
                if (touchPoints === void 0) { touchPoints = null; }
                var lines = this._getImageLines(this.oCoords), xPoints = this._findCrossPoints(point, lines);
                if (xPoints !== 0 && xPoints % 2 === 1)
                    return true;
                if (touchPoints != null) {
                    for (var i = 0; i < touchPoints.length; i++) {
                        xPoints = this._findCrossPoints(touchPoints[i], lines);
                        if (xPoints !== 0 && xPoints % 2 === 1)
                            return true;
                    }
                }
                return false;
            };
            Shape.prototype._constrainScale = function (value) {
                if (Math.abs(value) < this.minScaleLimit) {
                    if (value < 0)
                        return -this.minScaleLimit;
                    else
                        return this.minScaleLimit;
                }
                return value;
            };
            Shape.prototype.scale = function (value) {
                value = this._constrainScale(value);
                if (value < 0) {
                    this.flipX = !this.flipX;
                    this.flipY = !this.flipY;
                    value *= -1;
                }
                this.scaleX = value;
                this.scaleY = value;
                this.setCoords();
                return this;
            };
            Shape.prototype.scaleToHeight = function (value) {
                var boundingRectFactor = this.getBoundingRect().height / this.getHeight();
                return this.scale(value / this.height / boundingRectFactor);
            };
            Shape.prototype.scaleToWidth = function (value) {
                var boundingRectFactor = this.getBoundingRect().width / this.getWidth();
                return this.scale(value / this.width / boundingRectFactor);
            };
            Shape.prototype.intersectsWithObject = function (other) {
                function getCoords(oCoords) {
                    return {
                        tl: new NxtControl.Drawing.Point(oCoords.tl.x, oCoords.tl.y),
                        tr: new NxtControl.Drawing.Point(oCoords.tr.x, oCoords.tr.y),
                        bl: new NxtControl.Drawing.Point(oCoords.bl.x, oCoords.bl.y),
                        br: new NxtControl.Drawing.Point(oCoords.br.x, oCoords.br.y)
                    };
                }
                var thisCoords = getCoords(this.oCoords), otherCoords = getCoords(other.oCoords);
                var intersection = NxtControl.Util.Intersection.intersectPolygonPolygon([thisCoords.tl, thisCoords.tr, thisCoords.br, thisCoords.bl], [otherCoords.tl, otherCoords.tr, otherCoords.br, otherCoords.bl]);
                return intersection.status === 'Intersection';
            };
            Shape.prototype.intersectsWithRect = function (pointTL, pointBR) {
                var oCoords = this.oCoords, tl = new NxtControl.Drawing.Point(oCoords.tl.x, oCoords.tl.y), tr = new NxtControl.Drawing.Point(oCoords.tr.x, oCoords.tr.y), bl = new NxtControl.Drawing.Point(oCoords.bl.x, oCoords.bl.y), br = new NxtControl.Drawing.Point(oCoords.br.x, oCoords.br.y);
                var intersection = NxtControl.Util.Intersection.intersectPolygonRectangle([tl, tr, br, bl], pointTL, pointBR);
                return intersection.status === 'Intersection';
            };
            Shape.prototype._initPen = function (options, propName) {
                var pn = propName || 'pen';
                if (options.hasOwnProperty(pn)) {
                    var value = options[pn];
                    return NxtControl.Drawing.Pen.fromObject(value);
                }
                return null;
            };
            Shape.prototype._initBrush = function (options, propName) {
                var pn = propName || 'brush';
                if (options.hasOwnProperty(pn)) {
                    var value = options[pn];
                    return NxtControl.Drawing.Brush.fromObject(value);
                }
                return null;
            };
            Shape.prototype._initClipping = function (options) {
                if (!options.clipTo || typeof options.clipTo !== 'string')
                    return;
                var functionBody = NxtControl.Util.getFunctionBody(options.clipTo);
                if (typeof functionBody !== 'undefined') {
                    this.clipTo = new Function('ctx', functionBody);
                }
            };
            Shape.NUM_FRACTION_DIGITS = 2;
            __decorate([
                System.DefaultValue('shape')
            ], Shape.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('left'),
                System.Browsable
            ], Shape.prototype, "originX", void 0);
            __decorate([
                System.DefaultValue('top'),
                System.Browsable
            ], Shape.prototype, "originY", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], Shape.prototype, "top", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], Shape.prototype, "left", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], Shape.prototype, "width", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], Shape.prototype, "height", void 0);
            __decorate([
                System.DefaultValue(1),
                System.Browsable
            ], Shape.prototype, "scaleX", void 0);
            __decorate([
                System.DefaultValue(1),
                System.Browsable
            ], Shape.prototype, "scaleY", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], Shape.prototype, "flipX", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], Shape.prototype, "flipY", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], Shape.prototype, "angle", void 0);
            __decorate([
                System.DefaultValue(GuiFramework.AnchorStyles.none),
                System.Browsable
            ], Shape.prototype, "anchor", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], Shape.prototype, "openFaceplates", void 0);
            __decorate([
                System.DefaultValue(12)
            ], Shape.prototype, "cornerSize", void 0);
            __decorate([
                System.DefaultValue(true)
            ], Shape.prototype, "transparentCorners", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Shape.prototype, "hoverCursor", void 0);
            __decorate([
                System.DefaultValue(0)
            ], Shape.prototype, "padding", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Color({ rgba: [102, 153, 255, 0.75] }))
            ], Shape.prototype, "borderColor", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Color({ rgba: [102, 153, 255, 0.5] }))
            ], Shape.prototype, "cornerColor", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "centeredScaling", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "centeredRotation", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Brush.Black),
                System.Browsable
            ], Shape.prototype, "brush", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Pen.White),
                System.Browsable
            ], Shape.prototype, "pen", void 0);
            __decorate([
                System.DefaultValue(0.4)
            ], Shape.prototype, "borderOpacityWhenMoving", void 0);
            __decorate([
                System.DefaultValue(1)
            ], Shape.prototype, "borderScaleFactor", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], Shape.prototype, "transformMatrix", void 0);
            __decorate([
                System.DefaultValue(0.01)
            ], Shape.prototype, "minScaleLimit", void 0);
            __decorate([
                System.DefaultValue(true)
            ], Shape.prototype, "selectable", void 0);
            __decorate([
                System.DefaultValue(true)
            ], Shape.prototype, "evented", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Shape.prototype, "drawable", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Shape.prototype, "visible", void 0);
            __decorate([
                System.DefaultValue(true)
            ], Shape.prototype, "hasControls", void 0);
            __decorate([
                System.DefaultValue(true)
            ], Shape.prototype, "hasBorders", void 0);
            __decorate([
                System.DefaultValue(true)
            ], Shape.prototype, "hasRotatingPoint", void 0);
            __decorate([
                System.DefaultValue(40)
            ], Shape.prototype, "rotatingPointOffset", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "perPixelTargetFind", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "includeDefaultValues", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], Shape.prototype, "clipTo", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], Shape.prototype, "_events_", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "lockMovementX", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "lockMovementY", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "lockRotation", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "lockScalingX", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "lockScalingY", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "lockUniScaling", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], Shape.prototype, "name", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Shape.prototype, "active", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Shape.prototype, "group", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Shape.prototype, "canvas", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Shape.prototype, "designer", void 0);
            return Shape;
        }());
        GuiFramework.Shape = Shape;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var invoke = NxtControl.Util.array.invoke, extend = NxtControl.Util.object.extend;
        var _lockProperties = {
            lockMovementX: true,
            lockMovementY: true,
            lockRotation: true,
            lockScalingX: true,
            lockScalingY: true,
            lockUniScaling: true
        };
        var Group = (function (_super) {
            __extends(Group, _super);
            function Group() {
                _super.call(this);
                this.renderOnAddRemove = false;
                this.delegatedProperties = {
                    brush: true,
                    fontFamily: true,
                    fontWeight: true,
                    fontSize: true,
                    fontStyle: true,
                    lineHeight: true,
                    textDecoration: true,
                    textAlign: true,
                    backgroundColor: true
                };
            }
            Group.prototype.load = function (options, objects) {
                options = options || {};
                this._objects = objects || [];
                for (var i = this._objects.length; i--;) {
                    this._objects[i].group = this;
                }
                this.originalState = {};
                this._calcBounds();
                this._updateObjectsCoords();
                if (options) {
                    extend(this, options);
                }
                this._setOpacityIfSame();
                this.setCoords();
                this.saveCoords();
                return this;
            };
            Group.prototype.clear = function () { this._objects = []; };
            Group.prototype.add = function () {
                var shapes = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    shapes[_i - 0] = arguments[_i];
                }
                this._objects.push.apply(this._objects, shapes);
                for (var i = shapes.length; i--;) {
                    this._onObjectAdded(shapes[i]);
                }
                this.renderOnAddRemove && this.canvas.renderAll();
                return this;
            };
            Group.prototype.insertAt = function (object, index, nonSplicing) {
                var objects = this.getObjects();
                if (nonSplicing) {
                    objects[index] = object;
                }
                else {
                    objects.splice(index, 0, object);
                }
                this._onObjectAdded(object);
                this.renderOnAddRemove && this.canvas.renderAll();
                return this;
            };
            Group.prototype.remove = function (object) {
                if (typeof object === 'undefined')
                    return _super.prototype.remove.call(this);
                var objects = this.getObjects(), index = objects.indexOf(object);
                if (index !== -1) {
                    objects.splice(index, 1);
                    this._onObjectRemoved(object);
                }
                this.renderOnAddRemove && this.canvas.renderAll();
                return object;
            };
            Group.prototype.forEachObject = function (callback, context) {
                var objects = this.getObjects(), i = objects.length;
                while (i--) {
                    callback.call(context, objects[i], i, objects);
                }
                return this;
            };
            Group.prototype.getObjects = function (type) {
                if (typeof type === 'undefined') {
                    return this._objects;
                }
                return this._objects.filter(function (o) {
                    return o.getType() === type;
                });
            };
            Group.prototype.item = function (index) {
                return this.getObjects()[index];
            };
            Group.prototype.isEmpty = function () {
                return this.getObjects().length === 0;
            };
            Group.prototype.size = function () {
                return this.getObjects().length;
            };
            Group.prototype.contains = function (object) {
                return this.getObjects().indexOf(object) > -1;
            };
            Group.prototype._updateObjectsCoords = function () {
                this.forEachObject(this._updateObjectCoords, this);
            };
            Group.prototype._updateObjectCoords = function (object) {
                var objectLeft = object.getLeft(), objectTop = object.getTop();
                object.set({
                    originalLeft: objectLeft,
                    originalTop: objectTop,
                    left: objectLeft - this.left,
                    top: objectTop - this.top
                });
                object.setCoords();
                object['__origHasControls'] = object['hasControls'];
                object['hasControls'] = false;
            };
            Group.prototype.addWithUpdate = function (object) {
                this._restoreObjectsState();
                this._objects.push(object);
                object.group = this;
                this.forEachObject(this._setObjectActive, this);
                this._calcBounds();
                this._updateObjectsCoords();
                return this;
            };
            Group.prototype._setObjectActive = function (object) {
                object['active'] = true;
                object.group = this;
            };
            Group.prototype.removeWithUpdate = function (object) {
                this._moveFlippedObject(object);
                this._restoreObjectsState();
                this.forEachObject(this._setObjectActive, this);
                this.remove(object);
                this._calcBounds();
                this._updateObjectsCoords();
                return this;
            };
            Group.prototype._onObjectAdded = function (object) {
                object.group = this;
            };
            Group.prototype._onObjectRemoved = function (object) {
                object.group = null;
                object['active'] = false;
            };
            Group.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (key === 'brush')
                    value = NxtControl.Drawing.Brush.fromObject(value);
                else if (key === 'pen')
                    value = NxtControl.Drawing.Pen.fromObject(value);
                this[key] = value;
                invalidate && this.invalidate();
                return this;
            };
            Group.prototype.toObject = function (propertiesToInclude) {
                return extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    objects: invoke(this._objects, 'toObject', propertiesToInclude)
                });
            };
            Group.prototype.render = function (ctx, noTransform) {
                if (!this.getVisible())
                    return;
                ctx.save();
                this.transform(ctx);
                this.clipTo && NxtControl.Util.Clip.clipContext(this, ctx);
                for (var i = 0, len = this._objects.length; i < len; i++) {
                    this._renderObject(this._objects[i], ctx);
                }
                this.clipTo && ctx.restore();
                if (!noTransform && this.active) {
                    this.drawBorders(ctx);
                    this.drawControls(ctx);
                }
                ctx.restore();
            };
            Group.prototype.renderBordersAndControls = function (ctx) {
                if (!this.getVisible())
                    return;
                ctx.save();
                this.transform(ctx);
                this.clipTo && NxtControl.Util.Clip.clipContext(this, ctx);
                this.clipTo && ctx.restore();
                if (this.active) {
                    this.drawBorders(ctx);
                    this.drawControls(ctx);
                }
                ctx.restore();
            };
            Group.prototype._renderObject = function (object, ctx) {
                var originalScaleFactor = object['borderScaleFactor'], originalHasRotatingPoint = object['hasRotatingPoint'], groupScaleFactor = Math.max(this.scaleX, this.scaleY);
                if (!object.getVisible())
                    return;
                object['borderScaleFactor'] = groupScaleFactor;
                object['hasRotatingPoint'] = false;
                object.render(ctx);
                object['borderScaleFactor'] = originalScaleFactor;
                object['hasRotatingPoint'] = originalHasRotatingPoint;
            };
            Group.prototype._restoreObjectsState = function () {
                this._objects.forEach(this._restoreObjectState, this);
                return this;
            };
            Group.prototype._moveFlippedObject = function (object) {
                var oldOriginX = object.get('originX'), oldOriginY = object.get('originY'), center = object.getCenterPoint();
                object.set({
                    originX: 'center',
                    originY: 'center',
                    left: center.x,
                    top: center.y
                });
                this._toggleFlipping(object);
                var newOrigin = object.getPointByOrigin(oldOriginX, oldOriginY);
                object.set({
                    originX: oldOriginX,
                    originY: oldOriginY,
                    left: newOrigin.x,
                    top: newOrigin.y
                });
                return this;
            };
            Group.prototype._toggleFlipping = function (object) {
                if (this.flipX) {
                    object.toggle('flipX');
                    object.set('left', -object.get('left'));
                    object.setAngle(-object.getAngle());
                }
                if (this.flipY) {
                    object.toggle('flipY');
                    object.set('top', -object.get('top'));
                    object.setAngle(-object.getAngle());
                }
            };
            Group.prototype._restoreObjectState = function (object) {
                this._setObjectPosition(object);
                object.setCoords();
                object['hasControls'] = object['__origHasControls'];
                delete object['__origHasControls'];
                object['active'] = false;
                object.setCoords();
                object.group = null;
                return this;
            };
            Group.prototype._setObjectPosition = function (object) {
                var groupLeft = this.getLeft(), groupTop = this.getTop(), rotated = this._getRotatedLeftTop(object);
                object.set({
                    angle: object.getAngle() + this.getAngle(),
                    left: groupLeft + rotated.left,
                    top: groupTop + rotated.top,
                    scaleX: object.get('scaleX') * this.get('scaleX'),
                    scaleY: object.get('scaleY') * this.get('scaleY')
                });
            };
            Group.prototype._getRotatedLeftTop = function (object) {
                var groupAngle = this.getAngle() * (Math.PI / 180);
                return {
                    left: (-Math.sin(groupAngle) * object.getTop() * this.get('scaleY') +
                        Math.cos(groupAngle) * object.getLeft() * this.get('scaleX')),
                    top: (Math.cos(groupAngle) * object.getTop() * this.get('scaleY') +
                        Math.sin(groupAngle) * object.getLeft() * this.get('scaleX'))
                };
            };
            Group.prototype.destroy = function () {
                this._objects.forEach(this._moveFlippedObject, this);
                return this._restoreObjectsState();
            };
            Group.prototype.saveCoords = function () {
                this._originalLeft = this.get('left');
                this._originalTop = this.get('top');
                return this;
            };
            Group.prototype.hasMoved = function () {
                return this._originalLeft !== this.get('left') ||
                    this._originalTop !== this.get('top');
            };
            Group.prototype.setObjectsCoords = function () {
                this.forEachObject(function (object) {
                    object.setCoords();
                });
                return this;
            };
            Group.prototype._setOpacityIfSame = function () {
                var objects = this.getObjects(), firstValue = objects[0] ? objects[0].get('opacity') : 1;
                var isSameOpacity = objects.every(function (o) {
                    return o.get('opacity') === firstValue;
                });
                if (isSameOpacity) {
                    this['opacity'] = firstValue;
                }
            };
            Group.prototype._calcBounds = function () {
                var aX = [], aY = [], o;
                for (var i = 0, len = this._objects.length; i < len; ++i) {
                    o = this._objects[i];
                    if (o.getDrawable() === false)
                        continue;
                    o.setCoords();
                    for (var prop in o.oCoords) {
                        aX.push(o.oCoords[prop].x);
                        aY.push(o.oCoords[prop].y);
                    }
                }
                this.set(this._getBounds(aX, aY));
            };
            Group.prototype._getBounds = function (aX, aY) {
                var minX = Math.min.apply(Math, aX), maxX = Math.max.apply(Math, aX), minY = Math.min.apply(Math, aY), maxY = Math.max.apply(Math, aY), width = (maxX - minX) || 0, height = (maxY - minY) || 0;
                return {
                    left: (minX + width / 2) || 0,
                    top: (minY + height / 2) || 0,
                    width: width,
                    height: height,
                };
            };
            Group.prototype.get = function (prop) {
                if (prop in _lockProperties) {
                    if (this[prop]) {
                        return this[prop];
                    }
                    else {
                        for (var i = 0, len = this._objects.length; i < len; i++) {
                            if (this._objects[i][prop]) {
                                return true;
                            }
                        }
                        return false;
                    }
                }
                else {
                    if (prop in this.delegatedProperties) {
                        return this._objects[0] && this._objects[0].get(prop);
                    }
                    return this[prop];
                }
            };
            Group.fromObject = function (object, callback) {
                object = object || {};
                NxtControl.Util.enlivenObjects(object.objects, function (enlivenedObjects) {
                    delete object.objects;
                    var grp = new Group();
                    grp.load(object, enlivenedObjects);
                    callback && callback(grp);
                });
            };
            ;
            __decorate([
                System.DefaultValue('group')
            ], Group.prototype, "type", void 0);
            return Group;
        }(GuiFramework.Shape));
        GuiFramework.Group = Group;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var Services;
    (function (Services) {
        var extend = NxtControl.Util.object.extend, NGF = NxtControl.GuiFramework;
        var Communicator = (function () {
            function Communicator(deviceNames, address) {
                this.deviceNames = null;
                this.address = null;
                this.webSocket = null;
                this.subscribeId = 0;
                this.retries = 0;
                this.serverUpdate = false;
                this.deviceNames = deviceNames instanceof Array ? deviceNames : [deviceNames];
                this.address = address;
                this.registerServer = this.registerServer.bind(this);
                this.registerServer();
            }
            Communicator.prototype.registerServer = function () {
                try {
                    var __this = this;
                    this.webSocket = new WebSocket(this.address);
                }
                catch (Error) {
                    __this.retryOpeningWebSocket();
                    return;
                }
                var _this = this;
                this.webSocket.onopen = function () {
                    console.info('Connection opened');
                    _this.retries = 0;
                    for (var i = 0; i < _this.deviceNames.length; i++)
                        CommunicationService.instance.connection.trigger(CommunicationService.instance, { target: _this.deviceNames[i], state: true });
                };
                this.webSocket.onmessage = function (evt) {
                    if (typeof evt.data === 'string' && evt.data[0] !== '{')
                        return;
                    var v = JSON.parse(evt.data);
                    if (v.key === NGF.SendKey.ValueChanged)
                        CommunicationService.instance.onValueChanged(v);
                    else if (v.key === NGF.SendKey.History) {
                        var options = { target: v.deviceName, id: v.id, historyId: v.historyId, isInput: v.isInput };
                        if (v.hasOwnProperty('values')) {
                            options.values = v.values;
                            options.sec = v.sec;
                            options.usec = v.usec;
                        }
                        if (v.hasOwnProperty('archiveId'))
                            options.archiveId = v.archiveId;
                        CommunicationService.instance.history.trigger(CommunicationService.instance, options);
                    }
                    else if (v.key === NGF.SendKey.ArchiveAnswer) {
                        CommunicationService.instance.archiveAnswer.trigger(CommunicationService.instance, { values: v.values, times: v.times, id: v.id, isInput: v.isInput, start: v.start, end: v.end, continued: v.continued });
                    }
                    else if (v.key === NGF.SendKey.FaceplateSubscribeData) {
                        var faceplate = Services.CanvasTopologyService.instance.getFaceplate(v.id);
                        if (faceplate !== null)
                            faceplate.setFaceplateSubscribeData(v.data);
                    }
                    else if (v.key === NGF.SendKey.ServerUpdate) {
                        _this.serverUpdate = true;
                    }
                    else if (v.key === NGF.SendKey.AlarmUpdate) {
                        Services.AlarmService.instance.alarmUpdate({ action: v.action, index: v.index, alarmInfo: v.alarmInfo });
                    }
                    else if (v.key === NGF.SendKey.AlarmUpdate2) {
                        Services.AlarmService.instance.alarmUpdate2(v.id, { action: v.action, index: v.index, alarmInfo: v.alarmInfo });
                    }
                    else if (v.key === NGF.SendKey.AlarmUpdateClear) {
                        Services.AlarmService.instance.alarmUpdateClear();
                    }
                    else if (v.key === NGF.SendKey.AlarmUpdateColor) {
                        Services.AlarmService.instance.alarmUpdateColor({ index: v.index, alarmInfo: v.alarmInfo });
                    }
                };
                this.webSocket.onclose = function () {
                    console.info('Connection closed');
                    for (var i = 0; i < _this.deviceNames.length; i++)
                        CommunicationService.instance.connection.trigger(CommunicationService.instance, { target: _this.deviceNames[i], state: false });
                    if (_this.serverUpdate !== true)
                        _this.retryOpeningWebSocket();
                    else
                        alert('Project on the server is being updated. Reopen the page in a few minutes');
                };
            };
            Communicator.prototype.retryOpeningWebSocket = function () {
                if (this.retries < 20) {
                    setTimeout(this.registerServer, 1000);
                    this.retries++;
                }
                else {
                    console.error('Web Socket connection failed');
                    alert('Cannot connect to web socket');
                }
            };
            Communicator.prototype.isDeviceConnected = function () {
                return this.webSocket.readyState === 1;
            };
            Communicator.prototype.subscribe = function (isInput, deviceName, path, eventId, varNames, varTypes, dataId) {
                if (this.isDeviceConnected()) {
                    var packet = {};
                    packet.key = 'subscribe';
                    packet.isInput = isInput;
                    packet.id = this.subscribeId;
                    packet.device = deviceName;
                    packet.path = path;
                    packet.eventId = parseInt(eventId);
                    packet.varNames = varNames;
                    packet.varTypes = varTypes;
                    if (dataId != -1) {
                        var high = (dataId >> 16) << 16;
                        if (high !== 0) {
                            packet.historyId = high;
                            packet.dataId = dataId - high;
                        }
                        else
                            packet.dataId = dataId;
                    }
                    else
                        packet.dataId = -1;
                    this.subscribeId++;
                    this.webSocket.send(JSON.stringify(packet));
                    return packet.id;
                }
                return -1;
            };
            Communicator.prototype.getFaceplateSubscribeData = function (id, type, symbolPath) {
                if (this.isDeviceConnected()) {
                    var packet = {};
                    packet.key = 'getFaceplateSubscribeData';
                    packet.id = id;
                    packet.type = type;
                    packet.path = symbolPath;
                    this.webSocket.send(JSON.stringify(packet));
                }
            };
            Communicator.prototype.unsubscribe = function (isInput, deviceName, path, id) {
                if (this.isDeviceConnected()) {
                    var packet = {};
                    packet.key = 'unsubscribe';
                    packet.isInput = isInput;
                    packet.device = deviceName;
                    packet.path = path;
                    packet.id = id;
                    this.webSocket.send(JSON.stringify(packet));
                    return packet.id;
                }
                return -1;
            };
            Communicator.prototype.setValue = function (path, deviceName, id, eventId, dataId, value) {
                if (this.isDeviceConnected()) {
                    var packet = {};
                    packet.key = 'setValue';
                    packet.device = deviceName;
                    packet.path = path;
                    packet.id = id;
                    packet.eventId = parseInt(eventId);
                    packet.dataId = dataId;
                    packet.value = value;
                    this.webSocket.send(JSON.stringify(packet));
                    return packet.id;
                }
                return -1;
            };
            Communicator.prototype.setValues = function (path, deviceName, id, eventId, dataIds, values) {
                if (this.isDeviceConnected()) {
                    var packet = {};
                    packet.key = 'setValues';
                    packet.device = deviceName;
                    packet.path = path;
                    packet.id = id;
                    packet.eventId = parseInt(eventId);
                    packet.dataIds = dataIds;
                    packet.values = values;
                    this.webSocket.send(JSON.stringify(packet));
                    return packet.id;
                }
                return -1;
            };
            Communicator.prototype.subscribeAlarm = function (id) {
                if (this.isDeviceConnected()) {
                    var packet = {};
                    packet.key = 'subscribeAlarm';
                    packet.id = id;
                    this.webSocket.send(JSON.stringify(packet));
                    return 0;
                }
                return -1;
            };
            Communicator.prototype.unsubscribeAlarm = function () {
                if (this.isDeviceConnected()) {
                    var packet = {};
                    packet.key = 'unsubscribeAlarm';
                    this.webSocket.send(JSON.stringify(packet));
                    return 0;
                }
                return -1;
            };
            Communicator.prototype.ackAlarm = function (alarmId) {
                if (this.isDeviceConnected()) {
                    var packet = {};
                    packet.key = 'ackAlarm';
                    packet.alarmId = alarmId;
                    this.webSocket.send(JSON.stringify(packet));
                    return 0;
                }
                return -1;
            };
            Communicator.prototype.queryArchive = function (deviceName, minimum, maximum, data) {
                if (this.isDeviceConnected() === false)
                    return;
                var packet = {};
                packet.key = 'queryArchive';
                packet.device = deviceName;
                packet.min = minimum;
                packet.max = maximum;
                packet.data = data;
                this.webSocket.send(JSON.stringify(packet));
            };
            return Communicator;
        }());
        var CommunicationService = (function () {
            function CommunicationService() {
                this.connectors = {};
                this.connection = new NxtControl.event();
                this.history = new NxtControl.event();
                this.archiveAnswer = new NxtControl.event();
                this.valueChanged = new NxtControl.event();
            }
            CommunicationService.prototype.registerConnector = function (deviceNames, address) {
                var connector = new Communicator(deviceNames, address);
                for (var i = 0; i < connector.deviceNames.length; i++)
                    this.connectors[connector.deviceNames[i]] = connector;
                return connector;
            };
            CommunicationService.prototype.onValueChanged = function (value) {
                this.valueChanged.trigger(this, value);
            };
            CommunicationService.prototype.isDeviceConnected = function (deviceName) {
                if (this.connectors[deviceName])
                    return this.connectors[deviceName].isDeviceConnected();
                return false;
            };
            CommunicationService.prototype.isConnected = function () {
                for (var deviceName in this.connectors) {
                    if (this.connectors[deviceName])
                        return this.connectors[deviceName].isDeviceConnected();
                }
                return false;
            };
            CommunicationService.prototype.subscribe = function (isInput, deviceName, path, eventId, varNames, varTypes, dataId) {
                if (this.connectors[deviceName])
                    return this.connectors[deviceName].subscribe(isInput, deviceName, path, eventId, varNames, varTypes, dataId);
                return -1;
            };
            CommunicationService.prototype.unsubscribe = function (isInput, deviceName, path, id) {
                if (this.connectors[deviceName])
                    return this.connectors[deviceName].unsubscribe(isInput, deviceName, path, id);
                return -1;
            };
            CommunicationService.prototype.subscribeAlarm = function (id) {
                for (var deviceName in this.connectors) {
                    if (this.connectors[deviceName])
                        return this.connectors[deviceName].subscribeAlarm(id);
                }
                return -1;
            };
            CommunicationService.prototype.unsubscribeAlarm = function () {
                for (var deviceName in this.connectors) {
                    if (this.connectors[deviceName])
                        return this.connectors[deviceName].unsubscribeAlarm();
                }
                return -1;
            };
            CommunicationService.prototype.ackAlarm = function (alarmId) {
                for (var deviceName in this.connectors) {
                    if (this.connectors[deviceName])
                        return this.connectors[deviceName].ackAlarm(alarmId);
                }
                return -1;
            };
            CommunicationService.prototype.setValue = function (options, value) {
                if (this.connectors[options.deviceName])
                    return this.connectors[options.deviceName].setValue(options.path, options.deviceName, options.id, options.eventId, options.dataId, value);
                return -1;
            };
            CommunicationService.prototype.setValues = function (options, values) {
                if (this.connectors[options.deviceName])
                    return this.connectors[options.deviceName].setValues(options.path, options.deviceName, options.id, options.eventId, options.dataIds, values);
                return -1;
            };
            CommunicationService.prototype.queryArchive = function (minimum, maximum, data) {
                var map = {};
                for (var i = 0; i < data.length; i++) {
                    var o = data[i];
                    if (this.connectors[o.data.deviceName]) {
                        if (map.hasOwnProperty(o.data.deviceName) === false)
                            map[o.data.deviceName] = [];
                        map[o.data.deviceName].push({ path: o.data.path, id: o.data.id, archiveId: o.archiveId, max: o.max });
                    }
                }
                for (var m in map) {
                    if (!map.hasOwnProperty(m))
                        continue;
                    this.connectors[m].queryArchive(m, minimum, maximum, map[m]);
                }
            };
            CommunicationService.prototype.getFaceplateSubscribeData = function (faceplate) {
                for (var deviceName in this.connectors) {
                    if (!this.connectors.hasOwnProperty(deviceName))
                        continue;
                    this.connectors[deviceName].getFaceplateSubscribeData(faceplate.id, faceplate.type, faceplate.symbolPath);
                    break;
                }
            };
            return CommunicationService;
        }());
        Services.CommunicationService = CommunicationService;
        CommunicationService.instance = new CommunicationService();
    })(Services = NxtControl.Services || (NxtControl.Services = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var Services;
    (function (Services) {
        var extend = NxtControl.Util.object.extend;
        var AlarmService = (function () {
            function AlarmService() {
                this.id = 0;
                this.alarmControls = {};
                this.alarmCount = 0;
                this._onConnection = this._onConnection.bind(this);
                Services.CommunicationService.instance.connection.add(this._onConnection);
            }
            AlarmService.prototype.subscribe = function (alarmControl) {
                this.alarmCount++;
                this.id++;
                this.alarmControls[this.id] = alarmControl;
                if (Services.CommunicationService.instance)
                    Services.CommunicationService.instance.subscribeAlarm(this.id);
                return this.id;
            };
            AlarmService.prototype.unsubscribe = function (alarmId) {
                if (this.alarmControls.hasOwnProperty(alarmId.toString())) {
                    this.alarmCount--;
                    delete this.alarmControls[alarmId];
                    if (this.alarmCount === 0 && Services.CommunicationService.instance)
                        Services.CommunicationService.instance.unsubscribeAlarm();
                }
            };
            AlarmService.prototype.ackAlarm = function (alarmId) {
                Services.CommunicationService.instance.ackAlarm(alarmId);
            };
            AlarmService.prototype.alarmUpdate = function (options) {
                for (var alarmId in this.alarmControls)
                    if (this.alarmControls.hasOwnProperty(alarmId))
                        this.alarmControls[alarmId].alarmUpdate(options.action, options.index, options.alarmInfo);
            };
            AlarmService.prototype.alarmUpdate2 = function (alarmId, options) {
                if (this.alarmControls.hasOwnProperty(alarmId.toString()))
                    this.alarmControls[alarmId].alarmUpdate(options.action, options.index, options.alarmInfo);
            };
            AlarmService.prototype.alarmUpdateColor = function (options) {
                for (var alarmId in this.alarmControls)
                    if (this.alarmControls.hasOwnProperty(alarmId))
                        this.alarmControls[alarmId].alarmUpdateColor(options.index, options.alarmInfo);
            };
            AlarmService.prototype.alarmUpdateClear = function () {
                for (var alarmId in this.alarmControls)
                    if (this.alarmControls.hasOwnProperty(alarmId))
                        this.alarmControls[alarmId].alarmUpdateClear();
            };
            AlarmService.prototype._onConnection = function (sender, ea) {
                var commService = Services.CommunicationService.instance;
                if (this.alarmCount > 0 && commService && commService.isConnected()) {
                    for (var alarmId in this.alarmControls)
                        if (this.alarmControls.hasOwnProperty(alarmId))
                            commService.subscribeAlarm(alarmId);
                }
            };
            return AlarmService;
        }());
        Services.AlarmService = AlarmService;
        AlarmService.instance = new AlarmService();
    })(Services = NxtControl.Services || (NxtControl.Services = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend, getElementOffset = NxtControl.Util.getElementOffset, removeFromArray = NxtControl.Util.removeFromArray, CANVAS_INIT_ERROR = new Error('Could not initialize `canvas` element');
        var StaticCanvas = (function () {
            function StaticCanvas(el, options) {
                this.width = 0;
                this.height = 0;
                this._objects = [];
                this.backgroundColor = NxtControl.Drawing.Color.Transparent;
                this.backgroundImage = null;
                this.overlayColor = NxtControl.Drawing.Color.Transparent;
                this.overlayImage = null;
                this.includeDefaultValues = true;
                this.stateful = true;
                this.renderOnAddRemove = true;
                this.clipTo = null;
                this.controlsAboveOverlay = false;
                this.allowTouchScrolling = false;
                this._invalidateId = -1;
                this.toJSON = StaticCanvas.prototype.toObject;
                this.added = new NxtControl.event();
                this.modified = new NxtControl.event();
                this.removed = new NxtControl.event();
                this.beforeSelectionCleared = new NxtControl.event();
                this.selectionCleared = new NxtControl.event();
                this.canvasCleared = new NxtControl.event();
                this.beforeRender = new NxtControl.event();
                this.afterRender = new NxtControl.event();
                this.initialize(el, options);
            }
            StaticCanvas.prototype.initialize = function (el, options) {
                options || (options = {});
                this._initStatic(el, options);
                StaticCanvas.activeInstance = this;
            };
            StaticCanvas.prototype.onBeforeScaleRotate = function (target) {
            };
            StaticCanvas.prototype.getLowerCanvasElement = function () {
                return this.lowerCanvasEl;
            };
            StaticCanvas.prototype.getUpperCanvasElement = function () {
                return this.upperCanvasEl;
            };
            StaticCanvas.prototype._initStatic = function (el, options) {
                this._objects = [];
                this._createLowerCanvas(el);
                this._initOptions(options);
                if (options.overlayImage) {
                    this.setOverlayImage(options.overlayImage, this.renderAll.bind(this));
                }
                if (options.backgroundImage) {
                    this.setBackgroundImage(options.backgroundImage, this.renderAll.bind(this));
                }
                if (options.background) {
                    this.setBackgroundColor(options.background, this.renderAll.bind(this));
                }
                if (options.overlayColor) {
                    this.setOverlayColor(options.overlayColor, this.renderAll.bind(this));
                }
                this.calcOffset();
            };
            StaticCanvas.prototype.add = function () {
                var shapes = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    shapes[_i - 0] = arguments[_i];
                }
                this._objects.push.apply(this._objects, shapes);
                for (var i = shapes.length; i--;) {
                    this._onObjectAdded(shapes[i]);
                }
                this.renderOnAddRemove && this.renderAll();
                return this;
            };
            StaticCanvas.prototype.insertAt = function (object, index, nonSplicing) {
                var objects = this.getObjects();
                if (nonSplicing) {
                    objects[index] = object;
                }
                else {
                    objects.splice(index, 0, object);
                }
                this._onObjectAdded(object);
                this.renderOnAddRemove && this.renderAll();
                return this;
            };
            StaticCanvas.prototype.remove = function (object) {
                var objects = this.getObjects(), index = objects.indexOf(object);
                if (index !== -1) {
                    objects.splice(index, 1);
                    this._onObjectRemoved(object);
                }
                this.renderOnAddRemove && this.renderAll();
                return object;
            };
            StaticCanvas.prototype.find = function (path) {
                var pathParts = path.split('.');
                return this._find(pathParts, 0);
            };
            StaticCanvas.prototype._find = function (pathParts, index) {
                var namedObject = null;
                for (var i = 0; i < this._objects.length; i++) {
                    if (this._objects[i].getName() === pathParts[index]) {
                        namedObject = this._objects[i];
                        break;
                    }
                }
                if (namedObject !== null) {
                    index++;
                    if (pathParts.length === index)
                        return namedObject;
                    else
                        return namedObject._find(pathParts, index);
                }
                return null;
            };
            StaticCanvas.prototype.forEachObject = function (callback, context) {
                var objects = this.getObjects(), i = objects.length;
                while (i--) {
                    callback.call(context, objects[i], i, objects);
                }
                return this;
            };
            StaticCanvas.prototype.getObjects = function (type) {
                if (typeof type === 'undefined') {
                    return this._objects;
                }
                return this._objects.filter(function (o) {
                    return o.getType() === type;
                });
            };
            StaticCanvas.prototype.item = function (index) {
                return this.getObjects()[index];
            };
            StaticCanvas.prototype.isEmpty = function () {
                return this.getObjects().length === 0;
            };
            StaticCanvas.prototype.size = function () {
                return this.getObjects().length;
            };
            StaticCanvas.prototype.contains = function (object) {
                return this.getObjects().indexOf(object) > -1;
            };
            StaticCanvas.prototype.calcOffset = function () {
                this._offset = getElementOffset(this.lowerCanvasEl);
                return this;
            };
            StaticCanvas.prototype.setOverlayImage = function (image, callback, options) {
                return this.__setBgOverlayImage('overlayImage', image, callback, options);
            };
            StaticCanvas.prototype.setBackgroundImage = function (image, callback, options) {
                return this.__setBgOverlayImage('backgroundImage', image, callback, options);
            };
            StaticCanvas.prototype.setOverlayColor = function (overlayColor, callback) {
                return this.__setBgOverlayColor('overlayColor', overlayColor, callback);
            };
            StaticCanvas.prototype.setBackgroundColor = function (backgroundColor, callback) {
                return this.__setBgOverlayColor('backgroundColor', backgroundColor, callback);
            };
            StaticCanvas.prototype.__setBgOverlayImage = function (property, image, callback, options) {
                return this;
            };
            StaticCanvas.prototype.__setBgOverlayColor = function (property, color, callback) {
                if (color.source) {
                }
                else {
                    this[property] = NxtControl.Drawing.Color.fromObject(color);
                    callback && callback();
                }
                return this;
            };
            StaticCanvas.prototype._createCanvasElement = function () {
                var element = System.document.createElement('canvas');
                if (!element.style) {
                    element.style = {};
                }
                if (!element) {
                    throw CANVAS_INIT_ERROR;
                }
                this._initCanvasElement(element);
                return element;
            };
            StaticCanvas.prototype._initCanvasElement = function (element) {
                NxtControl.Util.createCanvasElement(element);
                if (typeof element.getContext === 'undefined') {
                    throw CANVAS_INIT_ERROR;
                }
            };
            StaticCanvas.prototype._initOptions = function (options) {
                for (var prop in options) {
                    this[prop] = options[prop];
                }
                this.width = options.width || this.lowerCanvasEl.width || 0;
                this.height = options.height || this.lowerCanvasEl.height || 0;
                if (!this.lowerCanvasEl.style)
                    return;
                this.lowerCanvasEl.width = this.width;
                this.lowerCanvasEl.height = this.height;
                this.lowerCanvasEl.style.width = this.width + 'px';
                this.lowerCanvasEl.style.height = this.height + 'px';
            };
            StaticCanvas.prototype._applyCanvasStyle = function (element) {
            };
            StaticCanvas.prototype._createLowerCanvas = function (canvasEl) {
                this.lowerCanvasEl = NxtControl.Util.getById(canvasEl) || this._createCanvasElement();
                this._initCanvasElement(this.lowerCanvasEl);
                NxtControl.Util.addClass(this.lowerCanvasEl, 'lower-canvas');
                if (this.interactive) {
                    this._applyCanvasStyle(this.lowerCanvasEl);
                }
                this.contextContainer = this.lowerCanvasEl.getContext('2d');
            };
            StaticCanvas.prototype.getWidth = function () {
                return this.width;
            };
            StaticCanvas.prototype.getHeight = function () {
                return this.height;
            };
            StaticCanvas.prototype.setWidth = function (value) {
                return this._setDimension('width', value);
            };
            StaticCanvas.prototype.setHeight = function (value) {
                return this._setDimension('height', value);
            };
            StaticCanvas.prototype.setDimensions = function (dimensions, options) {
                for (var prop in dimensions) {
                    this._setDimension(prop, dimensions[prop]);
                }
                return this;
            };
            StaticCanvas.prototype._setDimension = function (prop, value) {
                this.lowerCanvasEl[prop] = value;
                this.lowerCanvasEl.style[prop] = value + 'px';
                if (this.upperCanvasEl) {
                    this.upperCanvasEl[prop] = value;
                    this.upperCanvasEl.style[prop] = value + 'px';
                }
                if (this.cacheCanvasEl) {
                    this.cacheCanvasEl[prop] = value;
                }
                if (this.wrapperEl) {
                    this.wrapperEl.style[prop] = value + 'px';
                }
                this[prop] = value;
                this.calcOffset();
                this.renderAll();
                return this;
            };
            StaticCanvas.prototype.getElement = function () {
                return this.lowerCanvasEl;
            };
            StaticCanvas.prototype.getActiveObject = function () {
                return null;
            };
            StaticCanvas.prototype.getActiveGroup = function () {
                return null;
            };
            StaticCanvas.prototype._draw = function (ctx, object) {
                if (!object)
                    return;
                if (this.controlsAboveOverlay) {
                    var hasBorders = object['hasBorders'], hasControls = object['hasControls'];
                    object['hasControls'] = object['hasBorders'] = false;
                    object.render(ctx);
                    object['hasBorders'] = hasBorders;
                    object['hasControls'] = hasControls;
                }
                else {
                    object.render(ctx);
                }
            };
            StaticCanvas.prototype._onObjectAdded = function (obj) {
                this.stateful && obj.setupState();
                obj.setCoords();
                obj.canvas = this;
                this.added.trigger(this, { target: obj });
                obj.added.trigger(obj, NxtControl.EmptyEventArgs);
            };
            StaticCanvas.prototype._discardActiveObject = function () {
            };
            StaticCanvas.prototype._onObjectRemoved = function (obj) {
                if (this.getActiveObject() === obj) {
                    this.beforeSelectionCleared.trigger(this, { target: obj });
                    this._discardActiveObject();
                    this.selectionCleared.trigger(this, NxtControl.EmptyEventArgs);
                }
                this.removed.trigger(this, { target: obj });
                obj.removed.trigger(obj, NxtControl.EmptyEventArgs);
            };
            StaticCanvas.prototype.clearContext = function (ctx) {
                ctx.clearRect(0, 0, this.width, this.height);
                return this;
            };
            StaticCanvas.prototype.getContext = function () {
                return this.contextContainer;
            };
            StaticCanvas.prototype.discardActiveObject = function (e) {
                return this;
            };
            StaticCanvas.prototype.getPointer = function (e) {
                return new NxtControl.Drawing.Point(0, 0);
            };
            StaticCanvas.prototype.clear = function () {
                this._objects.length = 0;
                if (this.discardActiveGroup) {
                    this.discardActiveGroup();
                }
                if (this.discardActiveObject) {
                    this.discardActiveObject();
                }
                this.clearContext(this.contextContainer);
                if (this.contextTop) {
                    this.clearContext(this.contextTop);
                }
                this.canvasCleared.trigger(this, NxtControl.EmptyEventArgs);
                this.renderAll();
                return this;
            };
            StaticCanvas.prototype.drawControls = function (ctx) {
            };
            StaticCanvas.prototype.renderAll = function (allOnTop) {
                if (allOnTop === void 0) { allOnTop = false; }
                var canvasToDrawOn = this[(allOnTop === true && this.interactive) ? 'contextTop' : 'contextContainer'];
                var activeGroup = this.getActiveGroup();
                if (this.contextTop && this.selection && !this._groupSelector) {
                    this.clearContext(this.contextTop);
                }
                if (!allOnTop) {
                    this.clearContext(canvasToDrawOn);
                }
                this.beforeRender.trigger(this, NxtControl.EmptyEventArgs);
                if (this.clipTo) {
                    NxtControl.Util.Clip.clipContext(this, canvasToDrawOn);
                }
                this._renderBackground(canvasToDrawOn);
                this._renderObjects(canvasToDrawOn, activeGroup);
                this._renderActiveGroup(canvasToDrawOn, activeGroup);
                if (this.clipTo) {
                    canvasToDrawOn.restore();
                }
                this._renderOverlay(canvasToDrawOn);
                if (this.controlsAboveOverlay && this.interactive) {
                    this.drawControls(canvasToDrawOn);
                }
                this.afterRender.trigger(this, NxtControl.EmptyEventArgs);
                return this;
            };
            StaticCanvas.prototype.invalidate = function () {
                var _this = this;
                if (this._invalidateId != -1)
                    return;
                this._invalidateId = setTimeout(function () {
                    _this._invalidateId = -1;
                    _this.renderAll();
                }, 50);
            };
            StaticCanvas.prototype._renderObjects = function (ctx, activeGroup) {
                for (var i = 0, length = this._objects.length; i < length; ++i) {
                    if (!activeGroup) {
                        this._draw(ctx, this._objects[i]);
                    }
                    else if (this._objects[i]) {
                        if (activeGroup.contains(this._objects[i])) {
                            ctx.save();
                            activeGroup.transform(ctx);
                            this._draw(ctx, this._objects[i]);
                            ctx.restore();
                        }
                        else {
                            this._draw(ctx, this._objects[i]);
                        }
                    }
                }
            };
            StaticCanvas.prototype._renderActiveGroup = function (ctx, activeGroup) {
                if (activeGroup) {
                    if (!this.controlsAboveOverlay)
                        activeGroup.renderBordersAndControls(ctx);
                }
            };
            StaticCanvas.prototype._renderBackground = function (ctx) {
                if (this.backgroundColor) {
                    ctx.fillStyle = this.backgroundColor.toLive();
                    ctx.fillRect(this.backgroundColor.offsetX || 0, this.backgroundColor.offsetY || 0, this.width, this.height);
                }
                if (this.backgroundImage) {
                    this.backgroundImage.render(ctx);
                }
            };
            StaticCanvas.prototype._renderOverlay = function (ctx) {
                if (this.overlayColor) {
                    ctx.fillStyle = this.overlayColor.toLive();
                    ctx.fillRect(this.overlayColor.offsetX || 0, this.overlayColor.offsetY || 0, this.width, this.height);
                }
                if (this.overlayImage) {
                    this.overlayImage.render(ctx);
                }
            };
            StaticCanvas.prototype._drawSelection = function () { };
            StaticCanvas.prototype.renderTop = function () {
                var ctx = this.contextTop || this.contextContainer;
                this.clearContext(ctx);
                if (this.selection && this._groupSelector) {
                    this._drawSelection();
                }
                var activeGroup = this.getActiveGroup();
                if (activeGroup) {
                    activeGroup.render(ctx);
                }
                if (this.overlayImage) {
                    ctx.drawImage(this.overlayImage, this.overlayImageLeft, this.overlayImageTop);
                }
                this.afterRender.trigger(this, NxtControl.EmptyEventArgs);
                return this;
            };
            StaticCanvas.prototype.getCenter = function () {
                return {
                    top: this.getHeight() / 2,
                    left: this.getWidth() / 2
                };
            };
            StaticCanvas.prototype.centerObjectH = function (object) {
                this._centerObject(object, new NxtControl.Drawing.Point(this.getCenter().left, object.getCenterPoint().y));
                this.renderAll();
                return this;
            };
            StaticCanvas.prototype.centerObjectV = function (object) {
                this._centerObject(object, new NxtControl.Drawing.Point(object.getCenterPoint().x, this.getCenter().top));
                this.renderAll();
                return this;
            };
            StaticCanvas.prototype.centerObject = function (object) {
                var center = this.getCenter();
                this._centerObject(object, new NxtControl.Drawing.Point(center.left, center.top));
                this.renderAll();
                return this;
            };
            StaticCanvas.prototype._centerObject = function (object, center) {
                object.setPositionByOrigin(center, 'center', 'center');
                return this;
            };
            StaticCanvas.prototype.toDatalessJSON = function (propertiesToInclude) {
                return this.toDatalessObject(propertiesToInclude);
            };
            StaticCanvas.prototype.toObject = function (propertiesToInclude) {
                return this._toObjectMethod('toObject', propertiesToInclude);
            };
            StaticCanvas.prototype.toDatalessObject = function (propertiesToInclude) {
                return this._toObjectMethod('toDatalessObject', propertiesToInclude);
            };
            StaticCanvas.prototype.discardActiveGroup = function (e) {
                return this;
            };
            StaticCanvas.prototype.setActiveGroup = function (group, e) {
                return this;
            };
            StaticCanvas.prototype._toObjectMethod = function (methodName, propertiesToInclude) {
                var activeGroup = this.getActiveGroup();
                if (activeGroup) {
                    this.discardActiveGroup();
                }
                var data = {
                    objects: this._toObjects(methodName, propertiesToInclude)
                };
                extend(data, this.__serializeBgOverlay());
                NxtControl.Util.populateWithProperties(this, data, propertiesToInclude);
                if (activeGroup) {
                    this.setActiveGroup(new GuiFramework.Group().load(null, activeGroup.getObjects()));
                    activeGroup.forEachObject(function (o) {
                        o.set('active', true);
                    });
                }
                return data;
            };
            StaticCanvas.prototype._toObjects = function (methodName, propertiesToInclude) {
                return this.getObjects().map(function (instance) {
                    return this._toObject(instance, methodName, propertiesToInclude);
                }, this);
            };
            StaticCanvas.prototype._toObject = function (instance, methodName, propertiesToInclude) {
                var originalValue;
                if (!this.includeDefaultValues) {
                    originalValue = instance.includeDefaultValues;
                    instance.includeDefaultValues = false;
                }
                var object = instance[methodName](propertiesToInclude);
                if (!this.includeDefaultValues) {
                    instance.includeDefaultValues = originalValue;
                }
                return object;
            };
            StaticCanvas.prototype.__serializeBgOverlay = function () {
                var data = {
                    background: (this.backgroundColor && this.backgroundColor.toObject)
                        ? this.backgroundColor.toObject()
                        : this.backgroundColor
                };
                if (this.overlayColor) {
                    data.overlay = this.overlayColor.toObject
                        ? this.overlayColor.toObject()
                        : this.overlayColor;
                }
                if (this.backgroundImage) {
                    data.backgroundImage = this.backgroundImage.toObject();
                }
                if (this.overlayImage) {
                    data.overlayImage = this.overlayImage.toObject();
                }
                return data;
            };
            StaticCanvas.prototype.sendToBack = function (object) {
                removeFromArray(this._objects, object);
                this._objects.unshift(object);
                return this.renderAll && this.renderAll();
            };
            StaticCanvas.prototype.bringToFront = function (object) {
                removeFromArray(this._objects, object);
                this._objects.push(object);
                return this.renderAll && this.renderAll();
            };
            StaticCanvas.prototype.sendBackwards = function (object, intersecting) {
                var idx = this._objects.indexOf(object);
                if (idx !== 0) {
                    var newIdx = this._findNewLowerIndex(object, idx, intersecting);
                    removeFromArray(this._objects, object);
                    this._objects.splice(newIdx, 0, object);
                    this.renderAll && this.renderAll();
                }
                return this;
            };
            StaticCanvas.prototype._findNewLowerIndex = function (object, idx, intersecting) {
                var newIdx;
                if (intersecting) {
                    newIdx = idx;
                    for (var i = idx - 1; i >= 0; --i) {
                        var isIntersecting = object.intersectsWithObject(this._objects[i]) ||
                            object.isContainedWithinObject(this._objects[i]) ||
                            this._objects[i].isContainedWithinObject(object);
                        if (isIntersecting) {
                            newIdx = i;
                            break;
                        }
                    }
                }
                else {
                    newIdx = idx - 1;
                }
                return newIdx;
            };
            StaticCanvas.prototype.bringForward = function (object, intersecting) {
                var idx = this._objects.indexOf(object);
                if (idx !== this._objects.length - 1) {
                    var newIdx = this._findNewUpperIndex(object, idx, intersecting);
                    removeFromArray(this._objects, object);
                    this._objects.splice(newIdx, 0, object);
                    this.renderAll && this.renderAll();
                }
                return this;
            };
            StaticCanvas.prototype._findNewUpperIndex = function (object, idx, intersecting) {
                var newIdx;
                if (intersecting) {
                    newIdx = idx;
                    for (var i = idx + 1; i < this._objects.length; ++i) {
                        var isIntersecting = object.intersectsWithObject(this._objects[i]) ||
                            object.isContainedWithinObject(this._objects[i]) ||
                            this._objects[i].isContainedWithinObject(object);
                        if (isIntersecting) {
                            newIdx = i;
                            break;
                        }
                    }
                }
                else {
                    newIdx = idx + 1;
                }
                return newIdx;
            };
            StaticCanvas.prototype.moveTo = function (object, index) {
                removeFromArray(this._objects, object);
                this._objects.splice(index, 0, object);
                return this.renderAll && this.renderAll();
            };
            StaticCanvas.prototype.loadFromJSON = function (json, callback, reviver) {
                if (!json)
                    return;
                var serialized = (typeof json === 'string')
                    ? JSON.parse(json)
                    : json;
                this.clear();
                var _this = this;
                this._enlivenObjects(serialized.objects, function () {
                    _this._setBgOverlay(serialized);
                    callback && callback();
                }, reviver);
                return this;
            };
            StaticCanvas.prototype._setBgOverlay = function (serialized, callback) {
                var _this = this, loaded = {
                    backgroundColor: false,
                    overlayColor: false,
                    backgroundImage: false,
                    overlayImage: false
                };
                if (!serialized.backgroundImage && !serialized.overlayImage && !serialized.background && !serialized.overlay) {
                    callback && callback();
                    return;
                }
                var cbIfLoaded = function () {
                    if (loaded.backgroundImage && loaded.overlayImage && loaded.backgroundColor && loaded.overlayColor) {
                        _this.renderAll();
                        callback && callback();
                    }
                };
                this.__setBgOverlay('backgroundImage', serialized.backgroundImage, loaded, cbIfLoaded);
                this.__setBgOverlay('overlayImage', serialized.overlayImage, loaded, cbIfLoaded);
                this.__setBgOverlay('backgroundColor', serialized.background, loaded, cbIfLoaded);
                this.__setBgOverlay('overlayColor', serialized.overlay, loaded, cbIfLoaded);
                cbIfLoaded();
            };
            StaticCanvas.prototype.__setBgOverlay = function (property, value, loaded, callback) {
                var _this = this;
                if (!value) {
                    loaded[property] = true;
                    return;
                }
                if (property === 'backgroundImage' || property === 'overlayImage') {
                }
                else {
                    this['set' + NxtControl.Util.Parser.capitalize(property, true)](value, function () {
                        loaded[property] = true;
                        callback && callback();
                    });
                }
            };
            StaticCanvas.prototype._enlivenObjects = function (objects, callback, reviver) {
                var _this = this;
                if (objects.length === 0) {
                    callback && callback();
                }
                var renderOnAddRemove = this.renderOnAddRemove;
                this.renderOnAddRemove = false;
                NxtControl.Util.enlivenObjects(objects, function (enlivenedObjects) {
                    enlivenedObjects.forEach(function (obj, index) {
                        _this.insertAt(obj, index, true);
                    });
                    _this.renderOnAddRemove = renderOnAddRemove;
                    callback && callback();
                }, null, reviver);
            };
            StaticCanvas.prototype.removeListeners = function () {
            };
            StaticCanvas.prototype.dispose = function () {
                if (this._invalidateId != -1) {
                    clearTimeout(this._invalidateId);
                    this._invalidateId = -1;
                }
                this.clear();
                this.interactive && this.removeListeners();
                return this;
            };
            StaticCanvas.supports = function (methodName) {
                var el = NxtControl.Util.createCanvasElement();
                if (!el || !el.getContext) {
                    return null;
                }
                var ctx = el.getContext('2d');
                if (!ctx) {
                    return null;
                }
                switch (methodName) {
                    case 'getImageData':
                        return typeof ctx.getImageData !== 'undefined';
                    case 'setLineDash':
                        return typeof ctx.setLineDash !== 'undefined';
                    case 'toDataURL':
                        return typeof el.toDataURL !== 'undefined';
                    case 'toDataURLWithQuality':
                        try {
                            el.toDataURL('image/jpeg', 0);
                            return true;
                        }
                        catch (e) { }
                        return false;
                    default:
                        return null;
                }
            };
            StaticCanvas.EMPTY_JSON = '{"objects": [], "background": "White"}';
            __decorate([
                System.DefaultValue(true)
            ], StaticCanvas.prototype, "selection", void 0);
            return StaticCanvas;
        }());
        GuiFramework.StaticCanvas = StaticCanvas;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var getPointer = NxtControl.Util.getPointer, degreesToRadians = NxtControl.Util.Convert.degreesToRadians, radiansToDegrees = NxtControl.Util.Convert.radiansToDegrees, atan2 = Math.atan2, abs = Math.abs, STROKE_OFFSET = 0.5;
        var cursorMap = ['n-resize', 'ne-resize', 'e-resize', 'se-resize', 's-resize', 'sw-resize', 'w-resize', 'nw-resize'], cursorOffset = {
            'mt': 0,
            'tr': 1,
            'mr': 2,
            'br': 3,
            'mb': 4,
            'bl': 5,
            'ml': 6,
            'tl': 7
        }, addListener = NxtControl.Util.addListener, removeListener = NxtControl.Util.removeListener, getPointer = NxtControl.Util.getPointer;
        var InteractiveCanvas = (function (_super) {
            __extends(InteractiveCanvas, _super);
            function InteractiveCanvas(el, options) {
                _super.call(this, el, options);
                this.selectionDashArray = [];
                this.isMultiTouch = false;
            }
            InteractiveCanvas.prototype.initialize = function (el, options) {
                options || (options = {});
                this.selected = new NxtControl.event();
                this.mousedown = new NxtControl.event();
                this.mousemove = new NxtControl.event();
                this.mouseup = new NxtControl.event();
                this.click = new NxtControl.event();
                this.rotating = new NxtControl.event();
                this.scaling = new NxtControl.event();
                this.moving = new NxtControl.event();
                this.selectionCreated = new NxtControl.event();
                this._initStatic(el, options);
                this._initInteractive();
                this._createCacheCanvas();
                InteractiveCanvas.activeInstance = this;
            };
            InteractiveCanvas.prototype._initInteractive = function () {
                this._currentTransform = null;
                this._groupSelector = null;
                this._initWrapperElement();
                this._createUpperCanvas();
                this._initEventListeners();
                this.calcOffset();
            };
            InteractiveCanvas.prototype._resetCurrentTransform = function (e) {
                var t = this._currentTransform;
                t.target.set({
                    'scaleX': t.original.scaleX,
                    'scaleY': t.original.scaleY,
                    'left': t.original.left,
                    'top': t.original.top
                });
                if (this._shouldCenterTransform(e, t.target)) {
                    if (t.action === 'rotate') {
                        this._setOriginToCenter(t.target);
                    }
                    else {
                        if (t.originX !== 'center') {
                            if (t.originX === 'right') {
                                t.mouseXSign = -1;
                            }
                            else {
                                t.mouseXSign = 1;
                            }
                        }
                        if (t.originY !== 'center') {
                            if (t.originY === 'bottom') {
                                t.mouseYSign = -1;
                            }
                            else {
                                t.mouseYSign = 1;
                            }
                        }
                        t.originX = 'center';
                        t.originY = 'center';
                    }
                }
                else {
                    t.originX = t.original.originX;
                    t.originY = t.original.originY;
                }
            };
            InteractiveCanvas.prototype.containsPoint = function (e, target) {
                var pointer = this.getPointer(e), xy = this._normalizePointer(target, pointer);
                if (System.isTouchSupported && e instanceof TouchEvent && e.changedTouches.length != 0) {
                    var touchPoints = [];
                    var touch = e.changedTouches.item(0);
                    if (touch.radiusX && touch.radiusX != null) {
                        touchPoints.push({ x: xy.x + touch.radiusX, y: xy.y });
                        touchPoints.push({ x: xy.x - touch.radiusX, y: xy.y });
                        touchPoints.push({ x: xy.x, y: xy.y + touch.radiusY });
                        touchPoints.push({ x: xy.x, y: xy.y - touch.radiusY });
                        e.myTouchPoints = touchPoints;
                    }
                    else
                        touchPoints = null;
                    return (target.containsPoint(xy, touchPoints) || target.findTargetCorner(e, this._offset) != null);
                }
                return (target.containsPoint(xy) || target.findTargetCorner(e, this._offset) != null);
            };
            InteractiveCanvas.prototype._normalizePointer = function (object, pointer) {
                var activeGroup = this.getActiveGroup(), x = pointer.x, y = pointer.y;
                var isObjectInGroup = (activeGroup &&
                    object.getType() !== 'group' &&
                    activeGroup.contains(object));
                if (isObjectInGroup) {
                    x -= activeGroup.getLeft();
                    y -= activeGroup.getTop();
                }
                return { x: x, y: y };
            };
            InteractiveCanvas.prototype.isTargetTransparent = function (target, x, y) {
                var hasBorders = target['hasBorders'], transparentCorners = target.transparentCorners;
                target['hasBorders'] = target.transparentCorners = false;
                this._draw(this.contextCache, target);
                target['hasBorders'] = hasBorders;
                target.transparentCorners = transparentCorners;
                var isTransparent = NxtControl.Util.isTransparent(this.contextCache, x, y, this.targetFindTolerance);
                this.clearContext(this.contextCache);
                return isTransparent;
            };
            InteractiveCanvas.prototype._shouldClearSelection = function (e, target) {
                var activeGroup = this.getActiveGroup(), activeObject = this.getActiveObject();
                return (!target
                    ||
                        (target &&
                            activeGroup &&
                            !activeGroup.contains(target) &&
                            activeGroup !== target &&
                            !e.shiftKey)
                    ||
                        (target && !target.evented)
                    ||
                        (target &&
                            !target.selectable &&
                            activeObject &&
                            activeObject !== target));
            };
            InteractiveCanvas.prototype._shouldCenterTransform = function (e, target) {
                if (!target)
                    return;
                var t = this._currentTransform, centerTransform;
                if (t.action === 'scale' || t.action === 'scaleX' || t.action === 'scaleY') {
                    centerTransform = this.centeredScaling || target.centeredScaling;
                }
                else if (t.action === 'rotate') {
                    centerTransform = this.centeredRotation || target.centeredRotation;
                }
                return centerTransform ? !e.altKey : e.altKey;
            };
            InteractiveCanvas.prototype._getOriginFromCorner = function (target, corner) {
                var origin = {
                    x: target.originX,
                    y: target.originY
                };
                if (corner === 'ml' || corner === 'tl' || corner === 'bl') {
                    origin.x = 'right';
                }
                else if (corner === 'mr' || corner === 'tr' || corner === 'br') {
                    origin.x = 'left';
                }
                if (corner === 'tl' || corner === 'mt' || corner === 'tr') {
                    origin.y = 'bottom';
                }
                else if (corner === 'bl' || corner === 'mb' || corner === 'br') {
                    origin.y = 'top';
                }
                return origin;
            };
            InteractiveCanvas.prototype._getActionFromCorner = function (target, corner) {
                var action = 'drag';
                if (corner) {
                    action = (corner === 'ml' || corner === 'mr')
                        ? 'scaleX'
                        : (corner === 'mt' || corner === 'mb')
                            ? 'scaleY'
                            : corner === 'mtr'
                                ? 'rotate'
                                : 'scale';
                }
                return action;
            };
            InteractiveCanvas.prototype._setupCurrentTransform = function (e, target) {
                if (!target)
                    return;
                var corner = target.findTargetCorner(e, this._offset), pointer = getPointer(e, target.getCanvas().upperCanvasEl), action = this._getActionFromCorner(target, corner), origin = this._getOriginFromCorner(target, corner);
                this._currentTransform = {
                    target: target,
                    action: action,
                    scaleX: target.getScaleX(),
                    scaleY: target.getScaleY(),
                    offsetX: pointer.x - target.getLeft(),
                    offsetY: pointer.y - target.getTop(),
                    originX: origin.x,
                    originY: origin.y,
                    ex: pointer.x,
                    ey: pointer.y,
                    left: target.getLeft(),
                    top: target.getTop(),
                    theta: degreesToRadians(target.getAngle()),
                    width: target.getUnscaledWidth() * target.getScaleX(),
                    mouseXSign: 1,
                    mouseYSign: 1
                };
                this._currentTransform.original = {
                    left: target.getLeft(),
                    top: target.getTop(),
                    scaleX: target.getScaleX(),
                    scaleY: target.getScaleY(),
                    originX: origin.x,
                    originY: origin.y
                };
                this._resetCurrentTransform(e);
            };
            InteractiveCanvas.prototype._translateObject = function (x, y) {
                var target = this._currentTransform.target;
                if (!target.get('lockMovementX')) {
                    target.set('left', x - this._currentTransform.offsetX);
                }
                if (!target.get('lockMovementY')) {
                    target.set('top', y - this._currentTransform.offsetY);
                }
            };
            InteractiveCanvas.prototype._scaleObject = function (x, y, by) {
                var t = this._currentTransform, offset = this._offset, target = t.target, lockScalingX = target.get('lockScalingX'), lockScalingY = target.get('lockScalingY');
                if (lockScalingX && lockScalingY)
                    return;
                var constraintPosition = target.translateToOriginPoint(target.getCenterPoint(), t.originX, t.originY);
                var localMouse = target.toLocalPoint(new NxtControl.Drawing.Point(x - offset.left, y - offset.top), t.originX, t.originY);
                this._setLocalMouse(localMouse, t);
                this._setObjectScale(localMouse, t, lockScalingX, lockScalingY, by);
                target.setPositionByOrigin(constraintPosition, t.originX, t.originY);
            };
            InteractiveCanvas.prototype._setObjectScale = function (localMouse, transform, lockScalingX, lockScalingY, by) {
                var target = transform.target;
                transform.newScaleX = target.getScaleX();
                transform.newScaleY = target.getScaleY();
                if (by === 'equally' && !lockScalingX && !lockScalingY) {
                    this._scaleObjectEqually(localMouse, target, transform);
                }
                else if (!by) {
                    transform.newScaleX = localMouse.x / (target.getUnscaledWidth() + (target.getPen() ? target.getPen().width : 0));
                    transform.newScaleY = localMouse.y / (target.getUnscaledHeight() + (target.getPen() ? target.getPen().width : 0));
                    lockScalingX || target.set('scaleX', transform.newScaleX);
                    lockScalingY || target.set('scaleY', transform.newScaleY);
                }
                else if (by === 'x' && !target.get('lockUniScaling')) {
                    transform.newScaleX = localMouse.x / (target.getUnscaledWidth() + (target.getPen() ? target.getPen().width : 0));
                    lockScalingX || target.set('scaleX', transform.newScaleX);
                }
                else if (by === 'y' && !target.get('lockUniScaling')) {
                    transform.newScaleY = localMouse.y / (target.getUnscaledHeight() + (target.getPen() ? target.getPen().width : 0));
                    lockScalingY || target.set('scaleY', transform.newScaleY);
                }
                this._flipObject(transform);
            };
            InteractiveCanvas.prototype._scaleObjectEqually = function (localMouse, target, transform) {
                var dist = localMouse.y + localMouse.x;
                var lastDist = (target.getUnscaledHeight() + (target.getPen() ? target.getPen().width : 0)) * transform.original.scaleY +
                    (target.getUnscaledWidth() + (target.getPen() ? target.getPen().width : 0)) * transform.original.scaleX;
                transform.newScaleX = transform.original.scaleX * dist / lastDist;
                transform.newScaleY = transform.original.scaleY * dist / lastDist;
                target.set('scaleX', transform.newScaleX);
                target.set('scaleY', transform.newScaleY);
            };
            InteractiveCanvas.prototype._flipObject = function (transform) {
                if (transform.newScaleX < 0) {
                    if (transform.originX === 'left') {
                        transform.originX = 'right';
                    }
                    else if (transform.originX === 'right') {
                        transform.originX = 'left';
                    }
                }
                if (transform.newScaleY < 0) {
                    if (transform.originY === 'top') {
                        transform.originY = 'bottom';
                    }
                    else if (transform.originY === 'bottom') {
                        transform.originY = 'top';
                    }
                }
            };
            InteractiveCanvas.prototype._setLocalMouse = function (localMouse, t) {
                var target = t.target;
                if (t.originX === 'right') {
                    localMouse.x *= -1;
                }
                else if (t.originX === 'center') {
                    localMouse.x *= t.mouseXSign * 2;
                    if (localMouse.x < 0) {
                        t.mouseXSign = -t.mouseXSign;
                    }
                }
                if (t.originY === 'bottom') {
                    localMouse.y *= -1;
                }
                else if (t.originY === 'center') {
                    localMouse.y *= t.mouseYSign * 2;
                    if (localMouse.y < 0) {
                        t.mouseYSign = -t.mouseYSign;
                    }
                }
                if (abs(localMouse.x) > target.padding) {
                    if (localMouse.x < 0) {
                        localMouse.x += target.padding;
                    }
                    else {
                        localMouse.x -= target.padding;
                    }
                }
                else {
                    localMouse.x = 0;
                }
                if (abs(localMouse.y) > target.padding) {
                    if (localMouse.y < 0) {
                        localMouse.y += target.padding;
                    }
                    else {
                        localMouse.y -= target.padding;
                    }
                }
                else {
                    localMouse.y = 0;
                }
            };
            InteractiveCanvas.prototype._rotateObject = function (x, y) {
                var t = this._currentTransform, o = this._offset;
                if (t.target.get('lockRotation'))
                    return;
                var lastAngle = atan2(t.ey - t.top - o.top, t.ex - t.left - o.left), curAngle = atan2(y - t.top - o.top, x - t.left - o.left), angle = radiansToDegrees(curAngle - lastAngle + t.theta);
                if (angle < 0) {
                    angle = 360 + angle;
                }
                t.target.angle = angle;
            };
            InteractiveCanvas.prototype._setCursor = function (value) {
                this.upperCanvasEl.style.cursor = value;
            };
            InteractiveCanvas.prototype._resetObjectTransform = function (target) {
                target.scaleX = 1;
                target.scaleY = 1;
                target.setAngle(0);
            };
            InteractiveCanvas.prototype._drawSelection = function () {
                var ctx = this.contextTop, groupSelector = this._groupSelector, left = groupSelector.left, top = groupSelector.top, aleft = abs(left), atop = abs(top);
                ctx.fillStyle = this.selectionColor.toLive();
                ctx.fillRect(groupSelector.ex - ((left > 0) ? 0 : -left), groupSelector.ey - ((top > 0) ? 0 : -top), aleft, atop);
                ctx.lineWidth = this.selectionLineWidth;
                ctx.strokeStyle = this.selectionBorderColor.toLive();
                if (this.selectionDashArray.length > 1) {
                    var px = groupSelector.ex + STROKE_OFFSET - ((left > 0) ? 0 : aleft);
                    var py = groupSelector.ey + STROKE_OFFSET - ((top > 0) ? 0 : atop);
                    ctx.beginPath();
                    NxtControl.Util.drawDashedLine(ctx, px, py, px + aleft, py, this.selectionDashArray);
                    NxtControl.Util.drawDashedLine(ctx, px, py + atop - 1, px + aleft, py + atop - 1, this.selectionDashArray);
                    NxtControl.Util.drawDashedLine(ctx, px, py, px, py + atop, this.selectionDashArray);
                    NxtControl.Util.drawDashedLine(ctx, px + aleft - 1, py, px + aleft - 1, py + atop, this.selectionDashArray);
                    ctx.closePath();
                    ctx.stroke();
                }
                else {
                    ctx.strokeRect(groupSelector.ex + STROKE_OFFSET - ((left > 0) ? 0 : aleft), groupSelector.ey + STROKE_OFFSET - ((top > 0) ? 0 : atop), aleft, atop);
                }
            };
            InteractiveCanvas.prototype._isLastRenderedObject = function (e) {
                return (this.controlsAboveOverlay &&
                    this.lastRenderedObjectWithControlsAboveOverlay &&
                    this.lastRenderedObjectWithControlsAboveOverlay.getVisible() &&
                    this.containsPoint(e, this.lastRenderedObjectWithControlsAboveOverlay) &&
                    this.lastRenderedObjectWithControlsAboveOverlay.findTargetCorner(e, this._offset));
            };
            InteractiveCanvas.prototype.findTarget = function (e, skipGroup) {
                if (this.skipTargetFind)
                    return;
                if (this._isLastRenderedObject(e)) {
                    return this.lastRenderedObjectWithControlsAboveOverlay;
                }
                var activeGroup = this.getActiveGroup();
                if (activeGroup && !skipGroup && this.containsPoint(e, activeGroup)) {
                    return activeGroup;
                }
                return this._searchPossibleTargets(e);
            };
            InteractiveCanvas.prototype._searchPossibleTargets = function (e) {
                var possibleTargets = [], target, pointer = this.getPointer(e);
                for (var i = this._objects.length; i--;) {
                    if (this._objects[i] &&
                        this._objects[i].getVisible() &&
                        this._objects[i].evented &&
                        this.containsPoint(e, this._objects[i])) {
                        if (this.perPixelTargetFind || this._objects[i].perPixelTargetFind) {
                            possibleTargets[possibleTargets.length] = this._objects[i];
                        }
                        else {
                            target = this._objects[i];
                            this.relatedTarget = target;
                            break;
                        }
                    }
                }
                for (var j = 0, len = possibleTargets.length; j < len; j++) {
                    pointer = this.getPointer(e);
                    var isTransparent = this.isTargetTransparent(possibleTargets[j], pointer.x, pointer.y);
                    if (!isTransparent) {
                        target = possibleTargets[j];
                        this.relatedTarget = target;
                        break;
                    }
                }
                return target;
            };
            InteractiveCanvas.prototype.getPointer = function (e) {
                var pointer = getPointer(e, this.upperCanvasEl);
                return {
                    x: pointer.x - this._offset.left,
                    y: pointer.y - this._offset.top
                };
            };
            InteractiveCanvas.prototype._createUpperCanvas = function () {
                var lowerCanvasClass = this.lowerCanvasEl.className.replace(/\s*lower-canvas\s*/, '');
                this.upperCanvasEl = this._createCanvasElement();
                NxtControl.Util.addClass(this.upperCanvasEl, 'upper-canvas ' + lowerCanvasClass);
                this.wrapperEl.appendChild(this.upperCanvasEl);
                this._copyCanvasStyle(this.lowerCanvasEl, this.upperCanvasEl);
                this._applyCanvasStyle(this.upperCanvasEl);
                this.contextTop = this.upperCanvasEl.getContext('2d');
            };
            InteractiveCanvas.prototype._createCacheCanvas = function () {
                this.cacheCanvasEl = this._createCanvasElement();
                this.cacheCanvasEl.setAttribute('width', this.width.toString());
                this.cacheCanvasEl.setAttribute('height', this.height.toString());
                this.contextCache = this.cacheCanvasEl.getContext('2d');
            };
            InteractiveCanvas.prototype._initWrapperElement = function () {
                this.wrapperEl = NxtControl.Util.wrapElement(this.lowerCanvasEl, 'div', {
                    'class': this.containerClass
                });
                NxtControl.Util.setStyle(this.wrapperEl, {
                    width: this.getWidth() + 'px',
                    height: this.getHeight() + 'px',
                    position: 'relative'
                });
                NxtControl.Util.makeElementUnselectable(this.wrapperEl);
            };
            InteractiveCanvas.prototype._applyCanvasStyle = function (element) {
                var width = this.getWidth() || element.width, height = this.getHeight() || element.height;
                NxtControl.Util.setStyle(element, {
                    position: 'absolute',
                    width: width + 'px',
                    height: height + 'px',
                    left: 0,
                    top: 0
                });
                element.width = width;
                element.height = height;
                NxtControl.Util.makeElementUnselectable(element);
            };
            InteractiveCanvas.prototype._copyCanvasStyle = function (fromEl, toEl) {
                toEl.style.cssText = fromEl.style.cssText;
            };
            InteractiveCanvas.prototype.getSelectionContext = function () {
                return this.contextTop;
            };
            InteractiveCanvas.prototype.getSelectionElement = function () {
                return this.upperCanvasEl;
            };
            InteractiveCanvas.prototype._setActiveObject = function (object) {
                if (this._activeObject) {
                    this._activeObject.set('active', false);
                }
                this._activeObject = object;
                object.set('active', true);
            };
            InteractiveCanvas.prototype.setActiveObject = function (object, e) {
                this._setActiveObject(object);
                this.renderAll();
                this.selected.trigger(this, { target: object, e: e });
                object.selected.trigger(object, { e: e });
                return this;
            };
            InteractiveCanvas.prototype.getActiveObject = function () {
                return this._activeObject;
            };
            InteractiveCanvas.prototype._discardActiveObject = function () {
                if (this._activeObject) {
                    this._activeObject.set('active', false);
                }
                this._activeObject = null;
            };
            InteractiveCanvas.prototype.discardActiveObject = function (e) {
                this._discardActiveObject();
                this.renderAll();
                this.selectionCleared.trigger(this, { e: e });
                return this;
            };
            InteractiveCanvas.prototype._setActiveGroup = function (group) {
                this._activeGroup = group;
                if (group) {
                    group.canvas = this;
                    group.set('active', true);
                }
            };
            InteractiveCanvas.prototype.setActiveGroup = function (group, e) {
                this._setActiveGroup(group);
                if (group) {
                    this.selected.trigger(this, { target: group, e: e });
                    group.selected.trigger(group, { e: e });
                }
                return this;
            };
            InteractiveCanvas.prototype.getActiveGroup = function () {
                return this._activeGroup;
            };
            InteractiveCanvas.prototype._discardActiveGroup = function () {
                var g = this.getActiveGroup();
                if (g) {
                    g.destroy();
                }
                this.setActiveGroup(null);
            };
            InteractiveCanvas.prototype.discardActiveGroup = function (e) {
                this._discardActiveGroup();
                this.selectionCleared.trigger(this, { e: e });
                return this;
            };
            InteractiveCanvas.prototype.deactivateAll = function () {
                var allObjects = this.getObjects(), i = 0, len = allObjects.length;
                for (; i < len; i++) {
                    allObjects[i].set('active', false);
                }
                this._discardActiveGroup();
                this._discardActiveObject();
                return this;
            };
            InteractiveCanvas.prototype.captureMouse = function (shape) {
                this.isDrawingMode = shape !== null;
                this._isCurrentlyDrawing = shape !== null;
                this._captureMouseObject = shape;
            };
            InteractiveCanvas.prototype.deactivateAllWithDispatch = function (e) {
                var activeObject = this.getActiveGroup() || this.getActiveObject();
                if (activeObject) {
                    this.beforeSelectionCleared.trigger(this, { target: activeObject, e: e });
                }
                this.deactivateAll();
                if (activeObject) {
                    this.selectionCleared.trigger(this, { e: e });
                }
                return this;
            };
            InteractiveCanvas.prototype.drawControls = function (ctx) {
                var activeGroup = this.getActiveGroup();
                if (activeGroup) {
                    this._drawGroupControls(ctx, activeGroup);
                }
                else {
                    this._drawObjectsControls(ctx);
                }
            };
            InteractiveCanvas.prototype._drawGroupControls = function (ctx, activeGroup) {
                this._drawControls(ctx, activeGroup, 'Group');
            };
            InteractiveCanvas.prototype._drawObjectsControls = function (ctx) {
                for (var i = 0, len = this._objects.length; i < len; ++i) {
                    if (!this._objects[i] || !this._objects[i].active)
                        continue;
                    this._drawControls(ctx, this._objects[i], 'Rectangle');
                    this.lastRenderedObjectWithControlsAboveOverlay = this._objects[i];
                }
            };
            InteractiveCanvas.prototype._drawControls = function (ctx, object, klass) {
                ctx.save();
                NxtControl.GuiFramework[klass].prototype.transform.call(object, ctx);
                object.drawBorders(ctx).drawControls(ctx);
                ctx.restore();
            };
            InteractiveCanvas.prototype._initEventListeners = function () {
                this._bindEvents();
                addListener(System.window, 'resize', this._onResize);
                if (System.window.PointerEvent) {
                    addListener(this.upperCanvasEl, 'pointerdown', this._onMouseDown);
                    addListener(this.upperCanvasEl, 'pointermove', this._onMouseMove);
                }
                else if (System.window.MSPointerEvent) {
                    addListener(this.upperCanvasEl, 'MSPointerDown', this._onMouseDown);
                    addListener(this.upperCanvasEl, 'MSPointerMove', this._onMouseMove);
                }
                else {
                    addListener(this.upperCanvasEl, 'mousedown', this._onMouseDown);
                    addListener(this.upperCanvasEl, 'mousemove', this._onMouseMove);
                    addListener(this.upperCanvasEl, 'touchstart', this._onTouchDown);
                    addListener(this.upperCanvasEl, 'touchmove', this._onTouchMove);
                }
                addListener(this.upperCanvasEl, 'mousewheel', this._onMouseWheel);
                addListener(this.upperCanvasEl, 'click', this._onMouseClick);
                if (typeof Event !== 'undefined' && 'add' in Event) {
                    Event.add(this.upperCanvasEl, 'gesture', this._onGesture);
                    Event.add(this.upperCanvasEl, 'drag', this._onDrag);
                    Event.add(this.upperCanvasEl, 'orientation', this._onOrientationChange);
                    Event.add(this.upperCanvasEl, 'shake', this._onShake);
                }
            };
            InteractiveCanvas.prototype._bindEvents = function () {
                this._onMouseDown = this._onMouseDown.bind(this);
                this._onMouseMove = this._onMouseMove.bind(this);
                this._onMouseUp = this._onMouseUp.bind(this);
                this._onTouchDown = this._onTouchDown.bind(this);
                this._onTouchMove = this._onTouchMove.bind(this);
                this._onTouchEnd = this._onTouchEnd.bind(this);
                this._onMouseClick = this._onMouseClick.bind(this);
                this._onResize = this._onResize.bind(this);
                this._onGesture = this._onGesture.bind(this);
                this._onDrag = this._onDrag.bind(this);
                this._onShake = this._onShake.bind(this);
                this._onOrientationChange = this._onOrientationChange.bind(this);
                this._onMouseWheel = this._onMouseWheel.bind(this);
            };
            InteractiveCanvas.prototype.removeListeners = function () {
                removeListener(System.window, 'resize', this._onResize);
                if (System.window.PointerEvent) {
                    removeListener(this.upperCanvasEl, 'pointerdown', this._onMouseDown);
                    removeListener(this.upperCanvasEl, 'pointermove', this._onMouseMove);
                }
                else if (System.window.MSPointerEvent) {
                    removeListener(this.upperCanvasEl, 'MSPointerDown', this._onMouseDown);
                    removeListener(this.upperCanvasEl, 'MSPointerMove', this._onMouseMove);
                }
                else {
                    removeListener(this.upperCanvasEl, 'mousedown', this._onMouseDown);
                    removeListener(this.upperCanvasEl, 'mousemove', this._onMouseMove);
                    removeListener(this.upperCanvasEl, 'touchstart', this._onTouchDown);
                    removeListener(this.upperCanvasEl, 'touchmove', this._onTouchMove);
                }
                removeListener(this.upperCanvasEl, 'mousewheel', this._onMouseWheel);
                removeListener(this.upperCanvasEl, 'click', this._onMouseClick);
                if (typeof Event !== 'undefined' && 'remove' in Event) {
                    Event.remove(this.upperCanvasEl, 'gesture', this._onGesture);
                    Event.remove(this.upperCanvasEl, 'drag', this._onDrag);
                    Event.remove(this.upperCanvasEl, 'orientation', this._onOrientationChange);
                    Event.remove(this.upperCanvasEl, 'shake', this._onShake);
                }
            };
            InteractiveCanvas.prototype._onGesture = function (e, s) {
                this.__onTransformGesture && this.__onTransformGesture(e, s);
            };
            InteractiveCanvas.prototype._onDrag = function (e, s) {
                this.__onDrag && this.__onDrag(e, s);
            };
            InteractiveCanvas.prototype.__onMouseWheel = function (e, s) { };
            InteractiveCanvas.prototype._onMouseWheel = function (e, s) {
                this.__onMouseWheel && this.__onMouseWheel(e, s);
            };
            InteractiveCanvas.prototype._onOrientationChange = function (e, s) {
                this.__onOrientationChange && this.__onOrientationChange(e, s);
            };
            InteractiveCanvas.prototype._onShake = function (e, s) {
                this.__onShake && this.__onShake(e, s);
            };
            InteractiveCanvas.prototype._onTouchDown = function (e) {
                if (e.touches.length != 1) {
                    this.isMultiTouch = true;
                    return;
                }
                this._onMouseDown(e);
            };
            InteractiveCanvas.prototype._onMouseDown = function (e) {
                if (this.__onMouseDown(e) != null) {
                    if (e.type === 'touchstart' && e.touches.length == 1)
                        e.preventDefault && e.preventDefault();
                }
                if (System.window.PointerEvent) {
                    addListener(System.document, 'pointermove', this._onMouseMove);
                    addListener(System.document, 'pointerup', this._onMouseUp);
                    removeListener(this.upperCanvasEl, 'pointermove', this._onMouseMove);
                }
                else if (System.window.MSPointerEvent) {
                    addListener(System.document, 'MSPointerMove', this._onMouseMove);
                    addListener(System.document, 'MSPointerUp', this._onMouseUp);
                    removeListener(this.upperCanvasEl, 'MSPointerMove', this._onMouseMove);
                }
                else {
                    addListener(System.document, 'mouseup', this._onMouseUp);
                    addListener(System.document, 'touchend', this._onTouchEnd);
                    addListener(System.document, 'mousemove', this._onMouseMove);
                    addListener(System.document, 'touchmove', this._onTouchMove);
                    removeListener(this.upperCanvasEl, 'mousemove', this._onMouseMove);
                    removeListener(this.upperCanvasEl, 'touchmove', this._onTouchMove);
                }
            };
            InteractiveCanvas.prototype._onMouseClick = function (e) {
                this.__onMouseClick(e);
            };
            InteractiveCanvas.prototype._onTouchEnd = function (e) {
                if (e.touches.length + e.changedTouches.length > 1)
                    return;
                if (this.isMultiTouch) {
                    this.isMultiTouch = false;
                    this.detachFromMouseUp();
                    return;
                }
                this._onMouseUp(e);
            };
            InteractiveCanvas.prototype._onMouseUp = function (e) {
                if (this.__onMouseUp(e) != null) {
                    if (e.type === 'touchstart' && e.touches.length == 1)
                        e.preventDefault && e.preventDefault();
                }
                this.detachFromMouseUp();
            };
            InteractiveCanvas.prototype.detachFromMouseUp = function () {
                if (System.window.PointerEvent) {
                    removeListener(System.document, 'pointermove', this._onMouseMove);
                    removeListener(System.document, 'pointerup', this._onMouseUp);
                    addListener(this.upperCanvasEl, 'pointermove', this._onMouseMove);
                }
                else if (System.window.MSPointerEvent) {
                    removeListener(System.document, 'MSPointerMove', this._onMouseMove);
                    removeListener(System.document, 'MSPointerUp', this._onMouseUp);
                    addListener(this.upperCanvasEl, 'MSPointerMove', this._onMouseMove);
                }
                else {
                    removeListener(System.document, 'mouseup', this._onMouseUp);
                    removeListener(System.document, 'touchend', this._onTouchEnd);
                    removeListener(System.document, 'mousemove', this._onMouseMove);
                    removeListener(System.document, 'touchmove', this._onTouchMove);
                    addListener(this.upperCanvasEl, 'mousemove', this._onMouseMove);
                    addListener(this.upperCanvasEl, 'touchmove', this._onTouchMove);
                }
            };
            InteractiveCanvas.prototype._onTouchMove = function (e) {
                if (e.touches.length != 1) {
                    this.isMultiTouch = true;
                    return;
                }
                if (!this.isMultiTouch)
                    this._onMouseMove(e);
            };
            InteractiveCanvas.prototype._onMouseMove = function (e) {
                if (this.__onMouseMove(e) != null) {
                    if (e.type === 'touchstart' && e.touches.length == 1)
                        e.preventDefault && e.preventDefault();
                }
            };
            InteractiveCanvas.prototype._onResize = function () {
                this.calcOffset();
            };
            InteractiveCanvas.prototype._shouldRender = function (target, pointer) {
                var activeObject = this.getActiveGroup() || this.getActiveObject();
                return !!((target && (target.isMoving ||
                    target !== activeObject))
                    ||
                        (!target && !!activeObject)
                    ||
                        (!target && !activeObject && !this._groupSelector)
                    ||
                        (pointer &&
                            this._previousPointer &&
                            this.selection && (pointer.x !== this._previousPointer.x ||
                            pointer.y !== this._previousPointer.y)));
            };
            InteractiveCanvas.prototype.__onMouseUp = function (e) {
                var target;
                if (this.isDrawingMode && this._isCurrentlyDrawing) {
                    this._onMouseUpInDrawingMode(e);
                    return null;
                }
                if (this._currentTransform) {
                    this._finalizeCurrentTransform();
                    target = this._currentTransform.target;
                }
                else {
                    target = this.findTarget(e, true);
                }
                var shouldRender = this._shouldRender(target, this.getPointer(e));
                this._maybeGroupObjects(e);
                if (target) {
                    target.isMoving = false;
                }
                shouldRender && this.renderAll();
                this._handleCursorAndEvent(e, target);
                return target;
            };
            InteractiveCanvas.prototype._handleCursorAndEvent = function (e, target) {
                this._setCursorFromEvent(e, target);
                var _this = this;
                setTimeout(function () {
                    _this._setCursorFromEvent(e, target);
                }, 50);
                this.mouseup.trigger(this, { target: target, e: e });
                target && target.mouseup.trigger(target, { e: e });
            };
            InteractiveCanvas.prototype._finalizeCurrentTransform = function () {
                var transform = this._currentTransform;
                var target = transform.target;
                target.setCoords();
                if (this.stateful && target.hasStateChanged()) {
                    this.modified.trigger(this, { target: target });
                    target.modified.trigger(this, NxtControl.EmptyEventArgs);
                }
                this._restoreOriginXY(target);
            };
            InteractiveCanvas.prototype._restoreOriginXY = function (target) {
                if (this._previousOriginX && this._previousOriginY) {
                    var originPoint = target.translateToOriginPoint(target.getCenterPoint(), this._previousOriginX, this._previousOriginY);
                    target.originX = this._previousOriginX;
                    target.originY = this._previousOriginY;
                    target.left = originPoint.x;
                    target.top = originPoint.y;
                    this._previousOriginX = null;
                    this._previousOriginY = null;
                }
            };
            InteractiveCanvas.prototype._onMouseDownInDrawingMode = function (e) {
                this._isCurrentlyDrawing = true;
                this.discardActiveObject(e).renderAll();
                if (this.clipTo) {
                    NxtControl.Util.Clip.clipContext(this, this.contextTop);
                }
                this.mousedown.trigger(this, { e: e });
            };
            InteractiveCanvas.prototype._onMouseMoveInDrawingMode = function (e) {
                if (this._isCurrentlyDrawing) {
                    var pointer = this.getPointer(e);
                }
                this.upperCanvasEl.style.cursor = this.freeDrawingCursor;
                this.mousemove.trigger(this, { e: e });
            };
            InteractiveCanvas.prototype._onMouseUpInDrawingMode = function (e) {
                this._isCurrentlyDrawing = false;
                if (this.clipTo) {
                    this.contextTop.restore();
                }
                this.mouseup.trigger(this, { e: e });
            };
            InteractiveCanvas.prototype.__onMouseDown = function (e) {
                var isLeftClick = 'which' in e ? e.which === 1 : e.button === 1;
                if (!isLeftClick && !System.isTouchSupported)
                    return;
                if (this.isDrawingMode) {
                    this._onMouseDownInDrawingMode(e);
                    return;
                }
                if (this._currentTransform)
                    return;
                var target = this.findTarget(e), pointer = this.getPointer(e);
                this._previousPointer = pointer;
                var shouldRender = this._shouldRender(target, pointer), shouldGroup = this._shouldGroup(e, target);
                if (this._shouldClearSelection(e, target)) {
                    this._clearSelection(e, target, pointer);
                }
                else if (shouldGroup) {
                    this._handleGrouping(e, target);
                    target = this.getActiveGroup();
                }
                if (target && target.selectable && !shouldGroup) {
                    this._beforeTransform(e, target);
                    this._setupCurrentTransform(e, target);
                }
                shouldRender && this.renderAll();
                this.mousedown.trigger(this, { target: target, e: e });
                target && target.mousedown.trigger(target, { e: e });
            };
            InteractiveCanvas.prototype.__onMouseClick = function (e) {
                var isLeftClick = 'which' in e ? e.which === 1 : e.button === 1;
                if (!isLeftClick && !System.isTouchSupported)
                    return;
                var target = this.findTarget(e), pointer = this.getPointer(e);
                this.click.trigger(this, { target: target, e: e });
                target && target.click.trigger(target, { e: e });
            };
            InteractiveCanvas.prototype._beforeTransform = function (e, target) {
                var corner;
                this.stateful && target.saveState();
                if ((corner = target.findTargetCorner(e, this._offset))) {
                    this.onBeforeScaleRotate(target);
                }
                if (target !== this.getActiveGroup() && target !== this.getActiveObject()) {
                    this.deactivateAll();
                    this.setActiveObject(target, e);
                }
            };
            InteractiveCanvas.prototype._clearSelection = function (e, target, pointer) {
                this.deactivateAllWithDispatch(e);
                if (target && target.selectable) {
                    this.setActiveObject(target, e);
                }
                else if (this.selection) {
                    this._groupSelector = {
                        ex: pointer.x,
                        ey: pointer.y,
                        top: 0,
                        left: 0
                    };
                }
            };
            InteractiveCanvas.prototype._setOriginToCenter = function (target) {
                this._previousOriginX = this._currentTransform.target.originX;
                this._previousOriginY = this._currentTransform.target.originY;
                var center = target.getCenterPoint();
                target.originX = 'center';
                target.originY = 'center';
                this._currentTransform.left = target.left = center.x;
                this._currentTransform.top = target.top = center.y;
            };
            InteractiveCanvas.prototype._setCenterToOrigin = function (target) {
                var originPoint = target.translateToOriginPoint(target.getCenterPoint(), this._previousOriginX, this._previousOriginY);
                target.originX = this._previousOriginX;
                target.originY = this._previousOriginY;
                target.left = originPoint.x;
                target.top = originPoint.y;
                this._previousOriginX = null;
                this._previousOriginY = null;
            };
            InteractiveCanvas.prototype.__onMouseMove = function (e) {
                var target, pointer;
                if (this.isDrawingMode) {
                    this._onMouseMoveInDrawingMode(e);
                    return null;
                }
                var groupSelector = this._groupSelector;
                if (groupSelector) {
                    pointer = getPointer(e, this.upperCanvasEl);
                    groupSelector.left = pointer.x - this._offset.left - groupSelector.ex;
                    groupSelector.top = pointer.y - this._offset.top - groupSelector.ey;
                    this.renderTop();
                }
                else if (!this._currentTransform) {
                    target = this.findTarget(e);
                    if (!target || target && !target.selectable) {
                        this.upperCanvasEl.style.cursor = this.defaultCursor;
                    }
                    else {
                        this._setCursorFromEvent(e, target);
                    }
                }
                else {
                    this._transformObject(e);
                }
                this.mousemove.trigger(this, { target: target, e: e });
                target && target.mousemove.trigger(this, { e: e });
                return target;
            };
            InteractiveCanvas.prototype._transformObject = function (e) {
                var pointer = getPointer(e, this.upperCanvasEl), transform = this._currentTransform;
                transform.reset = false,
                    transform.target.isMoving = true;
                this._beforeScaleTransform(e, transform);
                this._performTransformAction(e, transform, pointer);
                this.renderAll();
            };
            InteractiveCanvas.prototype._performTransformAction = function (e, transform, pointer) {
                var x = pointer.x, y = pointer.y, target = transform.target, action = transform.action;
                if (action === 'rotate') {
                    this._rotateObject(x, y);
                    this._fire('rotating', target, e);
                }
                else if (action === 'scale') {
                    this._onScale(e, transform, x, y);
                    this._fire('scaling', target, e);
                }
                else if (action === 'scaleX') {
                    this._scaleObject(x, y, 'x');
                    this._fire('scaling', target, e);
                }
                else if (action === 'scaleY') {
                    this._scaleObject(x, y, 'y');
                    this._fire('scaling', target, e);
                }
                else {
                    this._translateObject(x, y);
                    this._fire('moving', target, e);
                    this._setCursor(this.moveCursor);
                }
            };
            InteractiveCanvas.prototype._fire = function (eventName, target, e) {
                switch (eventName) {
                    case 'rotating':
                        this.rotating.trigger(this, { target: target, e: e });
                        target.rotating.trigger(target, { e: e });
                        break;
                    case 'scaling':
                        this.scaling.trigger(this, { target: target, e: e });
                        target.scaling.trigger(target, { e: e });
                        break;
                    case 'moving':
                        this.moving.trigger(this, { target: target, e: e });
                        target.moving.trigger(target, { e: e });
                        break;
                }
            };
            InteractiveCanvas.prototype._beforeScaleTransform = function (e, transform) {
                if (transform.action === 'scale' || transform.action === 'scaleX' || transform.action === 'scaleY') {
                    var centerTransform = this._shouldCenterTransform(e, transform.target);
                    if ((centerTransform && (transform.originX !== 'center' || transform.originY !== 'center')) ||
                        (!centerTransform && transform.originX === 'center' && transform.originY === 'center')) {
                        this._resetCurrentTransform(e);
                        transform.reset = true;
                    }
                }
            };
            InteractiveCanvas.prototype._onScale = function (e, transform, x, y) {
                if ((e.shiftKey || this.uniScaleTransform) && !transform.target.get('lockUniScaling')) {
                    transform.currentAction = 'scale';
                    this._scaleObject(x, y);
                }
                else {
                    if (!transform.reset && transform.currentAction === 'scale') {
                        this._resetCurrentTransform(e);
                    }
                    transform.currentAction = 'scaleEqually';
                    this._scaleObject(x, y, 'equally');
                }
            };
            InteractiveCanvas.prototype._setCursorFromEvent = function (e, target) {
                var style = this.upperCanvasEl.style;
                if (!target || !target.selectable) {
                    style.cursor = this.defaultCursor;
                    return false;
                }
                else {
                    var activeGroup = this.getActiveGroup();
                    var corner = target.findTargetCorner
                        && (!activeGroup || !activeGroup.contains(target))
                        && target.findTargetCorner(e, this._offset);
                    if (!corner) {
                        style.cursor = target.hoverCursor || this.hoverCursor;
                    }
                    else {
                        this._setCornerCursor(corner, target);
                    }
                }
                return true;
            };
            InteractiveCanvas.prototype._setCornerCursor = function (corner, target) {
                var style = this.upperCanvasEl.style;
                if (corner in cursorOffset) {
                    style.cursor = this._getRotatedCornerCursor(corner, target);
                }
                else if (corner === 'mtr' && target.hasRotatingPoint) {
                    style.cursor = this.rotationCursor;
                }
                else {
                    style.cursor = this.defaultCursor;
                    return false;
                }
            };
            InteractiveCanvas.prototype._getRotatedCornerCursor = function (corner, target) {
                var n = Math.round((target.getAngle() % 360) / 45);
                if (n < 0) {
                    n += 8;
                }
                n += cursorOffset[corner];
                n %= 8;
                return cursorMap[n];
            };
            InteractiveCanvas.prototype._shouldGroup = function (e, target) {
                var activeObject = this.getActiveObject();
                return e.shiftKey &&
                    (this.getActiveGroup() || (activeObject && activeObject !== target))
                    && this.selection;
            };
            InteractiveCanvas.prototype._handleGrouping = function (e, target) {
                if (target === this.getActiveGroup()) {
                    target = this.findTarget(e, true);
                    if (!target || target.isType('group')) {
                        return;
                    }
                }
                if (this.getActiveGroup()) {
                    this._updateActiveGroup(target, e);
                }
                else {
                    this._createActiveGroup(target, e);
                }
                if (this._activeGroup) {
                    this._activeGroup.saveCoords();
                }
            };
            InteractiveCanvas.prototype._updateActiveGroup = function (target, e) {
                var activeGroup = this.getActiveGroup();
                if (activeGroup.contains(target)) {
                    activeGroup.removeWithUpdate(target);
                    this._resetObjectTransform(activeGroup);
                    target.set('active', false);
                    if (activeGroup.size() === 1) {
                        this.discardActiveGroup(e);
                        this.setActiveObject(activeGroup.item(0));
                        return;
                    }
                }
                else {
                    activeGroup.addWithUpdate(target);
                    this._resetObjectTransform(activeGroup);
                }
                this.selectionCreated.trigger(this, { target: activeGroup, e: e });
                activeGroup.set('active', true);
            };
            InteractiveCanvas.prototype._createActiveGroup = function (target, e) {
                if (this._activeObject && target !== this._activeObject) {
                    var group = this._createGroup(target);
                    this.setActiveGroup(group);
                    this._activeObject = null;
                    this.selectionCreated.trigger(this, { target: group, e: e });
                }
                target.set('active', true);
            };
            InteractiveCanvas.prototype._createGroup = function (target) {
                var objects = this.getObjects();
                var isActiveLower = objects.indexOf(this._activeObject) < objects.indexOf(target);
                var groupObjects = isActiveLower
                    ? [this._activeObject, target]
                    : [target, this._activeObject];
                return new GuiFramework.Group().load({
                    originX: 'center',
                    originY: 'center'
                }, groupObjects);
            };
            InteractiveCanvas.prototype._groupSelectedObjects = function (e) {
                var group = this._collectObjects();
                if (group.length === 1) {
                    this.setActiveObject(group[0], e);
                }
                else if (group.length > 1) {
                    var grp = new GuiFramework.Group().load({
                        originX: 'center',
                        originY: 'center'
                    }, group.reverse());
                    this.setActiveGroup(grp, e);
                    grp.saveCoords();
                    this.selectionCreated.trigger(this, { target: grp, e: e });
                    this.renderAll();
                }
            };
            InteractiveCanvas.prototype._collectObjects = function () {
                var group = [], currentObject, x1 = this._groupSelector.ex, y1 = this._groupSelector.ey, x2 = x1 + this._groupSelector.left, y2 = y1 + this._groupSelector.top, selectionX1Y1 = new NxtControl.Drawing.Point(Math.min(x1, x2), Math.min(y1, y2)), selectionX2Y2 = new NxtControl.Drawing.Point(Math.max(x1, x2), Math.max(y1, y2)), isClick = x1 === x2 && y1 === y2;
                for (var i = this._objects.length; i--;) {
                    currentObject = this._objects[i];
                    if (!currentObject || !currentObject.selectable || !currentObject.visible)
                        continue;
                    if (currentObject.intersectsWithRect(selectionX1Y1, selectionX2Y2) ||
                        currentObject.isContainedWithinRect(selectionX1Y1, selectionX2Y2) ||
                        currentObject.containsPoint(selectionX1Y1) ||
                        currentObject.containsPoint(selectionX2Y2)) {
                        currentObject.set('active', true);
                        group.push(currentObject);
                        if (isClick)
                            break;
                    }
                }
                return group;
            };
            InteractiveCanvas.prototype._maybeGroupObjects = function (e) {
                if (this.selection && this._groupSelector) {
                    this._groupSelectedObjects(e);
                }
                var activeGroup = this.getActiveGroup();
                if (activeGroup) {
                    activeGroup.setObjectsCoords().setCoords();
                    activeGroup.isMoving = false;
                    this._setCursor(this.defaultCursor);
                }
                this._groupSelector = null;
                this._currentTransform = null;
            };
            __decorate([
                System.DefaultValue(false)
            ], InteractiveCanvas.prototype, "uniScaleTransform", void 0);
            __decorate([
                System.DefaultValue(false)
            ], InteractiveCanvas.prototype, "centeredScaling", void 0);
            __decorate([
                System.DefaultValue(false)
            ], InteractiveCanvas.prototype, "centeredRotation", void 0);
            __decorate([
                System.DefaultValue(true)
            ], InteractiveCanvas.prototype, "interactive", void 0);
            __decorate([
                System.DefaultValue(true)
            ], InteractiveCanvas.prototype, "selection", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Color({ rgba: [100, 100, 255, 0.3] }))
            ], InteractiveCanvas.prototype, "selectionColor", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Color({ rgba: [255, 255, 255, 0.3] }))
            ], InteractiveCanvas.prototype, "selectionBorderColor", void 0);
            __decorate([
                System.DefaultValue(1)
            ], InteractiveCanvas.prototype, "selectionLineWidth", void 0);
            __decorate([
                System.DefaultValue('move')
            ], InteractiveCanvas.prototype, "hoverCursor", void 0);
            __decorate([
                System.DefaultValue('move')
            ], InteractiveCanvas.prototype, "moveCursor", void 0);
            __decorate([
                System.DefaultValue('default')
            ], InteractiveCanvas.prototype, "defaultCursor", void 0);
            __decorate([
                System.DefaultValue('crosshair')
            ], InteractiveCanvas.prototype, "freeDrawingCursor", void 0);
            __decorate([
                System.DefaultValue('crosshair')
            ], InteractiveCanvas.prototype, "rotationCursor", void 0);
            __decorate([
                System.DefaultValue('canvas-container')
            ], InteractiveCanvas.prototype, "containerClass", void 0);
            __decorate([
                System.DefaultValue(false)
            ], InteractiveCanvas.prototype, "perPixelTargetFind", void 0);
            __decorate([
                System.DefaultValue(0)
            ], InteractiveCanvas.prototype, "targetFindTolerance", void 0);
            __decorate([
                System.DefaultValue(false)
            ], InteractiveCanvas.prototype, "skipTargetFind", void 0);
            return InteractiveCanvas;
        }(GuiFramework.StaticCanvas));
        GuiFramework.InteractiveCanvas = InteractiveCanvas;
        for (var prop in GuiFramework.StaticCanvas) {
            if (prop !== 'prototype') {
                InteractiveCanvas[prop] = GuiFramework.StaticCanvas[prop];
            }
        }
        if (System.isTouchSupported) {
            InteractiveCanvas.prototype._setCursorFromEvent = function (e, target) { };
        }
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend, NGF = NxtControl.GuiFramework, NS = NxtControl.Services;
        var CanvasCore = (function (_super) {
            __extends(CanvasCore, _super);
            function CanvasCore() {
                _super.apply(this, arguments);
                this.ignoreRender = false;
                this._callbackCache = {};
                this._callbackCacheHasNewValues = false;
            }
            CanvasCore.prototype.initialize = function (element, options) {
                var serialized = this.json ? ((typeof this.json === 'string') ? JSON.parse(this.json) : this.json) : {};
                options = options || {};
                var origWidth = serialized.width || 800;
                var origHeight = serialized.height || 520;
                options.width = options.width || origWidth;
                options.height = options.height || origHeight;
                if (serialized != null) {
                    serialized.objects = serialized.objects || [];
                    for (var prop in serialized)
                        if (prop !== 'objects' && prop !== 'width' && prop !== 'height' && prop !== 'background')
                            options[prop] = serialized[prop];
                }
                _super.prototype.initialize.call(this, element, options);
                NS.CanvasTopologyService.instance.currentElement = this.lowerCanvasEl;
                if (serialized !== null)
                    this.loadFromJSON(serialized);
                this.forEachObject(function (o) {
                    o.hasBorders = o.hasControls = o.selectable = false;
                    if (o.getAnchor() !== NGF.AnchorStyles.none) {
                        var r = { x: o.getLeft(), y: o.getTop(), w: o.getWidth(), h: o.getHeight() };
                        r.w = origWidth - (r.x + r.w) - 1;
                        r.h = origHeight - (r.y + r.h) - 1;
                        o.setAnchorOffsets(r);
                        var bounds = { x: o.getLeft(), y: o.getTop(), w: o.getWidth(), h: o.getHeight() };
                        var changed = false;
                        if (options.width !== origWidth) {
                            if ((o.getAnchor() & NGF.AnchorStyles.right) !== 0) {
                                if ((o.getAnchor() & NGF.AnchorStyles.left) !== 0) {
                                    bounds.w = options.width - r.w - bounds.x;
                                    changed = true;
                                }
                                else {
                                    bounds.x = options.width - bounds.w - r.w;
                                    changed = true;
                                }
                            }
                        }
                        if (options.height !== origHeight) {
                            if ((o.getAnchor() & NGF.AnchorStyles.bottom) !== 0) {
                                if ((o.getAnchor() & NGF.AnchorStyles.top) !== 0) {
                                    bounds.h = options.height - r.h - bounds.y;
                                    changed = true;
                                }
                                else {
                                    bounds.y = options.height - bounds.h - r.h;
                                    changed = true;
                                }
                            }
                        }
                        if (changed) {
                            o.left = bounds.x;
                            o.top = bounds.y;
                            o.width = bounds.w;
                            o.height = bounds.h;
                            o.setCoords();
                        }
                    }
                });
            };
            CanvasCore.prototype.dispose = function () {
                this.forEachObject(function (o) {
                    if (typeof (o.dispose) === 'function')
                        o.dispose();
                });
                _super.prototype.dispose.call(this);
                return this;
            };
            CanvasCore.prototype._initEventListeners = function () {
                _super.prototype._initEventListeners.call(this);
                NxtControl.Util.addListener(this.upperCanvasEl, 'contextmenu', function (e) {
                    e.preventDefault && e.preventDefault();
                });
            };
            CanvasCore.prototype.renderAll = function (allOnTop) {
                if (allOnTop === void 0) { allOnTop = false; }
                if (this.ignoreRender)
                    return null;
                return GuiFramework.StaticCanvas.prototype.renderAll.call(this, allOnTop);
            };
            CanvasCore.prototype.setDimensions = function (dimensions, options) {
                var _this = this;
                this.forEachObject(function (o) {
                    if (o.anchor !== NGF.AnchorStyles.none) {
                        var r = o.anchorOffsets;
                        var bounds = { x: o.left, y: o.top, w: o.width, h: o.height };
                        var changed = false;
                        if (dimensions.width !== _this.width) {
                            if ((o.anchor & NGF.AnchorStyles.right) !== 0) {
                                if ((o.anchor & NGF.AnchorStyles.left) !== 0) {
                                    bounds.w = dimensions.width - r.w - bounds.x;
                                    changed = true;
                                }
                                else {
                                    bounds.x = dimensions.width - bounds.w - r.w;
                                    changed = true;
                                }
                            }
                        }
                        if (dimensions.height !== _this.height) {
                            if ((o.anchor & NGF.AnchorStyles.bottom) !== 0) {
                                if ((o.anchor & NGF.AnchorStyles.top) !== 0) {
                                    bounds.h = dimensions.height - r.h - bounds.y;
                                    changed = true;
                                }
                                else {
                                    bounds.y = dimensions.height - bounds.h - r.h;
                                    changed = true;
                                }
                            }
                        }
                        if (changed) {
                            o.left = bounds.x;
                            o.top = bounds.y;
                            o.width = bounds.w;
                            o.height = bounds.h;
                            o.setCoords();
                        }
                    }
                });
                _super.prototype.setDimensions.call(this, dimensions, options);
                return this;
            };
            CanvasCore.prototype._onMouseDownInDrawingMode = function (e) {
                if (this._captureMouseObject === null) {
                    _super.prototype._onMouseDownInDrawingMode.call(this, e);
                    return;
                }
                e.localPointer = this.getPointer(e);
                this._captureMouseObject.mousedown.trigger(this._captureMouseObject, { e: e });
            };
            CanvasCore.prototype._onMouseMoveInDrawingMode = function (e) {
                if (this._captureMouseObject === null) {
                    _super.prototype._onMouseMoveInDrawingMode.call(this, e);
                    return;
                }
                e.localPointer = this.getPointer(e);
                this._captureMouseObject.mousemove.trigger(this._captureMouseObject, { e: e });
            };
            CanvasCore.prototype._onMouseUpInDrawingMode = function (e) {
                if (this._captureMouseObject === null) {
                    _super.prototype._onMouseUpInDrawingMode.call(this, e);
                    return;
                }
                e.localPointer = this.getPointer(e);
                this._captureMouseObject.mouseup.trigger(this._captureMouseObject, { e: e });
            };
            CanvasCore.prototype.__onMouseDown = function (e) {
                var isLeftClick = 'which' in e ? e.which === 1 : e.button === 1;
                if (document.activeElement != document.body)
                    document.activeElement.blur();
                if (isLeftClick && this.isDrawingMode) {
                    this._onMouseDownInDrawingMode(e);
                    return null;
                }
                if (this._currentTransform)
                    return null;
                var target = this.findTarget(e, false), pointer = this.getPointer(e);
                this._previousPointer = pointer;
                var shouldRender = this._shouldRender(target, pointer);
                if (target && target.selectable) {
                    this._beforeTransform(e, target);
                    this._setupCurrentTransform(e, target);
                }
                shouldRender && this.renderAll();
                this.mousedown.trigger(this, { target: target, e: e });
                target && target.mousedown.trigger(target, { e: e });
                return target;
            };
            CanvasCore.prototype.loadFromJSON = function (serialized, callback, reviver) {
                this._objects.length = 0;
                var _this = this;
                NxtControl.Util.enlivenObjects(serialized.objects, function (enlivenedObjects) {
                    enlivenedObjects.forEach(function (obj, index) {
                        _this.insertAt(obj, index, true);
                    });
                    _this._setBgOverlay(serialized);
                    callback && callback();
                }, null, reviver);
                return this;
            };
            CanvasCore.prototype.toObject = function (propertiesToInclude) {
                return extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    width: this.width, height: this.height
                });
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.CanvasCore')
            ], CanvasCore.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.CanvasCore')
            ], CanvasCore.prototype, "csClass", void 0);
            return CanvasCore;
        }(GuiFramework.InteractiveCanvas));
        GuiFramework.CanvasCore = CanvasCore;
        var Canvas = (function (_super) {
            __extends(Canvas, _super);
            function Canvas() {
                _super.apply(this, arguments);
                this._valueChangedTimer = null;
            }
            Canvas.prototype.initialize = function (element, options) {
                options = options || {};
                _super.prototype.initialize.call(this, element, options);
                this._deviceConnected = this._deviceConnected.bind(this);
                this._valueChanged = this._valueChanged.bind(this);
                this._addHistoryValues = this._addHistoryValues.bind(this);
                this._archiveAnswer = this._archiveAnswer.bind(this);
                this._startColorChange = this._startColorChange.bind(this);
                this._stopColorChange = this._stopColorChange.bind(this);
                this._valueChangedTick = this._valueChangedTick.bind(this);
                NS.ColorService.instance.startColorChange.add(this._startColorChange);
                NS.ColorService.instance.stopColorChange.add(this._stopColorChange);
                var _this = this;
                this.forEachObject(function (o) {
                    if (typeof (o.initEvents) === 'function')
                        o.initEvents();
                    if (o._events_ != null) {
                        var l = o._events_.length;
                        for (var i = 0; i < l; i++) {
                            if (typeof (_this[o._events_[i].eventName]) === 'function') {
                                o[o._events_[i].name].add(_this[o._events_[i].eventName].bind(_this));
                            }
                        }
                    }
                });
                if (NS.CommunicationService.instance) {
                    this.connectorInputs = {};
                    this.connectorOutputs = {};
                    this.initializeCommunication();
                }
            };
            Canvas.prototype.dispose = function () {
                var callback, id;
                NS.ColorService.instance.startColorChange.remove(this._startColorChange);
                NS.ColorService.instance.stopColorChange.remove(this._stopColorChange);
                _super.prototype.dispose.call(this);
                var commService = NS.CommunicationService.instance;
                for (id in this.connectorInputs) {
                    if (this.connectorInputs.hasOwnProperty(id)) {
                        callback = this.connectorInputs[id];
                        commService.unsubscribe(true, callback.deviceName, callback.rpath, callback.id);
                    }
                }
                for (id in this.connectorOutputs) {
                    if (this.connectorOutputs.hasOwnProperty(id)) {
                        callback = this.connectorOutputs[id];
                        commService.unsubscribe(false, callback.deviceName, callback.rpath, callback.id);
                    }
                }
                this.connectorInputs = {};
                this.connectorOutputs = {};
                commService.connection.remove(this._deviceConnected);
                if (this._valueChangedTimer !== null) {
                    clearInterval(this._valueChangedTimer);
                    this._valueChangedTimer = null;
                }
                commService.valueChanged.remove(this._valueChanged);
                commService.history.remove(this._addHistoryValues);
                commService.archiveAnswer.remove(this._archiveAnswer);
                return this;
            };
            Canvas.prototype.initializeCommunication = function () {
                if (!this.db)
                    return;
                var commService = NS.CommunicationService.instance;
                var _this = this;
                var deviceName, deviceData, path, pathData;
                commService.connection.add(this._deviceConnected);
                commService.valueChanged.add(this._valueChanged);
                commService.history.add(this._addHistoryValues);
                commService.archiveAnswer.add(this._archiveAnswer);
                var toSubscribe = typeof this.db === 'string' ? JSON.parse(this.db) : this.db;
                var inputs = toSubscribe.input;
                for (deviceName in inputs) {
                    if (commService.isDeviceConnected(deviceName)) {
                        deviceData = inputs[deviceName];
                        for (path in deviceData) {
                            if (!deviceData.hasOwnProperty(path))
                                continue;
                            pathData = deviceData[path];
                            pathData.forEach(function (entry) {
                                for (var i = 0, keys = Object.keys(entry.bases), l = keys.length; i < l; ++i) {
                                    var baseSymPath = keys[i];
                                    var dataId = entry.bases[baseSymPath];
                                    var id = commService.subscribe(true, deviceName, path, entry.eventNr, entry.names, entry.types, dataId);
                                    var callback = {};
                                    callback.id = id;
                                    callback.path = baseSymPath;
                                    callback.deviceName = deviceName;
                                    callback.rpath = path;
                                    _this.connectorInputs[id] = callback;
                                }
                            });
                        }
                    }
                }
                var outputs = toSubscribe.output;
                for (deviceName in outputs) {
                    if (commService.isDeviceConnected(deviceName)) {
                        deviceData = outputs[deviceName];
                        for (path in deviceData) {
                            if (!deviceData.hasOwnProperty(path))
                                continue;
                            pathData = deviceData[path];
                            pathData.forEach(function (entry) {
                                for (var i = 0, keys = Object.keys(entry.bases), l = keys.length; i < l; ++i) {
                                    var baseSymPath = keys[i];
                                    var dataId = entry.bases[baseSymPath];
                                    var id = commService.subscribe(false, deviceName, path, entry.eventNr, entry.names, entry.types, dataId);
                                    var callback = {};
                                    callback.id = id;
                                    callback.path = baseSymPath;
                                    callback.deviceName = deviceName;
                                    callback.rpath = path;
                                    var high = (dataId >> 16) << 16;
                                    if (high !== 0)
                                        callback.dataId = dataId - high;
                                    else
                                        callback.dataId = dataId;
                                    var shape = _this.find(callback.path);
                                    if (shape) {
                                        callback.shape = shape;
                                        shape.callbackdata = callback;
                                    }
                                    _this.connectorOutputs[id] = callback;
                                }
                            });
                        }
                    }
                }
                this._valueChangedTimer = setInterval(this._valueChangedTick, 50);
            };
            Canvas.prototype._deviceConnected = function (sender, options) {
                var _this = this;
                var deviceName, deviceData, path, pathData;
                var commService = NS.CommunicationService.instance;
                if (options.state === true) {
                    if (!this.db)
                        return;
                    var toSubscribe = typeof this.db === 'string' ? JSON.parse(this.db) : this.db;
                    var inputs = toSubscribe.input;
                    for (deviceName in inputs) {
                        if (deviceName === options.target) {
                            deviceData = inputs[deviceName];
                            for (path in deviceData) {
                                if (!deviceData.hasOwnProperty(path))
                                    continue;
                                pathData = deviceData[path];
                                pathData.forEach(function (entry) {
                                    for (var i = 0, keys = Object.keys(entry.bases), l = keys.length; i < l; ++i) {
                                        var baseSymPath = keys[i];
                                        var dataId = entry.bases[baseSymPath];
                                        var id = commService.subscribe(true, deviceName, path, entry.eventNr, entry.names, entry.types, dataId);
                                        var callback = {};
                                        callback.id = id;
                                        callback.path = baseSymPath;
                                        callback.deviceName = deviceName;
                                        callback.rpath = path;
                                        _this.connectorInputs[id] = callback;
                                    }
                                });
                            }
                        }
                    }
                    var outputs = toSubscribe.output;
                    for (deviceName in outputs) {
                        if (deviceName === options.target) {
                            deviceData = outputs[deviceName];
                            for (path in deviceData) {
                                if (!deviceData.hasOwnProperty(path))
                                    continue;
                                pathData = deviceData[path];
                                pathData.forEach(function (entry) {
                                    for (var i = 0, keys = Object.keys(entry.bases), l = keys.length; i < l; ++i) {
                                        var baseSymPath = keys[i];
                                        var dataId = entry.bases[baseSymPath];
                                        var id = commService.subscribe(false, deviceName, path, entry.eventNr, entry.names, entry.types, dataId);
                                        var callback = {};
                                        callback.id = id;
                                        callback.path = baseSymPath;
                                        callback.deviceName = deviceName;
                                        callback.rpath = path;
                                        var high = (dataId >> 16) << 16;
                                        if (high !== 0)
                                            callback.dataId = dataId - high;
                                        else
                                            callback.dataId = dataId;
                                        var shape = _this.find(callback.path);
                                        if (shape) {
                                            callback.shape = shape;
                                            shape.callbackdata = callback;
                                        }
                                        _this.connectorOutputs[id] = callback;
                                    }
                                });
                            }
                        }
                    }
                }
            };
            Canvas.prototype._valueChangedTick = function () {
                if (this._callbackCacheHasNewValues) {
                    this.ignoreRender = true;
                    for (var key in this._callbackCache) {
                        if (!this._callbackCache.hasOwnProperty(key))
                            continue;
                        var options = this._callbackCache[key];
                        var callback = options.callback;
                        if (callback && callback !== null && callback.shape) {
                            callback.shape.runtimeValueChanged && callback.shape.runtimeValueChanged(options.value, options.time);
                        }
                        else if (callback && callback !== null) {
                            var shape = this.find(callback.path);
                            if (shape) {
                                callback.shape = shape;
                                shape.runtimeValueChanged && shape.runtimeValueChanged(options.value, options.time);
                            }
                        }
                    }
                    this._callbackCache = {};
                    this._callbackCacheHasNewValues = false;
                    this.ignoreRender = false;
                    this.renderAll();
                }
            };
            Canvas.prototype._valueChanged = function (sender, options) {
                var callback = options.isInput ? (this.connectorInputs.hasOwnProperty(options.id) ? this.connectorInputs[options.id] : null) : (this.connectorOutputs.hasOwnProperty(options.id) ? this.connectorOutputs[options.id] : null);
                if (callback && callback !== null) {
                    this._callbackCacheHasNewValues = true;
                    this._callbackCache[options.id] = { callback: callback, value: options.value, time: options.time };
                }
            };
            Canvas.prototype._addHistoryValues = function (sender, options) {
                var callback = options.isInput ? (this.connectorInputs.hasOwnProperty(options.id) ? this.connectorInputs[options.id] : null) : (this.connectorOutputs.hasOwnProperty(options.id) ? this.connectorOutputs[options.id] : null);
                if (callback && callback !== null && callback.shape) {
                    callback.shape.callback = callback;
                    if (callback.shape.addHistoryValues(options))
                        this.renderAll();
                }
                else if (callback && callback !== null) {
                    var shape = this.find(callback.path);
                    if (shape) {
                        callback.shape = shape;
                        shape.callback = callback;
                        if (shape.addHistoryValues(options))
                            this.renderAll();
                    }
                }
            };
            Canvas.prototype._archiveAnswer = function (sender, options) {
                var callback = options.isInput ? (this.connectorInputs.hasOwnProperty(options.id) ? this.connectorInputs[options.id] : null) : (this.connectorOutputs.hasOwnProperty(options.id) ? this.connectorOutputs[options.id] : null);
                if (callback && callback !== null && callback.shape) {
                    if (callback.shape.archiveAnswer(options))
                        this.renderAll();
                }
                else if (callback && callback !== null) {
                    var shape = this.find(callback.path);
                    if (shape) {
                        callback.shape = shape;
                        if (shape.archiveAnswer(options))
                            this.renderAll();
                    }
                }
            };
            Canvas.prototype._startColorChange = function (sender, ea) { };
            Canvas.prototype._stopColorChange = function (sender, eventArgs) {
                if (eventArgs.value)
                    this.renderAll();
            };
            Canvas.prototype.getSymbolPath = function (symbolPath) {
                return { canvas: this.type, symbolPath: symbolPath };
            };
            Canvas.fromObject = function (element, options) {
                return new Canvas(element, options);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Canvas')
            ], Canvas.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Canvas')
            ], Canvas.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Canvas.prototype, "connectorInputs", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Canvas.prototype, "connectorOutputs", void 0);
            return Canvas;
        }(CanvasCore));
        GuiFramework.Canvas = Canvas;
        var CanvasTopologyNavigator = (function (_super) {
            __extends(CanvasTopologyNavigator, _super);
            function CanvasTopologyNavigator() {
                _super.apply(this, arguments);
                this.type = 'NxtControl.GuiFramework.CanvasTopologyNavigator';
            }
            CanvasTopologyNavigator.prototype.initialize = function (element, options) {
                this._inInitialize = true;
                options = options || {};
                var logo = null;
                if (options.logo) {
                    logo = options.logo;
                    delete options.logo;
                }
                _super.prototype.initialize.call(this, element, options);
                this._inInitialize = false;
                this.ctLogo = this.find('ctLogo');
                if (logo !== null) {
                    this.ctLogo.imageLoaded.add(this.setNavigatorWidths.bind(this));
                    this.ctLogo.set('src', logo);
                }
                else
                    this.setNavigatorWidths();
            };
            CanvasTopologyNavigator.prototype.setNavigatorWidths = function (sender, ea) {
                this.find('ctHeader').set('width', this.width);
                var size = this.ctLogo.getOriginalSize();
                var width = this.ctLogo.getWidth();
                if (size.width === width) {
                    this.find('ctSibling').set('width', this.width - 200);
                    this.find('ctChild').set('width', this.width - 200);
                }
                else {
                    var offset = 10 + size.width;
                    this.ctLogo.set('width', size.width);
                    this.find('ctRose').setSymbolLeftTop(offset);
                    offset += 140;
                    this.find('ctSibling').setSymbolLeftTop(offset).set('width', this.width - offset - 5);
                    this.find('ctChild').setSymbolLeftTop(offset).set('width', this.width - offset - 5);
                }
                this.renderAll();
            };
            CanvasTopologyNavigator.fromObject = function (element, options) {
                return new NGF.CanvasTopologyNavigator(element, options);
            };
            return CanvasTopologyNavigator;
        }(Canvas));
        GuiFramework.CanvasTopologyNavigator = CanvasTopologyNavigator;
        CanvasTopologyNavigator.prototype.json = { "objects": [{ "type": "NxtControl.GuiFramework.Rectangle", "name": "ctHeader", "brush": { "name": "CanvasTopologyHeaderBrush" }, "left": 0, "top": 0, "width": 800, "height": 80 },
                { "type": "NxtControl.GuiFramework.Image", "name": "ctLogo", "left": 3, "top": 5, "width": 100, "height": 74, "autoSize": false },
                { "type": "NxtControl.GuiFramework.CanvasTopologyRose", "name": "ctRose", "left": 110, "top": 3, "width": 84, "height": 74 },
                { "type": "NxtControl.GuiFramework.CanvasTopologyPanel", "name": "ctSibling", "topologyType": 1, "left": 200, "width": 585, "height": 30 },
                { "type": "NxtControl.GuiFramework.CanvasTopologyPanel", "name": "ctChild", "topologyType": 2, "left": 200, "top": 35, "width": 585, "height": 30 }], "background": "CanvasBackColor", "width": 800, "height": 80 };
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var Services;
    (function (Services) {
        var extend = NxtControl.Util.object.extend;
        var CanvasTopologyService = (function () {
            function CanvasTopologyService() {
                this.currentCanvas = null;
                this.faceplates = {};
                this.canvasChanged = new NxtControl.event();
            }
            CanvasTopologyService.prototype.onLoad = function (element, canvasName, resolutionName, topologyName, startCnv) {
                var c, ct, j, k, firstCanvasId = -1;
                this.uniqueCanvasTopologyId = -2;
                this.uniqueId = 0;
                this.parentElement = NxtControl.Util.getById(element);
                this.canvasName = canvasName;
                this.firstCanvasId = -1;
                if (this.json) {
                    this.canvasTopologies = {};
                    this.canvases = {};
                    this.resolutions = typeof this.json === 'string' ? JSON.parse(this.json) : this.json;
                    for (var i = 0; i < this.resolutions.length; i++) {
                        var cr = this.resolutions[i];
                        if (cr.StartCanvasClass && cr.StartCanvasClass.length !== 0) {
                            for (j = 0; j < cr.Topologies.length; j++) {
                                ct = cr.Topologies[j];
                                ct.FirstCanvasId = -1;
                                ct.Parent = cr;
                                ct.Id = this.getUniqueCanvasTopologyId();
                                this.canvasTopologies[ct.Id] = ct;
                                var leftCanvas = null;
                                for (k = 0; k < ct.Canvases.length; k++) {
                                    c = ct.Canvases[k];
                                    c.ParentCanvasId = ct.Id;
                                    c.CanvasId = this.getUniqueId();
                                    if (c.Name === ct.FirstCanvas)
                                        ct.FirstCanvasId = c.CanvasId;
                                    this.canvases[c.CanvasId] = c;
                                    var id = this.loadFromDatabase(c, ct.FirstCanvas);
                                    if (ct.FirstCanvasId === -1 && id !== -1)
                                        ct.FirstCanvasId = id;
                                    if (this.firstCanvasId === -1 && id !== -1 && cr.Name === resolutionName && ct.Name == topologyName)
                                        this.firstCanvasId = id;
                                    if (leftCanvas !== null) {
                                        leftCanvas.RightCanvasId = c.CanvasId;
                                        c.LeftCanvasId = leftCanvas.CanvasId;
                                    }
                                    leftCanvas = c;
                                }
                                if (ct.FirstCanvasId === -1 && ct.Canvases.length !== 0)
                                    ct.FirstCanvasId = ct.Canvases[0].CanvasId;
                                if (this.firstCanvasId === -1 && ct.Canvases.length !== 0 && cr.Name === resolutionName && ct.Name == topologyName)
                                    this.firstCanvasId = ct.Canvases[0].CanvasId;
                                if (firstCanvasId === -1)
                                    firstCanvasId = ct.firstCanvasID;
                            }
                        }
                        else {
                            for (j = 0; j < cr.Topologies.length; j++) {
                                ct = cr.Topologies[j];
                                ct.FirstCanvasId = -1;
                                ct.Parent = cr;
                                ct.Id = this.getUniqueCanvasTopologyId();
                                this.canvasTopologies[ct.Id] = ct;
                                for (k = 0; k < ct.Canvases.length; k++) {
                                    c = ct.Canvases[k];
                                    c.ParentCanvasId = ct.Id;
                                    c.CanvasId = this.getUniqueId();
                                    this.canvases[c.CanvasId] = c;
                                }
                            }
                        }
                    }
                }
                var startCanvasId = this.getStartCanvasId(startCnv);
                if (startCanvasId !== -1)
                    this.firstCanvasId = startCanvasId;
                else if (firstCanvasId !== -1 && this.firstCanvasId === -1)
                    this.firstCanvasId = firstCanvasId;
                return this.firstCanvasId;
            };
            CanvasTopologyService.prototype.loadFromDatabase = function (parent, firstCanvas) {
                var firstCanvasID = -1;
                parent.Children = [];
                if (parent.Canvases !== null) {
                    var leftCanvas = null;
                    for (var k = 0; k < parent.Canvases.length; k++) {
                        var c = parent.Canvases[k];
                        c.CanvasId = this.getUniqueId();
                        c.ParentCanvasId = parent.CanvasId;
                        if (c.Name === firstCanvas)
                            firstCanvasID = c.CanvasId;
                        parent.Children.push(c.CanvasId);
                        this.canvases[c.CanvasId] = c;
                        var id = this.loadFromDatabase(c, firstCanvas);
                        if (firstCanvasID === -1 && id !== -1)
                            firstCanvasID = id;
                        if (leftCanvas !== null) {
                            leftCanvas.RightCanvasId = c.CanvasId;
                            c.LeftCanvasId = leftCanvas.CanvasId;
                        }
                        leftCanvas = c;
                    }
                }
                return firstCanvasID;
            };
            CanvasTopologyService.prototype.getUniqueCanvasTopologyId = function () {
                this.uniqueCanvasTopologyId--;
                return this.uniqueCanvasTopologyId;
            };
            CanvasTopologyService.prototype.getUniqueId = function () {
                this.uniqueId++;
                return this.uniqueId;
            };
            CanvasTopologyService.prototype.getCurrentCanvas = function () {
                return this.currentCanvas;
            };
            CanvasTopologyService.prototype.getStartCanvasId = function (startCnv) {
                if (startCnv === null || startCnv === '')
                    return -1;
                var i, j, k, resolution = null, topology = null, firstCanvas = null;
                var res = startCnv.split(';');
                for (i = 0; i < res.length; i++) {
                    var res1 = res[i].split('=');
                    if (res1.length === 2) {
                        if (res1[0] === 'Resolution')
                            resolution = res1[1];
                        else if (res1[0] === 'Topology')
                            topology = res1[1];
                        else if (res1[0] === 'FirstCanvas')
                            firstCanvas = res1[1];
                    }
                }
                if (resolution === null || topology === null || firstCanvas === null) {
                    console.error('Bad start canvas definition:' + startCnv);
                    return -1;
                }
                var cr = null;
                for (i = 0; i < this.resolutions.length; i++) {
                    if (this.resolutions[i].Name === resolution) {
                        cr = this.resolutions[i];
                        break;
                    }
                }
                if (cr === null) {
                    console.error('Bad start canvas definition, there is no resolution with name:' + resolution);
                    return -1;
                }
                var ct = null;
                for (j = 0; j < cr.Topologies.length; j++) {
                    if (cr.Topologies[i].Name === topology) {
                        ct = cr.Topologies[i];
                        break;
                    }
                }
                if (ct === null) {
                    console.error('Bad start canvas definition, there is no topology with name:' + topology);
                    return -1;
                }
                var fc = null;
                for (k = 0; k < ct.Canvases.length; k++) {
                    if (ct.Canvases[k].Name === firstCanvas) {
                        fc = ct.Canvases[k];
                        break;
                    }
                }
                if (fc === null) {
                    console.error('Bad start canvas definition, there is no canvas with name:' + firstCanvas);
                    return -1;
                }
                return fc.CanvasId;
            };
            CanvasTopologyService.prototype.getTopologyId = function (resolutionName, topologyName) {
                resolutionName = resolutionName || '';
                topologyName = topologyName || '';
                if (!this.resolutions || this.resolutions.length == 0)
                    return null;
                for (var i = 0; i < this.resolutions.length; i++) {
                    if (resolutionName == this.resolutions[i].Name) {
                        if (!this.resolutions[i].Topologies)
                            return null;
                        for (var j = 0; j < this.resolutions[i].Topologies.length; j++) {
                            if (topologyName == this.resolutions[i].Topologies[j].Name)
                                return this.resolutions[i].Topologies[j].Id;
                        }
                    }
                }
                return null;
            };
            CanvasTopologyService.prototype.openCanvas = function (canvasId) {
                var canvas = this.canvases[canvasId];
                var canvasElement = NxtControl.Util.getById(this.canvasName);
                if (canvasElement) {
                    if (canvasElement.hasOwnProperty('myCanvas')) {
                        this.closeFaceplates();
                        var myCanvas = canvasElement['myCanvas'];
                        myCanvas.dispose();
                        myCanvas.clear();
                        delete canvasElement['myCanvas'];
                    }
                    this.parentElement.innerHTML = "";
                }
                canvasElement = document.createElement('canvas');
                canvasElement.id = this.canvasName;
                this.parentElement.appendChild(canvasElement);
                if (!canvas.Instance && canvas.IsLink) {
                    var ci = CanvasTopologyService.instance.findCanvasFromType(canvas.Name);
                    if (ci.Instance)
                        canvas.Instance = ci.Instance;
                }
                var klass = NxtControl.Util.Parser.getKlass(canvas.Instance, null);
                if (klass) {
                    var shape = klass.fromObject(this.canvasName);
                    canvasElement['myCanvas'] = shape;
                    this.currentCanvas = canvas;
                    this.canvasChanged.trigger(this, { currentCanvas: shape });
                }
            };
            CanvasTopologyService.prototype.getChildCanvasIds = function (parentCanvasId) {
                if (parentCanvasId < 0) {
                    var canvasIds = [];
                    if (parentCanvasId in this.canvasTopologies) {
                        var canvases = this.canvasTopologies[parentCanvasId].Canvases;
                        for (var c in canvases)
                            if (canvases.hasOwnProperty(c))
                                canvasIds.push(canvases[c].CanvasId);
                    }
                    return canvasIds;
                }
                if (!(parentCanvasId in this.canvases))
                    return [];
                var cnv = this.canvases[parentCanvasId];
                var ids = [];
                for (var i = 0; i < cnv.Children.length; i++)
                    ids.push(cnv.Children[i]);
                return ids;
            };
            CanvasTopologyService.prototype.getCanvasData = function (canvasId) {
                var canvas = this.findCanvas(canvasId);
                if (canvas === null)
                    return { text: '', tooltip: '' };
                return {
                    text: !canvas.Title || canvas.Title.length === 0 ? canvas.Name : canvas.Title,
                    tooltip: canvas.Tooltip
                };
            };
            CanvasTopologyService.prototype.findCanvasFromType = function (canvasType) {
                for (var canvasId in this.canvases) {
                    if (this.canvases.hasOwnProperty(canvasId) && this.canvases[canvasId].Name === canvasType || this.canvases[canvasId].Instance === canvasType)
                        return this.canvases[canvasId];
                }
                return null;
            };
            CanvasTopologyService.prototype.findCanvas = function (canvasId) {
                if (canvasId in this.canvases)
                    return this.canvases[canvasId];
                return null;
            };
            CanvasTopologyService.prototype.getCanvasTopParentID = function (canvasId) {
                if (canvasId < 0)
                    return canvasId;
                var parentCanvasId = canvasId;
                while (parentCanvasId >= 0) {
                    if (parentCanvasId in this.canvases) {
                        parentCanvasId = this.canvases[parentCanvasId].ParentCanvasId;
                    }
                    else {
                        alert("GetCanvasTopParentID could not find canvas");
                        throw "GetCanvasTopParentID could not find canvas";
                    }
                }
                return parentCanvasId;
            };
            CanvasTopologyService.prototype.openParentCanvas = function () {
                if (this.currentCanvas !== null && this.currentCanvas.ParentCanvasId && this.currentCanvas.ParentCanvasId > 0)
                    this.openCanvas(this.currentCanvas.ParentCanvasId);
            };
            CanvasTopologyService.prototype.openLeftCanvas = function () {
                if (this.currentCanvas !== null && this.currentCanvas.LeftCanvasId && this.currentCanvas.LeftCanvasId !== -1)
                    this.openCanvas(this.currentCanvas.LeftCanvasId);
            };
            CanvasTopologyService.prototype.openRightCanvas = function () {
                if (this.currentCanvas !== null && this.currentCanvas.RightCanvasId && this.currentCanvas.RightCanvasId !== -1)
                    this.openCanvas(this.currentCanvas.RightCanvasId);
            };
            CanvasTopologyService.prototype.openChildCanvas = function (childIndex) {
                if (this.currentCanvas !== null && this.currentCanvas.Children.length > childIndex)
                    this.openCanvas(this.currentCanvas.Children[childIndex]);
            };
            CanvasTopologyService.prototype.getLeftTooltip = function () {
                if (this.currentCanvas === null)
                    return '';
                if (!this.currentCanvas.LeftCanvasId || this.currentCanvas.LeftCanvasId == -1)
                    return "There is no Left Sibling";
                var c = this.canvases[this.currentCanvas.LeftCanvasId];
                var wholeTooltip = c.Title || c.Name;
                var tooltip = c.Tooltip || '';
                if (tooltip.length !== 0) {
                    tooltip += '\r\n\r\n' + tooltip;
                }
                return tooltip;
            };
            CanvasTopologyService.prototype.getRightTooltip = function () {
                if (this.currentCanvas === null)
                    return '';
                if (!this.currentCanvas.RightCanvasId || this.currentCanvas.RightCanvasId == -1)
                    return "There is no Right Sibling";
                var c = this.canvases[this.currentCanvas.RightCanvasId];
                var wholeTooltip = c.Title || c.Name;
                var tooltip = c.Tooltip || '';
                if (tooltip.length !== 0) {
                    tooltip += '\r\n\r\n' + tooltip;
                }
                return tooltip;
            };
            CanvasTopologyService.prototype.getUpTooltip = function () {
                if (this.currentCanvas === null)
                    return '';
                if (!this.currentCanvas.ParentCanvasId || this.currentCanvas.ParentCanvasId < 0)
                    return "There is no Parent";
                var c = this.canvases[this.currentCanvas.ParentCanvasId];
                var wholeTooltip = c.Title || c.Name;
                var tooltip = c.Tooltip || '';
                if (tooltip.length !== 0) {
                    tooltip += '\r\n\r\n' + tooltip;
                }
                return tooltip;
            };
            CanvasTopologyService.prototype.getDownTooltip = function () {
                if (this.currentCanvas === null)
                    return '';
                if (!this.currentCanvas || this.currentCanvas.Children == null || this.currentCanvas.Children.length == 0 || this.currentCanvas.Children[0] == -1)
                    return "There is no Child";
                var c = this.canvases[this.currentCanvas.Children[0]];
                var wholeTooltip = c.Title || c.Name;
                var tooltip = c.Tooltip || '';
                if (tooltip.length !== 0) {
                    tooltip += '\r\n\r\n' + tooltip;
                }
                return tooltip;
            };
            CanvasTopologyService.prototype.registerFaceplate = function (faceplate) {
                var id = this.getUniqueId();
                this.faceplates[id] = faceplate;
                return id;
            };
            CanvasTopologyService.prototype.unregisterFaceplate = function (id) {
                if (this.faceplates.hasOwnProperty(id.toString()))
                    delete this.faceplates[id];
            };
            CanvasTopologyService.prototype.getFaceplate = function (id) {
                return this.faceplates.hasOwnProperty(id.toString()) ? this.faceplates[id] : null;
            };
            CanvasTopologyService.prototype.closeFaceplates = function () {
                var faceplates = NxtControl.Util.object.extend({}, this.faceplates);
                for (var id in faceplates) {
                    if (!faceplates.hasOwnProperty(id))
                        continue;
                    faceplates[id].close();
                }
            };
            return CanvasTopologyService;
        }());
        Services.CanvasTopologyService = CanvasTopologyService;
        CanvasTopologyService.instance = new CanvasTopologyService();
    })(Services = NxtControl.Services || (NxtControl.Services = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var FillDirection = NxtControl.Drawing.FillDirection;
        var Rectangle = (function (_super) {
            __extends(Rectangle, _super);
            function Rectangle() {
                _super.apply(this, arguments);
            }
            Rectangle.prototype.getFillDirection = function () {
                return this.fillDirection;
            };
            Rectangle.prototype.setFillDirection = function (fillDirection) {
                return this._set('fillDirection', fillDirection, true);
            };
            Rectangle.prototype.getFillPercent = function () {
                return this.fillPercent;
            };
            Rectangle.prototype.setFillPercent = function (fillPercent) {
                return this._set('fillPercent', fillPercent, true);
            };
            Rectangle.prototype.getRadius = function () {
                return this.radius;
            };
            Rectangle.prototype.setRadius = function (radius) {
                return this._set('radius', radius, true);
            };
            Rectangle.prototype.load = function (options) {
                options = options || {};
                _super.prototype.load.call(this, options);
                this.radius = options.radius || 0;
                this.x = options.x || 0;
                this.y = options.y || 0;
                return this;
            };
            Rectangle.prototype._render = function (ctx, noTransform) {
                var isRounded, radius = this.radius || 0, x = -this.width / 2, y = -this.height / 2, w = this.width, h = this.height, isInPathGroup = this.group && this.group.getType() === 'path-group';
                if (this.fillPercent === 100) {
                    ctx.beginPath();
                    if (this.transformMatrix && isInPathGroup) {
                        ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                    }
                    if (!this.transformMatrix && isInPathGroup) {
                        ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                    }
                    isRounded = radius !== 0;
                    ctx.moveTo(x + radius, y);
                    ctx.lineTo(x + w - radius, y);
                    isRounded && ctx.quadraticCurveTo(x + w, y, x + w, y + radius);
                    ctx.lineTo(x + w, y + h - radius);
                    isRounded && ctx.quadraticCurveTo(x + w, y + h, x + w - radius, y + h);
                    ctx.lineTo(x + radius, y + h);
                    isRounded && ctx.quadraticCurveTo(x, y + h, x, y + h - radius);
                    ctx.lineTo(x, y + radius);
                    isRounded && ctx.quadraticCurveTo(x, y, x + radius, y);
                    ctx.closePath();
                    this._renderFill(ctx);
                    this._renderStroke(ctx);
                }
                else {
                    var xx = -this.width / 2, yy = -this.height / 2, ww = this.width, hh = this.height;
                    switch (this.fillDirection) {
                        case FillDirection.LeftToRight:
                            {
                                ww = ww / 100 * this.fillPercent;
                            }
                            break;
                        case FillDirection.RightToLeft:
                            {
                                var www = ww;
                                ww = ww / 100 * this.fillPercent;
                                xx = xx + www - ww;
                            }
                            break;
                        case FillDirection.TopToDown:
                            {
                                hh = hh / 100 * this.fillPercent;
                            }
                            break;
                        case FillDirection.DownToTop:
                            {
                                var hhh = hh;
                                hh = hh / 100 * this.fillPercent;
                                yy = yy + hhh - hh;
                            }
                            break;
                    }
                    ctx.beginPath();
                    if (this.transformMatrix && isInPathGroup) {
                        ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                    }
                    if (!this.transformMatrix && isInPathGroup) {
                        ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                    }
                    isRounded = radius !== 0;
                    ctx.moveTo(xx + radius, yy);
                    ctx.lineTo(xx + ww - radius, yy);
                    isRounded && ctx.quadraticCurveTo(xx + ww, yy, xx + ww, yy + radius);
                    ctx.lineTo(xx + ww, yy + hh - radius);
                    isRounded && ctx.quadraticCurveTo(xx + ww, yy + hh, xx + ww - radius, yy + hh);
                    ctx.lineTo(xx + radius, yy + hh);
                    isRounded && ctx.quadraticCurveTo(xx, yy + hh, xx, yy + hh - radius);
                    ctx.lineTo(xx, yy + radius);
                    isRounded && ctx.quadraticCurveTo(xx, yy, xx + radius, yy);
                    ctx.closePath();
                    this._renderFill(ctx, this.brush, xx, yy, ww, hh);
                    ctx.beginPath();
                    if (this.transformMatrix && isInPathGroup) {
                        ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                    }
                    if (!this.transformMatrix && isInPathGroup) {
                        ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                    }
                    isRounded = radius !== 0;
                    ctx.moveTo(x + radius, y);
                    ctx.lineTo(x + w - radius, y);
                    isRounded && ctx.quadraticCurveTo(x + w, y, x + w, y + radius);
                    ctx.lineTo(x + w, y + h - radius);
                    isRounded && ctx.quadraticCurveTo(x + w, y + h, x + w - radius, y + h);
                    ctx.lineTo(x + radius, y + h);
                    isRounded && ctx.quadraticCurveTo(x, y + h, x, y + h - radius);
                    ctx.lineTo(x, y + radius);
                    isRounded && ctx.quadraticCurveTo(x, y, x + radius, y);
                    ctx.closePath();
                    this._renderStroke(ctx);
                }
            };
            Rectangle.prototype._renderDashedStroke = function (ctx) {
                if (!(this.pen && this.pen.dashArray && this.pen.dashArray.length != 0))
                    return;
                var x = -this.width / 2, y = -this.height / 2, w = this.width, h = this.height;
                ctx.beginPath();
                NxtControl.Util.drawDashedLine(ctx, x, y, x + w, y, this.pen.dashArray);
                NxtControl.Util.drawDashedLine(ctx, x + w, y, x + w, y + h, this.pen.dashArray);
                NxtControl.Util.drawDashedLine(ctx, x + w, y + h, x, y + h, this.pen.dashArray);
                NxtControl.Util.drawDashedLine(ctx, x, y + h, x, y, this.pen.dashArray);
                ctx.closePath();
            };
            Rectangle.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (this.designer !== null) {
                    if (key === 'scaleX') {
                        if (value <= 0)
                            return this;
                        this._set('width', value * this.width, invalidate);
                        this.scaleX = 1;
                        return this;
                    }
                    else if (key === 'scaleY') {
                        if (value <= 0)
                            return this;
                        this._set('height', value * this.height, invalidate);
                        this.scaleY = 1;
                        return this;
                    }
                }
                _super.prototype._set.call(this, key, value, invalidate);
                return this;
            };
            Rectangle.prototype._normalizeLeftTopProperties = function (parsedAttributes) {
                if ('left' in parsedAttributes) {
                    this.set('left', parsedAttributes.left + this.getWidth() / 2);
                }
                this.set('x', parsedAttributes.left || 0);
                if ('top' in parsedAttributes) {
                    this.set('top', parsedAttributes.top + this.getHeight() / 2);
                }
                this.set('y', parsedAttributes.top || 0);
                return this;
            };
            Rectangle.prototype.toObject = function (propertiesToInclude) {
                var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    fillDirection: this.fillDirection,
                    fillPercent: this.fillPercent,
                    radius: this.radius
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            Rectangle.fromObject = function (object) {
                return new Rectangle().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Rectangle')
            ], Rectangle.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Rectangle')
            ], Rectangle.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.RectangleDesigner')
            ], Rectangle.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(FillDirection.LeftToRight),
                System.Browsable
            ], Rectangle.prototype, "fillDirection", void 0);
            __decorate([
                System.DefaultValue(100),
                System.Browsable
            ], Rectangle.prototype, "fillPercent", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], Rectangle.prototype, "radius", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], Rectangle.prototype, "x", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], Rectangle.prototype, "y", void 0);
            return Rectangle;
        }(GuiFramework.Shape));
        GuiFramework.Rectangle = Rectangle;
        var Ellipse = (function (_super) {
            __extends(Ellipse, _super);
            function Ellipse() {
                _super.apply(this, arguments);
            }
            Ellipse.prototype.load = function (options) {
                options = options || {};
                _super.prototype.load.call(this, options);
                this.set('width', options.width || 0);
                this.set('height', options.height || 0);
                return this;
            };
            Ellipse.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (this.designer !== null) {
                    if (key === 'scaleX') {
                        if (value <= 0)
                            return this;
                        this._set('width', value * this.width, invalidate);
                        this.scaleX = 1;
                        return this;
                    }
                    else if (key === 'scaleY') {
                        if (value <= 0)
                            return this;
                        this._set('height', value * this.height, invalidate);
                        this.scaleY = 1;
                        return this;
                    }
                }
                _super.prototype._set.call(this, key, value, invalidate);
                return this;
            };
            Ellipse.prototype.toObject = function (propertiesToInclude) {
                var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {});
                return object;
            };
            Ellipse.prototype.render = function (ctx, noTransform) {
                if (this.width === 0 || this.height === 0)
                    return;
                return _super.prototype.render.call(this, ctx, noTransform);
            };
            Ellipse.prototype._render = function (ctx, noTransform) {
                ctx.beginPath();
                ctx.save();
                ctx.transform(1, 0, 0, this.height / this.width, 0, 0);
                ctx.arc(noTransform ? this.left : 0, noTransform ? this.top : 0, this.width / 2, 0, Math.PI * 2, false);
                this._renderFill(ctx);
                this._renderStroke(ctx);
                ctx.restore();
            };
            Ellipse.fromObject = function (object) {
                return new Ellipse().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Ellipse')
            ], Ellipse.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Ellipse')
            ], Ellipse.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.EllipseDesigner')
            ], Ellipse.prototype, "designerType", void 0);
            return Ellipse;
        }(GuiFramework.Shape));
        GuiFramework.Ellipse = Ellipse;
        var coordProps = { 'x1': 1, 'x2': 1, 'y1': 1, 'y2': 1 }, supportsLineDash = GuiFramework.StaticCanvas.supports('setLineDash');
        var Line = (function (_super) {
            __extends(Line, _super);
            function Line() {
                _super.apply(this, arguments);
            }
            Line.prototype.getStart = function () {
                return this.start;
            };
            Line.prototype.setStart = function (start) {
                return this._set('start', start, true);
            };
            Line.prototype.getEnd = function () {
                return this.end;
            };
            Line.prototype.setEnd = function (end) {
                return this._set('end', end, true);
            };
            Line.prototype.load = function (options) {
                this._inInitialize = true;
                options = options || {};
                options.start = options.start || { x: 0, y: 0 };
                options.end = options.end || { x: 0, y: 0 };
                _super.prototype.load.call(this, options);
                if (!('left' in options))
                    this.set('left', Math.min(options.start.x, options.end.x));
                if (!('top' in options))
                    this.set('top', Math.min(options.start.y, options.end.y));
                this.set('width', Math.abs(this.end.x - this.start.x) || 1);
                this.set('height', Math.abs(this.end.y - this.start.y) || 1);
                this._inInitialize = false;
                return this;
            };
            Line.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                var left = this.left;
                var top = this.top;
                _super.prototype._set.call(this, key, value, invalidate);
                if (this._inInitialize === false) {
                    if (key === 'left') {
                        this.start.x += this.left - left;
                        this.end.x += this.left - left;
                    }
                    if (key === 'top') {
                        this.start.y += this.top - top;
                        this.end.y += this.top - top;
                    }
                    if (key === 'start' || key === 'end') {
                        this.left = Math.min(this.start.x, this.end.x);
                        this.width = Math.abs(this.end.x - this.start.x) || 1;
                        this.top = Math.min(this.start.y, this.end.y);
                        this.height = Math.abs(this.end.y - this.start.y) || 1;
                    }
                }
                if (this.designer !== null)
                    this.designer.propertyChanged(key);
                return this;
            };
            Line.prototype._render = function (ctx, noTransform) {
                if (!this.pen)
                    return;
                ctx.beginPath();
                var isInPathGroup = this.group && this.group.getType() === 'path-group';
                if (isInPathGroup && !this.transformMatrix) {
                    ctx.translate(-this.group.getWidth() / 2 + this.left, -this.group.getHeight() / 2 + this.top);
                }
                if (!this.pen.dashArray || this.pen.dashArray && supportsLineDash) {
                    var xMult = this.start.x <= this.end.x ? -1 : 1;
                    var yMult = this.start.y <= this.end.y ? -1 : 1;
                    ctx.moveTo(this.width === 1 ? 0 : (xMult * this.width / 2), this.height === 1 ? 0 : (yMult * this.height / 2));
                    ctx.lineTo(this.width === 1 ? 0 : (xMult * -1 * this.width / 2), this.height === 1 ? 0 : (yMult * -1 * this.height / 2));
                }
                ctx.lineWidth = this.pen.width;
                var origStrokeStyle = ctx.strokeStyle;
                ctx.strokeStyle = this.pen.toLive();
                this._renderStroke(ctx);
                ctx.strokeStyle = origStrokeStyle;
            };
            Line.prototype._renderDashedStroke = function (ctx) {
                var xMult = this.start.x <= this.end.x ? -1 : 1, yMult = this.start.y <= this.end.y ? -1 : 1, x = this.width === 1 ? 0 : xMult * this.width / 2, y = this.height === 1 ? 0 : yMult * this.height / 2;
                ctx.beginPath();
                NxtControl.Util.drawDashedLine(ctx, x, y, -x, -y, this.pen.dashArray);
                ctx.closePath();
            };
            Line.prototype.toObject = function (propertiesToInclude) {
                return NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    start: this.start,
                    end: this.end,
                });
            };
            Line.prototype.setDesigner = function (designer, canvas) {
                this['hasControls'] = this['hasBorders'] = false;
                _super.prototype.setDesigner.call(this, designer, canvas);
            };
            Line.fromObject = function (object) {
                return new Line().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Line')
            ], Line.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Line')
            ], Line.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.LineDesigner')
            ], Line.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue({ x: 0, y: 0 }),
                System.Browsable
            ], Line.prototype, "start", void 0);
            __decorate([
                System.DefaultValue({ x: 0, y: 0 }),
                System.Browsable
            ], Line.prototype, "end", void 0);
            return Line;
        }(GuiFramework.Shape));
        GuiFramework.Line = Line;
        var min = NxtControl.Util.array.min, max = NxtControl.Util.array.max, toFixed = NxtControl.Util.Convert.toFixed;
        var Polygon = (function (_super) {
            __extends(Polygon, _super);
            function Polygon() {
                _super.apply(this, arguments);
            }
            Polygon.prototype.getFillDirection = function () {
                return this.fillDirection;
            };
            Polygon.prototype.setFillDirection = function (fillDirection) {
                return this._set('fillDirection', fillDirection, true);
            };
            Polygon.prototype.getFillPercent = function () {
                return this.fillPercent;
            };
            Polygon.prototype.setFillPercent = function (fillPercent) {
                return this._set('fillPercent', fillPercent, true);
            };
            Polygon.prototype.getClosed = function () {
                return this.closed;
            };
            Polygon.prototype.setClosed = function (closed) {
                return this._set('closed', closed, true);
            };
            Polygon.prototype.getPoints = function () {
                return this.points;
            };
            Polygon.prototype.load = function (options) {
                options = options || {};
                var points = [];
                if (!options.hasOwnProperty('points') || options.points === null) {
                    if (!options.hasOwnProperty('dpoints') || options.dpoints === null) {
                        points = [{ x: 0, y: 42 }, { x: 155, y: 0 }, { x: 155, y: 243 }, { x: 0, y: 256 }];
                    }
                    else {
                        points = options.dpoints;
                        delete options.dpoints;
                    }
                }
                else {
                    points = options.points;
                    delete options.points;
                }
                this.points = points;
                _super.prototype.load.call(this, options);
                this._calcDimensions(false);
                return this;
            };
            Polygon.prototype._calcDimensions = function (skipOffset) {
                var points = this.points, minX = min(points, 'x'), minY = min(points, 'y'), maxX = max(points, 'x'), maxY = max(points, 'y');
                this.width = (maxX - minX) || 1;
                this.height = (maxY - minY) || 1;
                this.minX = minX;
                this.minY = minY;
                if (skipOffset)
                    return;
                var halfWidth = this.width / 2 + this.minX, halfHeight = this.height / 2 + this.minY;
                this.points.forEach(function (p) {
                    p.x -= halfWidth;
                    p.y -= halfHeight;
                }, this);
            };
            Polygon.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                var p, start, center, i;
                if (this.designer !== null && (key === 'width' || key === 'height')) {
                    if (value === 0)
                        value = 1;
                    var ratio = value / this[key];
                    p = key === 'width' ? 'x' : 'y';
                    start = key === 'width' ? 'left' : 'top';
                    center = this.getCenterPoint();
                    for (i = 0; i < this.points.length; i++) {
                        this.points[i][p] += center[p];
                        this.points[i][p] = this[start] + (this.points[i][p] - this[start]) * ratio;
                    }
                }
                if (this.designer !== null && key === 'dpoints') {
                    this.points = value.slice(0);
                }
                _super.prototype._set.call(this, key, value, invalidate);
                if (this.designer !== null && (key === 'width' || key === 'height')) {
                    center = this.getCenterPoint();
                    for (i = 0; i < this.points.length; i++) {
                        this.points[i][p] -= center[p];
                    }
                }
                if (this.designer !== null)
                    this.designer.propertyChanged(key);
                return this;
            };
            Polygon.prototype._render = function (ctx, noTransform) {
                var i, len, point;
                if (this.fillPercent === 100) {
                    ctx.beginPath();
                    ctx.moveTo(this.points[0].x, this.points[0].y);
                    for (i = 0, len = this.points.length; i < len; i++) {
                        point = this.points[i];
                        ctx.lineTo(point.x, point.y);
                    }
                    this._renderFill(ctx);
                }
                else {
                    var pts = NxtControl.Drawing.SutherlandHodgman.clip(this.points, this.fillDirection, this.fillPercent);
                    ctx.beginPath();
                    ctx.moveTo(pts[0].x, pts[0].y);
                    for (i = 0, len = pts.length; i < len; i++) {
                        point = pts[i];
                        ctx.lineTo(point.x, point.y);
                        this._renderFill(ctx);
                    }
                }
                ctx.beginPath();
                ctx.moveTo(this.points[0].x, this.points[0].y);
                for (i = 0, len = this.points.length; i < len; i++) {
                    point = this.points[i];
                    ctx.lineTo(point.x, point.y);
                }
                if (this.pen || this.pen.dashArray) {
                    if (this.closed)
                        ctx.closePath();
                    this._renderStroke(ctx);
                }
            };
            Polygon.prototype._renderDashedStroke = function (ctx) {
                var p1, p2;
                ctx.beginPath();
                for (var i = 0, len = this.points.length; i < len; i++) {
                    p1 = this.points[i];
                    p2 = this.points[i + 1] || this.points[0];
                    NxtControl.Util.drawDashedLine(ctx, p1.x, p1.y, p2.x, p2.y, this.pen.dashArray);
                }
                ctx.closePath();
            };
            Polygon.prototype.get = function (property) {
                if (property === 'dpoints')
                    return this.points;
                return this[property];
            };
            Polygon.prototype.toObject = function (propertiesToInclude) {
                var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    points: this.points.concat(),
                    fillDirection: this.fillDirection,
                    fillPercent: this.fillPercent,
                    closed: this.closed
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            Polygon.prototype.setDesigner = function (designer, canvas) {
                this['hasControls'] = this['hasBorders'] = false;
                _super.prototype.setDesigner.call(this, designer, canvas);
            };
            Polygon.fromObject = function (object) {
                return new Polygon().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Polygon')
            ], Polygon.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Polygon')
            ], Polygon.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.PolygonDesigner')
            ], Polygon.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(FillDirection.LeftToRight),
                System.Browsable
            ], Polygon.prototype, "fillDirection", void 0);
            __decorate([
                System.DefaultValue(100),
                System.Browsable
            ], Polygon.prototype, "fillPercent", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Polygon.prototype, "closed", void 0);
            __decorate([
                System.Browsable
            ], Polygon.prototype, "dpoints", void 0);
            return Polygon;
        }(GuiFramework.Shape));
        GuiFramework.Polygon = Polygon;
        var Polyline = (function (_super) {
            __extends(Polyline, _super);
            function Polyline() {
                _super.apply(this, arguments);
            }
            Polyline.prototype.getClosed = function () {
                return this.closed;
            };
            Polyline.prototype.setClosed = function (closed) {
                return this._set('closed', closed, true);
            };
            Polyline.prototype.getPoints = function () {
                return this.points;
            };
            Polyline.prototype.load = function (options) {
                options = options || {};
                var points = [];
                if (!options.hasOwnProperty('points') || options.points === null) {
                    if (!options.hasOwnProperty('dpoints') || options.dpoints === null) {
                        points = [{ x: 0, y: 42 }, { x: 155, y: 0 }, { x: 155, y: 243 }, { x: 0, y: 256 }];
                    }
                    else {
                        points = options.dpoints;
                        delete options.dpoints;
                    }
                }
                else {
                    points = options.points;
                    delete options.points;
                }
                this.points = points;
                _super.prototype.load.call(this, options);
                this._calcDimensions(false);
                return this;
            };
            Polyline.prototype._calcDimensions = function (skipOffset) {
                var points = this.points, minX = min(points, 'x'), minY = min(points, 'y'), maxX = max(points, 'x'), maxY = max(points, 'y');
                this.width = (maxX - minX) || 1;
                this.height = (maxY - minY) || 1;
                this.minX = minX;
                this.minY = minY;
                if (skipOffset)
                    return;
                var halfWidth = this.width / 2 + this.minX, halfHeight = this.height / 2 + this.minY;
                this.points.forEach(function (p) {
                    p.x -= halfWidth;
                    p.y -= halfHeight;
                }, this);
            };
            Polyline.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                var i, p, start, center;
                if (this.designer !== null && (key === 'width' || key === 'height')) {
                    if (value === 0)
                        value = 1;
                    var ratio = value / this[key];
                    p = key === 'width' ? 'x' : 'y';
                    start = key === 'width' ? 'left' : 'top';
                    center = this.getCenterPoint();
                    for (i = 0; i < this.points.length; i++) {
                        this.points[i][p] += center[p];
                        this.points[i][p] = this[start] + (this.points[i][p] - this[start]) * ratio;
                    }
                }
                if (this.designer !== null && key === 'dpoints') {
                    this.points = value.slice(0);
                }
                _super.prototype._set.call(this, key, value, invalidate);
                if (this.designer !== null && (key === 'width' || key === 'height')) {
                    center = this.getCenterPoint();
                    for (i = 0; i < this.points.length; i++) {
                        this.points[i][p] -= center[p];
                    }
                }
                if (this.designer !== null)
                    this.designer.propertyChanged(key);
                return this;
            };
            Polyline.prototype.get = function (property) {
                if (property === 'dpoints')
                    return this.points;
                return this[property];
            };
            Polyline.prototype.setDesigner = function (designer, canvas) {
                this['hasControls'] = this['hasBorders'] = false;
                _super.prototype.setDesigner.call(this, designer, canvas);
            };
            Polyline.prototype._render = function (ctx, noTransform) {
                var point;
                ctx.beginPath();
                ctx.moveTo(this.points[0].x, this.points[0].y);
                for (var i = 0, len = this.points.length; i < len; i++) {
                    point = this.points[i];
                    ctx.lineTo(point.x, point.y);
                }
                if (this.closed)
                    ctx.closePath();
                this._renderStroke(ctx);
            };
            Polyline.prototype._renderDashedStroke = function (ctx) {
                var p1, p2;
                ctx.beginPath();
                for (var i = 0, len = this.points.length; i < len; i++) {
                    p1 = this.points[i];
                    p2 = this.points[i + 1] || p1;
                    NxtControl.Util.drawDashedLine(ctx, p1.x, p1.y, p2.x, p2.y, this.pen.dashArray);
                }
            };
            Polyline.prototype.toObject = function (propertiesToInclude) {
                var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    points: this.points.concat(),
                    closed: this.closed
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            Polyline.fromObject = function (object) {
                return new Polyline().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Polyline')
            ], Polyline.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Polyline')
            ], Polyline.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.PolygonDesigner')
            ], Polyline.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], Polyline.prototype, "closed", void 0);
            __decorate([
                System.Browsable
            ], Polyline.prototype, "dpoints", void 0);
            return Polyline;
        }(GuiFramework.Shape));
        GuiFramework.Polyline = Polyline;
        var clone = NxtControl.Util.object.clone;
        var FreeText = (function (_super) {
            __extends(FreeText, _super);
            function FreeText() {
                _super.apply(this, arguments);
            }
            FreeText.prototype.getText = function () {
                return this.text;
            };
            FreeText.prototype.setText = function (text) {
                return this._set('text', text, true);
            };
            FreeText.prototype.getFontSize = function () {
                return this.fontSize;
            };
            FreeText.prototype.setFontSize = function (fontSize) {
                return this._set('fontSize', fontSize, true);
            };
            FreeText.prototype.getFontWeight = function () {
                return this.fontWeight;
            };
            FreeText.prototype.setFontWeight = function (fontWeight) {
                return this._set('fontWeight', fontWeight, true);
            };
            FreeText.prototype.getFontFamily = function () {
                return this.fontFamily;
            };
            FreeText.prototype.setFontFamily = function (fontFamily) {
                return this._set('fontFamily', fontFamily, true);
            };
            FreeText.prototype.getTextDecoration = function () {
                return this.textDecoration;
            };
            FreeText.prototype.setTextDecoration = function (textDecoration) {
                return this._set('textDecoration', textDecoration, true);
            };
            FreeText.prototype.getTextAlign = function () {
                return this.textAlign;
            };
            FreeText.prototype.setTextAlign = function (textAlign) {
                return this._set('textAlign', textAlign, true);
            };
            FreeText.prototype.getFontStyle = function () {
                return this.fontStyle;
            };
            FreeText.prototype.setFontStyle = function (fontStyle) {
                return this._set('fontStyle', fontStyle, true);
            };
            FreeText.prototype.getTextColor = function () {
                return this.textColor;
            };
            FreeText.prototype.setTextColor = function (clr) {
                return this._set('textColor', clr, true);
            };
            FreeText.prototype.load = function (options) {
                options = options || {};
                var text = '';
                if (options.hasOwnProperty('text')) {
                    text = options.text;
                    delete options.text;
                }
                this.text = text;
                this.__skipDimension = true;
                this.setOptions(options);
                this.__skipDimension = false;
                this._initDimensions();
                this.setCoords();
                return this;
            };
            FreeText.prototype._initDimensions = function () {
                if (this.__skipDimension)
                    return;
                var canvasEl = NxtControl.Util.createCanvasElement();
                this._render(canvasEl.getContext('2d'));
            };
            FreeText.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (key === 'textColor' || key == 'textBackgroundColor') {
                    value = NxtControl.Drawing.Color.fromObject(value);
                    this.brush = new NxtControl.Drawing.SolidBrush({ color: value });
                }
                if (key === 'fontFamily' && this.path) {
                    this.path = this.path.replace(/(.*?)([^\/]*)(\.font\.js)/, '$1' + value + '$3');
                }
                _super.prototype._set.call(this, key, value, invalidate);
                if (key in this._dimensionAffectingProps) {
                    this._initDimensions();
                    this.setCoords();
                }
                if (this.designer !== null)
                    this.designer.propertyChanged(key);
                return this;
            };
            FreeText.prototype.setDesigner = function (designer, canvas) {
                this['hasControls'] = this['hasBorders'] = false;
                _super.prototype.setDesigner.call(this, designer, canvas);
            };
            FreeText.prototype._render = function (ctx, noTransform) {
                var isInPathGroup = this.group && this.group.getType() === 'path-group';
                if (isInPathGroup && !this.transformMatrix) {
                    ctx.translate(-this.group.getWidth() / 2 + this.left, -this.group.getHeight() / 2 + this.top);
                }
                else if (isInPathGroup && this.transformMatrix) {
                    ctx.translate(-this.group.getWidth() / 2, -this.group.getHeight() / 2);
                }
                if (typeof Cufon === 'undefined' || this.useNative === true) {
                    this._renderViaNative(ctx);
                }
                else {
                    this._renderViaCufon(ctx);
                }
            };
            FreeText.prototype._renderViaCufon = function (ctx) {
                var o = Cufon.textOptions || (Cufon.textOptions = {});
                o.left = this.left;
                o.top = this.top;
                o.context = ctx;
                o.color = this.brush;
                var el = this._initDummyElementForCufon();
                this.transform(ctx);
                Cufon.replaceElement(el, {
                    engine: 'canvas',
                    separate: 'none',
                    fontFamily: this.fontFamily,
                    fontWeight: this.fontWeight,
                    textDecoration: this.textDecoration,
                    textAlign: this.textAlign,
                    fontStyle: this.fontStyle,
                    lineHeight: this.lineHeight,
                    stroke: this.pen ? this.pen.toLive() : this.pen,
                    strokeWidth: this.pen ? this.pen.width : 0,
                    backgroundColor: this.textBackgroundColor && this.textBackgroundColor.toLive(),
                    textBackgroundColor: this.textBackgroundColor && this.textBackgroundColor.toLive(),
                });
                this.width = o.width;
                this.height = o.height;
                this._totalLineHeight = o.totalLineHeight;
                this._boundaries = o.boundaries;
                el = null;
                this.setCoords();
            };
            FreeText.prototype._initDummyElementForCufon = function () {
                var el = System.document.createElement('pre'), container = System.document.createElement('div');
                container.appendChild(el);
                el.innerText = this.text.replace(/\r?\n/gi, '\r');
                el.style.fontSize = this.fontSize + 'px';
                el.style.letterSpacing = 'normal';
                return el;
            };
            FreeText.prototype._renderViaNative = function (ctx) {
                var textLines = this.text.split(FreeText._reNewline);
                this.transform(ctx, System.isLikelyNode);
                this._setTextStyles(ctx);
                this.width = this._getTextWidth(ctx, textLines);
                this.height = this._getTextHeight(ctx, textLines);
                this.clipTo && NxtControl.Util.Clip.clipContext(this, ctx);
                this._renderTextBackground(ctx, textLines);
                this._translateForTextAlign(ctx);
                this._renderText(ctx, textLines);
                if (this.textAlign !== 'left' && this.textAlign !== 'justify') {
                    ctx.restore();
                }
                this._renderTextDecoration(ctx, textLines);
                this.clipTo && ctx.restore();
                this._setBoundaries(ctx, textLines);
                this._totalLineHeight = 0;
            };
            FreeText.prototype._renderText = function (ctx, textLines) {
                ctx.save();
                this._renderTextFill(ctx, textLines);
                ctx.restore();
            };
            FreeText.prototype._translateForTextAlign = function (ctx) {
                if (this.textAlign !== 'left' && this.textAlign !== 'justify') {
                    ctx.save();
                    ctx.translate(this.textAlign === 'center' ? (this.width / 2) : this.width, 0);
                }
            };
            FreeText.prototype._setBoundaries = function (ctx, textLines) {
                this._boundaries = [];
                for (var i = 0, len = textLines.length; i < len; i++) {
                    var lineWidth = this._getLineWidth(ctx, textLines[i]);
                    var lineLeftOffset = this._getLineLeftOffset(lineWidth);
                    this._boundaries.push({
                        height: this.fontSize * this.lineHeight,
                        width: lineWidth,
                        left: lineLeftOffset
                    });
                }
            };
            FreeText.prototype._setFillStyles = function (ctx) {
                ctx.fillStyle = this.textColor.toLive();
            };
            FreeText.prototype._setTextStyles = function (ctx) {
                this._setFillStyles(ctx);
                this._setStrokeStyles(ctx);
                ctx.textBaseline = 'alphabetic';
                if (!this.skipTextAlign) {
                    ctx.textAlign = this.textAlign;
                }
                ctx.font = this._getFontDeclaration();
            };
            FreeText.prototype._getTextHeight = function (ctx, textLines) {
                return this.fontSize * textLines.length * this.lineHeight;
            };
            FreeText.prototype._getTextWidth = function (ctx, textLines) {
                var maxWidth = ctx.measureText(textLines[0] || '|').width;
                for (var i = 1, len = textLines.length; i < len; i++) {
                    var currentLineWidth = ctx.measureText(textLines[i]).width;
                    if (currentLineWidth > maxWidth) {
                        maxWidth = currentLineWidth;
                    }
                }
                return maxWidth;
            };
            FreeText.prototype._renderChars = function (method, ctx, chars, left, top, lineIndex) {
                ctx[method](chars, left, top);
            };
            FreeText.prototype._renderTextLine = function (method, ctx, line, left, top, lineIndex) {
                top -= this.fontSize / 4;
                if (this.textAlign !== 'justify') {
                    this._renderChars(method, ctx, line, left, top, lineIndex);
                    return;
                }
                var lineWidth = ctx.measureText(line).width;
                var totalWidth = this.width;
                if (totalWidth > lineWidth) {
                    var words = line.split(/\s+/);
                    var wordsWidth = ctx.measureText(line.replace(/\s+/g, '')).width;
                    var widthDiff = totalWidth - wordsWidth;
                    var numSpaces = words.length - 1;
                    var spaceWidth = widthDiff / numSpaces;
                    var leftOffset = 0;
                    for (var i = 0, len = words.length; i < len; i++) {
                        this._renderChars(method, ctx, words[i], left + leftOffset, top, lineIndex);
                        leftOffset += ctx.measureText(words[i]).width + spaceWidth;
                    }
                }
                else {
                    this._renderChars(method, ctx, line, left, top, lineIndex);
                }
            };
            FreeText.prototype._getLeftOffset = function () {
                if (System.isLikelyNode) {
                    return 0;
                }
                return -this.width / 2;
            };
            FreeText.prototype._getTopOffset = function () {
                return -this.height / 2;
            };
            FreeText.prototype._renderTextFill = function (ctx, textLines) {
                if (!this.brush && !this._skipFillStrokeCheck)
                    return;
                this._boundaries = [];
                var lineHeights = 0;
                for (var i = 0, len = textLines.length; i < len; i++) {
                    var heightOfLine = this._getHeightOfLine(ctx, i, textLines);
                    lineHeights += heightOfLine;
                    this._renderTextLine('fillText', ctx, textLines[i], this._getLeftOffset(), this._getTopOffset() + lineHeights, i);
                }
            };
            FreeText.prototype._renderTextStroke = function (ctx, textLines) {
                if (!this.pen && !this._skipFillStrokeCheck)
                    return;
                var lineHeights = 0;
                ctx.save();
                if (this.pen.dashArray) {
                    if (1 & this.pen.dashArray.length) {
                        this.pen.dashArray.push.apply(this.pen.dashArray, this.pen.dashArray);
                    }
                    supportsLineDash && ctx.setLineDash(this.pen.dashArray);
                }
                ctx.beginPath();
                for (var i = 0, len = textLines.length; i < len; i++) {
                    var heightOfLine = this._getHeightOfLine(ctx, i, textLines);
                    lineHeights += heightOfLine;
                    this._renderTextLine('strokeText', ctx, textLines[i], this._getLeftOffset(), this._getTopOffset() + lineHeights, i);
                }
                ctx.closePath();
                ctx.restore();
            };
            FreeText.prototype._getHeightOfLine = function (ctx, index, textLines) {
                return this.fontSize * this.lineHeight;
            };
            FreeText.prototype._renderTextBackground = function (ctx, textLines) {
                this._renderTextBoxBackground(ctx);
                this._renderTextLinesBackground(ctx, textLines);
            };
            FreeText.prototype._renderTextBoxBackground = function (ctx) {
                if (!this.textBackgroundColor || this.textBackgroundColor.isTransparent())
                    return;
                ctx.save();
                ctx.fillStyle = this.textBackgroundColor.toLive();
                ctx.fillRect(this._getLeftOffset(), this._getTopOffset(), this.width, this.height);
                ctx.restore();
            };
            FreeText.prototype._renderTextLinesBackground = function (ctx, textLines) {
                if (!this.textBackgroundColor || this.textBackgroundColor.isTransparent())
                    return;
                ctx.save();
                ctx.fillStyle = this.textBackgroundColor.toLive();
                for (var i = 0, len = textLines.length; i < len; i++) {
                    if (textLines[i] !== '') {
                        var lineWidth = this._getLineWidth(ctx, textLines[i]);
                        var lineLeftOffset = this._getLineLeftOffset(lineWidth);
                        ctx.fillRect(this._getLeftOffset() + lineLeftOffset, this._getTopOffset() + (i * this.fontSize * this.lineHeight), lineWidth, this.fontSize * this.lineHeight);
                    }
                }
                ctx.restore();
            };
            FreeText.prototype._getLineLeftOffset = function (lineWidth) {
                if (this.textAlign === 'center') {
                    return (this.width - lineWidth) / 2;
                }
                if (this.textAlign === 'right') {
                    return this.width - lineWidth;
                }
                return 0;
            };
            FreeText.prototype._getLineWidth = function (ctx, line) {
                return this.textAlign === 'justify'
                    ? this.width
                    : ctx.measureText(line).width;
            };
            FreeText.prototype._renderTextDecoration = function (ctx, textLines) {
                if (!this.textDecoration)
                    return;
                var halfOfVerticalBox = this._getTextHeight(ctx, textLines) / 2;
                var _this = this;
                function renderLinesAtOffset(offset) {
                    for (var i = 0, len = textLines.length; i < len; i++) {
                        var lineWidth = _this._getLineWidth(ctx, textLines[i]);
                        var lineLeftOffset = _this._getLineLeftOffset(lineWidth);
                        ctx.fillRect(_this._getLeftOffset() + lineLeftOffset, ~~((offset + (i * _this._getHeightOfLine(ctx, i, textLines))) - halfOfVerticalBox), lineWidth, 1);
                    }
                }
                if (this.textDecoration.indexOf('underline') > -1) {
                    renderLinesAtOffset(this.fontSize * this.lineHeight);
                }
                if (this.textDecoration.indexOf('line-through') > -1) {
                    renderLinesAtOffset(this.fontSize * this.lineHeight - this.fontSize / 2);
                }
                if (this.textDecoration.indexOf('overline') > -1) {
                    renderLinesAtOffset(this.fontSize * this.lineHeight - this.fontSize);
                }
            };
            FreeText.prototype._getFontDeclaration = function () {
                return [
                    (System.isLikelyNode ? this.fontWeight : this.fontStyle),
                    (System.isLikelyNode ? this.fontStyle : this.fontWeight),
                    this.fontSize + 'px',
                    (System.isLikelyNode ? ('"' + this.fontFamily + '"') : this.fontFamily)
                ].join(' ');
            };
            FreeText.prototype.render = function (ctx, noTransform) {
                if (!this.visible)
                    return;
                ctx.save();
                this._render(ctx);
                if (!noTransform && this.active) {
                    this.drawBorders(ctx);
                    this.drawControls(ctx);
                }
                ctx.restore();
            };
            FreeText.prototype.toObject = function (propertiesToInclude) {
                var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    text: this.text,
                    fontSize: this.fontSize,
                    fontWeight: this.fontWeight,
                    fontFamily: this.fontFamily,
                    fontStyle: this.fontStyle,
                    textDecoration: this.textDecoration,
                    textAlign: this.textAlign,
                    path: this.path,
                    textColor: this.textColor,
                    textBackgroundColor: this.textBackgroundColor,
                    useNative: this.useNative
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                delete object['brush'];
                delete object['pen'];
                return object;
            };
            FreeText.fromObject = function (object) {
                return new FreeText().load(object);
            };
            ;
            FreeText._reNewline = /\r?\n/;
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.FreeText')
            ], FreeText.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.FreeText')
            ], FreeText.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.FreeTextDesigner')
            ], FreeText.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], FreeText.prototype, "text", void 0);
            __decorate([
                System.DefaultValue(14),
                System.Browsable
            ], FreeText.prototype, "fontSize", void 0);
            __decorate([
                System.DefaultValue('normal'),
                System.Browsable
            ], FreeText.prototype, "fontWeight", void 0);
            __decorate([
                System.DefaultValue('Arial'),
                System.Browsable
            ], FreeText.prototype, "fontFamily", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], FreeText.prototype, "textDecoration", void 0);
            __decorate([
                System.DefaultValue('left'),
                System.Browsable
            ], FreeText.prototype, "textAlign", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], FreeText.prototype, "fontStyle", void 0);
            __decorate([
                System.DefaultValue(1.3)
            ], FreeText.prototype, "lineHeight", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.Black),
                System.Browsable
            ], FreeText.prototype, "textColor", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], FreeText.prototype, "textBackgroundColor", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], FreeText.prototype, "path", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], FreeText.prototype, "useNative", void 0);
            __decorate([
                System.DefaultValue(null)
            ], FreeText.prototype, "pen", void 0);
            __decorate([
                System.DefaultValue({
                    fontSize: true,
                    fontWeight: true,
                    fontFamily: true,
                    textDecoration: true,
                    fontStyle: true,
                    lineHeight: true,
                    textColor: true,
                    text: true
                })
            ], FreeText.prototype, "_dimensionAffectingProps", void 0);
            return FreeText;
        }(GuiFramework.Shape));
        GuiFramework.FreeText = FreeText;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var Image = (function (_super) {
            __extends(Image, _super);
            function Image() {
                _super.call(this);
                this.autoSize = true;
                this._stroke = this.myStroke;
                this.imageLoaded = new NxtControl.event();
            }
            Image.prototype.load = function (options) {
                options || (options = {});
                options.crossOrigin = options.crossOrigin || this.crossOrigin;
                this._inInitialize = true;
                _super.prototype.load.call(this, options);
                this._element = null;
                this._initConfig(options);
                this._inInitialize = false;
                return this;
            };
            Image.prototype.setSrc = function (value) {
                return this.set('src', value);
            };
            Image.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (key === 'src') {
                    NxtControl.Util.loadImage(value, this._setElement.bind(this), null, value.indexOf('data') === 0 ? null : (this.crossOrigin || ''));
                    return this;
                }
                _super.prototype._set.call(this, key, value, invalidate);
                return this;
            };
            Image.prototype.getElement = function () {
                return this._element;
            };
            Image.prototype._setElement = function (element) {
                this._element = element;
                this._initConfig({ width: this.width, height: this.height });
                this.invalidate();
                this.imageLoaded.trigger(this, NxtControl.EmptyEventArgs);
                return this;
            };
            Image.prototype.setElement = function (element) {
                this._element = element;
                this._initConfig();
                this.setCoords();
                this.invalidate();
                return this;
            };
            Image.prototype.setCrossOrigin = function (value) {
                this.crossOrigin = value;
                this._element.crossOrigin = value;
                return this;
            };
            Image.prototype.getOriginalSize = function () {
                var element = this.getElement();
                if (element)
                    return { width: element.width, height: element.height };
                return { width: this.width, height: this.height };
            };
            Image.prototype.render = function (ctx, noTransform) {
                if (!this.getVisible())
                    return;
                ctx.save();
                var m = this.transformMatrix;
                var isInPathGroup = this.group && this.group.getType() === 'path-group';
                if (isInPathGroup) {
                    ctx.translate(-this.group.getWidth() / 2 + this.width / 2, -this.group.getHeight() / 2 + this.height / 2);
                }
                if (m) {
                    ctx.transform(m[0], m[1], m[2], m[3], m[4], m[5]);
                }
                if (!noTransform) {
                    this.transform(ctx, false);
                }
                ctx.save();
                this.clipTo && NxtControl.Util.Clip.clipContext(this, ctx);
                this._render(ctx);
                this._renderStroke(ctx);
                this.clipTo && ctx.restore();
                ctx.restore();
                if (this.active && !noTransform) {
                    this.drawBorders(ctx);
                    this.drawControls(ctx);
                }
                ctx.restore();
            };
            Image.prototype.myStroke = function (ctx) {
                ctx.save();
                this._setStrokeStyles(ctx);
                ctx.beginPath();
                ctx.strokeRect(-this.width / 2, -this.height / 2, this.width, this.height);
                ctx.closePath();
                ctx.restore();
            };
            Image.prototype._renderDashedStroke = function (ctx) {
                var x = -this.width / 2, y = -this.height / 2, w = this.width, h = this.height;
                ctx.save();
                this._setStrokeStyles(ctx);
                ctx.beginPath();
                NxtControl.Util.drawDashedLine(ctx, x, y, x + w, y, this.pen.dashArray);
                NxtControl.Util.drawDashedLine(ctx, x + w, y, x + w, y + h, this.pen.dashArray);
                NxtControl.Util.drawDashedLine(ctx, x + w, y + h, x, y + h, this.pen.dashArray);
                NxtControl.Util.drawDashedLine(ctx, x, y + h, x, y, this.pen.dashArray);
                ctx.closePath();
                ctx.restore();
            };
            Image.prototype.toObject = function (propertiesToInclude) {
                return extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    src: this._element ? (this._element.src || this._element._src) : null,
                    crossOrigin: this.crossOrigin,
                    autoSize: this.autoSize
                });
            };
            Image.prototype.getSrc = function () {
                return this.getElement().src || this.getElement()._src;
            };
            Image.prototype.clone = function (callback, propertiesToInclude) {
                return Image.fromObject(this.toObject(propertiesToInclude));
            };
            Image.prototype._render = function (ctx, noTransform) {
                if (this._element === null)
                    return;
                if (this.autoSize) {
                    try {
                        ctx.drawImage(this._element, -this.width / 2, -this.height / 2, this.width, this.height);
                    }
                    catch (err) { }
                }
                else {
                    ctx.save();
                    ctx.beginPath();
                    ctx.rect(-this.width / 2, -this.height / 2, this.width, this.height);
                    ctx.clip();
                    try {
                        ctx.drawImage(this._element, -this._element.width / 2, -this._element.height / 2, this._element.width, this._element.height);
                    }
                    catch (err) { }
                    ctx.restore();
                }
            };
            Image.prototype._resetWidthHeight = function () {
                var element = this.getElement();
                this.set('width', element.width);
                this.set('height', element.height);
            };
            Image.prototype._initConfig = function (options) {
                options || (options = {});
                this.setOptions(options);
                this._setWidthHeight(options);
            };
            Image.prototype._setWidthHeight = function (options) {
                this.width = 'width' in options
                    ? options.width
                    : (this.getElement().width || 0);
                this.height = 'height' in options
                    ? options.height
                    : (this.getElement().height || 0);
            };
            Image.fromObject = function (object) {
                return new Image().load(object);
            };
            Image.CSS_CANVAS = 'canvas-img';
            Image.pngCompression = 1;
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Image')
            ], Image.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Image')
            ], Image.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.ImageDesigner')
            ], Image.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Image.prototype, "autoSize", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], Image.prototype, "src", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Image.prototype, "pen", void 0);
            __decorate([
                System.DefaultValue('')
            ], Image.prototype, "crossOrigin", void 0);
            return Image;
        }(GuiFramework.Shape));
        GuiFramework.Image = Image;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var TextBase = (function (_super) {
            __extends(TextBase, _super);
            function TextBase() {
                _super.call(this);
                this.textChanged = new NxtControl.event();
            }
            TextBase.prototype.getText = function () {
                return this.text;
            };
            TextBase.prototype.setText = function (txt) {
                return this._set('text', txt, true);
            };
            TextBase.prototype.getTextColor = function () {
                return this.textColor;
            };
            TextBase.prototype.setTextColor = function (clr) {
                return this._set('textColor', clr, true);
            };
            TextBase.prototype.getTextDisabledColor = function () {
                return this.textDisabledColor;
            };
            TextBase.prototype.setTextDisabledColor = function (clr) {
                return this._set('textDisabledColor', clr, true);
            };
            TextBase.prototype.getTextAlign = function () {
                return this.textAlign;
            };
            TextBase.prototype.setTextAlign = function (textAlign) {
                return this._set('textAlign', textAlign, true);
            };
            TextBase.prototype.getTextPadding = function () {
                return this.textPadding;
            };
            TextBase.prototype.setTextPadding = function (textPadding) {
                return this._set('textPadding', textPadding, true);
            };
            TextBase.prototype.getPrefixColor = function () {
                return this.prefixColor;
            };
            TextBase.prototype.setPrefixColor = function (clr) {
                return this._set('prefixColor', clr, true);
            };
            TextBase.prototype.getBackColor = function () {
                return this.backColor;
            };
            TextBase.prototype.setBackColor = function (clr) {
                return this._set('backColor', clr, true);
            };
            TextBase.prototype.getPrefix = function () {
                return this.prefix;
            };
            TextBase.prototype.setPrefix = function (txt) {
                return this._set('prefix', txt, true);
            };
            TextBase.prototype.getPrefixPadding = function () {
                return this.prefixPadding;
            };
            TextBase.prototype.setPrefixPadding = function (prefixPadding) {
                return this._set('prefixPadding', prefixPadding, true);
            };
            TextBase.prototype.getSuffixColor = function () {
                return this.suffixColor;
            };
            TextBase.prototype.setSuffixColor = function (clr) {
                return this._set('suffixColor', clr, true);
            };
            TextBase.prototype.getSuffix = function () {
                return this.prefix;
            };
            TextBase.prototype.setSuffix = function (txt) {
                return this._set('suffix', txt, true);
            };
            TextBase.prototype.getSuffixPadding = function () {
                return this.suffixPadding;
            };
            TextBase.prototype.setSuffixPadding = function (suffixPadding) {
                return this._set('suffixPadding', suffixPadding, true);
            };
            TextBase.prototype.getIsPrefixSuffixOutside = function () {
                return this.isPrefixSuffixOutside;
            };
            TextBase.prototype.setIsPrefixSuffixOutside = function (isPrefixSuffixOutside) {
                return this._set('isPrefixSuffixOutside', isPrefixSuffixOutside, true);
            };
            TextBase.prototype.getFontFamily = function () {
                return this.fontFamily;
            };
            TextBase.prototype.setFontFamily = function (fontFamily) {
                return this._set('fontFamily', fontFamily, true);
            };
            TextBase.prototype.getFontWeight = function () {
                return this.fontWeight;
            };
            TextBase.prototype.setFontWeight = function (fontWeight) {
                return this._set('fontWeight', fontWeight, true);
            };
            TextBase.prototype.getFontSize = function () {
                return this.fontSize;
            };
            TextBase.prototype.setFontSize = function (fontSize) {
                return this._set('fontSize', fontSize, true);
            };
            TextBase.prototype.getFontStyle = function () {
                return this.fontStyle;
            };
            TextBase.prototype.setFontStyle = function (fontStyle) {
                return this._set('fontStyle', fontStyle, true);
            };
            TextBase.prototype.load = function (options) {
                options = options || {};
                this._inInitialize = true;
                _super.prototype.load.call(this, options);
                this.x = options.x || 0;
                this.y = options.y || 0;
                this._inInitialize = false;
                this._drawTextBase = true;
                this.__isMouseDown = false;
                this._calculateSize();
                return this;
            };
            TextBase.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (key === 'textColor' || key === 'textDisabledColor' || key === 'backColor' || key === 'suffixColor' || key === 'prefixColor')
                    value = NxtControl.Drawing.Color.fromObject(value);
                if (this.designer !== null && this._inInitialize === false) {
                    if (key === 'scaleX') {
                        if (value <= 0)
                            return this;
                        this._set('width', value * this.width, invalidate);
                        return this;
                    }
                    else if (key === 'scaleY') {
                        if (value <= 0)
                            return this;
                        this._set('height', value * this.height, invalidate);
                        return this;
                    }
                }
                if (key === 'text') {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    if (this._inInitialize === false) {
                        this._textWidth = this._getTextWidth(this.text);
                        this.textChanged.trigger(this, { text: value });
                    }
                }
                else if (key === 'backColor' && this._inInitialize === false) {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    _super.prototype._set.call(this, 'brush', { color: value }, invalidate);
                }
                else if ((key === 'fontSize' || key === 'fontWeight' || key === 'fontStyle' || key === 'textPadding' || key === 'prefix' || key === 'suffix' || key === 'prefixPadding' || key === 'suffixPadding') && this._inInitialize === false) {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    this._calculateSize();
                }
                else if ((key === 'width' || key === 'height') && this._inInitialize === false) {
                    if (key === 'width')
                        _super.prototype._set.call(this, key, value, invalidate);
                }
                else {
                    _super.prototype._set.call(this, key, value), invalidate;
                }
                this.invalidate();
                return this;
            };
            TextBase.prototype._calculateSize = function () {
                this._textWidth = this._getTextWidth(this.text);
                this._prefixTextWidth = this.prefix.length === 0 ? 0 : (this._getTextWidth(this.prefix) + this.prefixPadding);
                this._suffixTextWidth = this.suffix.length === 0 ? 0 : (this._getTextWidth(this.suffix) + this.suffixPadding);
                this.height = this.fontSize + this.textPadding * 2;
            };
            TextBase.prototype._getTextWidth = function (text) {
                var canvasEl = NxtControl.Util.createCanvasElement();
                var ctx = canvasEl.getContext('2d');
                this._setTextStyles(ctx, this.textColor);
                return ctx.measureText(text).width;
            };
            TextBase.prototype._setTextStyles = function (ctx, color) {
                ctx.fillStyle = color.toLive();
                ctx.strokeStyle = color.toLive();
                ctx.textBaseline = 'bottom';
                ctx.font = this._getFontDeclaration();
            };
            TextBase.prototype._getFontDeclaration = function () {
                return [
                    (System.isLikelyNode ? this.fontWeight : this.fontStyle),
                    (System.isLikelyNode ? this.fontStyle : this.fontWeight),
                    this.fontSize + 'px',
                    (System.isLikelyNode ? ('"' + this.fontFamily + '"') : this.fontFamily)
                ].join(' ');
            };
            TextBase.prototype._renderBackground = function (ctx, noTransform) {
                var x = -this.width / 2, y = -this.height / 2, w = this.width, h = this.height, isInPathGroup = this.group && this.group.getType() === 'path-group';
                ctx.beginPath();
                if (this.transformMatrix && isInPathGroup) {
                    ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                }
                if (!this.transformMatrix && isInPathGroup) {
                    ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                }
                ctx.moveTo(x, y);
                ctx.lineTo(x + w, y);
                ctx.lineTo(x + w, y + h);
                ctx.lineTo(x, y + h);
                ctx.lineTo(x, y);
                ctx.closePath();
                this._renderFill(ctx);
                this._renderStroke(ctx);
            };
            TextBase.prototype._render = function (ctx, noTransform) {
                this._renderBackground(ctx, noTransform);
                if (this._drawTextBase === false)
                    return;
                var x = -this.width / 2, y = -this.height / 2;
                if (this.textAlign === 'right') {
                    x = this.width / 2 - this.textPadding - this._textWidth;
                    if (!this.isPrefixSuffixOutside)
                        x -= this._prefixTextWidth + this._suffixTextWidth;
                }
                else if (this.textAlign === 'center') {
                    if (this.isPrefixSuffixOutside)
                        x = -this._textWidth / 2;
                    else
                        x = -(this._textWidth + this._prefixTextWidth + this._suffixTextWidth) / 2;
                }
                else
                    x += this.textPadding;
                y += this._getTextPos();
                ctx.save();
                ctx.beginPath();
                ctx.rect(-this.width / 2 + this.textPadding, -this.height / 2 + this.textPadding, this.width - 2 * this.textPadding, this.height - 2 * this.textPadding);
                ctx.clip();
                if (this.isPrefixSuffixOutside === false) {
                    if (this._prefixTextWidth !== 0) {
                        this._setTextStyles(ctx, this.prefixColor);
                        ctx.fillText(this.prefix, x, y);
                        x += this._prefixTextWidth;
                    }
                }
                this._setTextStyles(ctx, this._getTextColor());
                ctx.fillText(this.text, x, y);
                if (this.isPrefixSuffixOutside === false) {
                    if (this._suffixTextWidth !== 0) {
                        this._setTextStyles(ctx, this.suffixColor);
                        ctx.fillText(this.suffix, x + this._textWidth + this.suffixPadding, y);
                    }
                }
                ctx.restore();
                if (this.isPrefixSuffixOutside) {
                    if (this._prefixTextWidth !== 0) {
                        this._setTextStyles(ctx, this.prefixColor);
                        ctx.fillText(this.prefix, -this.width / 2 - this._prefixTextWidth, y);
                    }
                    if (this._suffixTextWidth !== 0) {
                        this._setTextStyles(ctx, this.suffixColor);
                        ctx.fillText(this.suffix, this.width / 2 + this.suffixPadding, y);
                    }
                }
            };
            TextBase.prototype._getTextPos = function () {
                return this.fontSize + this.textPadding + (NxtControl.Util.isMobile() ? 3 : 0);
            };
            TextBase.prototype._getTextColor = function () {
                return this.textColor;
            };
            TextBase.prototype.toObject = function (propertiesToInclude) {
                var object = extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    text: this.get('text'),
                    backColor: this.get('backColor'),
                    textColor: this.get('textColor'),
                    textPadding: this.get('textPadding'),
                    textAlign: this.get('textAlign'),
                    prefixColor: this.get('prefixColor'),
                    prefix: this.get('prefix'),
                    prefixPadding: this.get('prefixPadding'),
                    suffixColor: this.get('suffixColor'),
                    suffix: this.get('suffix'),
                    suffixPadding: this.get('suffixPadding'),
                    isPrefixSuffixOutside: this.get('isPrefixSuffixOutside'),
                    fontFamily: this.get('fontFamily'),
                    fontWeight: this.get('fontWeight'),
                    fontSize: this.get('fontSize'),
                    fontStyle: this.get('fontStyle')
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.TextBase')
            ], TextBase.prototype, "type", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], TextBase.prototype, "text", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('TextBoxTextColor')),
                System.Browsable
            ], TextBase.prototype, "textColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('TextBoxTextDisabledColor')),
                System.Browsable
            ], TextBase.prototype, "textDisabledColor", void 0);
            __decorate([
                System.DefaultValue('left'),
                System.Browsable
            ], TextBase.prototype, "textAlign", void 0);
            __decorate([
                System.DefaultValue(4),
                System.Browsable
            ], TextBase.prototype, "textPadding", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('TextBoxTextColor')),
                System.Browsable
            ], TextBase.prototype, "prefixColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('TextBoxBackColor')),
                System.Browsable
            ], TextBase.prototype, "backColor", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], TextBase.prototype, "prefix", void 0);
            __decorate([
                System.DefaultValue(2),
                System.Browsable
            ], TextBase.prototype, "prefixPadding", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('TextBoxTextColor')),
                System.Browsable
            ], TextBase.prototype, "suffixColor", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], TextBase.prototype, "suffix", void 0);
            __decorate([
                System.DefaultValue(2),
                System.Browsable
            ], TextBase.prototype, "suffixPadding", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], TextBase.prototype, "isPrefixSuffixOutside", void 0);
            __decorate([
                System.DefaultValue('Arial'),
                System.Browsable
            ], TextBase.prototype, "fontFamily", void 0);
            __decorate([
                System.DefaultValue('normal'),
                System.Browsable
            ], TextBase.prototype, "fontWeight", void 0);
            __decorate([
                System.DefaultValue(14),
                System.Browsable
            ], TextBase.prototype, "fontSize", void 0);
            __decorate([
                System.DefaultValue('normal'),
                System.Browsable
            ], TextBase.prototype, "fontStyle", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], TextBase.prototype, "x", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], TextBase.prototype, "y", void 0);
            return TextBase;
        }(GuiFramework.Shape));
        GuiFramework.TextBase = TextBase;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var DropDown = (function (_super) {
            __extends(DropDown, _super);
            function DropDown() {
                _super.call(this);
                this.itemSelected = new NxtControl.event();
                this._onClick = this._onClick.bind(this);
            }
            DropDown.prototype.getEnabled = function () {
                return this.enabled;
            };
            DropDown.prototype.setEnabled = function (enabled) {
                return this._set('enabled', enabled, true);
            };
            DropDown.prototype.getItems = function () {
                return this.items;
            };
            DropDown.prototype.setItems = function (items) {
                return this._set('items', items, false);
            };
            DropDown.prototype.getTextColor = function () {
                return this.textColor;
            };
            DropDown.prototype.setTextColor = function (clr) {
                return this._set('textColor', clr, true);
            };
            DropDown.prototype.getTextDisabledColor = function () {
                return this.textDisabledColor;
            };
            DropDown.prototype.setTextDisabledColor = function (clr) {
                return this._set('textDisabledColor', clr, true);
            };
            DropDown.prototype.getTextPadding = function () {
                return this.textPadding;
            };
            DropDown.prototype.setTextPadding = function (textPadding) {
                return this._set('textPadding', textPadding, true);
            };
            DropDown.prototype.getBackColor = function () {
                return this.backColor;
            };
            DropDown.prototype.setBackColor = function (clr) {
                return this._set('backColor', clr, true);
            };
            DropDown.prototype.getFontFamily = function () {
                return this.fontFamily;
            };
            DropDown.prototype.setFontFamily = function (fontFamily) {
                return this._set('fontFamily', fontFamily, true);
            };
            DropDown.prototype.getFontWeight = function () {
                return this.fontWeight;
            };
            DropDown.prototype.setFontWeight = function (fontWeight) {
                return this._set('fontWeight', fontWeight, true);
            };
            DropDown.prototype.getFontSize = function () {
                return this.fontSize;
            };
            DropDown.prototype.setFontSize = function (fontSize) {
                return this._set('fontSize', fontSize, true);
            };
            DropDown.prototype.getFontStyle = function () {
                return this.fontStyle;
            };
            DropDown.prototype.setFontStyle = function (fontStyle) {
                return this._set('fontStyle', fontStyle, true);
            };
            DropDown.prototype.initEvents = function () {
                _super.prototype.initEvents.call(this);
                this.click.add(this._onClick);
            };
            DropDown.prototype.disposeEvents = function () {
                _super.prototype.initEvents.call(this);
                this.click.remove(this._onClick);
            };
            DropDown.prototype.load = function (options) {
                options = options || {};
                if (!options.valueType)
                    options.valueType = 'int';
                this._inInitialize = true;
                _super.prototype.load.call(this, options);
                this.csClass = 'NxtControl.GuiHTMLFramework.DropDown<' + this.valueType + '>';
                this.x = options.x || 0;
                this.y = options.y || 0;
                this._inInitialize = false;
                this._drawDropDown = true;
                this.__isMouseDown = false;
                this._calculateSize();
                return this;
            };
            DropDown.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (key === 'textColor' || key === 'textDisabledColor' || key === 'backColor' || key === 'suffixColor' || key === 'prefixColor')
                    value = NxtControl.Drawing.Color.fromObject(value);
                if (this.designer !== null && this._inInitialize === false) {
                    if (key === 'scaleX') {
                        this._set('width', value * this.width, invalidate);
                        return this;
                    }
                    else if (key === 'scaleY') {
                        this._set('height', value * this.height, invalidate);
                        return this;
                    }
                }
                if (key === 'items') {
                    this[key] = value;
                    return this;
                }
                if (key === 'selectedItem') {
                    this.selectedText = '';
                    var found = false;
                    for (var j = 0; j < this.items.length; j++)
                        if (this.items[j].value == value) {
                            found = true;
                            this.selectedText = this.items[j].text || value.toString();
                            break;
                        }
                    if (!found)
                        value = null;
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    if (this._inInitialize === false) {
                        this._textWidth = this._getTextWidth(this.selectedText);
                        this.itemSelected.trigger(this, { text: this.selectedText, value: this.selectedItem });
                    }
                }
                else if (key === 'backColor' && this._inInitialize === false) {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    _super.prototype._set.call(this, 'brush', { color: value }, invalidate);
                }
                else if ((key === 'fontSize' || key === 'fontWeight' || key === 'fontStyle' || key === 'textPadding' || key === 'prefix' || key === 'suffix' || key === 'prefixPadding' || key === 'suffixPadding') && this._inInitialize === false) {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    this._calculateSize();
                }
                else if ((key === 'width' || key === 'height') && this._inInitialize === false) {
                    if (key === 'width')
                        _super.prototype._set.call(this, key, value, invalidate);
                }
                else {
                    _super.prototype._set.call(this, key, value), invalidate;
                }
                this.invalidate();
                return this;
            };
            DropDown.prototype._calculateSize = function () {
                if (this.selectedText)
                    this._textWidth = this._getTextWidth(this.selectedText);
                else
                    this._textWidth = 0;
                this.height = this.fontSize + this.textPadding * 2;
            };
            DropDown.prototype._getTextWidth = function (text) {
                var canvasEl = NxtControl.Util.createCanvasElement();
                var ctx = canvasEl.getContext('2d');
                this._setTextStyles(ctx, this.textColor);
                return ctx.measureText(text).width;
            };
            DropDown.prototype._setTextStyles = function (ctx, color) {
                ctx.fillStyle = color.toLive();
                ctx.strokeStyle = color.toLive();
                ctx.textBaseline = 'bottom';
                ctx.font = this._getFontDeclaration();
            };
            DropDown.prototype._getFontDeclaration = function () {
                return [
                    (System.isLikelyNode ? this.fontWeight : this.fontStyle),
                    (System.isLikelyNode ? this.fontStyle : this.fontWeight),
                    this.fontSize + 'px',
                    (System.isLikelyNode ? ('"' + this.fontFamily + '"') : this.fontFamily)
                ].join(' ');
            };
            DropDown.prototype._renderBackground = function (ctx, noTransform) {
                var x = -this.width / 2, y = -this.height / 2, w = this.width, h = this.height, isInPathGroup = this.group && this.group.getType() === 'path-group';
                ctx.beginPath();
                if (this.transformMatrix && isInPathGroup) {
                    ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                }
                if (!this.transformMatrix && isInPathGroup) {
                    ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                }
                ctx.moveTo(x, y);
                ctx.lineTo(x + w, y);
                ctx.lineTo(x + w, y + h);
                ctx.lineTo(x, y + h);
                ctx.lineTo(x, y);
                ctx.closePath();
                this._renderFill(ctx);
                this._renderStroke(ctx);
                ctx.strokeStyle = NxtControl.Drawing.Color.fromName(this.enabled ? 'ComboBoxArrowColor' : 'ComboBoxArrowDisabledColor').toLive();
                ctx.lineWidth = 3;
                ctx.beginPath();
                ctx.moveTo(x + w - 16, y + 2 * this.height / 5);
                ctx.lineTo(x + w - 10, y + 3 * this.height / 5);
                ctx.lineTo(x + w - 4, y + 2 * this.height / 5);
                ctx.stroke();
            };
            DropDown.prototype._render = function (ctx, noTransform) {
                this._renderBackground(ctx, noTransform);
                if (this._drawDropDown === false)
                    return;
                var x = -this.width / 2, y = -this.height / 2;
                x += this.textPadding;
                y += this._getTextPos();
                ctx.save();
                ctx.beginPath();
                ctx.rect(-this.width / 2 + this.textPadding, -this.height / 2 + this.textPadding, this.width - 2 * this.textPadding, this.height - 2 * this.textPadding);
                ctx.clip();
                if (this.selectedText) {
                    this._setTextStyles(ctx, this._getTextColor());
                    ctx.fillText(this.selectedText, x, y);
                }
                ctx.restore();
            };
            DropDown.prototype._getTextPos = function () {
                return this.fontSize + this.textPadding;
            };
            DropDown.prototype._getTextColor = function () {
                return this.textColor;
            };
            DropDown.prototype._onClick = function (sender, options) {
                if (this.enabled === false)
                    return;
                this.createContextMenu(options.e);
            };
            DropDown.prototype.destroyDropDown = function () {
                if (DropDown.menuContainer)
                    DropDown.menuContainer.parentElement.removeChild(DropDown.menuContainer);
                DropDown.menuContainer = null;
            };
            DropDown.prototype.createContextMenu = function (e) {
                var _this = this;
                if (DropDown.menuContainer) {
                    this.destroyDropDown();
                    return;
                }
                e.preventDefault();
                e.stopPropagation();
                var transformedLeftTop, transformedRightBottom;
                if (this.hasOwnProperty('group')) {
                    var m1 = this.getTransformation();
                    transformedLeftTop = m1.getTransformedPoint({ x: -this.width / 2, y: -this.height / 2 });
                    transformedRightBottom = m1.getTransformedPoint({ x: this.width / 2, y: this.height / 2 });
                }
                else {
                    transformedLeftTop = { x: this.left, y: this.top };
                    transformedRightBottom = { x: this.left + this.width, y: this.top + this.height };
                }
                NxtControl.Util.addListener(document.body, 'click', function (e) {
                    e.preventDefault();
                    _this.destroyDropDown();
                });
                var menuContainer = DropDown.menuContainer = document.createElement('div');
                menuContainer.style.position = 'absolute';
                menuContainer.style.left = transformedLeftTop.x + 'px';
                menuContainer.style.top = transformedRightBottom.y + 'px';
                menuContainer.style.width = (transformedRightBottom.x - transformedLeftTop.x) + 'px';
                menuContainer.style.background = this.backColor.toLive();
                menuContainer.style.zIndex = '99999';
                menuContainer.style.color = this.textColor.toLive();
                menuContainer.style.display = 'block';
                var menu = document.createElement('ul');
                menu.style.listStyleType = 'none';
                menu.style.margin = '0';
                menu.style.padding = '0 3px 0 3px';
                menu.style.left = '-2px';
                menu.style.top = '-2px';
                menu.style.border = 'solid 1px #ccc';
                menu.style.background = '#eee';
                menu.style.position = 'relative';
                NxtControl.Util.addListener(menu, 'mouseover', function () {
                    menu.style.cursor = 'pointer';
                });
                for (var ii = 0; ii < this.items.length; ii++) {
                    var item = this.items[ii];
                    var menuItem = document.createElement('li');
                    menuItem.style.textAlign = 'left';
                    menuItem.style.margin = '0';
                    menuItem.style.padding = '3px 0 3px 0';
                    menuItem.tag = item.value;
                    menuItem.innerHTML = '<div style="font:menu;width:100%;">' + (item.text || item.value.toString()) + '</div>';
                    menuItem.style.color = '#000';
                    NxtControl.Util.addListener(menuItem, 'mouseover', function () {
                        this.style.background = '#ccc';
                    });
                    NxtControl.Util.addListener(menuItem, 'mouseout', function () {
                        this.style.background = 'transparent';
                    });
                    NxtControl.Util.addListener(menuItem, 'click', function (e) {
                        e = e || window.event;
                        e.preventDefault();
                        e.stopPropagation();
                        _this.destroyDropDown();
                        _this._set('selectedItem', e.currentTarget.tag, true);
                        return false;
                    });
                    menu.appendChild(menuItem);
                }
                menuContainer.appendChild(menu);
                var currentCanvas = NxtControl.Services.CanvasTopologyService.instance.currentElement;
                currentCanvas.parentNode.insertBefore(menuContainer, currentCanvas.nextSibling);
            };
            DropDown.prototype.toObject = function (propertiesToInclude) {
                var object = extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    items: this.get('items'),
                    selectedItem: this.get('selectedItem'),
                    valueType: this.get('valueType'),
                    textColor: this.get('textColor'),
                    textPadding: this.get('textPadding'),
                    prefixColor: this.get('prefixColor'),
                    prefix: this.get('prefix'),
                    prefixPadding: this.get('prefixPadding'),
                    suffixColor: this.get('suffixColor'),
                    suffix: this.get('suffix'),
                    suffixPadding: this.get('suffixPadding'),
                    isPrefixSuffixOutside: this.get('isPrefixSuffixOutside'),
                    fontFamily: this.get('fontFamily'),
                    fontWeight: this.get('fontWeight'),
                    fontSize: this.get('fontSize'),
                    fontStyle: this.get('fontStyle')
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            DropDown.fromObject = function (object) {
                return new DropDown().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.DropDown')
            ], DropDown.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.DropDownDesigner')
            ], DropDown.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], DropDown.prototype, "valueType", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], DropDown.prototype, "enabled", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], DropDown.prototype, "selectedItem", void 0);
            __decorate([
                System.DefaultValue(null)
            ], DropDown.prototype, "selectedText", void 0);
            __decorate([
                System.DefaultValue([]),
                System.Browsable
            ], DropDown.prototype, "items", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ComboBoxTextColor')),
                System.Browsable
            ], DropDown.prototype, "textColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ComboBoxTextDisabledColor')),
                System.Browsable
            ], DropDown.prototype, "textDisabledColor", void 0);
            __decorate([
                System.DefaultValue(4),
                System.Browsable
            ], DropDown.prototype, "textPadding", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ComboBoxBackColor')),
                System.Browsable
            ], DropDown.prototype, "backColor", void 0);
            __decorate([
                System.DefaultValue('Arial'),
                System.Browsable
            ], DropDown.prototype, "fontFamily", void 0);
            __decorate([
                System.DefaultValue('normal'),
                System.Browsable
            ], DropDown.prototype, "fontWeight", void 0);
            __decorate([
                System.DefaultValue(14),
                System.Browsable
            ], DropDown.prototype, "fontSize", void 0);
            __decorate([
                System.DefaultValue('normal'),
                System.Browsable
            ], DropDown.prototype, "fontStyle", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], DropDown.prototype, "x", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], DropDown.prototype, "y", void 0);
            return DropDown;
        }(GuiFramework.Shape));
        GuiFramework.DropDown = DropDown;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var Button = (function (_super) {
            __extends(Button, _super);
            function Button() {
                _super.apply(this, arguments);
            }
            Button.prototype.getEnabled = function () {
                return this.enabled;
            };
            Button.prototype.setEnabled = function (enabled) {
                return this._set('enabled', enabled, true);
            };
            Button.prototype.getPushedBrush = function () {
                return this.pushedBrush;
            };
            Button.prototype.setPushedBrush = function (pushedBrush) {
                return this._set('pushedBrush', pushedBrush, true);
            };
            Button.prototype.getImage = function () {
                return this.image;
            };
            Button.prototype.setImage = function (image) {
                return this._set('image', image, true);
            };
            Button.prototype.getImageAutoSize = function () {
                return this.imageAutoSize;
            };
            Button.prototype.setImageAutoSize = function (imageAutoSize) {
                return this._set('imageAutoSize', imageAutoSize, true);
            };
            Button.prototype.getImagePadding = function () {
                return this.imagePadding;
            };
            Button.prototype.setImagePAdding = function (imagePadding) {
                return this._set('imagePadding', imagePadding, true);
            };
            Button.prototype.load = function (options) {
                options = options || {};
                options.rx = options.rx || 3;
                options.ry = options.ry || 3;
                options.height = options.height || ((options.fontSize || this.fontSize) + ((options.textPadding || this.textPadding) * 2));
                options.image = options.image || null;
                options.crossOrigin = options.crossOrigin || this.crossOrigin;
                this._image = null;
                this._inInitialize = true;
                _super.prototype.load.call(this, options);
                this._inInitialize = false;
                this.__isMouseDown = false;
                this._onMouseDown = this._onMouseDown.bind(this);
                this._onMouseUp = this._onMouseUp.bind(this);
                this._onClick = this._onClick.bind(this);
                return this;
            };
            Button.prototype.initEvents = function () {
                _super.prototype.initEvents.call(this);
                this.mousedown.add(this._onMouseDown);
                this.mouseup.add(this._onMouseUp);
                this.click.add(this._onClick);
            };
            Button.prototype.disposeEvents = function () {
                _super.prototype.initEvents.call(this);
                this.mousedown.remove(this._onMouseDown);
                this.mouseup.remove(this._onMouseUp);
                this.click.remove(this._onClick);
            };
            Button.prototype._calculateSize = function () {
                this._textWidth = this._getTextWidth(this.text);
                this._prefixTextWidth = this.prefix.length === 0 ? 0 : (this._getTextWidth(this.prefix) + this.prefixPadding);
                this._suffixTextWidth = this.suffix.length === 0 ? 0 : (this._getTextWidth(this.suffix) + this.suffixPadding);
            };
            Button.prototype.getImageElement = function () {
                return this._image;
            };
            Button.prototype.setImageElement = function (image) {
                this._image = image;
                this._image.crossOrigin = this.crossOrigin;
                this.invalidate();
                return this;
            };
            Button.prototype.setCrossOrigin = function (value) {
                this.crossOrigin = value;
                this._image.crossOrigin = value;
                return this;
            };
            Button.prototype.getOriginalSize = function () {
                var image = this.getImageElement();
                if (image === null)
                    return { width: 0, height: 0 };
                return { width: image.width, height: image.height };
            };
            Button.prototype._onMouseDown = function (sender, options) {
                if (this.enabled === false)
                    return;
                this.__isMouseDown = true;
                options.e.localTarget = this;
                this.captureMouse(this);
                this.invalidateImmediately();
            };
            Button.prototype._onMouseUp = function (sender, options) {
                if (this.enabled === false)
                    return;
                this.captureMouse(null);
                this.__isMouseDown = false;
                options.e.localTarget = this;
                this.invalidateImmediately();
            };
            Button.prototype._onClick = function (sender, options) {
                if (this.enabled === false)
                    return;
                options.e.localTarget = this;
            };
            Button.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (key === 'textDisabledColor')
                    value = NxtControl.Drawing.Color.fromObject(value);
                if (key === 'pushedBrush')
                    value = NxtControl.Drawing.Brush.fromObject(value);
                if (key === 'image') {
                    if (value === null || value === '')
                        this.image = null;
                    else if (typeof value === 'string') {
                        this.image = value;
                        NxtControl.Util.loadImage(value, this.setImageElement.bind(this), null, value.indexOf('data') === 0 ? null : (this.crossOrigin || ''));
                    }
                    invalidate && this.invalidate();
                    return this;
                }
                if (key === 'height') {
                    this.height = value;
                    if (this._inInitialize === false)
                        invalidate && this.invalidate();
                    return this;
                }
                _super.prototype._set.call(this, key, value, invalidate);
                return this;
            };
            Button.prototype._getTextPos = function () {
                return this.height / 2 + this.fontSize / 2;
            };
            Button.prototype._render = function (ctx) {
                var b;
                if (this.__isMouseDown) {
                    b = this.brush;
                    this.brush = this.pushedBrush;
                }
                _super.prototype._render.call(this, ctx);
                if (this.__isMouseDown) {
                    this.brush = b;
                }
                if (this.image !== null && this._image !== null) {
                    if (this.imageAutoSize) {
                        try {
                            var imgHeight = this.height - 2 * this.imagePadding;
                            if (imgHeight > 0) {
                                var imgRatio = imgHeight / this._image.height;
                                var imgWidth = this._image.width * imgRatio;
                                if (imgWidth <= this.width - 2 * this.imagePadding)
                                    ctx.drawImage(this._image, -imgWidth / 2, -imgHeight / 2, imgWidth, imgHeight);
                                else {
                                    imgWidth = this.width - 2 * this.imagePadding;
                                    if (imgWidth > 0) {
                                        imgRatio = imgWidth / this._image.width;
                                        imgHeight = this._image.height * imgRatio;
                                        ctx.drawImage(this._image, -imgWidth / 2, -imgHeight / 2, imgWidth, imgHeight);
                                    }
                                }
                            }
                        }
                        catch (err) { }
                    }
                    else {
                        ctx.save();
                        ctx.beginPath();
                        ctx.rect(-this.width / 2, -this.height / 2, this.width, this.height);
                        ctx.clip();
                        ctx.drawImage(this._image, -this._image.width / 2, -this._image.height / 2, this._image.width, this._image.height);
                        ctx.restore();
                    }
                }
            };
            Button.prototype.toObject = function (propertiesToInclude) {
                var object = extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    enabled: this.enabled,
                    image: this.image,
                    imageAutoSize: this.imageAutoSize,
                    imagePadding: this.imagePadding,
                    crossOrigin: this.crossOrigin,
                    textDisabledColor: this.textDisabledColor,
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            Button.fromObject = function (object) {
                return new Button().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Button')
            ], Button.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Button')
            ], Button.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.ButtonDesigner')
            ], Button.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Button.prototype, "enabled", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ButtonTextColor'))
            ], Button.prototype, "textColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ButtonTextDisabledColor')),
                System.Browsable
            ], Button.prototype, "textDisabledColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Brush.fromName('ButtonBrush'))
            ], Button.prototype, "brush", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Brush.fromName('ButtonPushedBrush')),
                System.Browsable
            ], Button.prototype, "pushedBrush", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Pen({ color: 'ButtonPenColor' }))
            ], Button.prototype, "pen", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], Button.prototype, "image", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Button.prototype, "imageAutoSize", void 0);
            __decorate([
                System.DefaultValue(4),
                System.Browsable
            ], Button.prototype, "imagePadding", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], Button.prototype, "crossOrigin", void 0);
            __decorate([
                System.DefaultValue('center')
            ], Button.prototype, "textAlign", void 0);
            return Button;
        }(GuiFramework.TextBase));
        GuiFramework.Button = Button;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var TwoStateButton = (function (_super) {
            __extends(TwoStateButton, _super);
            function TwoStateButton() {
                _super.call(this);
                this.checkedChanged = new NxtControl.event();
            }
            TwoStateButton.prototype.getChecked = function () {
                return this.checked;
            };
            TwoStateButton.prototype.setChecked = function (checked) {
                return this._set('checked', checked, true);
            };
            TwoStateButton.prototype.getEnabled = function () {
                return this.enabled;
            };
            TwoStateButton.prototype.setEnabled = function (enabled) {
                return this._set('enabled', enabled, true);
            };
            TwoStateButton.prototype.getTrueText = function () {
                return this.trueText;
            };
            TwoStateButton.prototype.setTrueText = function (trueText) {
                return this._set('trueText', trueText, true);
            };
            TwoStateButton.prototype.getFalseText = function () {
                return this.falseText;
            };
            TwoStateButton.prototype.setFalseText = function (falseText) {
                return this._set('falseText', falseText, true);
            };
            TwoStateButton.prototype.getTrueTextColor = function () {
                return this.trueTextColor;
            };
            TwoStateButton.prototype.setTrueTextColor = function (trueTextColor) {
                return this._set('trueTextColor', trueTextColor, true);
            };
            TwoStateButton.prototype.getTrueTextDisabledColor = function () {
                return this.trueTextDisabledColor;
            };
            TwoStateButton.prototype.setTrueTextDisabledColor = function (trueTextDisabledColor) {
                return this._set('trueTextDisabledColor', trueTextDisabledColor, true);
            };
            TwoStateButton.prototype.getFalseTextColor = function () {
                return this.falseTextColor;
            };
            TwoStateButton.prototype.setFalseTextColor = function (falseTextColor) {
                return this._set('falseTextColor', falseTextColor, true);
            };
            TwoStateButton.prototype.getFalseTextDisabledColor = function () {
                return this.falseTextColor;
            };
            TwoStateButton.prototype.setFalseTextDisabledColor = function (falseTextDisabledColor) {
                return this._set('falseTextDisabledColor', falseTextDisabledColor, true);
            };
            TwoStateButton.prototype.getRadius = function () {
                return this.radius;
            };
            TwoStateButton.prototype.setRadius = function (radius) {
                return this._set('radius', radius, true);
            };
            TwoStateButton.prototype.load = function (options) {
                options = options || {};
                if (!options.text)
                    options.text = options.trueText || this.trueText;
                if (!options.trueText)
                    options.trueText = options.text || this.trueText;
                if (!options.textColor)
                    options.textColor = options.trueTextColor || this.trueTextColor;
                if (!options.trueTextColor)
                    options.trueTextColor = options.textColor || this.trueTextColor;
                if (!options.brush)
                    options.brush = this.brush;
                options.radius = options.radius || 3;
                this._inInitialize = true;
                _super.prototype.load.call(this, options);
                this._inInitialize = false;
                this._drawTextBase = false;
                this.__isMouseDown = false;
                this._onMouseDown = this._onMouseDown.bind(this);
                return this;
            };
            TwoStateButton.prototype.initEvents = function () {
                this.mousedown.add(this._onMouseDown);
            };
            TwoStateButton.prototype.disposeEvents = function () {
                this.mousedown.remove(this._onMouseDown);
            };
            TwoStateButton.prototype._onMouseDown = function (sender, options) {
                if (this.enabled === false)
                    return;
                var pointer = options.e.localPointer || this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                localpointer.x += this.width / 2;
                localpointer.y += this.height / 2;
                this._set('checked', !this.checked, true);
            };
            TwoStateButton.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (key === 'trueTextColor' || key === 'trueTextDisabledColor' || key === 'falseTextColor' || key === 'falseTextDisabledColor')
                    value = NxtControl.Drawing.Color.fromObject(value);
                if (key === 'switchBrush')
                    value = NxtControl.Drawing.Brush.fromObject(value);
                if (key === 'innerBorderPen')
                    value = NxtControl.Drawing.Pen.fromObject(value);
                if (key === 'checked') {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    if (this._inInitialize === false) {
                        this.checkedChanged.trigger(this, NxtControl.EmptyEventArgs);
                    }
                    this.invalidate();
                    return this;
                }
                if (key === 'trueText') {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    _super.prototype._set.call(this, 'text', value, invalidate && this.checked === true);
                }
                else if (key === 'falseText') {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    this._calculateSize();
                    invalidate && this.checked === false && this.invalidate();
                    return this;
                }
                else
                    _super.prototype._set.call(this, key, value, invalidate);
                return this;
            };
            TwoStateButton.prototype._calculateSize = function () {
                _super.prototype._calculateSize.call(this);
                this._textWidthFalse = this._getTextWidth(this.falseText);
            };
            TwoStateButton.prototype._render = function (ctx) {
                _super.prototype._render.call(this, ctx);
                var x = this.checked ? -this.width / 4 - this._textWidth / 2 : this.width / 4 - this._textWidthFalse / 2, y = -this.height / 2 + this.fontSize + this.textPadding, xx = this.checked ? this.textPadding / 2 : -this.width / 2 + this.textPadding / 2, yy = -this.height / 2 + this.textPadding / 2, ww = this.width / 2 - this.textPadding, hh = this.height - this.textPadding, rradius = this.radius > 1 ? this.radius - 1 : 0;
                ctx.save();
                ctx.beginPath();
                ctx.rect(-this.width / 2, -this.height / 2, this.width, this.height);
                ctx.closePath();
                ctx.clip();
                var isRounded = rradius > 0;
                ctx.beginPath();
                ctx.moveTo(xx + rradius, yy);
                ctx.lineTo(xx + ww - rradius, yy);
                isRounded && ctx.quadraticCurveTo(xx + ww, yy, xx + ww, yy + rradius);
                ctx.lineTo(xx + ww, yy + hh - rradius);
                isRounded && ctx.quadraticCurveTo(xx + ww, yy + hh, xx + ww - rradius, yy + hh);
                ctx.lineTo(xx + rradius, yy + hh);
                isRounded && ctx.quadraticCurveTo(xx, yy + hh, xx, yy + hh - rradius);
                ctx.lineTo(xx, yy + rradius);
                isRounded && ctx.quadraticCurveTo(xx, yy, xx + rradius, yy);
                ctx.closePath();
                this._renderFill(ctx, this.switchBrush, xx, yy, ww, hh);
                this._renderStroke(ctx, this.innerBorderPen);
                var color = this.enabled ? (this.checked ? this.trueTextColor : this.falseTextColor) : (this.checked ? this.trueTextDisabledColor : this.falseTextDisabledColor);
                this._setTextStyles(ctx, color);
                ctx.fillText(this.checked ? this.trueText : this.falseText, x, y);
                ctx.restore();
            };
            TwoStateButton.prototype.toObject = function (propertiesToInclude) {
                var object = extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    enabled: this.get('enabled'),
                    checked: this.get('checked'),
                    falseText: this.get('falseText'),
                    trueTextDisabledColor: this.get('trueTextTextDisabledColor'),
                    falseTextColor: this.get('falseTextColor'),
                    falseTextDisabledColor: this.get('falseTextDisabledColor')
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                delete object['textColor'];
                delete object['text'];
                return object;
            };
            TwoStateButton.fromObject = function (object) {
                return new TwoStateButton().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.TwoStateButton')
            ], TwoStateButton.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.TwoStateButton')
            ], TwoStateButton.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.TwoStateButtonDesigner')
            ], TwoStateButton.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], TwoStateButton.prototype, "checked", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], TwoStateButton.prototype, "enabled", void 0);
            __decorate([
                System.DefaultValue('ON'),
                System.Browsable
            ], TwoStateButton.prototype, "trueText", void 0);
            __decorate([
                System.DefaultValue('OFF'),
                System.Browsable
            ], TwoStateButton.prototype, "falseText", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ButtonTextColorTrue')),
                System.Browsable
            ], TwoStateButton.prototype, "trueTextColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ButtonTextDisabledColor')),
                System.Browsable
            ], TwoStateButton.prototype, "trueTextDisabledColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ButtonTextColorFalse')),
                System.Browsable
            ], TwoStateButton.prototype, "falseTextColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ButtonTextDisabledColor')),
                System.Browsable
            ], TwoStateButton.prototype, "falseTextDisabledColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Brush.fromName('ButtonSwitchBackgroundBrush'))
            ], TwoStateButton.prototype, "brush", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Pen({ color: 'ButtonPenColor' }))
            ], TwoStateButton.prototype, "pen", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Brush.fromName('ButtonSwitchBrush')),
                System.Browsable
            ], TwoStateButton.prototype, "switchBrush", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Pen({ color: 'ButtonInnerBorderColor' })),
                System.Browsable
            ], TwoStateButton.prototype, "innerBorderPen", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], TwoStateButton.prototype, "radius", void 0);
            return TwoStateButton;
        }(GuiFramework.TextBase));
        GuiFramework.TwoStateButton = TwoStateButton;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var Label = (function (_super) {
            __extends(Label, _super);
            function Label() {
                _super.apply(this, arguments);
            }
            Label.prototype.load = function (options) {
                options = options || {};
                if (!options.backColor && !options.brush)
                    options.brush = new NxtControl.Drawing.SolidBrush({ color: this.backColor });
                else if (options.backColor)
                    options.brush = new NxtControl.Drawing.SolidBrush({ color: options.backColor });
                else if (options.brush && options.brush.color) {
                    options.backColor = NxtControl.Drawing.Color.fromObject(options.brush.color);
                }
                _super.prototype.load.call(this, options);
                return this;
            };
            Label.prototype.toObject = function (propertiesToInclude) {
                var object = extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    decimalPlacesCount: this.get('decimalPlacesCount')
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            Label.fromObject = function (object) {
                return new Label().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Label')
            ], Label.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Label')
            ], Label.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.LabelDesigner')
            ], Label.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('LabelTextColor'))
            ], Label.prototype, "textColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('LabelTextColor'))
            ], Label.prototype, "prefixColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('ButtonTextColorTrue'))
            ], Label.prototype, "suffixColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('LabelBackColor'))
            ], Label.prototype, "backColor", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Pen({ color: 'LabelPenColor' }))
            ], Label.prototype, "pen", void 0);
            __decorate([
                System.DefaultValue(2),
                System.Browsable
            ], Label.prototype, "decimalPlacesCount", void 0);
            return Label;
        }(GuiFramework.TextBase));
        GuiFramework.Label = Label;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var TextBoxControl = (function () {
            function TextBoxControl(id, parent, lefttop) {
                this.id = id;
                this.parent = parent;
                this.allowBlur = true;
                this.showControl(lefttop);
            }
            TextBoxControl.prototype.destroy = function () {
                try {
                    TextBoxControl.textBoxControl.style.visibility = 'hidden';
                }
                catch (err) { }
            };
            TextBoxControl.prototype.onBlur = function () {
                if (this.allowBlur) {
                    this.parent._onBlur(TextBoxControl.textBoxControl.value);
                    this.destroy();
                }
            };
            TextBoxControl.prototype.onKeyDown = function (e) {
                if (e.keyCode === 13) {
                    this.allowBlur = false;
                    this.parent._onReturn(TextBoxControl.textBoxControl.value);
                    this.destroy();
                }
                else if (e.keyCode === 27) {
                    this.allowBlur = false;
                    this.destroy();
                }
            };
            TextBoxControl.createControl = function () {
                var textBoxControl = document.createElement('input');
                textBoxControl.type = 'text';
                textBoxControl.id = '__textBox__';
                textBoxControl.style.position = 'absolute';
                textBoxControl.style.zIndex = '99999';
                textBoxControl.style.display = 'block';
                textBoxControl.style.visibility = 'hidden';
                document.body.appendChild(textBoxControl);
                TextBoxControl.textBoxControl = textBoxControl;
            };
            TextBoxControl.prototype.showControl = function (lefttop) {
                var textBoxControl = TextBoxControl.textBoxControl;
                textBoxControl.disabled = this.parent.readOnly;
                textBoxControl.value = this.parent.getText();
                if (textBoxControl.style.visibility != 'visible')
                    textBoxControl.style.visibility = 'visible';
                textBoxControl.style.left = lefttop.x + 'px';
                textBoxControl.style.top = lefttop.y + 'px';
                textBoxControl.style.width = this.parent.getWidth() + 'px';
                textBoxControl.style.height = this.parent.getHeight() + 'px';
                textBoxControl.style.background = this.parent.getBackColor().toLive();
                textBoxControl.style.color = this.parent.getTextColor().toLive();
                textBoxControl.style.margin = '0';
                textBoxControl.style.padding = '0 3px 0 3px';
                textBoxControl.style.border = 'solid 1px #ccc';
                textBoxControl.onkeydown = this.onKeyDown.bind(this);
                textBoxControl.onblur = this.onBlur.bind(this);
                textBoxControl.focus();
                textBoxControl.select();
            };
            return TextBoxControl;
        }());
        var extend = NxtControl.Util.object.extend;
        var TextBox = (function (_super) {
            __extends(TextBox, _super);
            function TextBox() {
                _super.call(this);
                if (!TextBoxControl.textBoxControl)
                    TextBoxControl.createControl();
            }
            TextBox.prototype.getEnabled = function () {
                return this.enabled;
            };
            TextBox.prototype.setEnabled = function (enabled) {
                return this._set('enabled', enabled, true);
            };
            TextBox.prototype.load = function (options) {
                if (!options.backColor && !options.brush)
                    options.brush = new NxtControl.Drawing.SolidBrush({ color: this.backColor });
                else if (options.backColor)
                    options.brush = new NxtControl.Drawing.SolidBrush({ color: options.backColor });
                else if (options.brush && options.brush.color) {
                    if (typeof options.brush.color === 'string')
                        options.backColor = NxtControl.Drawing.Color.fromString(options.brush.color);
                    else
                        options.backColor = NxtControl.Drawing.Color.fromObject(options.brush.color);
                }
                _super.prototype.load.call(this, options);
                this.__isMouseDown = false;
                this._onMouseDown = this._onMouseDown.bind(this);
                return this;
            };
            TextBox.prototype.initEvents = function () {
                this.mousedown.add(this._onMouseDown);
            };
            TextBox.prototype.disposeEvents = function () {
                this.mousedown.remove(this._onMouseDown);
            };
            TextBox.prototype._onMouseDown = function (sender, options) {
                if (this.enabled === false)
                    return;
                var pointer = options.e.localPointer || this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                localpointer.x += this.width / 2;
                localpointer.y += this.height / 2;
                var posx = 0;
                var posy = 0;
                if (System.isTouchSupported && options.e instanceof TouchEvent) {
                    var touch = options.e.changedTouches.item(0);
                    if (touch.pageX || touch.pageY) {
                        posx = touch.pageX;
                        posy = touch.pageY;
                    }
                    else if (touch.clientX || touch.clientY) {
                        posx = touch.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                        posy = touch.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                    }
                }
                else if (options.e.pageX || options.e.pageY) {
                    posx = options.e.pageX;
                    posy = options.e.pageY;
                }
                else if (options.e.clientX || options.e.clientY) {
                    posx = options.e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                    posy = options.e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                }
                options.e.preventDefault();
                new TextBoxControl('_textBox_', this, { x: posx - localpointer.x, y: posy - localpointer.y });
            };
            TextBox.prototype._getTextColor = function () {
                return this.enabled ? this.textColor : this.textDisabledColor;
            };
            TextBox.prototype._onNewValueChanged = function (e) {
                if (this.keyboardTextType === GuiFramework.TypeCode.String || this.keyboardTextType === GuiFramework.TypeCode.DateAndTime ||
                    this.keyboardTextType === GuiFramework.TypeCode.Date || this.keyboardTextType === GuiFramework.TypeCode.TimeOfDay)
                    this._set('text', String(e.newValue), true);
                else if (this.keyboardTextType === GuiFramework.TypeCode.Single || this.keyboardTextType === GuiFramework.TypeCode.Double ||
                    this.keyboardNumberBase === GuiFramework.NumberBase.Decimal)
                    this._set('text', this._checkDoubleValue(parseFloat(e.newValue)), true);
                else
                    this._set('text', String(e.newValue), true);
            };
            TextBox.prototype._checkDoubleValue = function (value) {
                if (this.useRange) {
                    if (value < this.minimum) {
                        value = this.minimum;
                        alert("$res:Range.ValueTooSmall");
                    }
                    if (value > this.maximum) {
                        value = this.maximum;
                        alert("$res:Range.ValueTooBig");
                    }
                }
                return NxtControl.Util.Convert.fromDoubleToString(value, this.keyboardNumberBase);
            };
            TextBox.prototype._checkStringValue = function (text) {
                if (this.keyboardTextType === GuiFramework.TypeCode.String)
                    return text;
                var converted = NxtControl.Util.Convert.tryFromStringToValueType(this.keyboardTextType, text, this.keyboardNumberBase);
                if (!converted.result) {
                    alert("$res:GuiFramework.BadValueFormat");
                    return this.text;
                }
                if (this.keyboardTextType === GuiFramework.TypeCode.DateAndTime || this.keyboardTextType === GuiFramework.TypeCode.Date)
                    return text;
                return this._checkDoubleValue(parseFloat(text));
            };
            TextBox.prototype._onReturn = function (value) {
                this._set('text', this._checkStringValue(value), true);
            };
            TextBox.prototype._onBlur = function (value) {
                if (this.sendValueOnLostFocus)
                    this._set('text', this._checkStringValue(value), true);
            };
            TextBox.prototype.toObject = function (propertiesToInclude) {
                var object = extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    enabled: this.get('enabled'),
                    readOnly: this.get('readOnly'),
                    textDisabledColor: this.get('textDisabledColor'),
                    keyboardTextType: this.get('keyboardTextType'),
                    keyboardNumberBase: this.get('keyboardNumberBase'),
                    sendValueOnLostFocus: this.get('sendValueOnLostFocus'),
                    decimalPlacesCount: this.get('decimalPlacesCount'),
                    minimum: this.get('minimum'),
                    maximum: this.get('maximum'),
                    useRange: this.get('useRange')
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                delete object['brush'];
                return object;
            };
            TextBox.fromObject = function (object) {
                return new TextBox().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.TextBox')
            ], TextBox.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.TextBox')
            ], TextBox.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.TextBoxDesigner')
            ], TextBox.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], TextBox.prototype, "enabled", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], TextBox.prototype, "readOnly", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Pen({ color: 'TextBoxPenColor' }))
            ], TextBox.prototype, "pen", void 0);
            __decorate([
                System.DefaultValue(GuiFramework.TypeCode.String),
                System.Browsable
            ], TextBox.prototype, "keyboardTextType", void 0);
            __decorate([
                System.DefaultValue(GuiFramework.NumberBase.Decimal),
                System.Browsable
            ], TextBox.prototype, "keyboardNumberBase", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], TextBox.prototype, "sendValueOnLostFocus", void 0);
            __decorate([
                System.DefaultValue(2),
                System.Browsable
            ], TextBox.prototype, "decimalPlacesCount", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], TextBox.prototype, "minimum", void 0);
            __decorate([
                System.DefaultValue(100),
                System.Browsable
            ], TextBox.prototype, "maximum", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], TextBox.prototype, "useRange", void 0);
            return TextBox;
        }(GuiFramework.TextBase));
        GuiFramework.TextBox = TextBox;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend, Orientation = NxtControl.Drawing.Orientation;
        var Tracker = (function (_super) {
            __extends(Tracker, _super);
            function Tracker() {
                _super.call(this);
                this.valueChanged = new NxtControl.event();
            }
            Tracker.prototype.getReferenceValue = function () {
                return this.referenceValue;
            };
            Tracker.prototype.setReferenceValue = function (referenceValue) {
                return this._set('referenceValue', referenceValue, true);
            };
            Tracker.prototype.getUseReferenceValue = function () {
                return this.useReferenceValue;
            };
            Tracker.prototype.setUseReferenceValue = function (useReferenceValue) {
                return this._set('useReferenceValue', useReferenceValue, true);
            };
            Tracker.prototype.getOrientation = function () {
                return this.orientation;
            };
            Tracker.prototype.setOrientation = function (orientation) {
                return this._set('orientation', orientation, true);
            };
            Tracker.prototype.getMinimum = function () {
                return this.minimum;
            };
            Tracker.prototype.setMinimum = function (minimum) {
                return this._set('minimum', minimum, true);
            };
            Tracker.prototype.getMaximum = function () {
                return this.maximum;
            };
            Tracker.prototype.setMaximum = function (maximum) {
                return this._set('maximum', maximum, true);
            };
            Tracker.prototype.getMaximumPosition = function () {
                return this.maximumPosition;
            };
            Tracker.prototype.setMaximumPosition = function (maximumPosition) {
                return this._set('maximumPosition', maximumPosition, true);
            };
            Tracker.prototype.getMinMaxDecimalPlacesCount = function () {
                return this.minMaxDecimalPlacesCount;
            };
            Tracker.prototype.setMinMaxDecimalPlacesCount = function (minMaxDecimalPlacesCount) {
                return this._set('minMaxDecimalPlacesCount', minMaxDecimalPlacesCount, true);
            };
            Tracker.prototype.getUseSnap = function () {
                return this.useSnap;
            };
            Tracker.prototype.setUseSnap = function (useSnap) {
                return this._set('useSnap', useSnap, false);
            };
            Tracker.prototype.getSnap = function () {
                return this.snap;
            };
            Tracker.prototype.setSnap = function (snap) {
                return this._set('snap', snap, false);
            };
            Tracker.prototype.getTrackValueBrush = function () {
                return this.trackValueBrush;
            };
            Tracker.prototype.setTrackValueBrush = function (trackValueBrush) {
                return this._set('trackValueBrush', trackValueBrush, true);
            };
            Tracker.prototype.getRadius = function () {
                return this.radius;
            };
            Tracker.prototype.setRadius = function (radius) {
                return this._set('radius', radius, true);
            };
            Tracker.prototype.getReadOnly = function () {
                return this.readOnly;
            };
            Tracker.prototype.setReadOnly = function (readOnly) {
                return this._set('readOnly', readOnly, true);
            };
            Tracker.prototype.getTickLength = function () {
                return this.tickLength;
            };
            Tracker.prototype.setTickLength = function (tickLength) {
                return this._set('tickLength', tickLength, true);
            };
            Tracker.prototype.getTickOffset = function () {
                return this.tickOffset;
            };
            Tracker.prototype.setTickOffset = function (tickOffset) {
                return this._set('tickOffset', tickOffset, true);
            };
            Tracker.prototype.getTickThickness = function () {
                return this.tickThickness;
            };
            Tracker.prototype.setTickThickness = function (tickThickness) {
                return this._set('tickThickness', tickThickness, true);
            };
            Tracker.prototype.getTickPercent = function () {
                return this.tickPercent;
            };
            Tracker.prototype.setTickPercent = function (tickPercent) {
                return this._set('tickPercent', tickPercent, true);
            };
            Tracker.prototype.getTickColor = function () {
                return this.tickColor;
            };
            Tracker.prototype.setTickColor = function (tickColor) {
                return this._set('tickColor', tickColor, true);
            };
            Tracker.prototype.getFontFamily = function () {
                return this.fontFamily;
            };
            Tracker.prototype.setFontFamily = function (fontFamily) {
                return this._set('fontFamily', fontFamily, true);
            };
            Tracker.prototype.getFontWeight = function () {
                return this.fontWeight;
            };
            Tracker.prototype.setFontWeight = function (fontWeight) {
                return this._set('fontWeight', fontWeight, true);
            };
            Tracker.prototype.getFontSize = function () {
                return this.fontSize;
            };
            Tracker.prototype.setFontSize = function (fontSize) {
                return this._set('fontSize', fontSize, true);
            };
            Tracker.prototype.getFontStyle = function () {
                return this.fontStyle;
            };
            Tracker.prototype.setFontStyle = function (fontStyle) {
                return this._set('fontStyle', fontStyle, true);
            };
            Tracker.prototype.getTextColor = function () {
                return this.textColor;
            };
            Tracker.prototype.setTextColor = function (clr) {
                return this._set('textColor', clr, true);
            };
            Tracker.prototype.getValueColor = function () {
                return this.valueColor;
            };
            Tracker.prototype.setValueColor = function (clr) {
                return this._set('valueColor', clr, true);
            };
            Tracker.prototype.getValueDecimalPlacesCount = function () {
                return this.valueDecimalPlacesCount;
            };
            Tracker.prototype.setValueDecimalPlacesCount = function (valueDecimalPlacesCount) {
                return this._set('valueDecimalPlacesCount', valueDecimalPlacesCount, true);
            };
            Tracker.prototype.getShowValue = function () {
                return this.showValue;
            };
            Tracker.prototype.setShowValue = function (showValue) {
                return this._set('showValue', showValue, true);
            };
            Tracker.prototype.getValueSuffix = function () {
                return this.valueSuffix;
            };
            Tracker.prototype.setValueSuffix = function (valueSuffix) {
                return this._set('valueSuffix', valueSuffix, true);
            };
            Tracker.prototype.getShowMinMax = function () {
                return this.showMinMax;
            };
            Tracker.prototype.setShowMinMax = function (showMinMax) {
                return this._set('showMinMax', showMinMax, true);
            };
            Tracker.prototype.getShowTicks = function () {
                return this.showTicks;
            };
            Tracker.prototype.setShowTicks = function (showTicks) {
                return this._set('showTicks', showTicks, true);
            };
            Tracker.prototype.getTrackLineThickness = function () {
                return this.trackLineThickness;
            };
            Tracker.prototype.setTrackLineThickness = function (trackLineThickness) {
                return this._set('trackLineThickness', trackLineThickness, true);
            };
            Tracker.prototype.getTrackLineBrush = function () {
                return this.trackLineBrush;
            };
            Tracker.prototype.setTrackLineBrush = function (trackLineBrush) {
                return this._set('trackLineBrush', trackLineBrush, true);
            };
            Tracker.prototype.getTrackHandleLength = function () {
                return this.trackHandleLength;
            };
            Tracker.prototype.setTrackHandleLength = function (trackHandleLength) {
                return this._set('trackHandleLength', trackHandleLength, true);
            };
            Tracker.prototype.getTrackHandleBrush = function () {
                return this.trackHandleBrush;
            };
            Tracker.prototype.setTrackHandleBrush = function (trackHandleBrush) {
                return this._set('trackHandleBrush', trackHandleBrush, true);
            };
            Tracker.prototype.getTrackHandlePen = function () {
                return this.trackHandlePen;
            };
            Tracker.prototype.setTrackHandlePen = function (trackHandlePen) {
                return this._set('trackHandlePen', trackHandlePen, true);
            };
            Tracker.prototype.getMouseMoveValueThreshold = function () {
                return this.mouseMoveValueThreshold;
            };
            Tracker.prototype.setMouseMoveValueThreshold = function (mouseMoveValueThreshold) {
                return this._set('mouseMoveValueThreshold', mouseMoveValueThreshold, true);
            };
            Tracker.prototype.load = function (options) {
                options = options || {};
                this._inInitialize = true;
                if (!options.hasOwnProperty('orientation'))
                    options.orientation = Orientation.Horizontal;
                if (!options.hasOwnProperty('minimum'))
                    options.minimum = 0;
                if (!options.hasOwnProperty('maximum'))
                    options.maximum = 100;
                if (!options.hasOwnProperty('value'))
                    options.value = 0;
                _super.prototype.load.call(this, options);
                this._inInitialize = false;
                this._valuesSetByMouseDown = false;
                this.__isMouseDown = false;
                this._calculateSize();
                this._calculateValues();
                this._onMouseDown = this._onMouseDown.bind(this);
                this._onMouseMove = this._onMouseMove.bind(this);
                this._onMouseUp = this._onMouseUp.bind(this);
                return this;
            };
            Tracker.prototype.dispose = function () {
                this.disposeEvents();
                return this;
            };
            Tracker.prototype.initEvents = function () {
                this.mousedown.add(this._onMouseDown);
                this.mousemove.add(this._onMouseMove);
                this.mouseup.add(this._onMouseUp);
            };
            Tracker.prototype.disposeEvents = function () {
                this.mousedown.remove(this._onMouseDown);
                this.mousemove.remove(this._onMouseMove);
                this.mouseup.remove(this._onMouseUp);
            };
            Tracker.prototype._onMouseDown = function (sender, options) {
                if (this.readOnly === true)
                    return;
                var pointer = options.e.localPointer || this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var x1 = this._trackHandleLT.x, x2 = this._trackHandleRB.x, y1 = this._trackHandleLT.y, y2 = this._trackHandleRB.y;
                if (System.isTouchSupported && options.e instanceof TouchEvent && options.e.changedTouches.length != 0) {
                    var touch = options.e.changedTouches.item(0);
                    if (touch.radiusX && touch.radiusX != 0) {
                        x1 -= touch.radiusX;
                        x2 += touch.radiusX;
                        y1 -= touch.radiusY;
                        y2 += touch.radiusY;
                    }
                }
                if (localpointer.x >= x1 && localpointer.x <= x2 &&
                    localpointer.y >= y1 && localpointer.y <= y2) {
                    this.__centerOffset = { x: localpointer.x - this._trackHandleLT.x, y: localpointer.y - this._trackHandleLT.y };
                    this.__isMouseDown = true;
                    this.captureMouse(this);
                }
            };
            Tracker.prototype._onMouseMove = function (sender, options) {
                if (this.readOnly === true)
                    return;
                if (!this.__isMouseDown)
                    return;
                var pointer = options.e.localPointer || this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var trackLinePosition;
                localpointer.x -= this.__centerOffset.x;
                localpointer.y -= this.__centerOffset.y;
                if (this.orientation === Orientation.Horizontal) {
                    trackLinePosition = localpointer.x + this.trackHandleLength / 2;
                }
                else {
                    trackLinePosition = localpointer.y + this.trackHandleLength / 2;
                }
                var fraction;
                if (this.mouseMoveValueThreshold !== 0 || this.useSnap) {
                    if (this.orientation === Orientation.Horizontal) {
                        fraction = (trackLinePosition - this._trackLineLT.x) / (this._trackLineRB.x - this._trackLineLT.x);
                    }
                    else {
                        fraction = (this._trackLineRB.y - trackLinePosition) / (this._trackLineRB.y - this._trackLineLT.y);
                    }
                    if (fraction < 0)
                        fraction = 0;
                    else if (fraction > 1)
                        fraction = 1;
                    if (this.maximumPosition !== GuiFramework.MaximumPosition.rightTop)
                        fraction = 1 - fraction;
                    this._setMouseValue(this.snapValue(this.minimum + (this.maximum - this.minimum) * fraction));
                }
            };
            Tracker.prototype.snapValue = function (val) {
                if (this.useSnap === false || this.snap === 0 || val === this.minimum || val === this.maximum)
                    return val;
                var snap = this.snap;
                var multi = 1;
                if (snap < 1) {
                    while (true) {
                        snap *= 10;
                        multi *= 10;
                        if (snap % 1 === 0)
                            break;
                    }
                }
                var divx = val > 0 ? Math.floor(val / snap) : Math.ceil(val / snap);
                var modx = val % snap;
                if (snap == 1 || modx < snap / 2)
                    val = divx * snap;
                else
                    val = (divx + 1) * snap;
                if (multi != 1)
                    val /= multi;
                if (val < this.minimum)
                    val = this.minimum;
                else if (val > this.maximum)
                    val = this.maximum;
                return val;
            };
            Tracker.prototype._onMouseUp = function (sender, options) {
                if (this.readOnly === true)
                    return;
                if (this.__isMouseDown === false)
                    return;
                this.captureMouse(null);
                this.__isMouseDown = false;
                var pointer = options.e.localPointer || this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var trackLinePosition;
                localpointer.x -= this.__centerOffset.x;
                localpointer.y -= this.__centerOffset.y;
                if (this.orientation === Orientation.Horizontal) {
                    trackLinePosition = localpointer.x + this.trackHandleLength / 2;
                }
                else {
                    trackLinePosition = localpointer.y + this.trackHandleLength / 2;
                }
                var fraction;
                if (this.mouseMoveValueThreshold !== 0) {
                    if (this.orientation === Orientation.Horizontal) {
                        fraction = (trackLinePosition - this._trackLineLT.x) / (this._trackLineRB.x - this._trackLineLT.x);
                    }
                    else {
                        fraction = (this._trackLineRB.y - trackLinePosition) / (this._trackLineRB.y - this._trackLineLT.y);
                    }
                    if (fraction < 0)
                        fraction = 0;
                    else if (fraction > 1)
                        fraction = 1;
                    if (this.maximumPosition !== GuiFramework.MaximumPosition.rightTop)
                        fraction = 1 - fraction;
                    this._setMouseValue(this.snapValue(this.minimum + (this.maximum - this.minimum) * fraction));
                }
            };
            Tracker.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (key === 'pen')
                    return this;
                if (key === 'trackHandleBrush' || key === 'trackValueBrush' || key === 'trackLineBrush') {
                    try {
                        value = NxtControl.Drawing.Brush.fromObject(value);
                    }
                    catch (err) {
                        console.error('Exception in tracker in brush definition in ' + key + ' ' + err);
                    }
                }
                if (key === 'trackHandlePen') {
                    try {
                        value = NxtControl.Drawing.Pen.fromObject(value);
                    }
                    catch (err) {
                        console.error('Exception in tracker in pen definition in ' + key + ' ' + err);
                    }
                }
                if (key === 'tickColor' || key == 'valueColor' || key == 'textColor') {
                    value = NxtControl.Drawing.Color.fromObject(value);
                }
                if (this.designer !== null && this._inInitialize === false) {
                    if (key === 'scaleX') {
                        if (this.orientation === Orientation.Horizontal)
                            this._set('width', value * this.width > 100 ? value * this.width : 100, invalidate);
                        else
                            this._set('width', value * this.width, invalidate);
                        return this;
                    }
                    else if (key === 'scaleY') {
                        if (this.orientation !== Orientation.Horizontal)
                            this._set('height', value * this.height > 100 ? value * this.height : 100, invalidate);
                        else
                            this._set('height', value * this.height, invalidate);
                        return this;
                    }
                }
                if (key === 'minimum' || key === 'maximum' || key === 'showMinMax' || key === 'showValue') {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    if (this._inInitialize === false) {
                        this._calculateValues();
                        this._calculateSize();
                        this.invalidate();
                    }
                }
                else if (key === 'value' || key == 'valueSuffix') {
                    if (this[key] === value)
                        return this;
                    this[key] = value;
                    if (this._inInitialize === false) {
                        if (this.orientation === Orientation.Vertical)
                            this._calculateSize();
                        this._calculateValues();
                        this.invalidate();
                    }
                }
                else if (key === 'orientation') {
                    var o = this.orientation;
                    this.orientation = value;
                    if (this._inInitialize === false) {
                        if (o !== value) {
                            if (this.designer !== null)
                                this.designer.propertyChanged(key);
                            if (this.orientation === Orientation.Horizontal)
                                this.width = this.height;
                            else
                                this.height = this.width;
                            this._calculateSize();
                        }
                    }
                }
                else if (key === 'maximumPosition') {
                    this[key] = value;
                    if (this._inInitialize === false) {
                        this._calculateValues();
                        this.invalidate();
                    }
                }
                else if (key === 'fontSize' || key === 'trackHandleLength' || key === 'trackLineThickness' || key === 'tickLength') {
                    this[key] = value;
                    if (this._inInitialize === false) {
                        this._calculateSize();
                        this.invalidate();
                    }
                }
                else if ((key === 'width' || key === 'height') && this._inInitialize === false) {
                    if ((this.orientation === Orientation.Horizontal && key === 'width') ||
                        (this.orientation === Orientation.Vertical && key === 'height'))
                        _super.prototype._set.call(this, key, value, invalidate);
                }
                else {
                    _super.prototype._set.call(this, key, value, invalidate);
                }
                return this;
            };
            Tracker.prototype._setMouseValue = function (value) {
                if (this.value === value && this.__isMouseDown === true)
                    return;
                this.value = value;
                if (this.orientation === Orientation.Vertical)
                    this._calculateSize();
                this._calculateValues();
                if (this.readOnly === false && this.__isMouseDown === false)
                    this.valueChanged.trigger(this, NxtControl.EmptyEventArgs);
                this.invalidate();
            };
            Tracker.prototype._calculateValues = function () {
                this.refPercent = 0;
                if (this.useReferenceValue) {
                    if (this.referenceValue > this.maximum)
                        this.refPercent = 100;
                    else if (this.referenceValue > this.minimum) {
                        if (this.minimum == this.maximum)
                            this.refPercent = 0;
                        else
                            this.refPercent = (((this.referenceValue - this.minimum) / (this.maximum - this.minimum)) * 100);
                    }
                    else
                        this.refPercent = 0;
                }
                if (this.value < this.minimum)
                    this._fillPercent = 0;
                else if (this.value > this.maximum)
                    this._fillPercent = 100;
                else
                    this._fillPercent = ((this.value - this.minimum) / (this.maximum - this.minimum)) * 100;
                if (this.readOnly === false)
                    this._calculateTrackHandlePosition();
            };
            Tracker.prototype._calculateSize = function () {
                this._minimumTextWidth = this.showMinMax ? this._getTextWidth(this.minimum.toFixed(2)) : 0;
                this._maximumTextWidth = this.showMinMax ? this._getTextWidth(this.maximum.toFixed(2)) : 0;
                var v = this.value.toFixed(this.valueDecimalPlacesCount);
                if (this.valueSuffix && this.valueSuffix.length > 0)
                    v += this.valueSuffix;
                this._valueTextWidth = this.showValue ? this._getTextWidth(v) : 0;
                if (this.orientation === Orientation.Horizontal) {
                    this._minimumTextOffset = Math.max(this._minimumTextWidth / 2, this.trackHandleLength / 2);
                    this._maximumTextOffset = Math.max(this._maximumTextWidth / 2, this.trackHandleLength / 2);
                }
                else {
                    this._minimumTextOffset = Math.max(this.fontSize / 2, this.trackHandleLength / 2);
                    this._maximumTextOffset = Math.max(this.fontSize / 2, this.trackHandleLength / 2);
                }
                if (this.orientation === Orientation.Horizontal) {
                    this.height = (this.showMinMax ? this.fontSize : 0) + this.trackLineThickness + this.tickLength + this.tickOffset * 5 + (this.showValue ? this.fontSize : 0);
                }
                else {
                    this.width = Math.max(this._valueTextWidth + 2 * this.tickOffset, this.trackLineThickness + this.tickLength + this.tickOffset * 3 + Math.max(this._minimumTextWidth, this._maximumTextWidth));
                }
            };
            Tracker.prototype._getTextWidth = function (text) {
                var canvasEl = NxtControl.Util.createCanvasElement();
                var ctx = canvasEl.getContext('2d');
                this._setTextStyles(ctx, NxtControl.Drawing.Color.Black);
                return ctx.measureText(text).width;
            };
            Tracker.prototype._setTextStyles = function (ctx, color) {
                ctx.fillStyle = ctx.strokeStyle = color.toLive();
                ctx.textBaseline = 'bottom';
                ctx.font = this._getFontDeclaration();
            };
            Tracker.prototype._getFontDeclaration = function () {
                return [
                    (System.isLikelyNode ? this.fontWeight : this.fontStyle),
                    (System.isLikelyNode ? this.fontStyle : this.fontWeight),
                    this.fontSize + 'px',
                    (System.isLikelyNode ? ('"' + this.fontFamily + '"') : this.fontFamily)
                ].join(' ');
            };
            Tracker.prototype._calculateTrackHandlePosition = function () {
                var x = -this.width / 2, y = -this.height / 2, w = this.orientation === Orientation.Horizontal ? this.width : this.trackLineThickness, h = this.orientation === Orientation.Horizontal ? this.trackLineThickness : this.height;
                if (this.orientation === Orientation.Horizontal) {
                    x += (this.maximumPosition === GuiFramework.MaximumPosition.rightTop ? this._minimumTextOffset : this._maximumTextOffset);
                    y += (this.showValue ? this.fontSize : 0) + 2 * this.tickOffset;
                    w -= this._minimumTextOffset + this._maximumTextOffset;
                    this._trackLineLT = { x: x, y: y };
                    this._trackLineRB = { x: x + w, y: y + h };
                }
                else {
                    x += this.tickOffset;
                    y += (this.showValue ? this.fontSize : 0) + this.tickOffset + (this.maximumPosition === GuiFramework.MaximumPosition.rightTop ? this._minimumTextOffset : this._maximumTextOffset);
                    h -= (this.showValue ? this.fontSize : 0) + this.tickOffset + this._minimumTextOffset + this._maximumTextOffset;
                    this._trackLineLT = { x: x, y: y };
                    this._trackLineRB = { x: x + w, y: y + h };
                }
                var xx = x, yy = y, ww = w, hh = h;
                if (this._fillPercent !== 100 && this._fillPercent !== 0) {
                    if (this.orientation === Orientation.Horizontal)
                        ww = ww / 100 * this._fillPercent;
                    else {
                        var hhh = hh;
                        hh = hh / 100 * this._fillPercent;
                        yy = yy + hhh - hh;
                    }
                }
                else if (this._fillPercent === 0) {
                    if (this.orientation === Orientation.Horizontal)
                        ww = 0;
                    else
                        yy = y + h;
                }
                else if (this._fillPercent === 100) {
                    if (this.orientation === Orientation.Horizontal)
                        ww = w;
                    else
                        yy = y;
                }
                if (this.orientation === Orientation.Horizontal) {
                    this._trackHandleLT = { x: (this.maximumPosition === GuiFramework.MaximumPosition.rightTop ? xx + ww : x + w - ww) - this.trackHandleLength / 2, y: yy - this.tickOffset };
                    this._trackHandleRB = { x: this._trackHandleLT.x + this.trackHandleLength, y: this._trackHandleLT.y + 2 * this.tickOffset + this.trackLineThickness };
                }
                else {
                    this._trackHandleLT = { x: xx - this.tickOffset, y: (this.maximumPosition === GuiFramework.MaximumPosition.rightTop ? yy : y + hh) - this.trackHandleLength / 2 };
                    this._trackHandleRB = { x: this._trackHandleLT.x + 2 * this.tickOffset + this.trackLineThickness, y: this._trackHandleLT.y + this.trackHandleLength };
                }
            };
            Tracker.prototype._render = function (ctx) {
                var r = this.radius || 0, x = -this.width / 2, y = -this.height / 2, w = this.orientation === Orientation.Horizontal ? this.width : this.trackLineThickness, h = this.orientation === Orientation.Horizontal ? this.trackLineThickness : this.height, isInPathGroup = this.group && this.group.getType() === 'path-group';
                if (this.orientation === Orientation.Horizontal) {
                    x += (this.maximumPosition === GuiFramework.MaximumPosition.rightTop ? this._minimumTextOffset : this._maximumTextOffset);
                    y += (this.showValue ? this.fontSize : 0) + 2 * this.tickOffset;
                    w -= this._minimumTextOffset + this._maximumTextOffset;
                }
                else {
                    x += this.tickOffset;
                    y += (this.showValue ? this.fontSize : 0) + this.tickOffset + this._minimumTextOffset;
                    h -= (this.showValue ? this.fontSize : 0) + this.tickOffset + this._minimumTextOffset + this._maximumTextOffset;
                }
                this._renderTrackLine(ctx, x, y, w, h, r, isInPathGroup, this.trackLineBrush, false);
                var xx = x, yy = y, ww = w, hh = h;
                var rvx = x, rvy = y, rvw = w, rvh = h;
                if (this._fillPercent !== 100 && this._fillPercent !== 0) {
                    if (this.orientation === Orientation.Horizontal) {
                        ww = ww / 100 * this._fillPercent;
                        rvw = rvw / 100 * this.refPercent;
                    }
                    else {
                        var hhh = hh;
                        hh = hh / 100 * this._fillPercent;
                        yy = yy + hhh - hh;
                        var rvhh = rvh;
                        rvh = rvh / 100 * this.refPercent;
                        rvy = rvy + rvhh - rvh;
                    }
                    if (this.orientation === Orientation.Horizontal) {
                        if (this.maximumPosition === GuiFramework.MaximumPosition.rightTop) {
                            if (this.refPercent < this._fillPercent) {
                                this._renderTrackLine(ctx, x + rvw, yy, ww - rvw, hh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                            else {
                                this._renderTrackLine(ctx, x + ww, yy, rvw - ww, hh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                        else {
                            if (this.refPercent < this._fillPercent) {
                                this._renderTrackLine(ctx, x + w - ww, yy, ww - rvw, hh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                            else {
                                this._renderTrackLine(ctx, x + w - rvw, yy, rvw - ww, hh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                    }
                    else {
                        if (this.maximumPosition === GuiFramework.MaximumPosition.rightTop) {
                            if (this.refPercent < this._fillPercent) {
                                this._renderTrackLine(ctx, x, yy, ww, hh - rvh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                            else {
                                this._renderTrackLine(ctx, x, rvy, ww, rvh - hh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                        else {
                            if (this.refPercent < this._fillPercent) {
                                this._renderTrackLine(ctx, x, y + rvh, ww, hh - rvh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                            else {
                                this._renderTrackLine(ctx, x, y + hh, ww, rvh - hh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                    }
                }
                else if (this._fillPercent === 0) {
                    if (this.orientation === Orientation.Horizontal) {
                        if (this.maximumPosition === GuiFramework.MaximumPosition.rightTop) {
                            ww = 0;
                            if (this.refPercent > 0) {
                                rvw = rvw / 100 * this.refPercent;
                                this._renderTrackLine(ctx, x, y, rvw, h, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                        else {
                            ww = 0;
                            xx = w;
                            if (this.refPercent > 0) {
                                rvw = rvw / 100 * this.refPercent;
                                this._renderTrackLine(ctx, x + w - rvw, y, rvw, h, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                    }
                    else {
                        if (this.maximumPosition === GuiFramework.MaximumPosition.rightTop) {
                            yy = y + h;
                            if (this.refPercent > 0) {
                                rvh = rvh / 100 * this.refPercent;
                                this._renderTrackLine(ctx, x, y + h - rvh, w, rvh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                        else {
                            yy = y + h;
                            if (this.refPercent > 0) {
                                rvh = rvh / 100 * this.refPercent;
                                this._renderTrackLine(ctx, x, y, w, rvh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                    }
                }
                else if (this._fillPercent === 100) {
                    if (this.orientation === Orientation.Horizontal) {
                        ww = w;
                        rvw = rvw / 100 * this.refPercent;
                        if (this.maximumPosition === GuiFramework.MaximumPosition.rightTop) {
                            if (this.refPercent > 0) {
                                this._renderTrackLine(ctx, x + rvw, y, w - rvw, h, r, isInPathGroup, this.trackValueBrush, false);
                            }
                            else {
                                this._renderTrackLine(ctx, x, y, w, h, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                        else {
                            if (this.refPercent > 0) {
                                this._renderTrackLine(ctx, x, y, w - rvw, h, r, isInPathGroup, this.trackValueBrush, false);
                            }
                            else {
                                this._renderTrackLine(ctx, x, y, w, h, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                    }
                    else {
                        yy = y;
                        rvh = rvh / 100 * this.refPercent;
                        if (this.maximumPosition === GuiFramework.MaximumPosition.rightTop) {
                            if (this.refPercent > 0) {
                                this._renderTrackLine(ctx, x, y, w, h - rvh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                            else {
                                this._renderTrackLine(ctx, x, y, w, h, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                        else {
                            if (this.refPercent > 0) {
                                this._renderTrackLine(ctx, x, y + rvh, w, h - rvh, r, isInPathGroup, this.trackValueBrush, false);
                            }
                            else {
                                this._renderTrackLine(ctx, x, y, w, h, r, isInPathGroup, this.trackValueBrush, false);
                            }
                        }
                    }
                }
                this._renderTrackLine(ctx, x, y, w, h, r, isInPathGroup, null, true);
                if (this.readOnly === false) {
                    if (this.orientation === Orientation.Horizontal) {
                        this._renderTrackHandle(ctx, this.maximumPosition === GuiFramework.MaximumPosition.rightTop ? xx + ww : x + w - ww, yy, isInPathGroup);
                    }
                    else {
                        this._renderTrackHandle(ctx, xx, this.maximumPosition === GuiFramework.MaximumPosition.rightTop ? yy : y + hh, isInPathGroup);
                    }
                }
                if (this.showTicks)
                    this._renderTicks(ctx, x, y, w, h, isInPathGroup);
                if (this.orientation === Orientation.Horizontal) {
                    if (this.showMinMax) {
                        this._setTextStyles(ctx, this.textColor);
                        if (this.maximumPosition === GuiFramework.MaximumPosition.rightTop) {
                            ctx.fillText(this.minimum.toFixed(this.minMaxDecimalPlacesCount), x - this._minimumTextWidth / 2, y + this.trackLineThickness + 2 * this.tickOffset + this.tickLength + this.fontSize);
                            ctx.fillText(this.maximum.toFixed(this.minMaxDecimalPlacesCount), x + w - this._maximumTextWidth / 2, y + this.trackLineThickness + 2 * this.tickOffset + this.tickLength + this.fontSize);
                        }
                        else {
                            ctx.fillText(this.maximum.toFixed(this.minMaxDecimalPlacesCount), x - this._maximumTextWidth / 2, y + this.trackLineThickness + 2 * this.tickOffset + this.tickLength + this.fontSize);
                            ctx.fillText(this.minimum.toFixed(this.minMaxDecimalPlacesCount), x + w - this._minimumTextWidth / 2, y + this.trackLineThickness + 2 * this.tickOffset + this.tickLength + this.fontSize);
                        }
                    }
                    if (this.showValue) {
                        this._setTextStyles(ctx, this.valueColor);
                        var v = this.value.toFixed(this.valueDecimalPlacesCount);
                        if (this.valueSuffix && this.valueSuffix.length > 0)
                            v += this.valueSuffix;
                        ctx.fillText(v, x + (w - this._valueTextWidth) / 2, y - 2 * this.tickOffset);
                    }
                }
                else {
                    if (this.showMinMax) {
                        this._setTextStyles(ctx, this.textColor);
                        if (this.maximumPosition === GuiFramework.MaximumPosition.rightTop) {
                            ctx.fillText(this.maximum.toFixed(this.minMaxDecimalPlacesCount), x + this.trackLineThickness + 2 * this.tickOffset + this.tickLength, y + this.fontSize / 2);
                            ctx.fillText(this.minimum.toFixed(this.minMaxDecimalPlacesCount), x + this.trackLineThickness + 2 * this.tickOffset + this.tickLength, y + h + this.fontSize / 2);
                        }
                        else {
                            ctx.fillText(this.minimum.toFixed(this.minMaxDecimalPlacesCount), x + this.trackLineThickness + 2 * this.tickOffset + this.tickLength, y + this.fontSize / 2);
                            ctx.fillText(this.maximum.toFixed(this.minMaxDecimalPlacesCount), x + this.trackLineThickness + 2 * this.tickOffset + this.tickLength, y + h + this.fontSize / 2);
                        }
                    }
                    if (this.showValue) {
                        this._setTextStyles(ctx, this.valueColor);
                        var v = this.value.toFixed(this.valueDecimalPlacesCount);
                        if (this.valueSuffix && this.valueSuffix.length > 0)
                            v += this.valueSuffix;
                        ctx.fillText(v, x - (this.showMinMax ? 0 : this.tickOffset) + (this.width - this._valueTextWidth) / 2, y - this.tickOffset - this._minimumTextOffset);
                    }
                }
            };
            Tracker.prototype._renderTrackLine = function (ctx, x, y, w, h, r, isInPathGroup, brush, stroke) {
                ctx.beginPath();
                if (this.transformMatrix && isInPathGroup) {
                    ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                }
                if (!this.transformMatrix && isInPathGroup) {
                    ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                }
                var isRounded = r !== 0;
                ctx.moveTo(x + r, y);
                ctx.lineTo(x + w - r, y);
                isRounded && ctx.quadraticCurveTo(x + w, y, x + w, y + r);
                ctx.lineTo(x + w, y + h - r);
                isRounded && ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
                ctx.lineTo(x + r, y + h);
                isRounded && ctx.quadraticCurveTo(x, y + h, x, y + h - r);
                ctx.lineTo(x, y + r);
                isRounded && ctx.quadraticCurveTo(x, y, x + r, y);
                ctx.closePath();
                if (stroke)
                    this._renderStroke(ctx);
                else
                    this._renderFill(ctx, brush, x, y, w, h);
            };
            Tracker.prototype._renderTrackHandle = function (ctx, x, y, isInPathGroup) {
                ctx.beginPath();
                if (this.transformMatrix && isInPathGroup) {
                    ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                }
                if (!this.transformMatrix && isInPathGroup) {
                    ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                }
                var xx, yy, ww, hh, r = 2;
                if (this.orientation === Orientation.Horizontal) {
                    xx = x - this.trackHandleLength / 2;
                    yy = y - this.tickOffset;
                    ww = this.trackHandleLength;
                    hh = 2 * this.tickOffset + this.trackLineThickness;
                    ctx.moveTo(xx + r, yy);
                    ctx.lineTo(xx + ww - r, yy);
                    ctx.quadraticCurveTo(xx + ww, yy, xx + ww, yy + r);
                    ctx.lineTo(xx + ww, yy + hh - r);
                    ctx.quadraticCurveTo(xx + ww, yy + hh, xx + ww - r, yy + hh);
                    ctx.lineTo(xx + r, yy + hh);
                    ctx.quadraticCurveTo(xx, yy + hh, xx, yy + hh - r);
                    ctx.lineTo(xx, yy + r);
                    ctx.quadraticCurveTo(xx, yy, xx + r, yy);
                    ctx.closePath();
                    if (this.trackHandleBrush instanceof NxtControl.Drawing.GradientBrush)
                        this.trackHandleBrush.orient = NxtControl.Drawing.FillOrientation.VerticalTop;
                }
                else {
                    xx = x - this.tickOffset;
                    yy = y - this.trackHandleLength / 2;
                    ww = 2 * this.tickOffset + this.trackLineThickness;
                    hh = this.trackHandleLength;
                    ctx.moveTo(xx + r, yy);
                    ctx.lineTo(xx + ww - r, yy);
                    ctx.quadraticCurveTo(xx + ww, yy, xx + ww, yy + r);
                    ctx.lineTo(xx + ww, yy + hh - r);
                    ctx.quadraticCurveTo(xx + ww, yy + hh, xx + ww - r, yy + hh);
                    ctx.lineTo(xx + r, yy + hh);
                    ctx.quadraticCurveTo(xx, yy + hh, xx, yy + hh - r);
                    ctx.lineTo(xx, yy + r);
                    ctx.quadraticCurveTo(xx, yy, xx + r, yy);
                    ctx.closePath();
                    if (this.trackHandleBrush instanceof NxtControl.Drawing.GradientBrush)
                        this.trackHandleBrush.orient = NxtControl.Drawing.FillOrientation.HorizontalLeft;
                }
                this._renderFill(ctx, this.trackHandleBrush, xx, yy, ww, hh);
                this._renderStroke(ctx, this.trackHandlePen);
            };
            Tracker.prototype._renderTicks = function (ctx, x, y, w, h, isInPathGroup) {
                ctx.beginPath();
                var smallSize;
                if (this.transformMatrix && isInPathGroup) {
                    ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                }
                if (!this.transformMatrix && isInPathGroup) {
                    ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                }
                if (this.orientation === Orientation.Horizontal) {
                    ctx.moveTo(x, y + this.trackLineThickness + this.tickOffset);
                    ctx.lineTo(x, y + this.trackLineThickness + this.tickOffset + this.tickLength);
                    if (w > 0 && this.tickPercent > 0 && this.tickPercent < 100) {
                        smallSize = w / (100 / this.tickPercent);
                        while (smallSize < 2 && smallSize !== 0)
                            smallSize *= 2;
                        var xx = x + smallSize;
                        if (smallSize !== 0) {
                            while (xx <= (x + w + 0.0000001)) {
                                ctx.moveTo(xx, y + this.trackLineThickness + this.tickOffset);
                                ctx.lineTo(xx, y + this.trackLineThickness + this.tickOffset + this.tickLength);
                                xx += smallSize;
                            }
                        }
                    }
                    ctx.moveTo(x + w, y + this.trackLineThickness + this.tickOffset);
                    ctx.lineTo(x + w, y + this.trackLineThickness + this.tickOffset + this.tickLength);
                }
                else {
                    ctx.moveTo(x + this.trackLineThickness + this.tickOffset, y);
                    ctx.lineTo(x + this.trackLineThickness + this.tickOffset + this.tickLength, y);
                    if (h > 0 && this.tickPercent > 0 && this.tickPercent < 100) {
                        smallSize = h / (100 / this.tickPercent);
                        while (smallSize < 2 && smallSize !== 0)
                            smallSize *= 2;
                        var yy = y + smallSize;
                        if (smallSize !== 0) {
                            while (yy <= (y + h + 0.0000001)) {
                                ctx.moveTo(x + this.trackLineThickness + this.tickOffset, yy);
                                ctx.lineTo(x + this.trackLineThickness + this.tickOffset + this.tickLength, yy);
                                yy += smallSize;
                            }
                        }
                    }
                    ctx.moveTo(x + this.trackLineThickness + this.tickOffset, y + h);
                    ctx.lineTo(x + this.trackLineThickness + this.tickOffset + this.tickLength, y + h);
                }
                ctx.closePath();
                ctx.lineWidth = this.tickThickness;
                ctx.strokeStyle = this.tickColor.toLive();
                ctx.stroke();
            };
            Tracker.prototype.toObject = function (propertiesToInclude) {
                var object = extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    orientation: this.orientation,
                    maximumPosition: this.maximumPosition,
                    readOnly: this.readOnly,
                    radius: this.radius || 0,
                    minimum: this.minimum || 0,
                    maximum: this.maximum || 100,
                    mouseMoveValueThreshold: this.mouseMoveValueThreshold,
                    value: this.value || 0,
                    referenceValue: this.referenceValue || 0,
                    useReferenceValue: this.useReferenceValue,
                    showValue: this.showValue,
                    showMinMax: this.showMinMax,
                    showTicks: this.showTicks,
                    tickLength: this.tickLength,
                    tickThickness: this.tickThickness,
                    tickPercent: this.tickPercent,
                    tickColor: this.tickColor,
                    trackLineBrush: this.trackLineBrush,
                    trackLineThickness: this.trackLineThickness,
                    trackValueBrush: this.trackValueBrush,
                    trackHandleLength: this.trackHandleLength,
                    trackHandleBrush: this.trackHandleBrush,
                    trackHandlePen: this.trackHandlePen,
                    fontFamily: this.fontFamily,
                    fontWeight: this.fontWeight,
                    fontSize: this.fontSize,
                    fontStyle: this.fontStyle,
                    valueColor: this.valueColor,
                    valueDecimalPlacesCount: this.valueDecimalPlacesCount,
                    valueSuffix: this.valueSuffix,
                    minMaxDecimalPlacesCount: this.minMaxDecimalPlacesCount,
                    textColor: this.textColor,
                    useSnap: this.useSnap,
                    snap: this.snap
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            Tracker.fromObject = function (object) {
                return new Tracker().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Tracker')
            ], Tracker.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Tracker')
            ], Tracker.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.TrackerDesigner')
            ], Tracker.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(0),
                System.Browsable
            ], Tracker.prototype, "referenceValue", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], Tracker.prototype, "useReferenceValue", void 0);
            __decorate([
                System.Browsable
            ], Tracker.prototype, "orientation", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Tracker.prototype, "pen", void 0);
            __decorate([
                System.Browsable
            ], Tracker.prototype, "minimum", void 0);
            __decorate([
                System.Browsable
            ], Tracker.prototype, "maximum", void 0);
            __decorate([
                System.DefaultValue(GuiFramework.MaximumPosition.rightTop),
                System.Browsable
            ], Tracker.prototype, "maximumPosition", void 0);
            __decorate([
                System.DefaultValue(2),
                System.Browsable
            ], Tracker.prototype, "minMaxDecimalPlacesCount", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], Tracker.prototype, "useSnap", void 0);
            __decorate([
                System.DefaultValue(2),
                System.Browsable
            ], Tracker.prototype, "snap", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.SolidBrush({ color: 'TrackerValueColor' })),
                System.Browsable
            ], Tracker.prototype, "trackValueBrush", void 0);
            __decorate([
                System.DefaultValue(3),
                System.Browsable
            ], Tracker.prototype, "radius", void 0);
            __decorate([
                System.DefaultValue(false),
                System.Browsable
            ], Tracker.prototype, "readOnly", void 0);
            __decorate([
                System.DefaultValue(5),
                System.Browsable
            ], Tracker.prototype, "tickLength", void 0);
            __decorate([
                System.DefaultValue(4),
                System.Browsable
            ], Tracker.prototype, "tickOffset", void 0);
            __decorate([
                System.DefaultValue(2),
                System.Browsable
            ], Tracker.prototype, "tickThickness", void 0);
            __decorate([
                System.DefaultValue(10),
                System.Browsable
            ], Tracker.prototype, "tickPercent", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('TrackerScaleColor')),
                System.Browsable
            ], Tracker.prototype, "tickColor", void 0);
            __decorate([
                System.DefaultValue('Arial'),
                System.Browsable
            ], Tracker.prototype, "fontFamily", void 0);
            __decorate([
                System.DefaultValue('normal'),
                System.Browsable
            ], Tracker.prototype, "fontWeight", void 0);
            __decorate([
                System.DefaultValue(14),
                System.Browsable
            ], Tracker.prototype, "fontSize", void 0);
            __decorate([
                System.DefaultValue('normal'),
                System.Browsable
            ], Tracker.prototype, "fontStyle", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('TrackerTextColor')),
                System.Browsable
            ], Tracker.prototype, "textColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('TrackerValueColor')),
                System.Browsable
            ], Tracker.prototype, "valueColor", void 0);
            __decorate([
                System.DefaultValue(2),
                System.Browsable
            ], Tracker.prototype, "valueDecimalPlacesCount", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Tracker.prototype, "showValue", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], Tracker.prototype, "valueSuffix", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Tracker.prototype, "showMinMax", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Tracker.prototype, "showTicks", void 0);
            __decorate([
                System.DefaultValue(10),
                System.Browsable
            ], Tracker.prototype, "trackLineThickness", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.SolidBrush({ color: 'TrackerLineBackColor' })),
                System.Browsable
            ], Tracker.prototype, "trackLineBrush", void 0);
            __decorate([
                System.DefaultValue(20),
                System.Browsable
            ], Tracker.prototype, "trackHandleLength", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Brush.fromName('TrackerHandleBrush')),
                System.Browsable
            ], Tracker.prototype, "trackHandleBrush", void 0);
            __decorate([
                System.DefaultValue(new NxtControl.Drawing.Pen({ color: 'TrackerLineBackColor' })),
                System.Browsable
            ], Tracker.prototype, "trackHandlePen", void 0);
            __decorate([
                System.DefaultValue(2),
                System.Browsable
            ], Tracker.prototype, "mouseMoveValueThreshold", void 0);
            return Tracker;
        }(GuiFramework.Shape));
        GuiFramework.Tracker = Tracker;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var DateTimePicker = (function (_super) {
            __extends(DateTimePicker, _super);
            function DateTimePicker() {
                _super.call(this);
                this.dateTimeChanged = new NxtControl.event();
            }
            DateTimePicker.prototype.load = function (options) {
                options = options || {};
                var dt = new Date();
                if (options.hasOwnProperty('value')) {
                    if (options.value instanceof Date)
                        dt = options.value;
                    else
                        dt = new Date(options.value);
                }
                this.cal = new Calendar().load({ date: dt });
                this.cal.dtp = this;
                if (options.hasOwnProperty('showTime')) {
                    this.showTime = options.showTime;
                    this.cal.showTime = options.showTime;
                }
                if (options.hasOwnProperty('timeMode') && ((options.timeMode === '12') || (options.timeMode === '24'))) {
                    this.timeMode = options.timeMode;
                    this.cal.timeMode = options.timeMode;
                }
                if (options.hasOwnProperty('format')) {
                    this.format = options.format;
                    this.cal.format = options.format.toUpperCase();
                }
                _super.prototype.load.call(this, options);
                return this;
            };
            DateTimePicker.prototype.show = function (left, top) {
                this.cal.show(left, top);
            };
            DateTimePicker.fromObject = function (object) {
                return new DateTimePicker().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.DateTimePicker')
            ], DateTimePicker.prototype, "type", void 0);
            return DateTimePicker;
        }(GuiFramework.Image));
        GuiFramework.DateTimePicker = DateTimePicker;
        var Calendar = (function (_super) {
            __extends(Calendar, _super);
            function Calendar() {
                _super.call(this);
                this.weekChar = 2;
                this.cellWidth = 20;
                this.dateSeparator = '-';
                this.timeMode = 24;
                this.showLongMonth = true;
                this.showMonthYear = true;
                this.dateTimeChanged = new NxtControl.event();
                this.closed = new NxtControl.event();
            }
            Calendar.prototype.load = function (options) {
                options = options || {};
                var pDate = options.date;
                this.date = pDate.getDate();
                this.month = pDate.getMonth();
                this.year = pDate.getFullYear();
                this.hours = pDate.getHours();
                if (pDate.getMinutes() < 10)
                    this.minutes = "0" + pDate.getMinutes();
                else
                    this.minutes = pDate.getMinutes();
                if (pDate.getSeconds() < 10)
                    this.seconds = "0" + pDate.getSeconds();
                else
                    this.seconds = pDate.getSeconds();
                if (options.hasOwnProperty('format'))
                    this.format = options.format;
                if (options.hasOwnProperty('dateSeparator'))
                    this.dateSeparator = options.dateSeparator;
                if (options.hasOwnProperty('showTime'))
                    this.showTime = options.showTime;
                if (pDate.getHours() < 12)
                    this.aMorPM = "AM";
                else
                    this.aMorPM = "PM";
                this._onMyClick = this._onMyClick.bind(this);
                return this;
            };
            Calendar.prototype.getMonthIndex = function (shortMonthName) {
                for (var i = 0; i < 12; i++) {
                    if (Calendar.monthName[i].substring(0, 3).toUpperCase() === shortMonthName.toUpperCase()) {
                        return i;
                    }
                }
            };
            Calendar.prototype.incYear = function () {
                this.year++;
            };
            Calendar.prototype.decYear = function () {
                this.year--;
            };
            Calendar.prototype.switchMth = function (intMth) {
                this.month = intMth;
            };
            Calendar.prototype.setHour = function (intHour) {
                var maxHour, minHour;
                if (this.timeMode === 24) {
                    maxHour = 23;
                    minHour = 0;
                }
                else if (this.timeMode === 12) {
                    maxHour = 12;
                    minHour = 1;
                }
                else
                    alert("TimeMode can only be 12 or 24");
                var hourExp = new RegExp("^\\d{1,2}$");
                if (hourExp.test(intHour) && (parseInt(intHour, 10) <= maxHour) && (parseInt(intHour, 10) >= minHour)) {
                    if ((this.timeMode === 12) && (this.aMorPM === "PM")) {
                        if (parseInt(intHour, 10) === 12)
                            this.hours = 12;
                        else
                            this.hours = parseInt(intHour, 10) + 12;
                    }
                    else if ((this.timeMode === 12) && (this.aMorPM === "AM")) {
                        if (intHour === '12')
                            intHour = '0';
                        this.hours = parseInt(intHour, 10);
                    }
                    else if (this.timeMode === 24)
                        this.hours = parseInt(intHour, 10);
                }
            };
            Calendar.prototype.setMinute = function (intMin) {
                var minExp = new RegExp("[0-5]?[0-9]");
                if (minExp.test(intMin) && (parseInt(intMin) < 60))
                    this.minutes = intMin;
            };
            Calendar.prototype.setSecond = function (intSec) {
                var secExp = new RegExp("[0-5]?[0-9]");
                if (secExp.test(intSec) && (parseInt(intSec) < 60))
                    this.seconds = intSec;
            };
            Calendar.prototype.setAmPm = function (pvalue) {
                this.aMorPM = pvalue;
                if (pvalue === "PM") {
                    this.hours = this.hours + 12;
                    if (this.hours === 24)
                        this.hours = 12;
                }
                else if (pvalue === "AM")
                    this.hours -= 12;
            };
            Calendar.prototype.getShowHour = function () {
                var finalHour;
                if (this.timeMode === 12) {
                    if (this.hours === 0) {
                        this.aMorPM = "AM";
                        finalHour = this.hours + 12;
                    }
                    else if (this.hours === 12) {
                        this.aMorPM = "PM";
                        finalHour = 12;
                    }
                    else if (this.hours > 12) {
                        this.aMorPM = "PM";
                        if ((this.hours - 12) < 10)
                            finalHour = "0" + (this.hours - 12);
                        else
                            finalHour = this.hours - 12;
                    }
                    else {
                        this.aMorPM = "AM";
                        if (this.hours < 10)
                            finalHour = "0" + this.hours;
                        else
                            finalHour = this.hours;
                    }
                }
                else if (this.timeMode === 24) {
                    if (this.hours < 10)
                        finalHour = "0" + this.hours;
                    else
                        finalHour = this.hours;
                }
                return finalHour;
            };
            Calendar.prototype.getMonthName = function (isLong) {
                var month = Calendar.monthName[this.month];
                if (isLong)
                    return month;
                else
                    return month.substr(0, 3);
            };
            Calendar.prototype.getMonDays = function () {
                var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                if (this.isLeapYear()) {
                    daysInMonth[1] = 29;
                }
                return daysInMonth[this.month];
            };
            Calendar.prototype.isLeapYear = function () {
                if ((this.year % 4) === 0) {
                    if ((this.year % 100 === 0) && (this.year % 400) !== 0) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                return false;
            };
            Calendar.prototype.formatDate = function (pDate) {
                if (this.format.toUpperCase() === "DDMMYYYY")
                    return (pDate + this.dateSeparator + (this.month + 1) + this.dateSeparator + this.year);
                else if (this.format.toUpperCase() === "DDMMMYYYY")
                    return (pDate + this.dateSeparator + this.getMonthName(false) + this.dateSeparator + this.year);
                else if (this.format.toUpperCase() === "MMDDYYYY")
                    return ((this.month + 1) + this.dateSeparator + pDate + this.dateSeparator + this.year);
                else if (this.format.toUpperCase() === "MMMDDYYYY")
                    return (this.getMonthName(false) + this.dateSeparator + pDate + this.dateSeparator + this.year);
            };
            Calendar.prototype.show = function (left, top) {
                NxtControl.GuiFramework.currentCalendar = this;
                var _this = this;
                setTimeout(function () { if (NxtControl.GuiFramework.currentCalendar !== null)
                    NxtControl.Util.addListener(document.body, 'mousedown', _this._onMyClick); }, 10);
                this.win = document.createElement('form');
                this.win.style.backgroundColor = '#cccccc';
                this.win.style.position = 'absolute';
                this.win.style.left = left.toString() + 'px';
                this.win.style.top = top.toString() + 'px';
                this.win.style.width = '195px';
                this.win.style.height = '245px';
                this.win.innerHTML = this.appendCal();
                this.win.setAttribute("tabindex", "0");
                document.body.appendChild(this.win);
            };
            Calendar.prototype._isDescendant = function (child) {
                var node = child.parentNode;
                while (node !== null) {
                    if (node === this.win) {
                        return true;
                    }
                    node = node.parentNode;
                }
                return false;
            };
            Calendar.prototype._onMyClick = function (e) {
                if (e.target === this || this._isDescendant(e.target))
                    return;
                this.hide();
            };
            Calendar.prototype.getMinutes = function () {
                if (typeof this.minutes === 'string')
                    return parseInt(this.minutes, 10);
                else
                    return this.minutes;
            };
            Calendar.prototype.getSeconds = function () {
                if (typeof this.seconds === 'string')
                    return parseInt(this.seconds, 10);
                else
                    return this.seconds;
            };
            Calendar.prototype.setDay = function (date) {
                this.date = parseInt(date);
                if (this.hasOwnProperty('dtp'))
                    this.dtp.dateTimeChanged.trigger(this.dtp, { date: new Date(this.year, this.month, this.date, this.hours, this.getMinutes(), this.getSeconds(), 0) });
                this.dateTimeChanged.trigger(this.dtp, { date: new Date(this.year, this.month, this.date, this.hours, this.getMinutes(), this.getSeconds(), 0) });
                this.hide();
            };
            Calendar.prototype.hide = function () {
                this.closed.trigger(this, NxtControl.EmptyEventArgs);
                NxtControl.Util.removeListener(document.body, 'mousedown', this._onMyClick);
                document.body.removeChild(this.win);
                this.win = null;
                delete NxtControl.GuiFramework.currentCalendar;
            };
            Calendar.prototype.updateCal = function () {
                this.win.innerHTML = this.appendCal();
                this.win.focus();
            };
            Calendar.prototype.appendCal = function () {
                var str, vCalHeader, vCalData, vCalTime, i, j, selectStr, vDayCount = 0, vFirstDay, dtToday = new Date();
                vCalHeader = "<table style='background-color:inherit' border=1 cellpadding=1 cellspacing=1 width='100%' align='center' valign='top'>\n";
                vCalHeader += "<tr>\n<td colspan='7'><table border=0 width='100%' cellpadding=0 cellspacing=0><tr><td align='left'>\n";
                vCalHeader += "<select name=\"MonthSelector\" onChange=\"javascript:NxtControl.GuiFramework.currentCalendar.switchMth(this.selectedIndex);NxtControl.GuiFramework.currentCalendar.updateCal();\">\n";
                for (i = 0; i < 12; i++) {
                    if (i === this.month)
                        selectStr = "Selected";
                    else
                        selectStr = "";
                    vCalHeader += "<option " + selectStr + " value >" + Calendar.monthName[i] + "\n";
                }
                vCalHeader += "</select></td>";
                vCalHeader += "\n<td align='right'><a href=\"javascript:NxtControl.GuiFramework.currentCalendar.decYear();NxtControl.GuiFramework.currentCalendar.updateCal()\"><b><font color=\"" + this.yrSelColor + "\"><</font></b></a><font face=\"Verdana\" color=\"" + this.yrSelColor + "\" size=2><b> " + this.year + " </b></font><a href=\"javascript:NxtControl.GuiFramework.currentCalendar.incYear();NxtControl.GuiFramework.currentCalendar.updateCal()\"><b><font color=\"" + this.yrSelColor + "\">></font></b></a></td></tr></table></td>\n";
                vCalHeader += "</tr>";
                if (this.showMonthYear)
                    vCalHeader += "<tr><td colspan='7'><font face='Verdana' size='2' align='center' color='" + this.monthYearColor + "'><b>" + this.getMonthName(this.showLongMonth) + " " + this.year + "</b></font></td></tr>\n";
                vCalHeader += "<tr bgcolor=" + this.weekHeadColor + ">";
                for (i = 1; i < 8; i++) {
                    vCalHeader += "<td align='center'><font face='Verdana' size='2'>" + Calendar.weekDayName[i % 7].substr(0, this.weekChar) + "</font></td>";
                }
                vCalHeader += "</tr>";
                str = vCalHeader;
                var calDate = new Date(this.year, this.month);
                calDate.setDate(1);
                vFirstDay = calDate.getDay() - 1;
                if (vFirstDay < 0)
                    vFirstDay = 6;
                vCalData = "<tr>";
                for (i = 0; i < vFirstDay; i++) {
                    vCalData = vCalData + this.genCell();
                    vDayCount = vDayCount + 1;
                }
                for (j = 1; j <= this.getMonDays(); j++) {
                    vDayCount = vDayCount + 1;
                    var strCell;
                    if ((j === dtToday.getDate()) && (this.month === dtToday.getMonth()) && (this.year === dtToday.getFullYear()))
                        strCell = this.genCell(j, true, this.todayColor);
                    else {
                        if (j === this.date) {
                            strCell = this.genCell(j, true, this.selDateColor);
                        }
                        else {
                            if (vDayCount % 7 === 0)
                                strCell = this.genCell(j, false, this.sundayColor);
                            else if ((vDayCount + 1) % 7 === 0)
                                strCell = this.genCell(j, false, this.saturdayColor);
                            else
                                strCell = this.genCell(j, null, this.weekDayColor);
                        }
                    }
                    vCalData = vCalData + strCell;
                    if ((vDayCount % 7 === 0) && (j < this.getMonDays())) {
                        vCalData = vCalData + "</tr>\n<tr>";
                    }
                }
                str += vCalData;
                if (this.showTime) {
                    var showHour;
                    showHour = this.getShowHour();
                    vCalTime = "<tr>\n<td colspan='7' align='center'>";
                    vCalTime += "<input type='text' name='hour' maxlength=2 size=1 style=\"WIDTH: 22px\" value=" + showHour + " onchange=\"javascript:NxtControl.GuiFramework.currentCalendar.setHour(this.value)\" onblur=\"javascript:NxtControl.GuiFramework.currentCalendar.setHour(this.value)\">";
                    vCalTime += " : ";
                    vCalTime += "<input type='text' name='minute' maxlength=2 size=1 style=\"WIDTH: 22px\" value=" + this.minutes + " onchange=\"javascript:NxtControl.GuiFramework.currentCalendar.setMinute(this.value)\" onblur=\"javascript:NxtControl.GuiFramework.currentCalendar.setMinute(this.value)\">";
                    vCalTime += " : ";
                    vCalTime += "<input type='text' name='second' maxlength=2 size=1 style=\"WIDTH: 22px\" value=" + this.seconds + " onchange=\"javascript:NxtControl.GuiFramework.currentCalendar.setSecond(this.value)\" onblur=\"javascript:NxtControl.GuiFramework.currentCalendar.setSecond(this.value)\">";
                    if (this.timeMode === 12) {
                        var selectAm = (this.hours < 12) ? "Selected" : "";
                        var selectPm = (this.hours >= 12) ? "Selected" : "";
                        vCalTime += "<select name=\"ampm\" onchange=\"javascript:NxtControl.GuiFramework.currentCalendar.setAmPm(this.options[this.selectedIndex].value);\">";
                        vCalTime += "<option " + selectAm + " value=\"AM\">AM</option>";
                        vCalTime += "<option " + selectPm + " value=\"PM\">PM<option>";
                        vCalTime += "</select>";
                    }
                    vCalTime += "\n</td>\n</tr>";
                    str += vCalTime;
                }
                str += "\n</table>";
                return str;
            };
            Calendar.prototype.genCell = function (pValue, pHighLight, pColor) {
                var PValue, PCellStr, vColor, vHLstr1, vHLstr2;
                if (pColor !== null)
                    vColor = "bgcolor=\"" + pColor + "\"";
                else
                    vColor = "";
                if ((pHighLight !== null) && (pHighLight)) {
                    vHLstr1 = "color='red'><b>";
                    vHLstr2 = "</b>";
                }
                else {
                    vHLstr1 = ">";
                    vHLstr2 = "";
                }
                if (!pValue) {
                    PValue = "";
                    PCellStr = "<td " + vColor + " width=" + this.cellWidth + " align='center'><font face='verdana' size='2'" + vHLstr1 + "<a href=\"javascript:void(0)\">" + PValue + "</a>" + vHLstr2 + "</font></td>";
                }
                else {
                    PValue = pValue;
                    PCellStr = "<td " + vColor + " width=" + this.cellWidth + " align='center'><font face='verdana' size='2'" + vHLstr1 + "<a href=\"javascript:void(0)\" onclick=\"javascript:NxtControl.GuiFramework.currentCalendar.setDay(" + PValue + ");\">" + PValue + "</a>" + vHLstr2 + "</font></td>";
                }
                return PCellStr;
            };
            Calendar.monthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            Calendar.weekDayName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromString('#cc0033'))
            ], Calendar.prototype, "monthYearColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromString('#0099CC'))
            ], Calendar.prototype, "weekHeadColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromString('#6699FF'))
            ], Calendar.prototype, "sundayColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromString("#CCCCFF"))
            ], Calendar.prototype, "saturdayColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromString('#fff'))
            ], Calendar.prototype, "weekDayColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromString('#000'))
            ], Calendar.prototype, "fontColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromString('#FFFF33'))
            ], Calendar.prototype, "todayColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromString('#FFFF99'))
            ], Calendar.prototype, "selDateColor", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromString('#cc0033'))
            ], Calendar.prototype, "yrSelColor", void 0);
            __decorate([
                System.DefaultValue(false)
            ], Calendar.prototype, "showTime", void 0);
            __decorate([
                System.DefaultValue('ddMMyyyy')
            ], Calendar.prototype, "format", void 0);
            return Calendar;
        }(GuiFramework.Shape));
        GuiFramework.Calendar = Calendar;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend, NS = NxtControl.Services;
        function TrendObject(data) {
            this._mergeRecursive(this, data);
        }
        TrendObject.prototype = {
            _mergeRecursive: function (dst, src) {
                if (typeof src !== 'object' || src === null) {
                    return dst;
                }
                for (var p in src) {
                    if (!src.hasOwnProperty(p))
                        continue;
                    if (('ignoreJSONProperties' in dst) && dst.ignoreJSONProperties.indexOf(p) !== -1)
                        continue;
                    if (typeof src[p] === 'undefined')
                        continue;
                    if (typeof src[p] !== 'object' || src[p] === null) {
                        dst[p] = src[p];
                    }
                    else if (typeof dst[p] !== 'object' || dst[p] === null) {
                        dst[p] = this._mergeRecursive(src[p].constructor === Array ? [] : {}, src[p]);
                    }
                    else {
                        this._mergeRecursive(dst[p], src[p]);
                    }
                }
                return dst;
            },
            toJSON: function () {
                var json = {};
                for (var prop in this) {
                    if (!this.hasOwnProperty(prop))
                        continue;
                    if (prop[0] === '_')
                        continue;
                    if (('ignoreJSONProperties' in this) && this.ignoreJSONProperties.indexOf(prop) !== -1)
                        continue;
                    if (this[prop] === null)
                        continue;
                    json[prop] = this[prop];
                }
                return json;
            }
        };
        function TrendDataCurve(data) { TrendObject.call(this, data); }
        TrendDataCurve.prototype = Object.create(TrendObject.prototype, {});
        TrendDataCurve.prototype.ignoreJSONProperties = ('ignoreJSONProperties data points bars marks datapoints xaxis yaxis').split(' ');
        function TrendData(data) { TrendObject.call(this, data); }
        TrendData.prototype = Object.create(TrendObject.prototype, {});
        TrendData.prototype.ignoreJSONProperties = ('ignoreJSONProperties legend crosshair zoom pan colors xaxis yaxis series interaction hooks canvas').split(' ');
        function TrendDataXAxis(data) { TrendObject.call(this, data); }
        TrendDataXAxis.prototype = Object.create(TrendObject.prototype, {});
        TrendDataXAxis.prototype.ignoreJSONProperties = ('ignoreJSONProperties transform inverseTransform min max timezone twelveHourClock').split(' ');
        function TrendDataYAxis(data) { TrendObject.call(this, data); }
        TrendDataYAxis.prototype = Object.create(TrendObject.prototype, {});
        TrendDataYAxis.prototype.ignoreJSONProperties = ('ignoreJSONProperties').split(' ');
        var TrendSeries = (function () {
            function TrendSeries(trend, tagName) {
                this.trend = trend;
                this.tagName = tagName;
                this.min = null;
                this.max = null;
                this.min = null;
                this.max = null;
            }
            TrendSeries.prototype.runtimeValueChanged = function (value, time) {
                var data = this.trend.trend.getData();
                var series = null;
                var vSeries = null;
                var vTagName = '__' + this.tagName + '__';
                for (var i = 0; i < data.length; i++) {
                    var tn = data[i].tagName || null;
                    if (this.tagName === tn)
                        series = data[i];
                    if (vTagName === tn)
                        vSeries = data[i];
                }
                if (this.max === null || time > this.max.getTime())
                    this.max = new Date(time);
                if (this.min === null)
                    this.min = new Date(time);
                if (series !== null)
                    series.data.push([time, value]);
                if (vSeries !== null) {
                    vSeries.data = [[time, value], [new Date().getTime(), value]];
                }
                this.trend.trend.processData();
                this.trend.trend.draw();
            };
            TrendSeries.prototype.search = function (data, low, high, x) {
                var arr = data;
                if (arr.length === 0 || x > arr[high][0])
                    return -1;
                if (x <= arr[low][0])
                    return low;
                var mid = Math.floor((low + high) / 2);
                if (arr[mid][0] === x)
                    return mid;
                else if (arr[mid][0] < x) {
                    if (mid + 1 <= high && x <= arr[mid + 1][0])
                        return mid + 1;
                    else
                        return this.search(data, mid + 1, high, x);
                }
                else {
                    if (mid - 1 >= low && x > arr[mid - 1][0])
                        return mid;
                    else
                        return this.search(data, low, mid - 1, x);
                }
            };
            TrendSeries.prototype.addHistoryValues = function (options) {
                if (this.trend._historyAnswerCount === 0)
                    return false;
                var data = this.trend.trend.getData();
                var i, series = null;
                for (i = 0; i < data.length; i++) {
                    var tn = data[i].tagName || null;
                    if (this.tagName === tn)
                        series = data[i];
                }
                var rval = false;
                if (options.hasOwnProperty('archiveId')) {
                    this.archiveId = options.archiveId;
                }
                if (options.hasOwnProperty('values')) {
                    if (options.values.length !== 0) {
                        var min = options.sec[0] * 1000 + options.usec[0] / 1000;
                        if (this.min === null || this.min.getTime() > min)
                            this.min = new Date(min);
                        if (this.max === null)
                            this.max = new Date(min);
                    }
                    for (i = 0; i < options.values.length; i++) {
                        var time = new Date(options.sec[i] * 1000 + options.usec[i] / 1000).getTime();
                        var index = this.search(series.data, 0, series.data.length - 1, time);
                        if (index === -1) {
                            rval = true;
                            series.data.push([time, options.values[i]]);
                        }
                        else if (time !== series.data[index][0]) {
                            series.data.splice(index, 0, [time, options.values[i]]);
                            rval = true;
                        }
                    }
                }
                if (rval) {
                    this.trend.trend.processData();
                }
                this.trend._historyAnswerCount--;
                if (this.trend._historyAnswerCount === 0)
                    this.trend._queryArchiveIfNecessary();
                return rval;
            };
            TrendSeries.prototype.archiveAnswer = function (options) {
                if (options.continued === false) {
                    if (options.values.length !== 0) {
                        var min = options.times[0];
                        if (this.min === null || this.min.getTime() > min)
                            this.min = new Date(min);
                        if (this.max === null)
                            this.max = new Date(min);
                    }
                }
                console.error('Archive answer ' + this.tagName + ' ' + options.values.length);
                var data = this.trend.trend.getData();
                var i, series = null;
                for (i = 0; i < data.length; i++) {
                    var tn = data[i].tagName || null;
                    if (this.tagName === tn)
                        series = data[i];
                }
                var rval = false;
                for (i = 0; i < options.values.length; i++) {
                    var time = new Date(options.times[i]).getTime();
                    var index = this.search(series.data, 0, series.data.length - 1, time);
                    if (index === -1) {
                        rval = true;
                        series.data.push([time, options.values[i]]);
                    }
                    else if (time !== series.data[index][0]) {
                        rval = true;
                        series.data.splice(index, 0, [time, options.values[i]]);
                    }
                }
                this.trend.trend.processData();
                return true;
            };
            return TrendSeries;
        }());
        GuiFramework.TrendSeries = TrendSeries;
        var Trend = (function (_super) {
            __extends(Trend, _super);
            function Trend() {
                _super.apply(this, arguments);
            }
            Trend.prototype.load = function (options) {
                options = options || {};
                var trendOptions = options.trend || {};
                options.trend && delete options.trend;
                var seriesOptions = options.series || null;
                options.series && delete options.series;
                if (options.tagNames)
                    delete options.tagNames;
                this._inInitialize = true;
                _super.prototype.load.call(this, options);
                this._inInitialize = true;
                var currentCanvas = NxtControl.Services.CanvasTopologyService.instance.currentElement;
                this.trendContainer = document.createElement('div');
                this.trendContainer.id = 'trendContainer' + '1';
                this.trendContainer.style.position = 'absolute';
                this.trendContainer.style.left = options.left + 'px';
                this.trendContainer.style.top = options.top + 'px';
                this.trendContainer.style.width = options.width + 'px';
                this.trendContainer.style.height = options.height + 'px';
                currentCanvas.parentNode.insertBefore(this.trendContainer, currentCanvas.nextSibling);
                this.toolBar = document.createElement('div');
                this.toolBar.style.position = 'absolute';
                this.toolBar.style.left = '0px';
                this.toolBar.style.top = '0px';
                this.toolBar.style.width = options.width + 'px';
                this.toolBar.style.height = '20px';
                this.trendContainer.appendChild(this.toolBar);
                this._showLegend = document.createElement('img');
                this._showLegend.style.position = 'absolute';
                this._showLegend.style.left = '20px';
                this._showLegend.style.top = '1px';
                this._showLegend.style.width = '18px';
                this._showLegend.style.height = '18px';
                this._loadImage(this._showLegend, "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwAAADsABataJCQAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTFH80I3AAAAr0lEQVQ4T62TQQ7DIAwESaW2134hl16A/z/PZS2KsLVWoOphHGsNKzuJk4j8yrNx7xwqllKk1mqAvoMGvYi083ejnLPpMsIYseIOVFzgmFDNH9hhmAANi6OZDjwacDG9W9IhRpGBHQ3/ETP6dnfFMIqKE49GOBagIgHrcHOagYoEdIOdYjVFw7xryMF8qAGj645gkOQckHfkgfFr1jRERniuYIxY0YEuwi9HxX0kfQCc107XCo8yvAAAAABJRU5ErkJggg==");
                this.toolBar.appendChild(this._showLegend);
                this._zoomOut = document.createElement('img');
                this._zoomOut.style.position = 'absolute';
                this._zoomOut.style.left = '40px';
                this._zoomOut.style.top = '1px';
                this._zoomOut.style.width = '18px';
                this._zoomOut.style.height = '18px';
                this._loadImage(this._zoomOut, "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwAAADsABataJCQAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTFH80I3AAADWElEQVQ4T3WUa0iTURjHXzWrb0VBRfStIAgCKdEPlt2omQZbdNumgboultnVWIKpuZnVZq/XTMxk2lKaXWdZdnW6XC2pzXRuEs3NZUVk2dUu+3fOaYtp9eHH877nPOf3/nceGPfj+/BoohzPXIJd2bWYF5eLuSuyWaXvZL2V7Af5+mj1P3OBAor80RO7bF1qBWLTL0BUbsd8lR0pGhvWHtBhfepJkP160jeb8H/R0NCgaFeOFuLsq2jqGsLFjlcIkxTiSJML5YYByJRXsDdXi/fv31JZYLKRIk+/My1SXIRttb24aX2NozWtGDMnBakld1F1rw+nmh1YmViMfrfzKumfTAgm/C2y93Txc9eWYGmBFVFJZZgeLWeiKQvkWJxcCk1zN2JlpSB9Vp9grK+OFNlsVj5yQxH2nXfhoM6GjUo9E6Wp9GgwONBuG2CJurstN/wCfw0UBfX0WOtjE9XYVtyCKtMbFDXaMDVqPxpa7LB73qFSZ4QwWQXSV+cX+GugiC3mFVQhbnMpMqvbcc7Uj2NaI0wkiVZvxpotPFL2KOBwPJW8HHBOIP3j6BnKaBHX1mbg5TklEG4qgHSfBtsP6SDLqMHKBAWWxCVhR9pu1Giq0XRNv+zlwHP2ccpoEV2cabzfwldW10B5tAg5eceRe5hHZrYC6en7ERMTA7FYDJXqGPT6SxMcjk52jvs2/IkS9PHDpwMnyr8hec9PRlbuIExGi6zT8mBJV6dZZH5oEDTotBKBYAXCwsIgEUuQn58PY9udRUQk8kv4+N1eLC8EpkUkQn29D2W3XVgn9w66+z4LSGMoYVKv3RLScO6sLDw8HBSpVEqSqVBXd0bAEh1Rf0fmpefIa3SC4zgmOW1wo+6+m6Wj0QkhBM7yuH111sEMWXT0QkREREAaH89kTLRqq5dJ+OY+BIeOZ7KJsxbjsrkf0TL4RRR2Hx1mw4zj6nx+tUjIZPTOmCgt4ycKiaTirguaVjcb+5VHHty0eEA/Qg7TUQcOheu0mPj6s9VOpSILxcXq3yJyD/VCuRe1bb8ljR0e3LK8wCqlFw8ffFWQg/40LNE/ntnUgijPer9UJuz0sp9CoUmIhCd7UwMOjBbMJ9BB/BEF+wj1vVNCfDVQEAiV0QGQf4Bh7hdZxoOz2pGtuwAAAABJRU5ErkJggg==");
                this.toolBar.appendChild(this._zoomOut);
                this._zoomIn = document.createElement('img');
                this._zoomIn.style.position = 'absolute';
                this._zoomIn.style.left = '60px';
                this._zoomIn.style.top = '1px';
                this._zoomIn.style.width = '18px';
                this._zoomIn.style.height = '18px';
                this._loadImage(this._zoomIn, "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwAAADsABataJCQAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTFH80I3AAADaElEQVQ4T42TeUiTYRzHZ9H5VxFE0EUQFEIkITMQNTucusxF1zYLrHWXnYYZpuaRlas37bAwk3lkOItylt3Hmm41o17N5jZkc3MtIlpldtK+Pc9jr8zqj/74vO/2e77PZ7/nmOj7t94/GWrrdEVtz6qUzpLmYEZMFuibfAepS8j44IBs0G9EgQJWbHlunbdsy9l58amXISuxYm6pFxs1Fizdq8XyLWdAxqNIbtCf8wJFrLg9u1oiz7qGxvaPMDi+oOpZD0qMPpTovVDl1WNXTjU+fHhHZVQgCAeKPN1OSZi8yLep0o47rW9Qb3Li1M1OqBvsKHvQhXO3bIhLLka320mX2L8s+mYPoWDtaJfMWHoCCWfsiNl6HlPmZ2DC3D4StpXhstGBeNVJkJwscB5F6IYVLJbWuLAVRdh9yYX9Wgv2acw4WNMCrrYFdXobjBYv6+jlS15K8nRZf4nYWjs6WhPik9XYVPwQZaa30LX3oNnRixevPsHqeY9SbRMS1xSC5GhHkwjCCfbvETO7nLao/KNlnHTdSWSUG1Fr6oax0wcT6aRaZ8aS9Rw27syFzfZC8drrjCZzJv6eO1BECDYY9NPTsk+sSlx7FMrdGmw+oIUqvQJxK3MRLV2NrSk7UKEpR+N1Xfhrr6P/KgiiUYQRBDoQ3tT8UFxaXiHLO1ykyM4/xuUc5LiMrFykpu5BbGws5HI5CguPQKe7IrHZ2sKpQxCFfurpnXm65PveNTt/gpKZ43OamvjoNv5xcHubeZb5iV5Vp61+JJHEICQkBAq5AgUFBWgy3GO3nUqCiCQ8aYcfC44D48TJUN/owqm7LixL8/vcXZ9nk8xkwiS7lR9fV3uBCw0NBUWpVJLOClFTUxXFOjqk/qHIuOJAfoMTIpGISc7r3ahpdrPuSGY0IZiwiH9mDMvcn94aGRkBsVgMZVISkzHRwg1+JuFudWHQkOFMNmrqHFw1dyNSBSoaSRAOJOipWT/xmLqAXyxLZDK6Z2wgJf0njhPJ2fsuaB652bHXt3hwm/eA/kiARGBaG2+SXbxQfjEvNxPFxeo+EdmHiMQ0PyoNfZKGpx7c4V9hYZ4fTx5/5WgmQPJP6IOGhnXav6Ss3OZnS6HQToiE3uCxBLrhQnaAQED4QAP/yzAClY8h0L8IuZC9Qb8A8qB/PM0BpIkAAAAASUVORK5CYII=");
                this.toolBar.appendChild(this._zoomIn);
                this._zoomReset = document.createElement('img');
                this._zoomReset.style.position = 'absolute';
                this._zoomReset.style.left = '80px';
                this._zoomReset.style.top = '1px';
                this._zoomReset.style.width = '18px';
                this._zoomReset.style.height = '18px';
                this._loadImage(this._zoomReset, "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwAAADsABataJCQAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTFH80I3AAADYElEQVQ4T42R/U9TZxTHz3OvCck0UX/X/QFLS+CXkSUaEkzEzJk24gtmZpv2h03NFnQSiSspATQag6LpSBcNQk2Jb8xtgIyQWHWmUSQoL6WlL1AUagVL328LmJyd87A5Ymb0Jp+c53nu9/nec74XXi/OKwTM5zRxs8tVXLq3AfNLa1C/2SIr7+ncxO9Z9y5gcTEHmpYSlfVtxXx544+dWNTgQ33jNH7T4sNyS4c0pfelpFvL+v+DTcDR7jSy+Nj1AHZ6svibO4O6nQ244dtmtD54hbU33WwWI933pP9oYUGDt4Hoq5ewafdp2Qmb8LPxq3O44pPvcNOhFmx7EserA1E8cKoTWUf6VQvzGUHAciA04QfOgsfhTk4230Wd8QSu+fQIfl7Rit3eNPZ44th2LygzI706n0sJgnJ9gwpj3hHgsTiTS30J7PBo2OPL4boSMxqP2NE1qeHDiSS6/FGZFenzctmkIGA54PEMSaN9LX5s/GsOWwdS+Ic3i+tLfsIdR+04GNZwdDqJdwZC0oj0ai6bICMJmSwBXu+Q2LC9FstrOmSwnEm3NyONdlW2oi+SxvFIAuubbiPrSL8iq8UEAcuBYGAUzCd+3sNfq7vhlsFyJh+XHMfdlZelyf2+MdnN4eNnqvx+t9AycwqhEkDwHmAmMgn3nL1ip6m+isX8dzhYzoTH4U74vKj0INZYLCG/bwT6H7sglZgBLR1VCa4AyUSEu1K6Om8pPxw7ZfrMQMHTxX8p2laNxVu+xLKyMqyoqECz2Yw2mw2Gh/shnZrJy6RnFQJ4A7G5MASDbnA6e8DhaAGbzQpNTRcEVWG1Nip1tZY9bFBYWIgGoxGrq6tjDodDtLdfE3R3DXkISCcjkkRsWkTCQQj4BmF0pB9Ghvq4imGqd51/KpebfznHRgUFBWg0SDPuTEw9G+P7q2nWFyohCF4zvH5DMh4Wsy8n4PGj++LSRZtJp9NJM4PRIM3sdrtCnVFG8fBy1H9QCGlC8LkIT/mF8063Ul6+62p+fv5SZ0tjRshMkGjqfZDZFMSikzAeGITurl+Faf/XVf91tjQmZfP8fQgij9fR2ZAa8D1d39vz+8rzjWfq9Hp9jM22frEVIT43+aGoXKOz4+qzkFu4HvSKK/aLYLWeFTeuX4G/AdpeRDawxN/wAAAAAElFTkSuQmCC");
                this.toolBar.appendChild(this._zoomReset);
                this._dtp = document.createElement('img');
                this._dtp.style.position = 'absolute';
                this._dtp.style.left = '100px';
                this._dtp.style.top = '1px';
                this._dtp.style.width = '18px';
                this._dtp.style.height = '18px';
                this._loadImage(this._dtp, "data:image/gif;base64,R0lGODlhEAAQAIIAAKVNSkpNpUpNSqWmpdbT1v///////wAAACH/C05FVFNDQVBFMi4wAwEBAAAh+QQBAAAGACwAAAAAEAAQAAAIZgANCBxIsKCBAQgTKlwoYECAhxADLETYMCLEiQMaFihAYGNHjh43agRJ8uPHhgRSqkwJoCWAlCNNbgQwU+SAkCRpFtCJcqVKly8JxMQps8DQkjh7+lwq9CbSp0eLnhRAtarVqwICAgA7");
                this.toolBar.appendChild(this._dtp);
                this.trendHolder = document.createElement('div');
                this.trendHolder.id = 'trendHolder';
                this.trendHolder.style.position = 'absolute';
                this.trendHolder.style.left = '0px';
                this.trendHolder.style.top = '20px';
                this.trendHolder.style.width = options.width + 'px';
                this.trendHolder.style.height = options.height + 'px';
                this.trendContainer.appendChild(this.trendHolder);
                this.trendLegend = document.createElement('div');
                this.trendLegend.id = 'trendLegend';
                this.trendLegend.style.position = 'absolute';
                this.trendLegend.style.bottom = '0';
                this.trendLegend.style.width = '100%';
                this.trendLegend.style.height = 'auto';
                this.trendLegend.style.padding = '5px';
                this.trendContainer.appendChild(this.trendLegend);
                this._objects = [];
                this._tipText = '';
                var data = [];
                var seriesCount = 0;
                if (seriesOptions !== null) {
                    for (var i = 0; i < seriesOptions.length; i++) {
                        if (seriesOptions[i].hasOwnProperty('tagName')) {
                            data = this.addCurve(seriesOptions[i].tagName, data, seriesOptions[i]);
                            seriesCount++;
                        }
                    }
                }
                if (seriesCount === 0) {
                    data = this._addInvisibleCurve(data);
                }
                trendOptions.canvas = true;
                trendOptions.legend = { show: false };
                trendOptions.crosshair = { mode: 'x', lineWidth: 2 };
                trendOptions.zoom = { interactive: true };
                trendOptions.pan = { interactive: true };
                if (trendOptions.hasOwnProperty('xaxes') === false)
                    trendOptions.xaxes = [{ mode: 'time', color: 'rgb(200,200,200)', timezone: 'browser', font: { size: 12 }, tickFormatter: this._timeFormatter.bind(this) }];
                else {
                    trendOptions.xaxes[0].timezone = 'browser';
                    trendOptions.xaxes[0].labelWidth = 40;
                    trendOptions.xaxes[0].labelHeight = 30;
                    trendOptions.xaxes[0].font.color = trendOptions.xaxes[0].color = 'rgb(200,200,200)';
                    trendOptions.xaxes[0].tickFormatter = this._timeFormatter.bind(this);
                }
                if (trendOptions.hasOwnProperty('yaxes') === false)
                    trendOptions.yaxes = [{ font: { size: 12 }, color: 'rgb(200,200,200)', zoomRange: false, panRange: false }, { position: 'right', font: { size: 12 }, color: 'rgb(200,200,200)', zoomRange: false, panRange: false }];
                else {
                    trendOptions.yaxes[0].panRange = false;
                    trendOptions.yaxes[1].panRange = false;
                    trendOptions.yaxes[0].zoomRange = false;
                    trendOptions.yaxes[1].zoomRange = false;
                    trendOptions.yaxes[0].font.color = trendOptions.yaxes[0].color = 'rgb(200,200,200)';
                    trendOptions.yaxes[1].font.color = trendOptions.yaxes[1].color = 'rgb(200,200,200)';
                }
                if (trendOptions.hasOwnProperty('grid') === false)
                    trendOptions.grid = { hoverable: true, autoHighlight: false };
                else {
                    trendOptions.grid.hoverable = true;
                    trendOptions.grid.color = 'rgb(200,200,200)';
                }
                if (trendOptions.hasOwnProperty('series') === false)
                    trendOptions.series = { lines: { lineWidth: 3 } };
                this.trend = $.plot('#trendHolder', data, trendOptions);
                this._insertLegend();
                this._interval = this._interval.bind(this);
                this._setTimeSpan();
                this._timer = null;
                this._inInitialize = false;
                this._onShowLegend = this._onShowLegend.bind(this);
                this.zoomOut = this.zoomOut.bind(this);
                this.zoomIn = this.zoomIn.bind(this);
                this.zoomReset = this.zoomReset.bind(this);
                this._openCalendar = this._openCalendar.bind(this);
                this._onScroll = this._onScroll.bind(this);
                this._onHover = this._onHover.bind(this);
                this._updateLegend = this._updateLegend.bind(this);
                return this;
            };
            Trend.prototype.initEvents = function () {
                var nextSibling = this.trendContainer.nextSibling;
                if (nextSibling !== null && nextSibling.className.indexOf('upper-canvas ') !== -1) {
                    this.trendContainer.parentNode.insertBefore(nextSibling, this.trendContainer.parentNode.firstChild.nextSibling);
                }
                this._showLegend.addEventListener('click', this._onShowLegend);
                this._zoomOut.addEventListener('click', this.zoomOut);
                this._zoomIn.addEventListener('click', this.zoomIn);
                this._zoomReset.addEventListener('click', this.zoomReset);
                this._dtp.addEventListener('click', this._openCalendar);
                $(this.trendHolder).on('plotpan', this._onScroll);
                $(this.trendHolder).on('plothover', this._onHover);
                this._inMouseMove = false;
                this._needArchiveQuery = false;
                this._historyAnswerCount = this._objects.length;
                this._startTrend();
            };
            Trend.prototype.dispose = function () {
                this.stop();
                _super.prototype.dispose.call(this);
                this.trend.destroy();
                this.trendContainer.parentNode.removeChild(this.trendContainer);
                return this;
            };
            Trend.prototype.disposeEvents = function () {
                this._showLegend.removeEventListener('click', this._onShowLegend);
                this._zoomOut.removeEventListener('click', this.zoomOut);
                this._zoomIn.removeEventListener('click', this.zoomIn);
                this._zoomReset.removeEventListener('click', this.zoomReset);
                this._dtp.removeEventListener('click', this._openCalendar);
                $(this.trendHolder).off("plotpan", this._onScroll);
                $(this.trendHolder).off("plothover", this._onHover);
            };
            Trend.prototype.setDesigner = function (designer, canvas) {
                _super.prototype.setDesigner.call(this, designer, canvas);
                console.info('setting designer');
                var data = this.trend.getData();
                for (var i = 0; i < data.length; i++) {
                    designer.addSeriesData(data, data[i]);
                }
                this.trend.setData(data);
                this._setTrendBounds(this.left, this.top, this.width, this.height);
            };
            Trend.prototype._insertLegend = function () {
                $(this.trendLegend).html('');
                var fragments = [], entries = [], rowStarted = false, s, label;
                var i, series = this.trend.getData();
                for (i = 0; i < series.length; ++i) {
                    s = series[i];
                    if (s.hasOwnProperty('tagName') && s.tagName[0] !== '_') {
                        label = s.tagName;
                        if (label) {
                            entries.push({ label: label, color: s.color });
                        }
                    }
                }
                var opt = this.trend.getOptions();
                var lbc = opt.legend.labelBoxBorderColor;
                for (i = 0; i < entries.length; ++i) {
                    var entry = entries[i];
                    fragments.push('<tr>');
                    rowStarted = true;
                    fragments.push('<td class="legendColorBox"><div style="border:1px solid ' + lbc + ';padding:1px"><div style="width:4px;height:0;border:5px solid ' + entry.color + ';overflow:hidden"></div></div></td>' +
                        '<td class="legendLabel">' + entry.label + '</td>' +
                        '<td class="legendTime"/>' +
                        '<td class="legendValue">');
                    fragments.push('</tr>');
                }
                if (fragments.length === 0)
                    return;
                var table = '<table style="width:100%;font-size:smaller;color:' + opt.grid.color + '">' + '<colgroup><col style="width:15px"/><col style="width:30%"/><col style="width:40%"/><col/></colgroup>' + fragments.join("") + '</table>';
                $(this.trendLegend).html(table);
            };
            Trend.prototype._updateLegend = function () {
                var legendTimes = $(this.trendLegend).find(' .legendTime');
                var legendValues = $(this.trendLegend).find(' .legendValue');
                this.updateLegendTimeout = null;
                var pos = this.latestPosition || { x: -1, y: -1 };
                var axes = this.trend.getAxes();
                if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max ||
                    pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) {
                    return;
                }
                var i, j, dataset = this.trend.getData();
                for (i = 0; i < dataset.length; ++i) {
                    var series = dataset[i];
                    if (series.hasOwnProperty('label')) {
                        var x = '###';
                        var y = '###';
                        if (series.data.length !== 0) {
                            for (j = 0; j < series.data.length; ++j) {
                                if (series.data[j][0] > pos.x) {
                                    break;
                                }
                            }
                            if (j > 0) {
                                x = series.data[j - 1][0];
                                y = series.data[j - 1][1];
                            }
                        }
                        var text = '';
                        if (y !== '###')
                            text = y.toFixed(2);
                        legendValues.eq(i).text(text);
                        text = '';
                        if (x !== '###')
                            text = new Date(x).toLocaleString('en');
                        legendTimes.eq(i).text(text);
                    }
                }
            };
            Trend.prototype._onHover = function (event, pos) {
                this.latestPosition = pos;
                if (!this.updateLegendTimeout) {
                    this.updateLegendTimeout = setTimeout(this._updateLegend, 50);
                }
            };
            Trend.prototype._onScroll = function (event) {
                var ts = this.trend.getXAxes()[0];
                var now = new Date();
                if (ts.max > now.getTime()) {
                    if (this._timer === null)
                        this._timer = setInterval(this._interval, 1000);
                }
                else {
                    this.stop();
                    this._updateLegend();
                    this._needArchiveQuery = true;
                    this._queryArchiveIfNecessary();
                }
            };
            Trend.prototype.stop = function () {
                if (this._timer !== null) {
                    clearInterval(this._timer);
                    this._timer = null;
                }
            };
            Trend.prototype._onShowLegend = function () {
                this._set('showLegend', !this.showLegend);
            };
            Trend.prototype.zoomOut = function () {
                var ts = this.trend.getXAxes()[0];
                var r = (ts.max - ts.min) * (1 + this.zoomPercent / 100);
                ts.options.min = ts.max - r;
                ts.options.max = ts.max;
                this.trend.setupGrid();
                this.trend.draw();
            };
            Trend.prototype.zoomIn = function () {
                var ts = this.trend.getXAxes()[0];
                var cr = ts.max - ts.min;
                var r = cr * (1 - this.zoomPercent / 100);
                if (this._timer !== null)
                    this._setTimeSpan(r);
                else {
                    ts.options.min = ts.min + (cr - r) / 2;
                    ts.options.max = ts.max - (cr - r) / 2;
                    this.trend.setupGrid();
                    this.trend.draw();
                }
            };
            Trend.prototype.zoomReset = function () {
                this.stop();
                this._setTimeSpan();
                this._onZoomReset();
            };
            Trend.prototype._onZoom = function () {
                this.stop();
            };
            Trend.prototype._onZoomReset = function () {
                if (this._timer === null)
                    this._timer = setInterval(this._interval, 1000);
                this._interval();
            };
            Trend.prototype._getScreenCordinates = function (obj) {
                var p = {};
                p.x = obj.offsetLeft;
                p.y = obj.offsetTop;
                while (obj.offsetParent) {
                    p.x = p.x + obj.offsetParent.offsetLeft;
                    p.y = p.y + obj.offsetParent.offsetTop;
                    if (obj === document.getElementsByTagName("body")[0]) {
                        break;
                    }
                    else {
                        obj = obj.offsetParent;
                    }
                }
                return p;
            };
            Trend.prototype._openCalendar = function () {
                if (this.hasOwnProperty('_cal'))
                    return;
                this._cal = new GuiFramework.Calendar().load({ date: new Date(this.trend.getXAxes()[0].min) });
                this._cal.showTime = true;
                this._cal.dateTimeChanged.add(this._onDateTimeChanged.bind(this));
                this._cal.closed.add(this._onCalClosed.bind(this));
                var p = this._getScreenCordinates(this._dtp);
                this._cal.show(p.x, p.y + 20);
            };
            Trend.prototype._onCalClosed = function (sender, ea) {
                delete this._cal;
            };
            Trend.prototype._onDateTimeChanged = function (sender, ea) {
                var ts = this.trend.getXAxes()[0];
                var timeRange = ts.max - ts.min;
                var newTime = ea.date.getTime();
                if (newTime < ts.min) {
                    this.stop();
                }
                else if (this._timer === null) {
                    var now = new Date().getTime();
                    if (newTime > now || newTime + timeRange > now) {
                        this._timer = setInterval(this._interval, 1000);
                        return;
                    }
                }
                else
                    return;
                ts.options.min = newTime;
                ts.options.max = ts.options.min + timeRange;
                this.trend.setupGrid();
                this.trend.draw();
                this._queryArchiveIfNecessary();
            };
            Trend.prototype._queryArchiveIfNecessary = function () {
                var ts = this.trend.getXAxes()[0];
                var maxc = Math.ceil(ts.max);
                var qas = [];
                for (var i = 0; i < this._objects.length; i++) {
                    var series = this._objects[i];
                    if (series.hasOwnProperty('archiveId')) {
                        var mc = series.min === null ? 0 : Math.ceil(series.min.getTime());
                        if (series.min === null || Math.floor(ts.min) < mc) {
                            var qa = { archiveId: series.archiveId, data: series.callback, max: (series.min !== null && mc < maxc) ? mc : maxc };
                            console.error('Query Archive ' + series.tagName + ' ' + new Date(mc).toString() + ' ' + new Date(ts.min).toString() + ' ' + new Date(qa.max).toString());
                            qas.push(qa);
                        }
                    }
                }
                if (qas.length > 0) {
                    var commService = NS.CommunicationService.instance;
                    commService.queryArchive(Math.floor(ts.min), maxc, qas);
                }
            };
            Trend.prototype._mergeRecursive = function (dst, src) {
                if (typeof src !== 'object' || src === null) {
                    return dst;
                }
                for (var p in src) {
                    if (!src.hasOwnProperty(p))
                        continue;
                    if (typeof src[p] === 'undefined')
                        continue;
                    if (typeof src[p] !== 'object' || src[p] === null) {
                        dst[p] = src[p];
                    }
                    else if (typeof dst[p] !== 'object' || dst[p] === null) {
                        dst[p] = this._mergeRecursive(src[p].constructor === Array ? [] : {}, src[p]);
                    }
                    else {
                        this._mergeRecursive(dst[p], src[p]);
                    }
                }
                return dst;
            };
            Trend.prototype._timeFormatter = function (value, axis) {
                if (value < 0)
                    return '';
                var v = new Date(value);
                var today = new Date().getDate();
                var showSeconds = false;
                var showDate = false;
                if (axis.max - axis.min <= 5 * 60 * 1000)
                    showSeconds = true;
                if (v.getDate() !== today || (axis.max - axis.min > 4 * 60 * 60 * 1000))
                    showDate = true;
                var r = '';
                if (showDate)
                    r += v.format('yy/mm/dd') + '\n';
                if (showSeconds)
                    r += v.format('HH:MM:ss');
                else
                    r += v.format('HH:MM');
                return r;
            };
            Trend.prototype._addInvisibleCurve = function (data) {
                var s = {};
                s.data = [];
                for (var t = 0; t < 2; t++) {
                    s.data.push([new Date(0 + t).getTime(), t * 100]);
                }
                data.push(s);
                return data;
            };
            Trend.prototype._render = function (ctx) {
                var m1, transformedLeftTop, transformedRightBottom;
                ctx.save();
                if (this.hasOwnProperty('group')) {
                    m1 = this.getTransformation();
                    transformedLeftTop = m1.getTransformedPoint({ x: -this.width / 2, y: -this.height / 2 });
                    transformedRightBottom = m1.getTransformedPoint({ x: this.width / 2, y: this.height / 2 });
                    this._setTrendBounds(transformedLeftTop.x, transformedLeftTop.y, transformedRightBottom.x - transformedLeftTop.x, transformedRightBottom.y - transformedLeftTop.y);
                }
                else {
                    m1 = this.getTransformation();
                    transformedLeftTop = m1.getTransformedPoint({ x: -this.width / 2, y: -this.height / 2 });
                    transformedRightBottom = m1.getTransformedPoint({ x: this.width / 2, y: this.height / 2 });
                    this._setTrendBounds(transformedLeftTop.x, transformedLeftTop.y, transformedRightBottom.x - transformedLeftTop.x, transformedRightBottom.y - transformedLeftTop.y);
                }
                ctx.restore();
            };
            Trend.prototype.get = function (property) {
                if (property === 'trend') {
                    var options = this.trend.getOptions();
                    var d = {};
                    for (var p in options) {
                        if (p === 'xaxes') {
                            d.xaxes = [new TrendDataXAxis(options.xaxes[0])];
                        }
                        else if (p === 'yaxes') {
                            d.yaxes = [];
                            for (var x = 0; x < options.yaxes.length; x++)
                                d.yaxes.push(new TrendDataYAxis(options.yaxes[x]));
                        }
                        else {
                            d[p] = new TrendObject(options[p]);
                        }
                    }
                    return new TrendData(d);
                }
                var as;
                if (property === 'leftAxis') {
                    as = this.trend.getYAxes();
                    return as[0].options;
                }
                if (property === 'rightAxis') {
                    as = this.trend.getYAxes();
                    return as[1].options;
                }
                if (property === 'series') {
                    var data = this.trend.getData();
                    var sd = [];
                    for (var i = 0; i < data.length; i++)
                        sd.push(new TrendDataCurve(data[i]));
                    return sd;
                }
                return this[property];
            };
            Trend.prototype.addCurve = function (tagName, data, options) {
                data = data || (this.trend != null ? this.trend.getData() : []);
                if (data.length === 1) {
                    var d = data[0];
                    if (d.hasOwnProperty('tagName') === false)
                        data = [];
                }
                var series = {};
                series.data = [];
                series.tagName = tagName;
                var trendSeries = new TrendSeries(this, tagName);
                this._objects.push(trendSeries);
                if (typeof (options) !== 'undefined')
                    this._mergeRecursive(series, options);
                else {
                    if (this.trend !== null) {
                        var opt = this.trend.getOptions();
                        $.extend(true, series, opt.series);
                    }
                    series.color = this.trend.getOptions().colors[data.length];
                    series.shadowSize = 0;
                }
                if (series.hasOwnProperty('label') === false)
                    series.label = tagName;
                if (series.hasOwnProperty('stairs') === true)
                    series.lines.steps = series.stairs;
                if (series.hasOwnProperty('scaleName')) {
                    if (series.scaleName === 'left') {
                        series.yaxis = 1;
                    }
                    if (series.scaleName === 'right') {
                        series.yaxis = 2;
                    }
                    else {
                        series.yaxis = 1;
                        series.scaleName = 'left';
                    }
                }
                if (series.hasOwnProperty('id') === false) {
                    var id = 1;
                    for (var i = 0; i < data.length; i++) {
                        var s = data[i];
                        if (s.hasOwnProperty('id') && s.id > id)
                            id = s.id;
                    }
                    id++;
                    series.id = id;
                }
                data.push(series);
                if (this.designer !== null) {
                    console.info('add curve, designer != null');
                    this.designer.addSeriesData(data, series);
                }
                return data;
            };
            Trend.prototype._loadImage = function (img, url) {
                img.onload = function () {
                    img.onload = img.onerror = null;
                };
                img.onerror = function () {
                    console.error('Error loading ' + img.src);
                    img.onload = img.onerror = null;
                };
                img.crossOrigin = null;
                img.src = url;
            };
            Trend.prototype._find = function (pathParts, index) {
                var namedObject = null;
                for (var i = 0; i < this._objects.length; i++) {
                    if (this._objects[i].tagName === pathParts[index]) {
                        namedObject = this._objects[i];
                        break;
                    }
                }
                if (namedObject !== null) {
                    index++;
                    if (pathParts.length === index)
                        return namedObject;
                }
                return null;
            };
            Trend.prototype._startTrend = function () {
                this._setTimeSpan();
                if (this.designer === null) {
                    var data = this.trend.getData();
                    var newData = [];
                    for (var i = 0; i < data.length; i++) {
                        var series = data[i];
                        if (series.hasOwnProperty('tagName')) {
                            var vSeries = {};
                            vSeries.data = [];
                            var opt = this.trend.getOptions();
                            $.extend(true, vSeries, opt.series);
                            var clr = new NxtControl.Drawing.Color(series.color);
                            clr.setA(0.2);
                            vSeries.color = clr.toLive();
                            vSeries.shadowSize = 0;
                            vSeries.series = series;
                            vSeries.tagName = '__' + series.tagName + '__';
                            newData.push(vSeries);
                        }
                    }
                    $.merge(data, newData);
                    this.trend.setData(data);
                    this._timer = setInterval(this._interval, 1000);
                }
            };
            Trend.prototype._interval = function () {
                var ts = this.trend.getXAxes()[0];
                var r = ts.max - ts.min;
                this._setTimeSpan(r);
                var d = new Date().getTime();
                var data = this.trend.getData();
                for (var i = 0; i < data.length; i++) {
                    var s = data[i];
                    if (s.hasOwnProperty('tagName') && s.data && s.data.length > 0) {
                        var vTagName = '__' + s.tagName + '__';
                        for (var j = i + 1; j < data.length; j++) {
                            if (data[j].hasOwnProperty('tagName') && data[j].tagName === vTagName) {
                                var vs = data[j];
                                vs.data = [s.data[s.data.length - 1], [d, s.data[s.data.length - 1][1]]];
                                break;
                            }
                        }
                    }
                }
                this.trend.processData();
                this._updateLegend();
                this.trend.draw();
            };
            Trend.prototype._setTimeSpan = function (timeSpan) {
                timeSpan = timeSpan || (this.timeSpan * 1000);
                var axis = this.trend.getXAxes()[0];
                axis.options.max = new Date(new Date().getTime() + timeSpan / 20).getTime();
                axis.options.min = axis.options.max - timeSpan;
                this.trend.setupGrid();
                this._updateLegend();
                this.trend.draw();
            };
            Trend.prototype._setTrendBounds = function (left, top, width, height) {
                if (this._inInitialize === false) {
                    if (this.trendContainer.style.left !== (left + 'px') ||
                        this.trendContainer.style.top !== (top + 'px') ||
                        this.trendContainer.style.width !== (width + 'px') ||
                        this.trendContainer.style.height !== (height + 'px') ||
                        this.trendHolder.style.height !== ((height - (this.showLegend ? this.trendLegend.clientHeight : 0) - 20) + 'px')) {
                        this.trendContainer.style.left = left + 'px';
                        this.trendContainer.style.top = top + 'px';
                        this.trendContainer.style.width = width + 'px';
                        this.trendContainer.style.height = height + 'px';
                        this.trendHolder.style.height = (height - (this.showLegend ? this.trendLegend.clientHeight : 0) - 20) + 'px';
                        this.trendHolder.style.width = width + 'px';
                        this.trend.resize();
                        this.trend.setupGrid();
                        this.trend.draw();
                    }
                }
            };
            Trend.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (this.designer !== null && this._inInitialize === false) {
                    if (key === 'scaleX') {
                        this._set('width', value * this.width, invalidate);
                        return this;
                    }
                    else if (key === 'scaleY') {
                        this._set('height', value * this.height, invalidate);
                        return this;
                    }
                }
                if (key === 'showLegend' && value !== this.showLegend) {
                    this.showLegend = value;
                    if (this._inInitialize === false) {
                        if (this.trendLegend !== null) {
                            this.trendLegend.style.display = this.showLegend ? 'block' : 'none';
                            this._setTrendBounds(this.left, this.top, this.width, this.height);
                        }
                    }
                }
                if (key === 'timeAxis') {
                }
                else if (key === 'leftAxis') {
                    this._setYAxisOptions(this.trend.getYAxes()[0].options, value);
                }
                else if (key === 'rightAxis') {
                    this._setYAxisOptions(this.trend.getYAxes()[1].options, value);
                }
                else if (key === 'series') {
                    if (value instanceof Array) {
                        var newData = [];
                        var data = this.trend.getData();
                        for (var i = 0; i < data.length; i++) {
                            var series = data[i];
                            if (series.hasOwnProperty('tagName') === false)
                                continue;
                            for (var j = 0; j < value.length; j++) {
                                if (series.id === value[j].id) {
                                    newData.push(series);
                                    var merge = true;
                                    if (value[j].hasOwnProperty('color')) {
                                        series.color = NxtControl.Drawing.Color.fromObject(value[j].color).toLive();
                                        merge = false;
                                    }
                                    if (value[j].hasOwnProperty('title')) {
                                        series.label = value[j].title;
                                        merge = false;
                                    }
                                    if (value[j].hasOwnProperty('tagName')) {
                                        series.tagName = value[j].tagName;
                                        if (!value[j].hasOwnProperty('title'))
                                            series.label = value[j].tagName;
                                        merge = false;
                                    }
                                    if (value[j].hasOwnProperty('stairs')) {
                                        series.stairs = series.lines.steps = value[j].stairs;
                                        merge = false;
                                    }
                                    if (value[j].hasOwnProperty('scaleName')) {
                                        if (value[j].scaleName === 'left') {
                                            series.yaxis = 1;
                                            series.scaleName = 'left';
                                        }
                                        else if (value[j].scaleName === '1') {
                                            series.yaxis = 1;
                                            series.scaleName = 'left';
                                        }
                                        if (value[j].scaleName === 'right') {
                                            series.yaxis = 2;
                                            series.scaleName = 'right';
                                        }
                                        else if (value[j].scaleName === '2') {
                                            series.yaxis = 2;
                                            series.scaleName = 'right';
                                        }
                                        else {
                                            series.yaxis = 1;
                                            series.scaleName = 'left';
                                        }
                                        merge = false;
                                    }
                                    if (merge)
                                        this._mergeRecursive(series, value[j]);
                                    break;
                                }
                            }
                        }
                        if (newData.length === 0)
                            newData = this._addInvisibleCurve(newData);
                        this.trend.setData(newData);
                        this._insertLegend();
                        this.trend.resize();
                        this.trend.setupGrid();
                        this.trend.draw();
                    }
                }
                else if (key === 'timeSpan') {
                    this.timeSpan = value;
                    if (this._inInitialize === false)
                        this._setTimeSpan();
                }
                else
                    _super.prototype._set.call(this, key, value, invalidate);
                if (key === 'left' || key === 'top' || key === 'width' || key === 'height') {
                    this._setTrendBounds(this.left, this.top, this.width, this.height);
                }
                if (this._inInitialize === false)
                    this.invalidate();
                return this;
            };
            Trend.prototype._setYAxisOptions = function (options, value) {
                for (var vk in value) {
                    if (vk === 'automatic') {
                        if (value[vk] === true) {
                            if (options.hasOwnProperty('min'))
                                delete options.min;
                            if (options.hasOwnProperty('max'))
                                delete options.max;
                            break;
                        }
                    }
                    else
                        options[vk] = value[vk];
                }
                this.trend.setupGrid();
                this.trend.draw();
            };
            Trend.prototype._getTagNames = function () {
                var tagNames = [];
                var series = this.trend.getData();
                for (var i = 0; i < series.length; i++) {
                    if (series[i].hasOwnProperty('tagName')) {
                        if (series[i].hasOwnProperty('id'))
                            tagNames.push(series[i].tagName + ',' + series[i].id);
                        else
                            tagNames.push(series[i].tagName);
                    }
                }
                return tagNames;
            };
            Trend.prototype.toObject = function (propertiesToInclude) {
                var object = extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    timeSpan: this.get('timeSpan'),
                    trend: this.get('trend'),
                    series: this.get('series'),
                    tagNames: this._getTagNames()
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            Trend.fromObject = function (object) {
                return new Trend().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Trend')
            ], Trend.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Trend')
            ], Trend.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.TrendDesigner')
            ], Trend.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Trend.prototype, "enabled", void 0);
            __decorate([
                System.DefaultValue(300),
                System.Browsable
            ], Trend.prototype, "timeSpan", void 0);
            __decorate([
                System.DefaultValue(20),
                System.Browsable
            ], Trend.prototype, "zoomPercent", void 0);
            __decorate([
                System.DefaultValue(true),
                System.Browsable
            ], Trend.prototype, "showLegend", void 0);
            __decorate([
                System.Browsable
            ], Trend.prototype, "series", void 0);
            __decorate([
                System.Browsable
            ], Trend.prototype, "timeAxis", void 0);
            __decorate([
                System.Browsable
            ], Trend.prototype, "leftAxis", void 0);
            __decorate([
                System.Browsable
            ], Trend.prototype, "rightAxis", void 0);
            return Trend;
        }(GuiFramework.Shape));
        GuiFramework.Trend = Trend;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var toFixed = NxtControl.Util.Convert.toFixed;
        var Symbol = (function (_super) {
            __extends(Symbol, _super);
            function Symbol() {
                _super.call(this);
                this.oneShape = false;
                this.delegatedProperties = {
                    brush: true,
                    fontFamily: true,
                    fontWeight: true,
                    fontSize: true,
                    fontStyle: true,
                    lineHeight: true,
                    textDecoration: true,
                    textAlign: true
                };
                this._onMouseDown = this._onMouseDown.bind(this);
                this._onMouseMove = this._onMouseMove.bind(this);
                this._onMouseUp = this._onMouseUp.bind(this);
                this._onClick = this._onClick.bind(this);
            }
            Symbol.prototype.load = function (options) {
                options = options || {};
                var objects = options.objects || null;
                this._left = options.left || 0;
                this._top = options.top || 0;
                this._inInitialize = true;
                var hasObjects = objects !== null;
                delete options.objects;
                _super.prototype.load.call(this, options, objects);
                if (hasObjects === false)
                    this._loadFromSrc();
                if (this._objects.length > 0) {
                    for (var i = this._objects.length; i--;) {
                        this._objects[i].group = this;
                    }
                    this._calcBounds();
                    this._updateObjectsCoords();
                    this.left = this._left;
                    this.top = this._top;
                    this._setOpacityIfSame();
                    this.setCoords();
                    this.saveCoords();
                }
                else {
                    this.left = this._left;
                    this.top = this._top;
                }
                this._inInitialize = false;
                return this;
            };
            Symbol.prototype.dispose = function () {
                this.disposeEvents();
                this.forEachObject(function (o) { if (typeof (o.dispose) === 'function')
                    o.dispose(); });
                return this;
            };
            Symbol.prototype.initEvents = function () {
                this.mousedown.add(this._onMouseDown);
                this.mousemove.add(this._onMouseMove);
                this.mouseup.add(this._onMouseUp);
                this.click.add(this._onClick);
                var _this = this;
                this.forEachObject(function (o) {
                    if (typeof (o.initEvents) === 'function')
                        o.initEvents();
                    if (o._events_ != null) {
                        var l = o._events_.length;
                        for (var i = 0; i < l; i++) {
                            if (typeof (_this[o._events_[i].eventName]) === 'function') {
                                o[o._events_[i].name].add(_this[o._events_[i].eventName].bind(_this));
                            }
                        }
                    }
                });
            };
            Symbol.prototype.disposeEvents = function () {
                this.mousedown.remove(this._onMouseDown);
                this.mousemove.remove(this._onMouseMove);
                this.mouseup.remove(this._onMouseUp);
                this.click.remove(this._onClick);
                var _this = this;
                this.forEachObject(function (o) {
                    if (typeof (o.disposeEvents) === 'function')
                        o.disposeEvents();
                    if (o._events_ != null) {
                        var l = o._events_.length;
                        for (var i = 0; i < l; i++) {
                            if (typeof (_this[o._events_[i].eventName]) === 'function') {
                                o[o._events_[i].name].remove(_this[o._events_[i].eventName].bind(_this));
                            }
                        }
                    }
                });
            };
            Symbol.prototype._onMouseDown = function (sender, options) {
                var pointer = null;
                if (options.e.hasOwnProperty('localPointer'))
                    pointer = options.e.localPointer;
                else
                    pointer = this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var target = this._findTarget(localpointer);
                if (target) {
                    options.e.localPointer = pointer;
                    options.e.localTarget = target;
                    target.mousedown.trigger(target, options);
                    this._doOpenFaceplates(target, options, true);
                }
            };
            Symbol.prototype._onMouseMove = function (sender, options) {
                var pointer = null;
                if (options.e.hasOwnProperty('localPointer'))
                    pointer = options.e.localPointer;
                else
                    pointer = this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var target = this._findTarget(localpointer);
                if (target) {
                    options.e.localPointer = pointer;
                    options.e.localTarget = target;
                    target.mousemove.trigger(target, options);
                }
            };
            Symbol.prototype._onMouseUp = function (sender, options) {
                var pointer = null;
                if (options.e.hasOwnProperty('localPointer'))
                    pointer = options.e.localPointer;
                else
                    pointer = this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var target = this._findTarget(localpointer);
                if (target) {
                    options.e.localPointer = pointer;
                    options.e.localTarget = target;
                    target.mouseup.trigger(target, options);
                    this._doOpenFaceplates(target, options, false);
                }
            };
            Symbol.prototype._onClick = function (sender, options) {
                var pointer = null;
                if (options.e.hasOwnProperty('localPointer'))
                    pointer = options.e.localPointer;
                else
                    pointer = this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var target = this._findTarget(localpointer);
                if (target) {
                    options.e.localPointer = pointer;
                    options.e.localTarget = target;
                    target.click.trigger(target, options);
                    this._doOpenFaceplates(target, options, false);
                }
            };
            Symbol.prototype._doOpenFaceplates = function (target, options, mouseDown) {
            };
            Symbol.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (this.oneShape && this.designer !== null && this._inInitialize === false) {
                    if (key === 'scaleX') {
                        this.width = value * this.width;
                        this._resetBounds('width', this.width);
                        return this;
                    }
                    else if (key === 'scaleY') {
                        this.height = value * this.height;
                        this._resetBounds('height', this.height);
                        return this;
                    }
                    else if (key === 'width' || key === 'height') {
                        this[key] = value;
                        this._resetBounds(key, value);
                    }
                }
                _super.prototype._set.call(this, key, value, invalidate);
                if (key === 'src') {
                    this.setSrc(value);
                }
                return this;
            };
            Symbol.prototype.setSymbolLeftTop = function (left, top) {
                if (left)
                    this._left = left;
                if (top)
                    this._top = top;
                this._calcBounds();
                this._updateObjectsCoords();
                this.left = this._left;
                this.top = this._top;
                this._setOpacityIfSame();
                this.setCoords();
                this.saveCoords();
                return this;
            };
            Symbol.prototype._resetBounds = function (key, value) {
                if (this._objects.length === 1) {
                    var shape = this._objects[0];
                    shape.left = shape['originalLeft'];
                    shape.top = shape['originalTop'];
                    shape.set(key, value);
                    this._calcBounds();
                    this._updateObjectsCoords();
                    this.left = this._left;
                    this.top = this._top;
                    this._setOpacityIfSame();
                    this.setCoords();
                    this.saveCoords();
                }
            };
            Symbol.prototype._loadFromSrc = function (callback) {
                if (typeof this.json !== 'undefined' && this.json !== null) {
                    this.loadFromJSON(this.json, callback);
                    return true;
                }
                else {
                    return false;
                }
            };
            Symbol.prototype.loadFromJSON = function (json, callback) {
                if (!json)
                    return;
                var serialized = (typeof json === 'string')
                    ? JSON.parse(json)
                    : json;
                this._objects.length = 0;
                this._enlivenObjects(serialized.objects, callback);
                return this;
            };
            Symbol.prototype._enlivenObjects = function (objects, callback, reviver) {
                var _this = this;
                if (objects.length === 0) {
                    callback && callback();
                }
                NxtControl.Util.enlivenObjects(objects, function (enlivenedObjects) {
                    enlivenedObjects.forEach(function (obj, index) {
                        _this.insertAt(obj, index, true);
                    });
                    callback && callback();
                }, null, reviver);
            };
            Symbol.prototype.getSrc = function () {
                return this.src;
            };
            Symbol.prototype.setSrc = function (value) {
                this.src = value;
            };
            Symbol.prototype.find = function (path) {
                var pathParts = path.split('.');
                return this._find(pathParts, 0);
            };
            Symbol.prototype._find = function (pathParts, index) {
                var namedObject = null;
                for (var i = 0; i < this._objects.length; i++) {
                    if (this._objects[i].getName() === pathParts[index]) {
                        namedObject = this._objects[i];
                        break;
                    }
                }
                if (namedObject !== null) {
                    index++;
                    if (pathParts.length === index)
                        return namedObject;
                    else
                        return namedObject._find(pathParts, index);
                }
                return null;
            };
            Symbol.prototype._findTarget = function (pointer) {
                var target;
                for (var i = this._objects.length; i--;) {
                    if (this._objects[i] &&
                        this._objects[i].getVisible() &&
                        this._objects[i].evented &&
                        this._objects[i].containsPoint(pointer)) {
                        target = this._objects[i];
                        break;
                    }
                }
                return target;
            };
            Symbol.prototype.getSymbolPath = function (symbolPath) {
                if (symbolPath) {
                    if (this.group && this.group !== null && this.group instanceof Symbol)
                        return this.group.getSymbolPath(this.name + '.' + symbolPath);
                    else if (this.canvas && this.canvas !== null)
                        return this.canvas.getSymbolPath(this.name + '.' + symbolPath);
                    else
                        return this.name + '.' + symbolPath;
                }
                else if (this.group && this.group !== null && this.group instanceof Symbol)
                    return this.group.getSymbolPath(this.name);
                else if (this.canvas && this.canvas !== null)
                    return this.canvas.getSymbolPath(this.name);
                else
                    return this.name;
            };
            Symbol.prototype.toObject = function (propertiesToInclude) {
                var NUM_FRACTION_DIGITS = GuiFramework.Shape.NUM_FRACTION_DIGITS;
                var object = {
                    type: this.type,
                    name: this.name,
                    originX: this.originX,
                    originY: this.originY,
                    left: toFixed(this.left, NUM_FRACTION_DIGITS),
                    top: toFixed(this.top, NUM_FRACTION_DIGITS),
                    width: toFixed(this.width, NUM_FRACTION_DIGITS),
                    height: toFixed(this.height, NUM_FRACTION_DIGITS),
                    scaleX: toFixed(this.scaleX, NUM_FRACTION_DIGITS),
                    scaleY: toFixed(this.scaleY, NUM_FRACTION_DIGITS),
                    angle: toFixed(this.getAngle(), NUM_FRACTION_DIGITS),
                    visible: this.visible,
                    drawable: this.drawable,
                    openFaceplates: this.openFaceplates
                };
                if (!this.includeDefaultValues) {
                    object = this._removeDefaultValues(object);
                }
                if (this._events_ != null && this._events_.length != 0)
                    object._events_ = this._events_;
                NxtControl.Util.populateWithProperties(this, object, propertiesToInclude);
                return object;
            };
            Symbol.fromObject = function (object) {
                return new Symbol().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Symbol')
            ], Symbol.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.SymbolDesigner')
            ], Symbol.prototype, "designerType", void 0);
            return Symbol;
        }(GuiFramework.Group));
        GuiFramework.Symbol = Symbol;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var toFixed = NxtControl.Util.Convert.toFixed;
        var Graphic = (function (_super) {
            __extends(Graphic, _super);
            function Graphic() {
                _super.call(this);
                this.oneShape = false;
                this.delegatedProperties = {
                    brush: true,
                    fontFamily: true,
                    fontWeight: true,
                    fontSize: true,
                    fontStyle: true,
                    lineHeight: true,
                    textDecoration: true,
                    textAlign: true
                };
                this._onMouseDown = this._onMouseDown.bind(this);
                this._onMouseMove = this._onMouseMove.bind(this);
                this._onMouseUp = this._onMouseUp.bind(this);
                this._onClick = this._onClick.bind(this);
            }
            Graphic.prototype.load = function (options) {
                options = options || {};
                var objects = options.objects || null;
                this._left = options.left || 0;
                this._top = options.top || 0;
                this._inInitialize = true;
                var hasObjects = objects !== null;
                delete options.objects;
                _super.prototype.load.call(this, options, objects);
                if (hasObjects === false)
                    this._loadFromSrc();
                if (this._objects.length > 0) {
                    for (var i = this._objects.length; i--;) {
                        this._objects[i].group = this;
                    }
                    this._calcBounds();
                    this._updateObjectsCoords();
                    this.left = this._left;
                    this.top = this._top;
                    this._setOpacityIfSame();
                    this.setCoords();
                    this.saveCoords();
                }
                else {
                    this.left = this._left;
                    this.top = this._top;
                }
                this._inInitialize = false;
                return this;
            };
            Graphic.prototype.dispose = function () {
                this.disposeEvents();
                this.forEachObject(function (o) { if (typeof (o.dispose) === 'function')
                    o.dispose(); });
                return this;
            };
            Graphic.prototype.initEvents = function () {
                this.mousedown.add(this._onMouseDown);
                this.mousemove.add(this._onMouseMove);
                this.mouseup.add(this._onMouseUp);
                this.click.add(this._onClick);
                var _this = this;
                this.forEachObject(function (o) {
                    if (typeof (o.initEvents) === 'function')
                        o.initEvents();
                    if (o._events_ != null) {
                        var l = o._events_.length;
                        for (var i = 0; i < l; i++) {
                            if (typeof (_this[o._events_[i].eventName]) === 'function') {
                                o[o._events_[i].name].add(_this[o._events_[i].eventName].bind(_this));
                            }
                        }
                    }
                });
            };
            Graphic.prototype.disposeEvents = function () {
                this.mousedown.remove(this._onMouseDown);
                this.mousemove.remove(this._onMouseMove);
                this.mouseup.remove(this._onMouseUp);
                this.click.remove(this._onClick);
                var _this = this;
                this.forEachObject(function (o) {
                    if (typeof (o.disposeEvents) === 'function')
                        o.disposeEvents();
                    if (o._events_ != null) {
                        var l = o._events_.length;
                        for (var i = 0; i < l; i++) {
                            if (typeof (_this[o._events_[i].eventName]) === 'function') {
                                o[o._events_[i].name].remove(_this[o._events_[i].eventName].bind(_this));
                            }
                        }
                    }
                });
            };
            Graphic.prototype._onMouseDown = function (sender, options) {
                var pointer = null;
                if (options.e.hasOwnProperty('localPointer'))
                    pointer = options.e.localPointer;
                else
                    pointer = this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var target = this._findTarget(localpointer);
                if (target) {
                    options.e.localPointer = pointer;
                    options.e.localTarget = target;
                    target.mousedown.trigger(target, options);
                }
            };
            Graphic.prototype._onMouseMove = function (sender, options) {
                var pointer = null;
                if (options.e.hasOwnProperty('localPointer'))
                    pointer = options.e.localPointer;
                else
                    pointer = this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var target = this._findTarget(localpointer);
                if (target) {
                    options.e.localPointer = pointer;
                    options.e.localTarget = target;
                    target.mousemove.trigger(target, options);
                }
            };
            Graphic.prototype._onMouseUp = function (sender, options) {
                var pointer = null;
                if (options.e.hasOwnProperty('localPointer'))
                    pointer = options.e.localPointer;
                else
                    pointer = this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var target = this._findTarget(localpointer);
                if (target) {
                    options.e.localPointer = pointer;
                    options.e.localTarget = target;
                    target.mouseup.trigger(target, options);
                }
            };
            Graphic.prototype._onClick = function (sender, options) {
                var pointer = null;
                if (options.e.hasOwnProperty('localPointer'))
                    pointer = options.e.localPointer;
                else
                    pointer = this.canvas.getPointer(options.e);
                var localpointer = this.getInverseTransformation().getTransformedPoint(pointer);
                var target = this._findTarget(localpointer);
                if (target) {
                    options.e.localPointer = pointer;
                    options.e.localTarget = target;
                    target.click.trigger(target, options);
                }
            };
            Graphic.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (this.oneShape && this.designer !== null && this._inInitialize === false) {
                    if (key === 'scaleX') {
                        this.width = value * this.width;
                        this._resetBounds('width', this.width);
                        return this;
                    }
                    else if (key === 'scaleY') {
                        this.height = value * this.height;
                        this._resetBounds('height', this.height);
                        return this;
                    }
                    else if (key === 'width' || key === 'height') {
                        this[key] = value;
                        this._resetBounds(key, value);
                    }
                }
                _super.prototype._set.call(this, key, value, invalidate);
                if (key === 'src') {
                    this.setSrc(value);
                }
                return this;
            };
            Graphic.prototype._resetBounds = function (key, value) {
                if (this._objects.length === 1) {
                    var shape = this._objects[0];
                    shape.left = shape['originalLeft'];
                    shape.top = shape['originalTop'];
                    shape.set(key, value);
                    this._calcBounds();
                    this._updateObjectsCoords();
                    this.left = this._left;
                    this.top = this._top;
                    this._setOpacityIfSame();
                    this.setCoords();
                    this.saveCoords();
                }
            };
            Graphic.prototype._loadFromSrc = function (callback) {
                if (typeof this.json !== 'undefined' && this.json !== null) {
                    this.loadFromJSON(this.json, callback);
                    return true;
                }
                else {
                    return false;
                }
            };
            Graphic.prototype.loadFromJSON = function (json, callback) {
                if (!json)
                    return;
                var serialized = (typeof json === 'string')
                    ? JSON.parse(json)
                    : json;
                this._objects.length = 0;
                this._enlivenObjects(serialized.objects, callback);
                return this;
            };
            Graphic.prototype._enlivenObjects = function (objects, callback, reviver) {
                var _this = this;
                if (objects.length === 0) {
                    callback && callback();
                }
                NxtControl.Util.enlivenObjects(objects, function (enlivenedObjects) {
                    enlivenedObjects.forEach(function (obj, index) {
                        _this.insertAt(obj, index, true);
                    });
                    callback && callback();
                }, null, reviver);
            };
            Graphic.prototype.getSrc = function () {
                return this.src;
            };
            Graphic.prototype.setSrc = function (value) {
                this.src = value;
            };
            Graphic.prototype.find = function (path) {
                var pathParts = path.split('.');
                return this._find(pathParts, 0);
            };
            Graphic.prototype._find = function (pathParts, index) {
                var namedObject = null;
                for (var i = 0; i < this._objects.length; i++) {
                    if (this._objects[i].getName() === pathParts[index]) {
                        namedObject = this._objects[i];
                        break;
                    }
                }
                if (namedObject !== null) {
                    index++;
                    if (pathParts.length === index)
                        return namedObject;
                    else
                        return namedObject._find(pathParts, index);
                }
                return null;
            };
            Graphic.prototype._findTarget = function (pointer) {
                var target;
                for (var i = this._objects.length; i--;) {
                    if (this._objects[i] &&
                        this._objects[i].getVisible() &&
                        this._objects[i].evented &&
                        this._objects[i].containsPoint(pointer)) {
                        target = this._objects[i];
                        break;
                    }
                }
                return target;
            };
            Graphic.prototype.toObject = function (propertiesToInclude) {
                var NUM_FRACTION_DIGITS = GuiFramework.Shape.NUM_FRACTION_DIGITS;
                var object = {
                    type: this.type,
                    name: this.name,
                    originX: this.originX,
                    originY: this.originY,
                    left: toFixed(this.left, NUM_FRACTION_DIGITS),
                    top: toFixed(this.top, NUM_FRACTION_DIGITS),
                    width: toFixed(this.width, NUM_FRACTION_DIGITS),
                    height: toFixed(this.height, NUM_FRACTION_DIGITS),
                    scaleX: toFixed(this.scaleX, NUM_FRACTION_DIGITS),
                    scaleY: toFixed(this.scaleY, NUM_FRACTION_DIGITS),
                    angle: toFixed(this.getAngle(), NUM_FRACTION_DIGITS),
                    visible: this.visible,
                    drawable: this.drawable,
                };
                if (!this.includeDefaultValues) {
                    object = this._removeDefaultValues(object);
                }
                if (this._events_ != null && this._events_.length != 0)
                    object._events_ = this._events_;
                NxtControl.Util.populateWithProperties(this, object, propertiesToInclude);
                return object;
            };
            Graphic.fromObject = function (object) {
                return new Graphic().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Graphic')
            ], Graphic.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.GraphicDesigner')
            ], Graphic.prototype, "designerType", void 0);
            return Graphic;
        }(GuiFramework.Group));
        GuiFramework.Graphic = Graphic;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var RuntimeSymbol = (function (_super) {
            __extends(RuntimeSymbol, _super);
            function RuntimeSymbol() {
                _super.call(this);
                this._onCloseDialog = this._onCloseDialog.bind(this);
            }
            RuntimeSymbol.prototype._doOpenFaceplates = function (target, options, isDown) {
                var isLeftClick = 'which' in options.e ? options.e.which === 1 : options.e.button === 1;
                var isRightClick = 'which' in options.e ? options.e.which === 3 : options.e.button === 2;
                if (target.openFaceplates !== null && target.openFaceplates.length > 0) {
                    for (var i = 0; i < target.openFaceplates.length; i++) {
                        if (target.openFaceplates[i].fp && target.openFaceplates[i].fp.length > 0) {
                            if ((isDown && (isLeftClick && ((target.openFaceplates[i].mt & GuiFramework.MouseButtonType.MouseDownLeft) === GuiFramework.MouseButtonType.MouseDownLeft)) ||
                                (isRightClick && ((target.openFaceplates[i].mt & GuiFramework.MouseButtonType.MouseDownRight) === GuiFramework.MouseButtonType.MouseDownRight))) ||
                                (!isDown && (isLeftClick && ((target.openFaceplates[i].mt & GuiFramework.MouseButtonType.MouseUpLeft) === GuiFramework.MouseButtonType.MouseUpLeft ||
                                    (target.openFaceplates[i].mt & GuiFramework.MouseButtonType.Click) === GuiFramework.MouseButtonType.Click)) ||
                                    (isRightClick && ((target.openFaceplates[i].mt & GuiFramework.MouseButtonType.MouseUpRight) === GuiFramework.MouseButtonType.MouseUpRight)))) {
                                if (this.hasOwnProperty('_faceplates') === false)
                                    this._faceplates = {};
                                if (this._faceplates.hasOwnProperty(target.openFaceplates[i].fp)) {
                                    this._faceplates[target.openFaceplates[i].fp].bringToFront();
                                }
                                else {
                                    var ts = this.type.split('.');
                                    ts[2] = 'Faceplates';
                                    ts[ts.length - 1] = target.openFaceplates[i].fp;
                                    var dialog = new GuiFramework.Dialog(target.openFaceplates[i].fp, ts.join('.'), this.getSymbolPath());
                                    dialog.onclose.add(this._onCloseDialog);
                                    this._faceplates[target.openFaceplates[i].fp] = dialog;
                                }
                            }
                        }
                    }
                }
            };
            RuntimeSymbol.prototype._onCloseDialog = function (sender, options) {
                if (this._faceplates.hasOwnProperty(options.faceplate))
                    delete this._faceplates[options.faceplate];
            };
            RuntimeSymbol.prototype.toObject = function (propertiesToInclude) {
                return extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    tagName: this.get('tagName')
                });
            };
            RuntimeSymbol.fromObject = function (object) {
                return new RuntimeSymbol();
            };
            ;
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.RuntimeSymbol')
            ], RuntimeSymbol.prototype, "type", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], RuntimeSymbol.prototype, "tagName", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.RuntimeSymbol')
            ], RuntimeSymbol.prototype, "csClass", void 0);
            return RuntimeSymbol;
        }(GuiFramework.Symbol));
        GuiFramework.RuntimeSymbol = RuntimeSymbol;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var RuntimeBaseSymbol = (function (_super) {
            __extends(RuntimeBaseSymbol, _super);
            function RuntimeBaseSymbol() {
                _super.apply(this, arguments);
                this.value = null;
            }
            RuntimeBaseSymbol.prototype.onValueChanged = function (value, time) { return false; };
            RuntimeBaseSymbol.prototype.runtimeValueChanged = function (value, time) {
                if (this.onValueChanged)
                    return this.onValueChanged(value, time);
                else
                    this.value = value;
                return false;
            };
            RuntimeBaseSymbol.getValue = function (ranges, propertyName, value) {
                var defaultValue = ranges.defaultProps[propertyName] || null;
                for (var i = 0; i < ranges.range.length; i++) {
                    var range = ranges.range[i];
                    if (typeof range.value !== 'undefined') {
                        if (value == range.value) {
                            return range.props[propertyName] || defaultValue;
                        }
                        continue;
                    }
                    var inside = true;
                    if (typeof range.min !== 'undefined' && range.min != null) {
                        if (range.min == value) {
                            if (range.incMin)
                                return range.props[propertyName] || defaultValue;
                            else
                                inside = false;
                        }
                        else if (value < range.min)
                            inside = false;
                    }
                    if (inside == false)
                        continue;
                    if (typeof range.max !== 'undefined' && range.max != null) {
                        if (range.max == value) {
                            if (range.incMax)
                                return range.props[propertyName] || defaultValue;
                            else
                                inside = false;
                        }
                        else if (value > range.max)
                            inside = false;
                    }
                    if (inside == false)
                        continue;
                    return range.props[propertyName] || defaultValue;
                }
                return defaultValue;
            };
            RuntimeBaseSymbol.prototype.toObject = function (propertiesToInclude) {
                return extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    tagName: this.get('tagName'), valueType: this.get('valueType')
                });
            };
            RuntimeBaseSymbol.fromObject = function (object) {
                return new RuntimeBaseSymbol().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.RuntimeBaseSymbol')
            ], RuntimeBaseSymbol.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.RuntimeBaseSymbol')
            ], RuntimeBaseSymbol.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], RuntimeBaseSymbol.prototype, "tagName", void 0);
            __decorate([
                System.DefaultValue(null),
                System.Browsable
            ], RuntimeBaseSymbol.prototype, "valueType", void 0);
            return RuntimeBaseSymbol;
        }(GuiFramework.Symbol));
        GuiFramework.RuntimeBaseSymbol = RuntimeBaseSymbol;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend, ColorService = NxtControl.Services.ColorService, AlarmService = NxtControl.Services.AlarmService;
        var alarmColumnTypes = [];
        alarmColumnTypes.push({ Column: 'Shortcut', Visible: true }, { Column: 'Ack', Visible: true }, { Column: 'Origin', Visible: true }, { Column: 'Text', Visible: true }, { Column: 'CameTime', Visible: true }, { Column: 'GoneTime', Visible: true }, { Column: 'Present', Visible: true }, { Column: 'State', Visible: true }, { Column: 'Value', Visible: true }, { Column: 'InfoValue', Visible: false }, { Column: 'CameAckTime', Visible: false }, { Column: 'GoneAckTime', Visible: false }, { Column: 'Interval', Visible: false }, { Column: 'CameAckUser', Visible: false }, { Column: 'GoneAckUser', Visible: false }, { Column: 'Path', Visible: false }, { Column: 'Alias', Visible: false }, { Column: 'Time', Visible: false });
        var AlarmControl = (function (_super) {
            __extends(AlarmControl, _super);
            function AlarmControl() {
                _super.call(this);
                this.alarmId = -1;
                this.alarmInfos = {};
                this.alarmColors = {};
            }
            AlarmControl.prototype.load = function (options) {
                options = options || {};
                this._inInitialize = true;
                _super.prototype.load.call(this, options);
                this._inInitialize = true;
                var currentCanvas = NxtControl.Services.CanvasTopologyService.instance.currentElement;
                this.alarmControl = document.createElement('div');
                this.alarmControl.id = 'alarmControl' + '1';
                this.alarmControl.style.position = 'absolute';
                this.alarmControl.style.left = options.left + 'px';
                this.alarmControl.style.top = options.top + 'px';
                this.alarmControl.style.width = options.width + 'px';
                this.alarmControl.style.height = options.height + 'px';
                currentCanvas.parentNode.insertBefore(this.alarmControl, currentCanvas.nextSibling);
                this._insertAlarmTable();
                this._inInitialize = false;
                this.set('height', this.table.outerHeight() + $(this.alarmControl).find('.pager').height());
                this._setAlarmBounds();
                return this;
            };
            AlarmControl.prototype.dispose = function () {
                $(this.alarmControl).off('click', '.ACK', this._onAckClick);
                ColorService.instance.startColorChange.remove(this._startColorChange);
                ColorService.instance.stopColorChange.remove(this._stopColorChange);
                if (AlarmService.instance && this.alarmId !== -1) {
                    AlarmService.instance.unsubscribe(this.alarmId);
                    this.alarmId = -1;
                }
                if (this.alarmControl !== null) {
                    this.alarmControl.parentNode.removeChild(this.alarmControl);
                    this.alarmControl = null;
                }
                return this;
            };
            AlarmControl.prototype.hasClass = function (element, cls) {
                return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
            };
            AlarmControl.prototype.initEvents = function () {
                this._startColorChange = this._startColorChange.bind(this);
                this._stopColorChange = this._stopColorChange.bind(this);
                ColorService.instance.startColorChange.add(this._startColorChange);
                ColorService.instance.stopColorChange.add(this._stopColorChange);
                var nextSibling = this.alarmControl.nextSibling;
                if (nextSibling !== null && nextSibling.classList.length !== 0 && nextSibling.classList.contains('upper-canvas')) {
                    this.alarmControl.parentNode.insertBefore(nextSibling, this.alarmControl.parentNode.firstChild.nextSibling);
                }
                if (AlarmService.instance && this.alarmId === -1)
                    this.alarmId = AlarmService.instance.subscribe(this);
                this._onAckClick = this._onAckClick.bind(this);
                $(this.alarmControl).on('click', '.ACK', this._onAckClick);
            };
            AlarmControl.prototype._render = function (ctx, noTransform) {
                ctx.save();
                if (this.hasOwnProperty('group')) {
                    var m1 = this.getTransformation();
                    var transformedLeftTop = m1.getTransformedPoint({ x: -this.width / 2, y: -this.height / 2 });
                    var transformedRightBottom = m1.getTransformedPoint({ x: this.width / 2, y: this.height / 2 });
                    this._setAlarmBounds(transformedLeftTop.x, transformedLeftTop.y, transformedRightBottom.x - transformedLeftTop.x, transformedRightBottom.y - transformedLeftTop.y);
                }
                ctx.restore();
            };
            AlarmControl.prototype._set = function (key, value, invalidate) {
                if (invalidate === void 0) { invalidate = false; }
                if (key === 'alarmColumns' && this._inInitialize === false) {
                    this.alarmColumns = value;
                    this._insertAlarmTable();
                }
                else if (key === 'pagerSize' && this._inInitialize === false) {
                    if (value !== this.pagerSize) {
                        this.pagerSize = value;
                        $(this.alarmControl).find('.pagesize').html('<option selected="selected" value="' + this.pagerSize + '">' + this.pagerSize + '</option>').trigger('change');
                        if (this.designer !== null) {
                            this.set('height', this.table.outerHeight() + $(this.alarmControl).find('.pager').height());
                        }
                    }
                }
                else {
                    _super.prototype._set.call(this, key, value, invalidate);
                    if (this.designer !== null)
                        this.designer.propertyChanged(key);
                    if (key === 'left' || key === 'top' || key === 'width' || key === 'height') {
                        this._setAlarmBounds(this.left, this.top, this.width, this.height);
                    }
                }
                if (this._inInitialize === false)
                    this.invalidate();
                return this;
            };
            AlarmControl.prototype._setAlarmBounds = function (left, top, width, height) {
                if (this._inInitialize === false) {
                    if (this.alarmControl.style.left !== (left + 'px') ||
                        this.alarmControl.style.top !== (top + 'px') ||
                        this.alarmControl.style.width !== (width + 'px') ||
                        this.alarmControl.style.height !== (height + 'px')) {
                        this.alarmControl.style.left = left + 'px';
                        this.alarmControl.style.top = top + 'px';
                        this.alarmControl.style.width = width + 'px';
                        this.alarmControl.style.height = height + 'px';
                    }
                }
            };
            AlarmControl.prototype._insertAlarmTable = function () {
                $(this.alarmControl).html('');
                var colDef = [], fragments = [], label, width;
                colDef.push('<colgroup>');
                fragments.push('<thead>');
                var j = 0;
                var sortColumn = -1;
                for (var i = 0; i < this.alarmColumns.length; ++i) {
                    var ci = this.alarmColumns[i];
                    if (!ci.Visible)
                        continue;
                    switch (ci.Column) {
                        case 'Shortcut':
                            {
                                label = ci.HeaderText || 'SC';
                                width = ci.Width || 32;
                            }
                            break;
                        case 'Origin':
                        case 'Alias':
                        case 'Path':
                            {
                                label = ci.HeaderText || ci.Column;
                                width = ci.Width || 200;
                            }
                            break;
                        case 'Value':
                            {
                                label = ci.HeaderText || ci.Column;
                                width = ci.Width || 80;
                            }
                            break;
                        case 'InfoValue':
                            {
                                label = ci.HeaderText || 'Info';
                                width = ci.Width || 80;
                            }
                            break;
                        case 'State':
                            {
                                label = ci.HeaderText || ci.Column;
                                width = ci.Width || 80;
                            }
                            break;
                        case 'Ack':
                            {
                                label = ci.HeaderText || ci.Column;
                                width = ci.Width || 40;
                            }
                            break;
                        case 'CameTime':
                            {
                                label = ci.HeaderText || 'Came';
                                width = ci.Width || 130;
                                sortColumn = j;
                            }
                            break;
                        case 'GoneTime':
                            {
                                label = ci.HeaderText || 'Gone';
                                width = ci.Width || 130;
                                if (sortColumn === -1)
                                    sortColumn = j;
                            }
                            break;
                        case 'Time':
                            {
                                label = ci.HeaderText || ci.Column;
                                width = ci.Width || 130;
                                if (sortColumn === -1)
                                    sortColumn = j;
                            }
                            break;
                        case 'Present':
                            {
                                label = ci.HeaderText || ci.Column;
                                width = ci.Width || 48;
                            }
                            break;
                        default:
                            {
                                label = ci.HeaderText || ci.Column;
                                width = ci.Width || 100;
                            }
                            break;
                    }
                    j++;
                    colDef.push('<col style="width:' + width + 'px"/>');
                    fragments.push('<th style="background-color:' + NxtControl.Drawing.Color.fromName('AlarmControlHeaderBackColor').toLive() + ';color:' + NxtControl.Drawing.Color.fromName('AlarmControlHeaderForeColor').toLive() + '">' + label + '</th>');
                }
                colDef.push('</colgroup>');
                fragments.push('</thead>');
                fragments.push('<tbody></tbody>');
                if (fragments.length === 0)
                    return;
                var table = '<table class="tablesorter" id="alarm" style="width:100%;border:1px;border-color:black;color:' + 'black' + '">' + colDef.join('') + fragments.join('') + '</table>';
                var pager = '<div class="pager"><img src="css/first.png" class="first"/><img src="css/prev.png" class="prev"/><span class="pagedisplay"></span><img src="css/next.png" class="next"/><img src="css/last.png" class="last"/>' +
                    '<select class="gotoPage" title="Select page number"></select><select class="pagesize" title="Select page size" style="visibility:hidden"><option selected="selected" value="' + this.pagerSize + '">' + this.pagerSize + '</option></select></div>';
                $(this.alarmControl).append(table);
                $(this.alarmControl).append(pager);
                this.table = $(this.alarmControl).find("#alarm");
                var pagerOptions = {
                    container: $(this.alarmControl).find(".pager"),
                    ajaxUrl: null,
                    customAjaxUrl: function (table, url) { return url; },
                    ajaxProcessing: function (ajax) {
                        if (ajax && ajax.hasOwnProperty('data')) {
                            return [ajax.total_rows, ajax.data];
                        }
                    },
                    output: '{startRow:input} to {endRow} ({totalRows})',
                    updateArrows: true,
                    page: 0,
                    savePages: false,
                    storageKey: 'tablesorter-pager',
                    fixedHeight: true,
                    removeRows: false,
                    cssNext: '.next',
                    cssPrev: '.prev',
                    cssFirst: '.first',
                    cssLast: '.last',
                    cssGoto: '.gotoPage',
                    cssPageDisplay: '.pagedisplay',
                    cssPageSize: '.pagesize',
                    cssDisabled: 'disabled',
                    cssErrorRow: 'tablesorter-errorRow'
                };
                pagerOptions.size = this.pagerSize;
                var sorterOptions = { widthFixed: true, widgets: ['zebra'] };
                if (sortColumn !== -1)
                    sorterOptions.sortList = [[sortColumn, 0]];
                this.table.tablesorter(sorterOptions).
                    tablesorterPager(pagerOptions);
            };
            AlarmControl.prototype._onAckClick = function (e) {
                var cell = e.target || window.event.srcElement;
                if (cell.innerText === '!!!') {
                    AlarmService.instance.ackAlarm($(cell.parentNode).attr("data-id"));
                }
            };
            AlarmControl.prototype.setDesigner = function (designer, canvas) {
                _super.prototype.setDesigner.call(this, designer, canvas);
                this._setAlarmBounds(this.left, this.top, this.width, this.height);
            };
            AlarmControl.prototype.alarmUpdate = function (action, index, alarmInfo) {
                var ai, fragments, oldColor, key = alarmInfo.Device + ':' + alarmInfo.TableId;
                switch (action) {
                    case 'Add':
                        {
                            if (this.alarmInfos.hasOwnProperty(key))
                                $(this.alarmControl).find('[data-id="' + key + '"]').remove();
                            this.alarmInfos[key] = { alarmInfo: alarmInfo, color: null };
                            fragments = $(this._defineRow(key, alarmInfo, null).join(''));
                            $(this.alarmControl).find('#alarm tbody').append(fragments).trigger("addRows", [fragments, true]);
                        }
                        break;
                    case 'Update':
                        {
                            ai = this.alarmInfos.hasOwnProperty(key) ? this.alarmInfos[key] : null;
                            if (ai === null) {
                                console.error('AlarmControl: Row ' + key + 'not found');
                                return;
                            }
                            oldColor = (ai !== null && ai.color !== null) ? ai.color : null;
                            var row = $(this.alarmControl).find('[data-id="' + key + '"]');
                            if (row === null || typeof row === 'undefined') {
                                console.error('AlarmControl: Row ' + key + 'not found');
                                return;
                            }
                            fragments = this._defineRow(key, alarmInfo, oldColor);
                            $(this.alarmControl).find('[data-id="' + key + '"]').replaceWith(fragments.join(''));
                            this.table.trigger("update");
                        }
                        break;
                    case 'Remove':
                        {
                            ai = this.alarmInfos.hasOwnProperty(key) ? this.alarmInfos[key] : null;
                            if (ai === null) {
                                console.error('AlarmControl: Row ' + key + 'not found');
                                return;
                            }
                            delete this.alarmInfos[key];
                            oldColor = (ai !== null && ai.color !== null) ? ai.color : null;
                            if (oldColor !== null) {
                                this._detachColor(oldColor);
                            }
                            $(this.alarmControl).find('[data-id="' + key + '"]').remove();
                            this.table.trigger("update");
                        }
                        break;
                }
            };
            AlarmControl.prototype._detachColor = function (color) {
                if (this.alarmColors.hasOwnProperty(color)) {
                    this.alarmColors[color].count--;
                    if (this.alarmColors[color].count === 0) {
                        ColorService.instance.detach(this.alarmColors[color].color);
                    }
                }
            };
            AlarmControl.prototype._defineRow = function (row, alarmInfo, oldColor) {
                var fragments = [];
                fragments.push('<tr data-id="' + row + '" style="color:' + NxtControl.Drawing.Color.fromName('AlarmControlForeCellColor').toLive() + ';background-color:' + NxtControl.Drawing.Color.fromName('AlarmControlLightBackCellColor').toLive() + '">');
                for (var i = 0; i < this.alarmColumns.length; i++) {
                    var ci = this.alarmColumns[i];
                    if (ci.Visible === false)
                        continue;
                    switch (ci.Column) {
                        case 'Shortcut':
                            {
                                var color;
                                var aicolor = NxtControl.Drawing.Color.fromString(alarmInfo.Color);
                                if (aicolor instanceof NxtControl.Drawing.BlinkColor) {
                                    if (oldColor !== null && aicolor.name !== oldColor)
                                        this._detachColor(oldColor);
                                    this.alarmInfos[row].color = aicolor;
                                    if (this.alarmColors.hasOwnProperty(aicolor.name) === false)
                                        this.alarmColors[aicolor.name] = { color: aicolor, count: 0 };
                                    if (this.alarmColors[aicolor.name].count === 0)
                                        ColorService.instance.attach(aicolor);
                                    this.alarmColors[aicolor.name].count++;
                                    fragments.push('<td data-clr="' + aicolor.name + '" style="background-color:' + aicolor.toLive() + '">' + alarmInfo.Shortcut + '</td>');
                                }
                                else {
                                    this.alarmInfos[row].color = null;
                                    if (oldColor !== null)
                                        this._detachColor(oldColor);
                                    if (aicolor.name === 'Transparent')
                                        color = NxtControl.Drawing.Color.fromName('AlarmControlLightBackCellColor').toLive();
                                    else
                                        color = aicolor.toLive();
                                    fragments.push('<td data-key="SC" style="background-color:' + color + '">' + alarmInfo.Shortcut + '</td>');
                                }
                            }
                            break;
                        case 'Origin':
                            fragments.push('<td>' + alarmInfo.Origin + '</td>');
                            break;
                        case 'Alias':
                            fragments.push('<td>' + alarmInfo.Alias + '</td>');
                            break;
                        case 'Path':
                            fragments.push('<td>' + alarmInfo.Path + '</td>');
                            break;
                        case 'Value':
                            fragments.push('<td>' + alarmInfo.Value + '</td>');
                            break;
                        case 'InfoValue':
                            fragments.push('<td>' + alarmInfo.InfoValue + '</td>');
                            break;
                        case 'State':
                            fragments.push('<td>' + alarmInfo.State + '</td>');
                            break;
                        case 'CameAckTime':
                            fragments.push('<td>' + (alarmInfo.CameAckTime === 0 ? '' : new Date(alarmInfo.CameAckTime).toLocaleString('de')) + '</td>');
                            break;
                        case 'CameAckUser':
                            fragments.push('<td>' + alarmInfo.CameAckUser + '</td>');
                            break;
                        case 'GoneAckTime':
                            fragments.push('<td>' + (alarmInfo.GoneAckTime === 0 ? '' : new Date(alarmInfo.GoneAckTime).toLocaleString('de')) + '</td>');
                            break;
                        case 'GoneAckUser':
                            fragments.push('<td>' + alarmInfo.GoneAckUser + '</td>');
                            break;
                        case 'CameTime':
                            fragments.push('<td>' + (alarmInfo.CameTime === 0 ? '' : new Date(alarmInfo.CameTime).toLocaleString('de')) + '</td>');
                            break;
                        case 'GoneTime':
                            fragments.push('<td>' + (alarmInfo.GoneTime === 0 ? '' : new Date(alarmInfo.GoneTime).toLocaleString('de')) + '</td>');
                            break;
                        case 'Time':
                            {
                                var t = alarmInfo.CameTime;
                                if (t === 0) {
                                    t = alarmInfo.GoneTime;
                                }
                                if (t === 0) {
                                    t = alarmInfo.CameAckTime;
                                }
                                if (t === 0) {
                                    t = alarmInfo.GoneAckTime;
                                }
                                fragments.push('<td data-t="' + t + '">' + (t === 0 ? '' : new Date(t).toLocaleString('de')) + '</td>');
                            }
                            break;
                        case 'Interval':
                            fragments.push('<td>' + alarmInfo.Interval + '</td>');
                            break;
                        case 'Present':
                            fragments.push('<td>' + alarmInfo.Present + '</td>');
                            break;
                        case 'Text':
                            fragments.push('<td>' + alarmInfo.Text + '</td>');
                            break;
                        case 'Ack':
                            fragments.push('<td class="ACK">' + alarmInfo.Ack + '</td>');
                            break;
                    }
                }
                fragments.push('</tr>');
                return fragments;
            };
            AlarmControl.prototype._startColorChange = function () {
            };
            AlarmControl.prototype._stopColorChange = function (sender, eventArgs) {
                if (eventArgs.value == false)
                    return;
                for (var color in this.alarmColors) {
                    if (this.alarmColors.hasOwnProperty(color)) {
                        var clr = this.alarmColors[color].color.toLive();
                        $(this.alarmControl).find('[data-clr="' + color + '"]').css('background-color', clr);
                    }
                }
            };
            AlarmControl.prototype.alarmUpdateColor = function (index, alarmInfo) {
            };
            AlarmControl.prototype.alarmUpdateClear = function () {
                $(this.alarmControl).find('#alarm tbody').replaceWith('<tbody></tbody>');
                this.table.trigger("update");
            };
            AlarmControl.prototype.toObject = function (propertiesToInclude) {
                var object = extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    alarmType: this.get('alarmType'),
                    alarmColumns: this.get('alarmColumns'),
                    pagerSize: this.get('pagerSize')
                });
                if (!this.includeDefaultValues) {
                    this._removeDefaultValues(object);
                }
                return object;
            };
            AlarmControl.fromObject = function (object) {
                return new AlarmControl().load(object);
            };
            ;
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.AlarmControl')
            ], AlarmControl.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.AlarmControl')
            ], AlarmControl.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.AlarmControlDesigner')
            ], AlarmControl.prototype, "designerType", void 0);
            __decorate([
                System.DefaultValue('Online'),
                System.Browsable
            ], AlarmControl.prototype, "alarmType", void 0);
            __decorate([
                System.DefaultValue(alarmColumnTypes),
                System.Browsable
            ], AlarmControl.prototype, "alarmColumns", void 0);
            __decorate([
                System.DefaultValue(10),
                System.Browsable
            ], AlarmControl.prototype, "pagerSize", void 0);
            return AlarmControl;
        }(GuiFramework.Shape));
        GuiFramework.AlarmControl = AlarmControl;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var DialogCanvas = (function (_super) {
            __extends(DialogCanvas, _super);
            function DialogCanvas() {
                _super.apply(this, arguments);
            }
            return DialogCanvas;
        }(GuiFramework.CanvasCore));
        DialogCanvas.prototype.json = '{"objects":[{"type":"NxtControl.GuiFramework.Rectangle","name":"fpRectBackground","left":2,"top":2,"width":594,"height":594,"pen":{"color":[0,0,0,1],"width":5},"anchor":15,"radius":16},{"type":"NxtControl.GuiFramework.Rectangle","name":"fpRectHeadBackground","left":4,"top":3,"width":592,"height":24,"brush":{"color":"FpStyleRoundHeaderBackground"},"pen":{"color":[0,0,0,1]},"anchor":12,"radius":16},{"type":"NxtControl.GuiFramework.Rectangle","name":"fpRectHeadBackground2","left":4,"top":17,"width":592,"height":10,"brush":{"color":"FpStyleRoundHeaderBackground"},"pen":{"color":[0,0,0,1]},"anchor":12},{"type":"NxtControl.GuiFramework.Line","name":"fpLineHead","left":4,"top":27,"width":592,"height":1,"pen":{"color":"FpStyleRoundHeaderLine"},"anchor":12,"start":{"x":4,"y":27},"end":{"x":596,"y":27}},{"type":"NxtControl.GuiFramework.Rectangle","name":"fpRectHeadShadow","left":8,"top":4,"width":586,"height":15,"pen":{"color":[0,0,0,1]},"anchor":12},{"type":"NxtControl.GuiFramework.Rectangle","name":"fpRectBorderLine","left":2,"top":2,"width":596,"height":596,"brush":{"color":"Transparent"},"pen":{"color":"FpStyleRoundWindowBorder","width":2},"anchor":15,"radius":16},{"type":"NxtControl.GuiFramework.Label","name":"fpLblInstanceName","left":10,"top":6,"width":570,"height":18,"brush":{"color":"Transparent"},"pen":{"color":"Transparent"},"anchor":12,"textColor":"FpStyleRoundTitle","textPadding":2},{"type":"NxtControl.GuiFramework.Rectangle","name":"fpRectMove","left":10,"top":6,"width":560,"height":17,"brush":{"color":"Transparent"},"pen":{"color":"Transparent"},"anchor":12},{"type":"NxtControl.GuiFramework.Line","name":"line1","left":578,"top":10,"width":8,"height":8,"pen":{"color":"FpStyleRoundLineClose","width":3},"anchor":8,"start":{"x":578,"y":10},"end":{"x":586,"y":18}},{"type":"NxtControl.GuiFramework.Line","name":"line2","left":578,"top":10,"width":8,"height":8,"pen":{"color":"FpStyleRoundLineClose","width":3},"anchor":8,"start":{"x":586,"y":10},"end":{"x":578,"y":18}},{"type":"NxtControl.GuiFramework.Rectangle","name":"rectCloseDialog","left":576,"top":7,"width":14,"height":14,"brush":{"color":"Transparent"},"pen":{"color":"Transparent"},"anchor":8}],"width":600,"height":600}';
        var Dialog = (function () {
            function Dialog(faceplateName, faceplateClass, symbolPath) {
                this.type = 'NxtControl.GuiFramework.Dialog';
                this.__isMouseDown = false;
                this.onclose = new NxtControl.event();
                this.faceplateName = faceplateName;
                var klass = NxtControl.Util.Parser.getKlass(faceplateClass, null);
                if (klass) {
                    var dialogCanvas = NxtControl.Util.makeElement('canvas', { 'class': faceplateClass });
                    var parentDivEl = NxtControl.Util.getById('canvasParent').firstChild;
                    parentDivEl.appendChild(dialogCanvas);
                    this.canvas = new DialogCanvas(dialogCanvas);
                    this.divContainer = this.canvas.getLowerCanvasElement().parentNode;
                    this.divContainer.style.cssText = 'position:absolute;top:40px;left:40px';
                    this.divContainer.style.backgroundColor = 'transparent';
                    this.canvas.calcOffset();
                    var faceplateCanvas = NxtControl.Util.makeElement('canvas', { 'class': 'faceplate' });
                    this.divContainer.appendChild(faceplateCanvas);
                    this.faceplateInstance = klass.fromObject({ element: faceplateCanvas, dialog: this });
                    this.faceplateInstance.initEvents();
                    this.faceplateInstance.getLowerCanvasElement().parentNode.style.cssText = 'position:absolute;left:5px;top:28px';
                    this.canvas.find('fpLblInstanceName').set('text', this.faceplateInstance.title || faceplateName);
                    this.canvas.setDimensions({ width: this.faceplateInstance.getWidth() + 12, height: this.faceplateInstance.getHeight() + 36 });
                    this.faceplateInstance.calcOffset();
                    this.faceplateInstance.renderAll();
                    this.canvas.renderAll();
                    this.faceplateInstance.initializeCommunication(symbolPath);
                    this.__isMouseDown = false;
                    this._fpRectMove_mouseDown = this._fpRectMove_mouseDown.bind(this);
                    this._rectCloseDialog_mouseDown = this._rectCloseDialog_mouseDown.bind(this);
                    this.canvas.find('fpRectMove').mousedown.add(this._fpRectMove_mouseDown);
                    this.canvas.find('rectCloseDialog').mousedown.add(this._rectCloseDialog_mouseDown);
                    this.canvas.mousemove.add(this._onMouseMove.bind(this));
                    this.canvas.mouseup.add(this._onMouseUp.bind(this));
                }
            }
            Dialog.prototype.close = function () {
                this.onclose.trigger(this, { faceplate: this.faceplateName });
                this.faceplateInstance.dispose();
                this.canvas.dispose();
                this.divContainer.parentNode.removeChild(this.divContainer);
            };
            Dialog.prototype.bringToFront = function () {
                this.divContainer.parentNode.appendChild(this.divContainer);
            };
            Dialog.prototype._rectCloseDialog_mouseDown = function () {
                this.close();
            };
            Dialog.prototype._fpRectMove_mouseDown = function (sender, e) {
                this._onMouseDownPosition = {
                    style: { left: this.canvas.getLowerCanvasElement().parentNode.offsetLeft, top: this.canvas.getLowerCanvasElement().parentNode.offsetTop },
                    pointer: { x: e.e.pageX, y: e.e.pageY }
                };
                this.__isMouseDown = true;
                this.bringToFront();
            };
            Dialog.prototype._onMouseMove = function (sender, e) {
                if (this.__isMouseDown === false)
                    return;
                var pointer = NxtControl.Util.getPointer(e.e, this.canvas.getUpperCanvasElement());
                this.divContainer.style.left = (this._onMouseDownPosition.style.left + (pointer.x - this._onMouseDownPosition.pointer.x)) + 'px';
                this.divContainer.style.top = (this._onMouseDownPosition.style.top + (pointer.y - this._onMouseDownPosition.pointer.y)) + 'px';
                this.canvas.calcOffset();
                this.faceplateInstance.calcOffset();
                e.e.preventDefault && e.e.preventDefault();
            };
            Dialog.prototype._onMouseUp = function (sender, e) {
                if (this.__isMouseDown === false)
                    return;
                this.__isMouseDown = false;
                var pointer = NxtControl.Util.getPointer(e.e, this.canvas.getUpperCanvasElement());
                this.divContainer.style.left = (this._onMouseDownPosition.style.left + (pointer.x - this._onMouseDownPosition.pointer.x)) + 'px';
                this.divContainer.style.top = (this._onMouseDownPosition.style.top + (pointer.y - this._onMouseDownPosition.pointer.y)) + 'px';
                this.canvas.calcOffset();
                this.faceplateInstance.calcOffset();
                e.e.preventDefault && e.e.preventDefault();
            };
            return Dialog;
        }());
        GuiFramework.Dialog = Dialog;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend;
        var statePropertiesFaceplate = [];
        statePropertiesFaceplate.push('tagName', 'width', 'height', 'title');
        var Faceplate = (function (_super) {
            __extends(Faceplate, _super);
            function Faceplate() {
                _super.apply(this, arguments);
                this.tagName = null;
                this._valueChangedTimer = null;
            }
            Faceplate.prototype.initialize = function (options) {
                options = options || {};
                var element = options.element || null;
                delete options.element;
                this.dialog = options.dialog;
                delete options.dialogElement;
                _super.prototype.initialize.call(this, element, options);
                this._deviceConnected = this._deviceConnected.bind(this);
                this._valueChanged = this._valueChanged.bind(this);
                this._addHistoryValues = this._addHistoryValues.bind(this);
                this._archiveAnswer = this._archiveAnswer.bind(this);
                this._onObjectMouseDown = this._onObjectMouseDown.bind(this);
                this._onObjectMouseUp = this._onObjectMouseUp.bind(this);
                this._onObjectClick = this._onObjectClick.bind(this);
                this._onCloseDialog = this._onCloseDialog.bind(this);
                this._valueChangedTick = this._valueChangedTick.bind(this);
            };
            Faceplate.prototype.dispose = function () {
                this.disposeEvents();
                var id, callback, commService = NxtControl.Services.CommunicationService.instance;
                for (id in this.connectorInputs) {
                    if (!this.connectorInputs.hasOwnProperty(id))
                        continue;
                    callback = this.connectorInputs[id];
                    commService.unsubscribe(true, callback.deviceName, callback.rpath, callback.id);
                }
                for (id in this.connectorOutputs) {
                    if (!this.connectorOutputs.hasOwnProperty(id))
                        continue;
                    callback = this.connectorOutputs[id];
                    commService.unsubscribe(false, callback.deviceName, callback.rpath, callback.id);
                }
                if (this._valueChangedTimer !== null) {
                    clearInterval(this._valueChangedTimer);
                    this._valueChangedTimer = null;
                }
                this.connectorInputs = {};
                this.connectorOutputs = {};
                commService.connection.remove(this._deviceConnected);
                commService.valueChanged.remove(this._valueChanged);
                commService.history.remove(this._addHistoryValues);
                commService.archiveAnswer.remove(this._archiveAnswer);
                if (this.id)
                    NxtControl.Services.CanvasTopologyService.instance.unregisterFaceplate(this.id);
                _super.prototype.dispose.call(this);
            };
            Faceplate.prototype.initEvents = function () {
                this.mousedown.add(this._onObjectMouseDown);
                this.mouseup.add(this._onObjectMouseUp);
                this.click.add(this._onObjectClick);
                this.forEachObject(function (o) {
                    if (typeof (o.initEvents) === 'function')
                        o.initEvents();
                });
            };
            Faceplate.prototype.disposeEvents = function () {
                this.mousedown.remove(this._onObjectMouseDown);
                this.mouseup.remove(this._onObjectMouseUp);
                this.click.remove(this._onObjectClick);
            };
            Faceplate.prototype.initializeCommunication = function (symbolPath) {
                if (NxtControl.Services.CommunicationService.instance) {
                    this.connectorInputs = {};
                    this.connectorOutputs = {};
                }
                this.symbolPath = symbolPath || null;
                this.id = NxtControl.Services.CanvasTopologyService.instance.registerFaceplate(this);
                NxtControl.Services.CommunicationService.instance.getFaceplateSubscribeData(this);
            };
            Faceplate.prototype._doOpenFaceplates = function (target, options, isDown) {
                var isLeftClick = 'which' in options.e ? options.e.which === 1 : options.e.button === 1;
                var isRightClick = 'which' in options.e ? options.e.which === 3 : options.e.button === 2;
                if (target.openFaceplates !== null && target.openFaceplates.length > 0) {
                    for (var i = 0; i < target.openFaceplates.length; i++) {
                        if (target.openFaceplates[i].fp && target.openFaceplates[i].fp.length > 0) {
                            if ((isDown && (isLeftClick && ((target.openFaceplates[i].mt & GuiFramework.MouseButtonType.MouseDownLeft) === GuiFramework.MouseButtonType.MouseDownLeft)) ||
                                (isRightClick && ((target.openFaceplates[i].mt & GuiFramework.MouseButtonType.MouseDownRight) === GuiFramework.MouseButtonType.MouseDownRight))) ||
                                (!isDown && (isLeftClick && ((target.openFaceplates[i].mt & GuiFramework.MouseButtonType.MouseUpLeft) === GuiFramework.MouseButtonType.MouseUpLeft ||
                                    (target.openFaceplates[i].mt & GuiFramework.MouseButtonType.Click) === GuiFramework.MouseButtonType.Click)) ||
                                    (isRightClick && ((target.openFaceplates[i].mt & GuiFramework.MouseButtonType.MouseUpRight) === GuiFramework.MouseButtonType.MouseUpRight)))) {
                                if (this.hasOwnProperty('_faceplates') === false)
                                    this._faceplates = {};
                                if (this._faceplates.hasOwnProperty(target.openFaceplates[i].fp)) {
                                    this._faceplates[target.openFaceplates[i].fp].bringToFront();
                                }
                                else {
                                    var ts = this.type.split('.');
                                    ts[2] = 'Faceplates';
                                    ts[ts.length - 1] = target.openFaceplates[i].fp;
                                    var dialog = new GuiFramework.Dialog(target.openFaceplates[i].fp, ts.join('.'), this.getSymbolPath());
                                    dialog.onclose.add(this._onCloseDialog);
                                    this._faceplates[target.openFaceplates[i].fp] = dialog;
                                }
                            }
                        }
                    }
                }
            };
            Faceplate.prototype._onCloseDialog = function (sender, options) {
                if (this._faceplates.hasOwnProperty(options.faceplate))
                    delete this._faceplates[options.faceplate];
            };
            Faceplate.prototype._onObjectMouseDown = function (sender, options) {
                if (options.hasOwnProperty('target') && typeof (options.target) !== 'undefined' && typeof (options.target._doOpenFaceplates) !== 'function')
                    this._doOpenFaceplates(options.target, options, true);
            };
            Faceplate.prototype._onObjectMouseUp = function (sender, options) {
                if (options.hasOwnProperty('target') && typeof (options.target) !== 'undefined' && typeof (options.target._doOpenFaceplates) !== 'function')
                    this._doOpenFaceplates(options.target, options, false);
            };
            Faceplate.prototype._onObjectClick = function (sender, options) {
                if (options.hasOwnProperty('target') && typeof (options.target) !== 'undefined' && typeof (options.target._doOpenFaceplates) !== 'function')
                    this._doOpenFaceplates(options.target, options, false);
            };
            Faceplate.prototype.setFaceplateSubscribeData = function (subscribeData) {
                var commService = NxtControl.Services.CommunicationService.instance;
                var _this = this;
                var path, pathData, deviceName, deviceData;
                commService.connection.add(this._deviceConnected);
                commService.valueChanged.add(this._valueChanged);
                commService.history.add(this._addHistoryValues);
                commService.archiveAnswer.add(this._archiveAnswer);
                this.db = subscribeData;
                if (!this.db)
                    return;
                var inputs = this.db.input;
                for (deviceName in inputs) {
                    if (!inputs.hasOwnProperty(deviceName))
                        continue;
                    if (commService.isDeviceConnected(deviceName)) {
                        deviceData = inputs[deviceName];
                        for (path in deviceData) {
                            if (!deviceData.hasOwnProperty(path))
                                continue;
                            pathData = deviceData[path];
                            pathData.forEach(function (entry) {
                                for (var i = 0, keys = Object.keys(entry.bases), l = keys.length; i < l; ++i) {
                                    var baseSymPath = keys[i];
                                    var dataId = entry.bases[baseSymPath];
                                    var id = commService.subscribe(true, deviceName, path, entry.eventNr, entry.names, entry.types, dataId);
                                    var callback = {};
                                    callback.id = id;
                                    callback.path = baseSymPath;
                                    callback.deviceName = deviceName;
                                    callback.rpath = path;
                                    _this.connectorInputs[id] = callback;
                                }
                            });
                        }
                    }
                }
                var outputs = this.db.output;
                for (deviceName in outputs) {
                    if (commService.isDeviceConnected(deviceName)) {
                        deviceData = outputs[deviceName];
                        for (path in deviceData) {
                            if (!deviceData.hasOwnProperty(path))
                                continue;
                            pathData = deviceData[path];
                            pathData.forEach(function (entry) {
                                for (var i = 0, keys = Object.keys(entry.bases), l = keys.length; i < l; ++i) {
                                    var baseSymPath = keys[i];
                                    var dataId = entry.bases[baseSymPath];
                                    var id = commService.subscribe(false, deviceName, path, entry.eventNr, entry.names, entry.types, dataId);
                                    var callback = {};
                                    callback.id = id;
                                    callback.path = baseSymPath;
                                    callback.deviceName = deviceName;
                                    callback.rpath = path;
                                    var high = (dataId >> 16) << 16;
                                    if (high !== 0)
                                        callback.dataId = dataId - high;
                                    else
                                        callback.dataId = dataId;
                                    var shape = _this.find(callback.path);
                                    if (shape) {
                                        callback.shape = shape;
                                        shape.callbackdata = callback;
                                    }
                                    _this.connectorOutputs[id] = callback;
                                }
                            });
                        }
                    }
                }
                this._valueChangedTimer = setInterval(this._valueChangedTick, 50);
            };
            Faceplate.prototype._deviceConnected = function (sender, options) {
                var _this = this;
                var commService = NxtControl.Services.CommunicationService.instance;
                var deviceData, deviceName, path, pathData;
                if (options.state === true) {
                    if (!this.db)
                        return;
                    var inputs = this.db.input;
                    for (deviceName in inputs) {
                        if (!inputs.hasOwnProperty(deviceName))
                            continue;
                        if (deviceName === options.target) {
                            deviceData = inputs[deviceName];
                            for (path in deviceData) {
                                if (!deviceData.hasOwnProperty(path))
                                    continue;
                                pathData = deviceData[path];
                                pathData.forEach(function (entry) {
                                    for (var i = 0, keys = Object.keys(entry.bases), l = keys.length; i < l; ++i) {
                                        var baseSymPath = keys[i];
                                        var dataId = entry.bases[baseSymPath];
                                        var id = commService.subscribe(true, deviceName, path, entry.eventNr, entry.names, entry.types, dataId);
                                        var callback = {};
                                        callback.id = id;
                                        callback.path = baseSymPath;
                                        callback.deviceName = deviceName;
                                        callback.rpath = path;
                                        _this.connectorInputs[id] = callback;
                                    }
                                });
                            }
                        }
                    }
                    var outputs = this.db.output;
                    for (deviceName in outputs) {
                        if (!outputs.hasOwnProperty(deviceName))
                            continue;
                        if (deviceName === options.target) {
                            deviceData = outputs[deviceName];
                            for (path in deviceData) {
                                if (!deviceData.hasOwnProperty(path))
                                    continue;
                                pathData = deviceData[path];
                                pathData.forEach(function (entry) {
                                    for (var i = 0, keys = Object.keys(entry.bases), l = keys.length; i < l; ++i) {
                                        var baseSymPath = keys[i];
                                        var dataId = entry.bases[baseSymPath];
                                        var id = commService.subscribe(false, deviceName, path, entry.eventNr, entry.names, entry.types, dataId);
                                        var callback = {};
                                        callback.id = id;
                                        callback.path = baseSymPath;
                                        callback.deviceName = deviceName;
                                        callback.rpath = path;
                                        var high = (dataId >> 16) << 16;
                                        if (high !== 0)
                                            callback.dataId = dataId - high;
                                        else
                                            callback.dataId = dataId;
                                        var shape = _this.find(callback.path);
                                        if (shape) {
                                            callback.shape = shape;
                                            shape.callbackdata = callback;
                                        }
                                        _this.connectorOutputs[id] = callback;
                                    }
                                });
                            }
                        }
                    }
                }
            };
            Faceplate.prototype._valueChangedTick = function () {
                if (this._callbackCacheHasNewValues) {
                    this.ignoreRender = true;
                    for (var key in this._callbackCache) {
                        if (!this._callbackCache.hasOwnProperty(key))
                            continue;
                        var options = this._callbackCache[key];
                        var callback = options.callback;
                        if (callback && callback !== null && callback.shape) {
                            callback.shape.runtimeValueChanged && callback.shape.runtimeValueChanged(options.value, options.time);
                        }
                        else if (callback && callback !== null) {
                            var shape = this.find(callback.path);
                            if (shape) {
                                callback.shape = shape;
                                shape.runtimeValueChanged && shape.runtimeValueChanged(options.value, options.time);
                            }
                        }
                    }
                    this._callbackCache = {};
                    this._callbackCacheHasNewValues = false;
                    this.ignoreRender = false;
                    this.renderAll();
                }
            };
            Faceplate.prototype._valueChanged = function (sender, options) {
                var callback = options.isInput ? (this.connectorInputs.hasOwnProperty(options.id) ? this.connectorInputs[options.id] : null) : (this.connectorOutputs.hasOwnProperty(options.id) ? this.connectorOutputs[options.id] : null);
                if (callback && callback !== null) {
                    this._callbackCacheHasNewValues = true;
                    this._callbackCache[options.id] = { callback: callback, value: options.value, time: options.time };
                }
            };
            Faceplate.prototype._addHistoryValues = function (sender, options) {
                var callback = options.isInput ? (this.connectorInputs.hasOwnProperty(options.id) ? this.connectorInputs[options.id] : null) : (this.connectorOutputs.hasOwnProperty(options.id) ? this.connectorOutputs[options.id] : null);
                if (callback && callback !== null && callback.shape) {
                    callback.shape.callback = callback;
                    if (callback.shape.addHistoryValues(options))
                        this.renderAll();
                }
                else if (callback && callback !== null) {
                    var shape = this.find(callback.path);
                    if (shape) {
                        callback.shape = shape;
                        shape.callback = callback;
                        if (shape.addHistoryValues(options))
                            this.renderAll();
                    }
                }
            };
            Faceplate.prototype._archiveAnswer = function (sender, options) {
                var callback = options.isInput ? (this.connectorInputs.hasOwnProperty(options.id) ? this.connectorInputs[options.id] : null) : (this.connectorOutputs.hasOwnProperty(options.id) ? this.connectorOutputs[options.id] : null);
                if (callback && callback !== null && callback.shape) {
                    if (callback.shape.archiveAnswer(options))
                        this.renderAll();
                }
                else if (callback && callback !== null) {
                    var shape = this.find(callback.path);
                    if (shape) {
                        callback.shape = shape;
                        if (shape.archiveAnswer(options))
                            this.renderAll();
                    }
                }
            };
            Faceplate.prototype.close = function () {
                if (this.dialog)
                    this.dialog.close();
            };
            Faceplate.prototype.getSymbolPath = function (symbolPath) {
                return { faceplate: this.type, symbolPath: symbolPath, parent: this.symbolPath };
            };
            Faceplate.prototype.toObject = function (propertiesToInclude) {
                return extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    title: this.title
                });
            };
            Faceplate.fromObject = function () {
                return new Faceplate();
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.Faceplate')
            ], Faceplate.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('NxtControl.GuiHTMLFramework.Faceplate')
            ], Faceplate.prototype, "csClass", void 0);
            __decorate([
                System.DefaultValue(''),
                System.Browsable
            ], Faceplate.prototype, "title", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Faceplate.prototype, "connectorInputs", void 0);
            __decorate([
                System.DefaultValue(null)
            ], Faceplate.prototype, "connectorOutputs", void 0);
            return Faceplate;
        }(GuiFramework.CanvasCore));
        GuiFramework.Faceplate = Faceplate;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var CanvasTS = NxtControl.Services.CanvasTopologyService.instance;
        var CTRoseRectangle = (function (_super) {
            __extends(CTRoseRectangle, _super);
            function CTRoseRectangle() {
                _super.apply(this, arguments);
            }
            CTRoseRectangle.prototype.setTooltip = function (tooltip) {
            };
            CTRoseRectangle.prototype._render = function (ctx) {
                _super.prototype._render.call(this, ctx);
                var isInPathGroup = this.group && this.group.getType() === 'path-group';
                if (this.transformMatrix && isInPathGroup) {
                    ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                }
                if (!this.transformMatrix && isInPathGroup) {
                    ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                }
                if (this.name === 'siblingLeft') {
                    ctx.beginPath();
                    ctx.moveTo(0, -7);
                    ctx.lineTo(-5, 0);
                    ctx.lineTo(0, 8);
                    ctx.lineTo(5, 8);
                    ctx.lineTo(0, 0);
                    ctx.lineTo(5, -7);
                    ctx.lineTo(0, -7);
                    ctx.closePath();
                    this._renderFill(ctx, NxtControl.Drawing.Brush.White);
                }
                else if (this.name === 'siblingRight') {
                    ctx.beginPath();
                    ctx.moveTo(0, -7);
                    ctx.lineTo(5, 0);
                    ctx.lineTo(0, 8);
                    ctx.lineTo(-5, 8);
                    ctx.lineTo(0, 0);
                    ctx.lineTo(-5, -7);
                    ctx.lineTo(0, -7);
                    ctx.closePath();
                    this._renderFill(ctx, NxtControl.Drawing.Brush.White);
                }
                else if (this.name === 'levelUp') {
                    ctx.beginPath();
                    ctx.moveTo(-7, 0);
                    ctx.lineTo(0, -5);
                    ctx.lineTo(7, 0);
                    ctx.lineTo(7, 5);
                    ctx.lineTo(0, 0);
                    ctx.lineTo(-7, 5);
                    ctx.lineTo(-7, 0);
                    ctx.closePath();
                    this._renderFill(ctx, NxtControl.Drawing.Brush.White);
                }
                else if (this.name === 'levelDown') {
                    ctx.beginPath();
                    ctx.moveTo(-7, 0);
                    ctx.lineTo(0, 5);
                    ctx.lineTo(7, 0);
                    ctx.lineTo(7, -5);
                    ctx.lineTo(0, 0);
                    ctx.lineTo(-7, -5);
                    ctx.lineTo(-7, 0);
                    ctx.closePath();
                    this._renderFill(ctx, NxtControl.Drawing.Brush.White);
                }
                else if (this.name === 'home') {
                    ctx.beginPath();
                    ctx.moveTo(-7, -1);
                    ctx.lineTo(0, -7);
                    ctx.lineTo(7, -1);
                    ctx.lineTo(7, 9);
                    ctx.lineTo(3, 9);
                    ctx.lineTo(3, 2);
                    ctx.lineTo(-3, 2);
                    ctx.lineTo(-3, 9);
                    ctx.lineTo(-7, 9);
                    ctx.lineTo(-7, -1);
                    ctx.closePath();
                    this._renderFill(ctx, NxtControl.Drawing.Brush.White);
                    ctx.beginPath();
                    ctx.moveTo(-9, -1);
                    ctx.lineTo(0, -9);
                    ctx.lineTo(9, -1);
                    ctx.lineTo(11, -1);
                    ctx.lineTo(0, -11);
                    ctx.lineTo(-11, -1);
                    ctx.lineTo(-9, -1);
                    ctx.closePath();
                    this._renderFill(ctx, NxtControl.Drawing.Brush.White);
                }
            };
            CTRoseRectangle.fromObject = function (object) {
                return new CTRoseRectangle().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.CTRoseRectangle')
            ], CTRoseRectangle.prototype, "type", void 0);
            return CTRoseRectangle;
        }(GuiFramework.Rectangle));
        GuiFramework.CTRoseRectangle = CTRoseRectangle;
        var CanvasTopologyRose = (function (_super) {
            __extends(CanvasTopologyRose, _super);
            function CanvasTopologyRose() {
                _super.apply(this, arguments);
            }
            CanvasTopologyRose.prototype.load = function (options) {
                options = options || {};
                _super.prototype.load.call(this, options);
                this.siblingLeft = this.find('siblingLeft');
                this.siblingRight = this.find('siblingRight');
                this.levelUp = this.find('levelUp');
                this.levelDown = this.find('levelDown');
                this.home = this.find('home');
                this._levelUpClick = this._levelUpClick.bind(this);
                this._levelDownClick = this._levelDownClick.bind(this);
                this._siblingLeftClick = this._siblingLeftClick.bind(this);
                this._siblingRightClick = this._siblingRightClick.bind(this);
                this._homeClick = this._homeClick.bind(this);
                this._canvasChanged = this._canvasChanged.bind(this);
                return this;
            };
            CanvasTopologyRose.prototype.initEvents = function () {
                _super.prototype.initEvents.call(this);
                this.siblingLeft.mouseup.add(this._siblingLeftClick);
                this.siblingRight.mouseup.add(this._siblingRightClick);
                this.levelUp.mouseup.add(this._levelUpClick);
                this.levelDown.mouseup.add(this._levelDownClick);
                this.home.mouseup.add(this._homeClick);
                CanvasTS.canvasChanged.add(this._canvasChanged);
            };
            CanvasTopologyRose.prototype.disposeEvents = function () {
                _super.prototype.disposeEvents.call(this);
                CanvasTS.canvasChanged.remove(this._canvasChanged);
            };
            CanvasTopologyRose.prototype._canvasChanged = function () {
                var currentCanvas = CanvasTS.getCurrentCanvas();
                if (currentCanvas !== null) {
                    this.siblingLeft.setTooltip(CanvasTS.getLeftTooltip());
                    this.siblingRight.setTooltip(CanvasTS.getRightTooltip());
                    this.levelDown.setTooltip(CanvasTS.getDownTooltip());
                    this.levelUp.setTooltip(CanvasTS.getUpTooltip());
                }
            };
            CanvasTopologyRose.prototype._homeClick = function () {
                if (CanvasTS.firstCanvasId !== -1)
                    CanvasTS.openCanvas(CanvasTS.firstCanvasId);
            };
            CanvasTopologyRose.prototype._levelUpClick = function () {
                CanvasTS.openParentCanvas();
            };
            CanvasTopologyRose.prototype._siblingLeftClick = function () {
                CanvasTS.openLeftCanvas();
            };
            CanvasTopologyRose.prototype._siblingRightClick = function () {
                CanvasTS.openRightCanvas();
            };
            CanvasTopologyRose.prototype._levelDownClick = function () {
                CanvasTS.openChildCanvas(0);
            };
            CanvasTopologyRose.fromObject = function (object) {
                return new CanvasTopologyRose().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.CanvasTopologyRose')
            ], CanvasTopologyRose.prototype, "type", void 0);
            return CanvasTopologyRose;
        }(GuiFramework.Symbol));
        GuiFramework.CanvasTopologyRose = CanvasTopologyRose;
        CanvasTopologyRose.prototype.json = '{\
  "objects": [\
    {"type": "NxtControl.GuiFramework.CTRoseRectangle","left": 10,"top": 31,"width": 25,"height": 30,"brush": {"name":"CanvasTopologyButtonBrush"},"name": "siblingLeft"},\
    {"type": "NxtControl.GuiFramework.CTRoseRectangle","left": 69,"top": 31,"width": 25,"height": 30,"brush": {"name":"CanvasTopologyButtonBrush"},"name": "siblingRight"},\
    {"type": "NxtControl.GuiFramework.CTRoseRectangle","left": 37,"top": 31,"width": 30,"height": 30,"brush": {"name":"CanvasTopologyButtonBrush"},"name": "home"},\
    {"type": "NxtControl.GuiFramework.CTRoseRectangle","left": 37,"top": 9,"width": 30,"height": 20,"brush": {"name":"CanvasTopologyButtonBrush"},"name": "levelUp"},\
    {"type": "NxtControl.GuiFramework.CTRoseRectangle","left": 37,"top": 63,"width": 30,"height": 20,"brush": {"name":"CanvasTopologyButtonBrush"},"name": "levelDown"}\
  ]}';
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var extend = NxtControl.Util.object.extend, CanvasTS = NxtControl.Services.CanvasTopologyService.instance;
        (function (CanvasTopologyType) {
            CanvasTopologyType[CanvasTopologyType["Top"] = 0] = "Top";
            CanvasTopologyType[CanvasTopologyType["Sibling"] = 1] = "Sibling";
            CanvasTopologyType[CanvasTopologyType["Child"] = 2] = "Child";
        })(GuiFramework.CanvasTopologyType || (GuiFramework.CanvasTopologyType = {}));
        var CanvasTopologyType = GuiFramework.CanvasTopologyType;
        var CTPanelRectangle = (function (_super) {
            __extends(CTPanelRectangle, _super);
            function CTPanelRectangle() {
                _super.apply(this, arguments);
            }
            CTPanelRectangle.prototype.getTextAlign = function () {
                return this.textAlign;
            };
            CTPanelRectangle.prototype.setTextAlign = function (textAlign) {
                this._set('textAlign', textAlign, true);
            };
            CTPanelRectangle.prototype.getTextColor = function () {
                return this.textColor;
            };
            CTPanelRectangle.prototype.setTextColor = function (clr) {
                return this._set('textColor', clr, true);
            };
            CTPanelRectangle.prototype.getFontWeight = function () {
                return this.fontWeight;
            };
            CTPanelRectangle.prototype.setFontWeight = function (fontWeight) {
                return this._set('fontWeight', fontWeight, true);
            };
            CTPanelRectangle.prototype.getFontSize = function () {
                return this.fontSize;
            };
            CTPanelRectangle.prototype.setFontSize = function (fontSize) {
                return this._set('fontSize', fontSize, true);
            };
            CTPanelRectangle.prototype.getFontStyle = function () {
                return this.fontStyle;
            };
            CTPanelRectangle.prototype.setFontStyle = function (fontStyle) {
                return this._set('fontStyle', fontStyle, true);
            };
            CTPanelRectangle.prototype._render = function (ctx) {
                _super.prototype._render.call(this, ctx);
                var x = -this.width / 2, y = -this.height / 2, isInPathGroup = this.group && this.group.getType() === 'path-group';
                if (this.transformMatrix && isInPathGroup) {
                    ctx.translate(this.width / 2 + this.x, this.height / 2 + this.y);
                }
                if (!this.transformMatrix && isInPathGroup) {
                    ctx.translate(-this.group.getWidth() / 2 + this.width / 2 + this.x, -this.group.getHeight() / 2 + this.height / 2 + this.y);
                }
                if (this.text.length !== 0) {
                    x += this.textPadding;
                    y += this.fontSize + this.textPadding + (this.height - this.fontSize - 2 * this.textPadding) / 2;
                    ctx.save();
                    ctx.beginPath();
                    ctx.rect(-this.width / 2 + this.textPadding, -this.height / 2 + this.textPadding, this.width - 2 * this.textPadding, this.height - 2 * this.textPadding);
                    ctx.clip();
                    this._setTextStyles(ctx, this.textColor);
                    ctx.fillText(this.text, x, y);
                    ctx.restore();
                }
            };
            CTPanelRectangle.prototype._setTextStyles = function (ctx, color) {
                ctx.strokeStyle = ctx.fillStyle = color.toLive();
                ctx.textBaseline = 'bottom';
                ctx.font = this._getFontDeclaration();
            };
            CTPanelRectangle.prototype._getFontDeclaration = function () {
                return [
                    (System.isLikelyNode ? this.fontWeight : this.fontStyle),
                    (System.isLikelyNode ? this.fontStyle : this.fontWeight),
                    this.fontSize + 'px',
                    (System.isLikelyNode ? ('"' + this.fontFamily + '"') : this.fontFamily)
                ].join(' ');
            };
            CTPanelRectangle.fromObject = function (object) {
                return new CTPanelRectangle().load(object);
            };
            ;
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.CTPanelRectangle')
            ], CTPanelRectangle.prototype, "type", void 0);
            __decorate([
                System.DefaultValue('')
            ], CTPanelRectangle.prototype, "text", void 0);
            __decorate([
                System.DefaultValue('left')
            ], CTPanelRectangle.prototype, "textAlign", void 0);
            __decorate([
                System.DefaultValue(4)
            ], CTPanelRectangle.prototype, "textPadding", void 0);
            __decorate([
                System.DefaultValue(NxtControl.Drawing.Color.fromName('CanvasTopologyButtonTextColor'))
            ], CTPanelRectangle.prototype, "textColor", void 0);
            __decorate([
                System.DefaultValue('Arial')
            ], CTPanelRectangle.prototype, "fontFamily", void 0);
            __decorate([
                System.DefaultValue('normal')
            ], CTPanelRectangle.prototype, "fontWeight", void 0);
            __decorate([
                System.DefaultValue(14)
            ], CTPanelRectangle.prototype, "fontSize", void 0);
            __decorate([
                System.DefaultValue('normal')
            ], CTPanelRectangle.prototype, "fontStyle", void 0);
            return CTPanelRectangle;
        }(GuiFramework.Rectangle));
        var CanvasTopologyPanel = (function (_super) {
            __extends(CanvasTopologyPanel, _super);
            function CanvasTopologyPanel() {
                _super.apply(this, arguments);
                this.topologyType = CanvasTopologyType.Sibling;
                this.inCreateButtons = false;
                this.btnCountToShow = -1;
                this.btnColor = NxtControl.Drawing.Color.fromSource([128, 128, 128]);
                this.currentBtnColor = null;
                this.btnWidth = 140;
                this.btnHeight = 30;
                this.startIndex = 0;
                this.currentParentCanvasId = -1;
                this.realBtnCountToShow = 0;
                this.toLeft = null;
                this.toRight = null;
            }
            CanvasTopologyPanel.prototype.load = function (options) {
                this._inInitialize = true;
                options = options || {};
                options.height = options.btnHeight || this.btnHeight;
                options.height += 10;
                _super.prototype.load.call(this, options);
                this._inInitialize = true;
                this._toLeftClick = this._toLeftClick.bind(this);
                this._btnClick = this._btnClick.bind(this);
                this._toRightClick = this._toRightClick.bind(this);
                this._buttons = [];
                this._createButtons();
                this._canvasChanged = this._canvasChanged.bind(this);
                this._inInitialize = false;
                return this;
            };
            CanvasTopologyPanel.prototype.initEvents = function () {
                _super.prototype.initEvents.call(this);
                CanvasTS.canvasChanged.add(this._canvasChanged);
                this.toLeft.mouseup.add(this._toLeftClick);
                for (var i = 0; i < this._buttons.length; i++)
                    this._buttons[i].mouseup.add(this._btnClick);
                this.toRight.mouseup.add(this._toRightClick);
            };
            CanvasTopologyPanel.prototype.disposeEvents = function () {
                _super.prototype.disposeEvents.call(this);
                CanvasTS.canvasChanged.remove(this._canvasChanged);
            };
            CanvasTopologyPanel.prototype._createButtons = function () {
                this.inCreateButtons = true;
                var i, offset = 0, newBtnCountToShow = Math.floor((this.width - 50) / this.btnWidth), left = this.left, top = this.top;
                this.left = 0;
                this.top = 0;
                if (newBtnCountToShow === this.btnCountToShow && this._buttons.length !== 0)
                    return;
                this.btnCountToShow = newBtnCountToShow;
                if (this._objects.length !== 0) {
                    this._objects.length = 0;
                    for (i = 0; i < this._buttons.length; i++) {
                        this._buttons[i].mouseup.remove(this._btnClick);
                    }
                    this._buttons.length = 0;
                }
                this._objects.push(new GuiFramework.Rectangle().load({ name: 'back', left: 0, top: 0, width: this.width, height: this.height, visible: false }));
                i = 0;
                var posX = 5, posY = 5;
                var options = {
                    name: 'siblingLeft',
                    left: posX,
                    top: posY,
                    width: 20,
                    height: this.btnHeight,
                    brush: NxtControl.Drawing.Brush.fromName('CanvasTopologyButtonBrush'),
                    pen: new NxtControl.Drawing.Pen({ color: '#000' }),
                };
                this.toLeft = new GuiFramework.CTRoseRectangle().load(options);
                if (this._inInitialize === false)
                    this.toLeft.mouseup.add(this._toLeftClick);
                this._objects.push(this.toLeft);
                posX = 25;
                for (; i < this.btnCountToShow; i++) {
                    options = {
                        name: 'btn' + i.toString(),
                        left: posX,
                        top: posY,
                        width: this.btnWidth,
                        height: this.btnHeight,
                        brush: NxtControl.Drawing.Brush.fromName('CanvasTopologyButtonBrush'),
                        pen: new NxtControl.Drawing.Pen({ color: '#000' }),
                        radius: 2
                    };
                    var rr = new CTPanelRectangle().load(options);
                    if (this._inInitialize === false)
                        rr.mouseup.add(this._btnClick);
                    posX += this.btnWidth + offset;
                    this._objects.push(rr);
                    this._buttons.push(rr);
                }
                options = {
                    name: 'siblingRight',
                    left: posX,
                    top: posY,
                    width: 20,
                    height: this.btnHeight,
                    brush: NxtControl.Drawing.Brush.fromName('CanvasTopologyButtonBrush'),
                    pen: new NxtControl.Drawing.Pen({ color: '#000' }),
                };
                this.toRight = new GuiFramework.CTRoseRectangle().load(options);
                if (this._inInitialize === false)
                    this.toRight.mouseup.add(this._toRightClick);
                this._objects.push(this.toRight);
                this._calcBounds();
                this._updateObjectsCoords();
                this.left = left;
                this.top = top;
                this._setOpacityIfSame();
                this.setCoords();
                this.saveCoords();
                this._inCreateButtons = false;
            };
            CanvasTopologyPanel.prototype._manage = function (data, currentCanvasId) {
                var i;
                if (data.length < this.btnCountToShow)
                    this.realBtnCountToShow = data.length;
                else
                    this.realBtnCountToShow = this.btnCountToShow;
                for (i = 0; i < this.realBtnCountToShow; i++) {
                    var res = CanvasTS.getCanvasData(data[i + this.startIndex]);
                    this._buttons[i].set('text', res.text);
                    if (res.tooltip.length === 0)
                        this._buttons[i].set('tooltip', res.text);
                    else
                        this._buttons[i].set('tooltip', res.text + '\n' + res.tooltip);
                    if (res.text.length !== 0)
                        this._buttons[i].set('visible', true);
                    else
                        this._buttons[i].set('visible', false);
                    if (data[i + this.startIndex] === currentCanvasId) {
                        this._buttons[i].setBrush(NxtControl.Drawing.Brush.fromName("CanvasTopologyButtonCurrentBrush"));
                        this._buttons[i].setTextColor(NxtControl.Drawing.Color.fromName("CanvasTopologyButtonCurrentTextColor"));
                    }
                    else {
                        this._buttons[i].setBrush(NxtControl.Drawing.Brush.fromName("CanvasTopologyButtonBrush"));
                        this._buttons[i].setTextColor(NxtControl.Drawing.Color.fromName("CanvasTopologyButtonTextColor"));
                    }
                }
                for (i = this.realBtnCountToShow; i < this.btnCountToShow; i++)
                    this._buttons[i].set('visible', false);
                if (this.realBtnCountToShow >= data.length || this.realBtnCountToShow === 0) {
                    this.toLeft.set('visible', false);
                    this.toRight.set('visible', false);
                }
                else {
                    if (this.startIndex === 0)
                        this.toLeft.set('visible', false);
                    else
                        this.toLeft.set('visible', true);
                    if (this.startIndex + this.realBtnCountToShow < data.length)
                        this.toRight.set('visible', true);
                    else
                        this.toRight.set('visible', false);
                }
                this.invalidate();
            };
            CanvasTopologyPanel.prototype._canvasChanged = function (sender, ea) {
                if (this.btnCountToShow < 0)
                    return;
                var currentCanvas = CanvasTS.getCurrentCanvas();
                var data, i;
                var oldCurrentParentCanvasId = this.currentParentCanvasId;
                var currentCanvasId = currentCanvas.CanvasId;
                this.currentParentCanvasId = currentCanvas.ParentCanvasId;
                var oldStartIndex = this.startIndex;
                this.startIndex = 0;
                if (this.topologyType === CanvasTopologyType.Sibling) {
                    data = CanvasTS.getChildCanvasIds(this.currentParentCanvasId);
                    for (i = 0; i < data.length; i++)
                        if (data[i] === currentCanvasId) {
                            this.startIndex = i;
                            break;
                        }
                    if (this.startIndex + this.btnCountToShow >= data.length)
                        this.startIndex = data.length - this.btnCountToShow;
                    if (this.startIndex < 0)
                        this.startIndex = 0;
                    this._manage(data, currentCanvasId);
                }
                else if (this.topologyType === CanvasTopologyType.Child)
                    this._manage(CanvasTS.getChildCanvasIds(currentCanvasId), currentCanvasId);
                else if (this.topologyType === CanvasTopologyType.Top) {
                    var parentCanvasId = currentCanvas.ParentCanvasId;
                    if (parentCanvasId >= 0)
                        parentCanvasId = CanvasTS.getCanvasTopParentID(parentCanvasId);
                    data = CanvasTS.getChildCanvasIds(parentCanvasId);
                    for (i = 0; i < data.length; i++)
                        if (data[i] === currentCanvasId) {
                            this.startIndex = i;
                            break;
                        }
                    if (this.startIndex + this.btnCountToShow >= data.length)
                        this.startIndex = data.length - this.btnCountToShow;
                    if (this.startIndex < 0)
                        this.startIndex = 0;
                    if (this.startIndex > oldStartIndex && oldStartIndex + this.btnCountToShow > this.startIndex && parentCanvasId === oldCurrentParentCanvasId)
                        this.startIndex = oldStartIndex;
                    this._manage(data, currentCanvasId);
                }
                this.invalidate();
            };
            CanvasTopologyPanel.prototype._btnClick = function (sender, e) {
                for (var index = 0; index < this._buttons.length; index++)
                    if (this._buttons[index] === e.e.localTarget)
                        break;
                if (index !== this._buttons.length) {
                    if (this.topologyType === CanvasTopologyType.Sibling)
                        CanvasTS.openCanvas(CanvasTS.getChildCanvasIds(CanvasTS.getCurrentCanvas().ParentCanvasId)[index + this.startIndex]);
                    else if (this.topologyType === CanvasTopologyType.Child)
                        CanvasTS.openCanvas(CanvasTS.getChildCanvasIds(CanvasTS.getCurrentCanvas().CanvasId)[index + this.startIndex]);
                    else if (this.topologyType === CanvasTopologyType.Top)
                        CanvasTS.openCanvas(CanvasTS.getChildCanvasIds(CanvasTS.getCanvasTopParentID(CanvasTS.getCurrentCanvas().CanvasId))[index + this.startIndex]);
                }
            };
            CanvasTopologyPanel.prototype._toLeftClick = function (sender, e) {
                if (this.startIndex === 0)
                    return;
                this.startIndex--;
                var currentCanvas = CanvasTS.getCurrentCanvas();
                if (this.topologyType === CanvasTopologyType.Sibling)
                    this._manage(CanvasTS.getChildCanvasIds(currentCanvas.ParentCanvasId), currentCanvas.CanvasId);
                else if (this.topologyType === CanvasTopologyType.Child)
                    this._manage(CanvasTS.getChildCanvasIds(currentCanvas.CanvasId), currentCanvas.CanvasId);
                else if (this.topologyType === CanvasTopologyType.Top)
                    this._manage(CanvasTS.getChildCanvasIds(CanvasTS.getCanvasTopParentID(currentCanvas.CanvasId)), currentCanvas.CanvasId);
            };
            CanvasTopologyPanel.prototype._toRightClick = function (sender, e) {
                var data = [];
                var currentCanvas = CanvasTS.getCurrentCanvas();
                if (this.topologyType === CanvasTopologyType.Sibling)
                    data = CanvasTS.getChildCanvasIds(currentCanvas.ParentCanvasId);
                else if (this.topologyType === CanvasTopologyType.Child)
                    data = CanvasTS.getChildCanvasIds(currentCanvas.CanvasId);
                else if (this.topologyType === CanvasTopologyType.Top)
                    data = CanvasTS.getChildCanvasIds(CanvasTS.getCanvasTopParentID(currentCanvas.CanvasId));
                if (this.startIndex + this.btnCountToShow >= data.length)
                    return;
                this.startIndex++;
                this._manage(data, currentCanvas.CanvasId);
            };
            CanvasTopologyPanel.prototype.toObject = function (propertiesToInclude) {
                return extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                    topologyType: this.get('topologyType')
                });
            };
            CanvasTopologyPanel.fromObject = function (object) {
                return new CanvasTopologyPanel().load(object);
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.CanvasTopologyPanel')
            ], CanvasTopologyPanel.prototype, "type", void 0);
            __decorate([
                System.DefaultValue(CanvasTopologyType.Sibling)
            ], CanvasTopologyPanel.prototype, "topologyType", void 0);
            return CanvasTopologyPanel;
        }(GuiFramework.Symbol));
        GuiFramework.CanvasTopologyPanel = CanvasTopologyPanel;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
//# sourceMappingURL=NxtControl.GuiFramework.js.map