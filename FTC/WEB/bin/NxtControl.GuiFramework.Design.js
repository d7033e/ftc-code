var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
if (typeof designer === 'undefined')
    designer = null;
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var LoadingState;
        (function (LoadingState) {
            LoadingState[LoadingState["NotLoaded"] = 0] = "NotLoaded";
            LoadingState[LoadingState["Loaded"] = 1] = "Loaded";
            LoadingState[LoadingState["Loading"] = 2] = "Loading";
        })(LoadingState || (LoadingState = {}));
        var min = NxtControl.Util.array.min;
        NxtControl.Services.ColorService.instance.isDesign = true;
        var DesignCanvas = (function (_super) {
            __extends(DesignCanvas, _super);
            function DesignCanvas(element, options) {
                _super.call(this, element, options);
                this._callDesigner = true;
                this._hasLoadingError = false;
                this.__onObjectModified = false;
                this._dragDropKeywords = [];
                this.cache = [];
                this.designMode = 'select';
                this.lastPoints = [];
                this.firstPos = { x: 0, y: 0 };
                this.currentDesignShape = null;
                this.loadingState = LoadingState.NotLoaded;
                NxtControl.Services.CanvasTopologyService.instance.currentElement = this.lowerCanvasEl;
            }
            DesignCanvas.prototype.callDesigner = function (value) {
                this._callDesigner = value;
            };
            DesignCanvas.prototype._initEventListeners = function () {
                var _this = this;
                _super.prototype._initEventListeners.call(this);
                this._selectionChanged = this._selectionChanged.bind(this);
                this._handleDblClick = this._handleDblClick.bind(this);
                this._handleDragEnter = this._handleDragEnter.bind(this);
                this._handleDragOver = this._handleDragOver.bind(this);
                this._handleDragLeave = this._handleDragLeave.bind(this);
                this._handleDrop = this._handleDrop.bind(this);
                this._onFocus = this._onFocus.bind(this);
                NxtControl.Util.addListener(document.body, 'mousewheel', this.__onMouseWheel.bind(this));
                NxtControl.Util.addListener(this.upperCanvasEl, 'dblclick', this._handleDblClick);
                NxtControl.Util.addListener(this.upperCanvasEl, 'dragenter', this._handleDragEnter);
                NxtControl.Util.addListener(this.upperCanvasEl, 'dragover', this._handleDragOver);
                NxtControl.Util.addListener(this.upperCanvasEl, 'dragleave', this._handleDragLeave);
                NxtControl.Util.addListener(this.upperCanvasEl, 'drop', this._handleDrop);
                NxtControl.Util.addListener(System.document, 'keydown', this._onKeyDown.bind(this));
                window.addEventListener ? window.addEventListener('focus', this._onFocus, true) : window.attachEvent('onfocusin', this._onFocus);
                window.onerror = function (message, filename, lineno, colno, error) {
                    var errormessage = 'Error during design process.\nIt can cause future troubles in the design.\nThis message is placed also in clipboard.\nError:\n';
                    errormessage += message;
                    if (filename && filename.length != 0) {
                        errormessage += '\
Filename: ' + filename;
                    }
                    errormessage += !lineno ? '' : '\nLine: ' + lineno;
                    errormessage += !colno ? '' : '\nColumn: ' + colno;
                    errormessage += !error ? '' : '\nError: ' + error;
                    errormessage += !(error && error.stack) ? '' : '\nStack: ' + error.stack;
                    designer && designer.ShowError(errormessage);
                };
                NxtControl.Util.addListener(this.upperCanvasEl, 'contextmenu', function (e) {
                });
                NxtControl.Util.addListener(this.upperCanvasEl, 'contextmenu', function (e) {
                    e.preventDefault();
                    var posx = 0;
                    var posy = 0;
                    if (!e)
                        e = window.event;
                    if (e.pageX || e.pageY) {
                        posx = e.pageX;
                        posy = e.pageY;
                    }
                    else if (e.clientX || e.clientY) {
                        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                    }
                    var enabled = { cutEnabled: _this.cutEnabled(), deleteEnabled: _this.deleteEnabled(), copyEnabled: _this.copyEnabled(), pasteEnabled: _this.pasteEnabled() };
                    if (_this.getActiveObject()) {
                        designer && designer.CreateObjectContextMenu(_this.getActiveObject().getName(), enabled, posx, posy);
                    }
                    else if (_this.getActiveGroup()) {
                        designer && designer.CreateGroupContextMenu(enabled, posx, posy);
                    }
                    else {
                        designer && designer.CreateRootContextMenu(enabled, posx, posy);
                    }
                    e.preventDefault();
                });
                this.selected.add(this._selectionChanged);
                this.selectionCreated.add(this._selectionChanged);
                this.selectionCleared.add(function () {
                    if (!_this._callDesigner)
                        return;
                    if (_this.loadingState !== LoadingState.Loaded)
                        return;
                    var props = { width: _this.width, height: _this.height, background: _this.backgroundColor };
                    if (_this.designerType === 'Faceplate')
                        props.title = _this.hasOwnProperty('title') ? _this.title : '';
                    if (_this.designerType === 'RuntimeSymbol' && _this.hasOwnProperty('openFaceplates') && _this.openFaceplates !== null)
                        props.openFaceplates = _this.openFaceplates;
                    designer && designer.RootSelected(JSON.stringify(props));
                });
                this.added.add(function (sender, ev) {
                    if (_this._callDesigner && _this.loadingState === LoadingState.Loaded && _this.isSelectDesignMode() === false && ev.target !== _this.currentDesignShape) {
                        ev.target.saveState();
                        var csClass = '';
                        if (ev.target.constructor.prototype.hasOwnProperty('csClass'))
                            csClass = ev.target.constructor.prototype.csClass;
                        else
                            csClass = ev.target.getCsClass();
                        if (csClass === 'NxtControl.GuiHTMLFramework.RuntimeSymbol')
                            csClass = '';
                        designer && designer.ShapeCreated(ev.target.getName(), ev.target.getType(), csClass, ev.target.valueType || '', _this._stringifyObject(ev.target.getOriginalState()));
                    }
                });
                this.modified.add(function (sender, ev) {
                    _this.__onObjectModified = true;
                    if (!_this._callDesigner || ev.target.hasOwnProperty('isGrabber'))
                        return;
                    if (ev.target === null)
                        return;
                    if (ev.target === _this.getActiveGroup()) {
                        var group = _this.getActiveGroup();
                        group.destroy();
                        var shapes = [];
                        for (var i = 0; i < group.getObjects().length; i++) {
                            var o = group.getObjects()[i];
                            shapes.push(o);
                            o.saveState();
                        }
                        _this.getActiveGroup().clear();
                        _this._discardActiveGroup();
                        var savedState = _this._stringifyObject(shapes);
                        _this._setActiveGroup(new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, shapes));
                        _this._setCorners(_this.getActiveGroup());
                        ev.target = _this.getActiveGroup();
                        designer && designer.ShapesModified(savedState);
                    }
                    else {
                        ev.target.saveState();
                        designer && designer.ShapeModified(ev.target.getName(), _this._stringifyObject(ev.target.getOriginalState()));
                    }
                    _this.renderAll();
                });
            };
            DesignCanvas.prototype._stringifyObject = function (object, space) {
                if (space === void 0) { space = null; }
                this.cache = [];
                if (space === null)
                    return JSON.stringify(object, this._stringifyReplacer.bind(this));
                else
                    return JSON.stringify(object, this._stringifyReplacer.bind(this), space);
            };
            DesignCanvas.prototype._stringifyReplacer = function (key, value) {
                if (key === 'chart')
                    return undefined;
                if (typeof value === 'object' && value !== null) {
                    if (value.hasOwnProperty('type') && value.type === 'NxtControl.GuiFramework.EditCircle')
                        return undefined;
                    if (this.cache.indexOf(value) !== -1) {
                        return value;
                    }
                    this.cache.push(value);
                }
                return value;
            };
            DesignCanvas.prototype.reportException = function (err) {
                var errormessage = 'Error has been caught. It can cause future troubles in the design.\nThis message is placed also in clipboard.\nError:\n';
                errormessage += err.message;
                errormessage += !err.stack ? '' : '\nStack: ' + err.stack;
                setTimeout(function () {
                    designer && designer.ShowError(errormessage);
                });
            };
            DesignCanvas.prototype._selectionChanged = function (sender, ev) {
                this.setSelectDesignMode();
                if (ev.target === null)
                    return;
                var activeGroup = this.getActiveGroup();
                if (!ev.target.hasOwnProperty('isGrabber') && ev.target !== activeGroup) {
                    ev.target.saveState();
                    this._setCorners(ev.target);
                    this.renderAll();
                }
                if (ev.target === this.getActiveGroup()) {
                    ev.target.forEachObject(function (o) {
                        if (o.designer)
                            o.designer.propertyChanged('active');
                    });
                    if (this._callDesigner) {
                        activeGroup.destroy();
                        var shapes = [];
                        for (var i = 0; i < activeGroup.getObjects().length; i++) {
                            var o = activeGroup.getObjects()[i];
                            shapes.splice(0, 0, o);
                            o.saveState();
                        }
                        var savedState = this._stringifyObject(shapes);
                        activeGroup.load({ originX: 'center', originY: 'center' }, shapes);
                        designer && designer.ShapesSelected(savedState);
                    }
                    this._setCorners(ev.target);
                    this.renderAll();
                }
                else if (this._callDesigner && !ev.target.hasOwnProperty('isGrabber'))
                    designer && designer.ShapeSelected(this._stringifyObject(ev.target.getOriginalState()));
            };
            DesignCanvas.prototype.__onMouseDown = function (e) {
                designer && designer.MouseDown();
                if (this.isSelectDesignMode())
                    return _super.prototype.__onMouseDown.call(this, e);
                this.designNewShapeMouseDown(e);
                return null;
            };
            DesignCanvas.prototype.__onMouseMove = function (e) {
                if (this.isSelectDesignMode())
                    return _super.prototype.__onMouseMove.call(this, e);
                this.designNewShapeMouseMove(e);
                return null;
            };
            DesignCanvas.prototype.__onMouseUp = function (e) {
                if (this.isSelectDesignMode())
                    return _super.prototype.__onMouseUp.call(this, e);
                this.designNewShapeMouseUp(e);
                return null;
            };
            DesignCanvas.prototype.__onMouseWheel = function (e, s) {
                e.stopPropagation();
                e.preventDefault();
                designer && designer.OnEventMouseWheel(e.wheelDelta);
            };
            DesignCanvas.prototype._handleDblClick = function (e) {
                e.stopPropagation();
                e.preventDefault();
                if (!this.isSelectDesignMode())
                    this.designNewShapeDblClick(e);
                return false;
            };
            DesignCanvas.prototype._checkDragDrop = function (e) {
                var r = 'none';
                if (e.dataTransfer.types.indexOf('text/plain') !== -1)
                    r = 'copy';
                return r;
            };
            DesignCanvas.prototype._handleDragEnter = function (e) {
                e.dataTransfer.dropEffect = this._checkDragDrop(e);
            };
            DesignCanvas.prototype._handleDragOver = function (e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                e.dataTransfer.dropEffect = this._checkDragDrop(e);
                return false;
            };
            DesignCanvas.prototype._handleDragLeave = function (e) {
            };
            DesignCanvas.prototype._handleDrop = function (e) {
                e.stopPropagation();
                e.preventDefault();
                var text = e.dataTransfer.getData('text/plain');
                if (text) {
                    var parts = text.split('|');
                    if (parts.length > 1) {
                        var target = this.findTarget(e);
                        if (target instanceof GuiFramework.Trend) {
                            designer.TrendDragDrop(text, Math.round(e.x), Math.round(e.y), target.getName());
                        }
                        else {
                            var doDrop = false;
                            for (var i = 0; i < this._dragDropKeywords.length; i++) {
                                if (parts[0] === this._dragDropKeywords[i]) {
                                    doDrop = true;
                                    break;
                                }
                            }
                            if (doDrop)
                                designer && designer.DragDrop(text, Math.round(e.x), Math.round(e.y));
                            else
                                designer && designer.UnableToDrop();
                        }
                        return false;
                    }
                }
                return false;
            };
            DesignCanvas.prototype._onFocus = function () {
                designer && designer.OnFocus();
            };
            DesignCanvas.prototype.setDesignMode = function (mode) {
                if (this.designMode !== mode) {
                    this.designMode = mode;
                    designer && designer.DesignerModeChanged(mode);
                }
            };
            DesignCanvas.prototype.setSelectDesignMode = function () {
                this.setDesignMode('select');
            };
            DesignCanvas.prototype.isSelectDesignMode = function () {
                return this.designMode === 'select';
            };
            DesignCanvas.prototype.designNewShapeMouseDown = function (e) {
                var pointer = this.getPointer(e);
                if (this.currentDesignShape === null)
                    this.firstPos = { x: pointer.x, y: pointer.y };
                var obj = {
                    pen: NxtControl.Drawing.Pen.Black,
                    selectable: false
                };
                var shape;
                switch (this.designMode) {
                    case 'NxtControl.GuiHTMLFramework.FreeText':
                        obj = {};
                        obj.brush = NxtControl.Drawing.Brush.Black;
                        obj.fontFamily = 'Arial';
                        obj.fontSize = 14;
                        obj.text = 'freeText';
                        obj.left = this.firstPos.x;
                        obj.top = this.firstPos.y;
                        shape = new GuiFramework.FreeText().load(obj);
                        shape.setName(designer && designer.GetName(this.designMode) || 'shapeNameNotDefined');
                        this.createDesigner(shape);
                        this.add(shape);
                        this.currentDesignShape = shape;
                        return;
                    case 'NxtControl.GuiHTMLFramework.Rectangle':
                        obj.brush = new NxtControl.Drawing.SolidBrush({ color: '#f00' });
                        shape = new GuiFramework.Rectangle().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.Trend':
                    case 'NxtControl.GuiHTMLFramework.AlarmControl':
                        obj.brush = NxtControl.Drawing.Brush.Black;
                        shape = new GuiFramework.Rectangle().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.TextBox':
                        shape = new GuiFramework.TextBox().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.Image':
                        shape = new GuiFramework.Rectangle().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.TwoStateButton':
                        shape = new GuiFramework.TwoStateButton().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.Button':
                        shape = new GuiFramework.Button().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.DropDown':
                        shape = new GuiFramework.DropDown().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.Label':
                        shape = new GuiFramework.Label().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.Ellipse':
                        obj.brush = new NxtControl.Drawing.SolidBrush({ color: '#f00' });
                        shape = new GuiFramework.Ellipse().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.Line':
                        shape = new GuiFramework.Line().load(obj);
                        break;
                    case 'NxtControl.GuiHTMLFramework.Polygon':
                    case 'NxtControl.GuiHTMLFramework.Polyline':
                        if (this.designMode === 'NxtControl.GuiHTMLFramework.Polygon')
                            obj.brush = new NxtControl.Drawing.SolidBrush({ color: '#f00' });
                        else {
                            obj.pen = new NxtControl.Drawing.Pen({ color: 'Black', width: 1 });
                        }
                        if (!this.currentDesignShape) {
                            this.lastPoints = [{ x: 0, y: 0 }];
                            obj.points = [{ x: 0, y: 0 }];
                            if (this.designMode === 'NxtControl.GuiHTMLFramework.Polygon')
                                shape = new GuiFramework.Polygon().load(obj);
                            else
                                shape = new GuiFramework.Polyline().load(obj);
                            this.firstPos = { x: pointer.x, y: pointer.y };
                        }
                        else {
                            obj.left = this.firstPos.x;
                            obj.top = this.firstPos.y;
                            obj.brush = this.currentDesignShape.getBrush();
                            this.lastPoints.push({ x: pointer.x - this.firstPos.x, y: pointer.y - this.firstPos.y });
                            obj.points = JSON.parse(JSON.stringify(this.lastPoints));
                            var minX = min(obj.points, 'x');
                            var minY = min(obj.points, 'y');
                            obj.left += minX;
                            obj.top += minY;
                            obj.points.forEach(function (p) {
                                p.x -= minX;
                                p.y -= minY;
                            }, this);
                            if (this.designMode === 'NxtControl.GuiHTMLFramework.Polygon')
                                shape = new GuiFramework.Polygon().load(obj);
                            else
                                shape = new GuiFramework.Polyline().load(obj);
                            this.remove(this.currentDesignShape);
                        }
                        break;
                    case 'NxtControl.GuiHTMLFramework.Tracker':
                        delete obj.pen;
                        shape = new GuiFramework.Tracker().load(obj);
                        break;
                }
                this.currentDesignShape = shape;
                this.add(shape);
                this.renderAll();
            };
            DesignCanvas.prototype.designNewShapeMouseMove = function (e) {
                if (this.currentDesignShape === null)
                    return;
                if (this.designMode === 'NxtControl.GuiHTMLFramework.Polygon' ||
                    this.designMode === 'NxtControl.GuiHTMLFramework.Polyline' ||
                    this.designMode === 'NxtControl.GuiHTMLFramework.Image' ||
                    this.designMode === 'NxtControl.GuiHTMLFramework.FreeText')
                    return;
                var pointer = this.getPointer(e);
                var obj = {
                    pen: NxtControl.Drawing.Pen.Black,
                    selectable: false
                };
                var shape;
                if (this.designMode === 'NxtControl.GuiHTMLFramework.Line') {
                    obj.start = this.firstPos;
                    obj.end = pointer;
                    shape = new GuiFramework.Line().load(obj);
                }
                else {
                    obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                    obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                    switch (this.designMode) {
                        case 'NxtControl.GuiHTMLFramework.Rectangle':
                            {
                                obj.brush = '#f00';
                                obj.width = Math.abs(this.firstPos.x - pointer.x);
                                obj.height = Math.abs(this.firstPos.y - pointer.y);
                                if (obj.width === 0)
                                    obj.width = 1;
                                if (obj.height === 0)
                                    obj.height = 1;
                                shape = new GuiFramework.Rectangle().load(obj);
                                break;
                            }
                        case 'NxtControl.GuiHTMLFramework.Trend':
                        case 'NxtControl.GuiHTMLFramework.AlarmControl':
                            {
                                obj.brush = '#000';
                                obj.width = Math.abs(this.firstPos.x - pointer.x);
                                obj.height = Math.abs(this.firstPos.y - pointer.y);
                                if (obj.width === 0)
                                    obj.width = 1;
                                if (obj.height === 0)
                                    obj.height = 1;
                                shape = new GuiFramework.Rectangle().load(obj);
                                break;
                            }
                        case 'NxtControl.GuiHTMLFramework.Ellipse':
                            {
                                obj.brush = '#f00';
                                obj.width = Math.abs(this.firstPos.x - pointer.x);
                                obj.height = Math.abs(this.firstPos.y - pointer.y);
                                if (obj.width === 0)
                                    obj.width = 1;
                                if (obj.height === 0)
                                    obj.height = 1;
                                shape = new GuiFramework.Ellipse().load(obj);
                                break;
                            }
                        case 'NxtControl.GuiHTMLFramework.TextBox':
                            {
                                obj.width = Math.abs(this.firstPos.x - pointer.x);
                                if (obj.width === 0)
                                    obj.width = 1;
                                shape = new GuiFramework.TextBox().load(obj);
                                break;
                            }
                        case 'NxtControl.GuiHTMLFramework.TwoStateButton':
                            {
                                obj.width = Math.abs(this.firstPos.x - pointer.x);
                                if (obj.width === 0)
                                    obj.width = 1;
                                shape = new GuiFramework.TwoStateButton().load(obj);
                                break;
                            }
                        case 'NxtControl.GuiHTMLFramework.Button':
                            {
                                obj.width = Math.abs(this.firstPos.x - pointer.x);
                                if (obj.width === 0)
                                    obj.width = 1;
                                shape = new GuiFramework.Button().load(obj);
                                break;
                            }
                        case 'NxtControl.GuiHTMLFramework.DropDown':
                            {
                                obj.width = Math.abs(this.firstPos.x - pointer.x);
                                if (obj.width === 0)
                                    obj.width = 1;
                                shape = new GuiFramework.DropDown().load(obj);
                                break;
                            }
                        case 'NxtControl.GuiHTMLFramework.Label':
                            {
                                obj.width = Math.abs(this.firstPos.x - pointer.x);
                                if (obj.width === 0)
                                    obj.width = 1;
                                shape = new GuiFramework.Label().load(obj);
                                break;
                            }
                        case 'NxtControl.GuiHTMLFramework.Tracker':
                            {
                                obj.width = Math.abs(this.firstPos.x - pointer.x);
                                obj.height = Math.abs(this.firstPos.y - pointer.y);
                                if (obj.width === 0)
                                    obj.width = 1;
                                if (obj.height === 0)
                                    obj.height = 1;
                                delete obj.pen;
                                shape = new GuiFramework.Tracker().load(obj);
                                break;
                            }
                    }
                }
                this.remove(this.currentDesignShape);
                this.currentDesignShape = shape;
                this.add(shape);
            };
            DesignCanvas.prototype.createDesigner = function (shape) {
                if (typeof (shape.designerType) !== 'undefined') {
                    var klass = NxtControl.Util.Parser.getKlass(shape.designerType, null);
                    if (klass)
                        var designer = new klass();
                    shape.setDesigner(designer, this);
                }
            };
            DesignCanvas.prototype.designNewShapeMouseUp = function (e) {
                if (this.designMode === 'NxtControl.GuiHTMLFramework.Polygon' ||
                    this.designMode === 'NxtControl.GuiHTMLFramework.Polyline')
                    return;
                var pointer = this.getPointer(e);
                var obj = {
                    pen: NxtControl.Drawing.Pen.Black
                };
                var shape;
                switch (this.designMode) {
                    case 'NxtControl.GuiHTMLFramework.FreeText':
                        {
                            shape = this.currentDesignShape;
                            this._setCorners(shape);
                            this.currentDesignShape = null;
                            this.setSelectDesignMode();
                            this.setActiveObject(shape);
                            return;
                        }
                    case 'NxtControl.GuiHTMLFramework.Rectangle':
                        {
                            obj.brush = '#f00';
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            obj.height = Math.abs(this.firstPos.y - pointer.y);
                            if (obj.width === 0)
                                obj.width = 1;
                            if (obj.height === 0)
                                obj.height = 1;
                            shape = new GuiFramework.Rectangle().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.Trend':
                        {
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            obj.height = Math.abs(this.firstPos.y - pointer.y);
                            if (obj.width === 0)
                                obj.width = 1;
                            if (obj.height === 0)
                                obj.height = 1;
                            shape = new GuiFramework.Trend().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.AlarmControl':
                        {
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            obj.height = Math.abs(this.firstPos.y - pointer.y);
                            if (obj.width === 0)
                                obj.width = 1;
                            if (obj.height === 0)
                                obj.height = 1;
                            shape = new GuiFramework.AlarmControl().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.TextBox':
                        {
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            if (obj.width === 0)
                                obj.width = 1;
                            shape = new GuiFramework.TextBox().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.Image':
                        {
                            this.remove(this.currentDesignShape);
                            this.currentDesignShape = null;
                            this.setSelectDesignMode();
                            designer && designer.DragDrop('htmlShape|NxtControl.GuiHTMLFramework.Image', Math.round(this.firstPos.x), Math.round(this.firstPos.y));
                            return;
                        }
                    case 'NxtControl.GuiHTMLFramework.TwoStateButton':
                        {
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            if (obj.width === 0)
                                obj.width = 1;
                            shape = new GuiFramework.TwoStateButton().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.Button':
                        {
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            if (obj.width === 0)
                                obj.width = 1;
                            shape = new GuiFramework.Button().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.DropDown':
                        {
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            if (obj.width === 0)
                                obj.width = 1;
                            shape = new GuiFramework.DropDown().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.Label':
                        {
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            if (obj.width === 0)
                                obj.width = 1;
                            shape = new GuiFramework.Label().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.Ellipse':
                        {
                            obj.brush = '#f00';
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            obj.height = Math.abs(this.firstPos.y - pointer.y);
                            if (obj.width === 0)
                                obj.width = 1;
                            if (obj.height === 0)
                                obj.height = 1;
                            shape = new GuiFramework.Ellipse().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.Line':
                        {
                            obj.start = { x: this.firstPos.x, y: this.firstPos.y };
                            obj.end = { x: pointer.x, y: pointer.y };
                            shape = new GuiFramework.Line().load(obj);
                            break;
                        }
                    case 'NxtControl.GuiHTMLFramework.Tracker':
                        {
                            obj.left = this.firstPos.x < pointer.x ? this.firstPos.x : pointer.x;
                            obj.top = this.firstPos.y < pointer.y ? this.firstPos.y : pointer.y;
                            obj.width = Math.abs(this.firstPos.x - pointer.x);
                            obj.height = Math.abs(this.firstPos.y - pointer.y);
                            if (obj.width === 0)
                                obj.width = 1;
                            if (obj.height === 0)
                                obj.height = 1;
                            delete obj.pen;
                            shape = new GuiFramework.Tracker().load(obj);
                            break;
                        }
                }
                this.remove(this.currentDesignShape);
                this.currentDesignShape = null;
                this._setCorners(shape);
                shape.setName(designer && designer.GetName(this.designMode) || 'shapeNameNotDefined');
                this.createDesigner(shape);
                this.add(shape);
                this.setSelectDesignMode();
                this.setActiveObject(shape);
            };
            DesignCanvas.prototype.designNewShapeDblClick = function (e) {
                if (this.currentDesignShape !== null) {
                    var obj = {};
                    obj.left = this.firstPos.x;
                    obj.top = this.firstPos.y;
                    obj.brush = this.currentDesignShape.getBrush();
                    obj.pen = this.currentDesignShape.getPen();
                    this.remove(this.currentDesignShape);
                    this.currentDesignShape = null;
                    if (this.lastPoints.length > 3) {
                        this.lastPoints.length -= 1;
                        obj.points = JSON.parse(JSON.stringify(this.lastPoints));
                        var minX = min(obj.points, 'x');
                        var minY = min(obj.points, 'y');
                        obj.left += minX;
                        obj.top += minY;
                        obj.points.forEach(function (p) {
                            p.x -= minX;
                            p.y -= minY;
                        }, this);
                        var shape;
                        if (this.designMode === 'NxtControl.GuiHTMLFramework.Polygon')
                            shape = new GuiFramework.Polygon().load(obj);
                        else
                            shape = new GuiFramework.Polyline().load(obj);
                        shape.name = designer && designer.GetName(this.designMode) || 'shapeNameNotDefined';
                        this.createDesigner(shape);
                        this.add(shape);
                        this.setSelectDesignMode();
                        this.setActiveObject(shape);
                    }
                    else {
                        this.setSelectDesignMode();
                    }
                }
            };
            DesignCanvas.prototype.load = function (json, designerType, dragDropKeywords) {
                this.designerType = designerType;
                this._dragDropKeywords = dragDropKeywords.split('|');
                var serialized = (typeof json === 'string') ? JSON.parse(json) : json;
                this.setWidth(serialized.width || 600);
                this.setHeight(serialized.height || 600);
                if (designerType === 'Faceplate')
                    this.title = serialized.title || '';
                if (this.designerType === 'RuntimeSymbol' && serialized.hasOwnProperty('openFaceplates') && serialized.openFaceplates !== null)
                    this.openFaceplates = serialized.openFaceplates;
                this.loadingState = LoadingState.Loading;
                var _this = this;
                try {
                    this.loadFromJSON(serialized, this._onJSONLoaded.bind(this), function (o, currentObject) {
                        if (!currentObject)
                            return;
                        _this._setCorners(currentObject);
                        _this.createDesigner(currentObject);
                    });
                }
                catch (err) {
                    this._hasLoadingError = true;
                    this.loadingState = LoadingState.NotLoaded;
                    var errormessage = err.message;
                    errormessage += !err.stack ? '' : '\nStack: ' + err.stack;
                    designer && designer.LoadingError(errormessage);
                    this._callDesigner = false;
                }
            };
            DesignCanvas.prototype._onJSONLoaded = function () {
                this.loadingState = LoadingState.Loaded;
                this.renderAll();
                var currentObject;
                var shapesArray = [];
                var j = 0;
                for (var i = 0; i < this._objects.length; i++, j++) {
                    currentObject = this._objects[i];
                    if (!currentObject)
                        continue;
                    if (!currentObject.getName() || currentObject.getName().length === 0)
                        continue;
                    var shape = {};
                    shape.name = currentObject.getName();
                    shape.type = currentObject.getType();
                    shape.csClass = (currentObject.constructor.prototype.hasOwnProperty('csClass') ? currentObject.constructor.prototype.csClass : currentObject.getCsClass()) || '';
                    if (shape.csClass === 'NxtControl.GuiHTMLFramework.RuntimeSymbol')
                        shape.csClass = '';
                    shape.valueType = currentObject.valueType || '';
                    if (shape.type === 'System.WEB.Symbols.Base.Execute')
                        shape.text = currentObject.text;
                    if (shape.type === 'System.WEB.Symbols.Base.Setter')
                        shape.text = currentObject.text;
                    shapesArray[j] = shape;
                }
                designer && designer.ShapesLoaded(this._stringifyObject(shapesArray));
                var _this = this;
                setTimeout(function () {
                    _this._discardActiveObject();
                    _this._discardActiveGroup();
                    _this.selectionCleared.trigger(_this, { e: null });
                });
            };
            DesignCanvas.prototype.save = function (callback) {
                var _this = this;
                if (this._hasLoadingError) {
                    setTimeout(function () {
                        var state = '';
                        designer && designer.SetSaveResult(state);
                        if (callback)
                            callback(state);
                    });
                    return;
                }
                if (this.isSelectDesignMode() === false) {
                    if (this.currentDesignShape !== null)
                        this.remove(this.currentDesignShape);
                    this.setSelectDesignMode();
                }
                if (this.getActiveGroup() !== null)
                    this.discardActiveGroup(null);
                else if (this.getActiveObject())
                    this.discardActiveObject(null);
                setTimeout(function () {
                    var state = _this._stringifyObject(_this, '\t');
                    designer && designer.SetSaveResult(state);
                    if (callback)
                        callback(state);
                });
            };
            DesignCanvas.prototype.__serializeBgOverlay = function () {
                var data = _super.prototype.__serializeBgOverlay.call(this);
                if (this.designerType === 'Faceplate' || this.designerType === 'Canvas') {
                    data.width = this.width;
                    data.height = this.height;
                }
                if (this.designerType === 'Faceplate' && this.hasOwnProperty('title'))
                    data.title = this.title;
                if (this.designerType === 'RuntimeSymbol') {
                    if (!data.hasOwnProperty('_design_'))
                        data._design_ = {};
                    data._design_.width = this.width;
                    data._design_.height = this.height;
                    if (data.hasOwnProperty('background')) {
                        data._design_.background = data.background;
                        delete data.background;
                    }
                    if (data.hasOwnProperty('overlay')) {
                        data._design_.overlay = data.overlay;
                        delete data.overlay;
                    }
                }
                if (this.designerType === 'RuntimeSymbol' && this.hasOwnProperty('openFaceplates') && this.openFaceplates !== null)
                    data.openFaceplates = this.openFaceplates;
                return data;
            };
            DesignCanvas.prototype._setBgOverlay = function (serialized, callback) {
                if (this.designerType === 'RuntimeSymbol' && serialized.hasOwnProperty('_design_')) {
                    if (serialized._design_.hasOwnProperty('width'))
                        this.width = serialized._design_.width;
                    if (serialized._design_.hasOwnProperty('height'))
                        this.height = serialized._design_.height;
                    if (serialized._design_.hasOwnProperty('background'))
                        serialized.background = serialized._design_.background;
                    if (serialized._design_.hasOwnProperty('overlay'))
                        serialized.overlay = serialized._design_.overlay;
                    delete serialized._design_;
                }
                _super.prototype._setBgOverlay.call(this, serialized, callback);
            };
            DesignCanvas.prototype._setCorners = function (shape) {
                shape.hasRotatingPoint = false;
                shape.transparentCorners = false;
                shape.cornerSize = 5;
                if (shape.isType('group')) {
                    shape.cornerColor = DesignCanvas.BlueColor;
                }
                else {
                    shape.cornerColor = DesignCanvas.GreenColor;
                    if (!shape.designer || (shape.designer !== null && shape.designer.showSingleSelectBorders))
                        shape['hasBorders'] = true;
                    else
                        shape['hasBorders'] = false;
                }
            };
            DesignCanvas.prototype.shapeToCreate = function (type, creationOptions, callBack) {
                var _this = this;
                this._callDesigner = callBack;
                try {
                    var serialized = JSON.parse(creationOptions);
                    if (type) {
                        this.designMode = type;
                        var klass = NxtControl.Util.Parser.getKlass(type, null);
                        if (klass) {
                            var shape = klass.fromObject(serialized);
                            this._setCorners(shape);
                            this.createDesigner(shape);
                            this.add(shape);
                            if (shape.hasOwnProperty('isAsync') && shape.isAsync) {
                                shape.onCreated = function () {
                                    shape.onCreated = null;
                                    _this.renderAll();
                                    _this.setSelectDesignMode();
                                    _this.setActiveObject(shape);
                                };
                            }
                            else {
                                this.renderAll();
                                this.setSelectDesignMode();
                                this.setActiveObject(shape);
                            }
                        }
                    }
                }
                catch (err) {
                    this.reportException(err);
                }
                this._callDesigner = true;
            };
            DesignCanvas.prototype.shapesToCreate = function (json, callBack) {
                this._callDesigner = callBack;
                try {
                    var serialized = JSON.parse(json);
                    var shapes = [];
                    for (var i = 0; i < serialized.length; i++) {
                        if (serialized[i].type) {
                            this.designMode = serialized[i].type;
                            var klass = NxtControl.Util.Parser.getKlass(serialized[i].type, null);
                            if (klass) {
                                var shape = klass.fromObject(JSON.parse(serialized[i].state));
                                this._setCorners(shape);
                                this.createDesigner(shape);
                                this.add(shape);
                                shapes.push(shape);
                            }
                        }
                    }
                    if (shapes.length > 1) {
                        var group = new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, shapes);
                        this.setActiveGroup(group);
                    }
                    else if (shapes.length === 1)
                        this.setActiveObject(shapes[0]);
                    this.renderAll();
                    this.setSelectDesignMode();
                }
                catch (err) {
                    this.reportException(err);
                }
                this._callDesigner = true;
            };
            DesignCanvas.prototype.trendAddCurve = function (trendName, tagName) {
                for (var i = 0; i < this._objects.length; i++) {
                    var shape = this._objects[i];
                    if (typeof shape === 'undefined' || shape === null)
                        continue;
                    if (shape.getName() === trendName) {
                        shape.saveState();
                        var data = shape.trend.getData();
                        data = shape.addCurve(tagName, data);
                        shape.trend.setData(data);
                        shape._insertLegend();
                        shape._setTrendBounds(shape.getLeft(), shape.getTop(), shape.getWidth(), shape.getHeight());
                        if (this.stateful && shape.hasStateChanged()) {
                            this.modified.trigger(this, { target: shape });
                            shape.modified.trigger(shape, NxtControl.EmptyEventArgs);
                        }
                    }
                }
            };
            DesignCanvas.prototype.selectedShapesToDelete = function () {
                var i, o, shapesToDelete = [];
                if (this.getActiveGroup()) {
                    var group = this.getActiveGroup();
                    group.destroy();
                    for (i = 0; i < group.getObjects().length; i++) {
                        o = group.getObjects()[i];
                        o.saveState();
                        shapesToDelete.push({ type: o.getType(), name: o.getName(), csClass: o.getCsClass() || null, valueType: o.valueType || null, state: this._stringifyObject(o.getOriginalState()) });
                    }
                    designer && designer.ShapesDeleted(JSON.stringify(shapesToDelete));
                    for (i = 0; i < group.getObjects().length; i++) {
                        o = group.getObjects()[i];
                        o.dispose();
                        this.remove(o);
                    }
                    group.clear();
                    this.discardActiveGroup();
                }
                else {
                    o = this.getActiveObject();
                    o.saveState();
                    shapesToDelete.push({ type: o.getType(), name: o.getName(), csClass: o.getCsClass() || null, valueType: o.valueType || null, state: this._stringifyObject(o.getOriginalState()) });
                    designer && designer.ShapesDeleted(JSON.stringify(shapesToDelete));
                    o.dispose();
                    this.remove(o);
                }
                this.renderAll();
            };
            DesignCanvas.prototype.deleteShapes = function (shapeNames) {
                this._callDesigner = false;
                var shapesToDelete = [];
                this.forEachObject(function (object) {
                    if (shapeNames.indexOf(object.name) !== -1) {
                        shapesToDelete.push(object);
                    }
                });
                for (var i = 0; i < shapesToDelete.length; i++)
                    this.remove(shapesToDelete[i]);
                this._callDesigner = true;
            };
            DesignCanvas.prototype.rootChanged = function (propertyName, json) {
                var value;
                try {
                    value = JSON.parse(json);
                }
                catch (e) {
                    value = json;
                }
                if (propertyName === 'width' || propertyName === 'height') {
                    this._setDimension(propertyName, value);
                }
                else if (propertyName === 'background') {
                    this.backgroundColor = NxtControl.Drawing.Color.fromObject(value);
                }
                else
                    this[propertyName] = value;
                this.renderAll();
            };
            DesignCanvas.prototype.shapeChanged = function (shapeName, propertyName, json) {
                try {
                    for (var i = 0; i < this._objects.length; i++) {
                        var shape = this._objects[i];
                        if (typeof shape === 'undefined' || shape === null)
                            continue;
                        if (shape.getName() === shapeName) {
                            this.__onObjectModified = false;
                            var value;
                            try {
                                value = JSON.parse(json);
                            }
                            catch (e) {
                                value = json;
                            }
                            var group = this.getActiveGroup();
                            if (group !== null) {
                                group.destroy();
                                var shapes = [];
                                group.forEachObject(function (o) {
                                    shapes.splice(0, 0, o);
                                });
                                this.getActiveGroup().clear();
                                this._discardActiveGroup();
                            }
                            if (propertyName == '_events_') {
                                var eventName = value.eventName || '';
                                if (shape._events_ === null) {
                                    if (eventName.length != 0) {
                                        shape._events_ = [];
                                        shape._events_.push(value);
                                    }
                                }
                                else {
                                    var l = shape._events_.length;
                                    var index = -1;
                                    for (var ev = 0; ev < l; ev++) {
                                        if (shape._events_[ev].name === value.name) {
                                            index = ev;
                                            break;
                                        }
                                    }
                                    if (index !== -1) {
                                        if (eventName.length != 0)
                                            shape._events_[index].eventName = eventName;
                                        else
                                            shape._events_.splice(index, 1);
                                    }
                                    else if (eventName.length != 0) {
                                        shape._events_.push(value);
                                    }
                                }
                            }
                            else
                                shape.set(propertyName, value);
                            shape.setCoords();
                            if (group !== null) {
                                this._setActiveGroup(new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, shapes));
                                this._setCorners(this.getActiveGroup());
                            }
                            if (this.__onObjectModified === false) {
                                if (shape.hasStateChanged()) {
                                    this.modified.trigger(this, { target: shape });
                                    shape.modified.trigger(shape, NxtControl.EmptyEventArgs);
                                }
                            }
                            break;
                        }
                    }
                }
                catch (err) {
                    this.reportException(err);
                }
                this.renderAll();
                if (designer)
                    designer.SetShapeChanged();
            };
            DesignCanvas.prototype.shapesSelected = function (selection, callBack) {
                System.document.body.focus();
                this._callDesigner = callBack;
                this.discardActiveObject(null);
                this.discardActiveGroup(null);
                var _this = this;
                setTimeout(function () {
                    var objectsToSelect = [];
                    for (var i = 0; i < _this._objects.length; i++) {
                        var object = _this._objects[i];
                        if (object && object.getName() !== '' && selection.indexOf(object.getName()) !== -1) {
                            objectsToSelect.push(object);
                        }
                    }
                    if (objectsToSelect.length === 1) {
                        _this.setActiveObject(objectsToSelect[0], null);
                    }
                    else if (objectsToSelect.length > 1) {
                        _this.setActiveGroup(new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, objectsToSelect));
                    }
                    _this.renderAll();
                    _this._callDesigner = true;
                });
            };
            DesignCanvas.prototype.setProperties = function (shapeNames, properties) {
                this._callDesigner = false;
                var objectsToSelect = [];
                var i, len, options, prop, propsAdded, propsToSet, state, names = shapeNames.split(',');
                for (i = 0; i < this._objects.length; i++) {
                    var o = this._objects[i];
                    if (names.indexOf(o.getName()) !== -1) {
                        objectsToSelect.push(o);
                    }
                }
                this.discardActiveGroup(null);
                this.discardActiveObject(null);
                var _this = this;
                if (objectsToSelect.length === 1) {
                    setTimeout(function () {
                        try {
                            state = JSON.parse(properties);
                            propsToSet = {};
                            propsAdded = false;
                            var os = objectsToSelect[0].getOriginalState();
                            for (prop in state) {
                                if (os.hasOwnProperty(prop)) {
                                    if (state[prop] !== os[prop]) {
                                        propsToSet[prop] = state[prop];
                                        propsAdded = true;
                                    }
                                }
                            }
                            if (propsAdded)
                                objectsToSelect[0].setOptions(propsToSet);
                            objectsToSelect[0].setCoords();
                            objectsToSelect[0].saveState();
                            _this.setActiveObject(objectsToSelect[0], null);
                            _this.renderAll();
                        }
                        catch (err) {
                            this.reportException(err);
                        }
                        _this._callDesigner = true;
                    });
                }
                else if (objectsToSelect.length > 1) {
                    setTimeout(function () {
                        try {
                            var states = JSON.parse(properties);
                            for (i = 0, len = objectsToSelect.length; i < len; i++) {
                                var obj = objectsToSelect[i];
                                for (var j = 0, len2 = states.length; j < len2; j++) {
                                    if (states[j].name === obj.getName()) {
                                        state = states[j];
                                        propsToSet = {};
                                        propsAdded = false;
                                        for (prop in state) {
                                            if (state[prop] !== obj.getOriginalState()[prop]) {
                                                propsToSet[prop] = state[prop];
                                                propsAdded = true;
                                            }
                                        }
                                        if (propsAdded)
                                            obj.setOptions(propsToSet);
                                        obj.setCoords();
                                        obj.saveState();
                                    }
                                }
                            }
                            _this.setActiveGroup(new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, objectsToSelect));
                            _this._setCorners(_this.getActiveGroup());
                            _this.renderAll();
                        }
                        catch (err) {
                            this.reportException(err);
                        }
                        _this._callDesigner = true;
                    });
                }
                else {
                    this.discardActiveObject(null);
                    this.discardActiveGroup(null);
                    this.renderAll();
                    this._callDesigner = true;
                }
            };
            DesignCanvas.prototype.reorderShapes = function (newOrder) {
                var i, order = JSON.parse(newOrder);
                var inGroup = false;
                var shapes = [];
                var activeShape = null;
                if (this.getActiveGroup()) {
                    inGroup = true;
                    var group = this.getActiveGroup();
                    group.destroy();
                    for (i = 0; i < group.getObjects().length; i++) {
                        var o = group.getObjects()[i];
                        shapes.push(o);
                        o.saveState();
                    }
                    group.clear();
                    this._discardActiveGroup();
                }
                var ordered = [];
                for (i = 0; i < order.length; i++) {
                    for (var j = 0; j < this._objects.length; j++) {
                        var shape = this._objects[j];
                        if (typeof shape === 'undefined' || shape === null || shape.getName() === order[i] || order[i] === '') {
                            ordered.push(shape);
                            break;
                        }
                    }
                }
                this._objects = ordered;
                if (inGroup) {
                    this._setActiveGroup(new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, shapes));
                    this._setCorners(this.getActiveGroup());
                }
                this.renderAll();
            };
            DesignCanvas.prototype.deleteEnabled = function () {
                return this.getActiveGroup() !== null || this.getActiveObject() !== null;
            };
            DesignCanvas.prototype.deleteActive = function () {
                this.selectedShapesToDelete();
            };
            DesignCanvas.prototype.cutEnabled = function () {
                return this.getActiveGroup() !== null || this.getActiveObject() !== null;
            };
            DesignCanvas.prototype.cut = function () {
                this.copy();
                this.deleteActive();
            };
            DesignCanvas.prototype.copyEnabled = function () {
                return this.getActiveGroup() !== null || this.getActiveObject() !== null;
            };
            DesignCanvas.prototype.copy = function () {
                var clipboard = null;
                var clipboardData = null;
                if (this.getActiveGroup()) {
                    var shapesJSON = [];
                    var group = this.getActiveGroup();
                    group.destroy();
                    var shapes = [];
                    for (var i = 0; i < group.getObjects().length; i++) {
                        var o = group.getObjects()[i];
                        shapes.push(o);
                        o.saveState();
                        shapesJSON.push(o.toJSON());
                    }
                    clipboard = {
                        name: 'webhtml',
                        data: shapesJSON
                    };
                    clipboardData = this._stringifyObject(clipboard);
                    group.load({ originX: 'center', originY: 'center' }, shapes);
                }
                else if (this.getActiveObject()) {
                    var objects = [];
                    objects.push(this.getActiveObject().toJSON());
                    clipboard = {
                        name: 'webhtml',
                        data: objects
                    };
                    clipboardData = this._stringifyObject(clipboard);
                }
                if (clipboardData !== null)
                    designer && designer.SetClipboardData(clipboardData);
            };
            DesignCanvas.prototype.pasteEnabled = function () {
                return true;
            };
            DesignCanvas.prototype.paste = function () {
                try {
                    var clipboarddata = JSON.parse(designer && designer.GetClipboardData());
                    if (clipboarddata.name && clipboarddata.name === 'webhtml') {
                        var _this = this;
                        if (this.getActiveGroup())
                            this.discardActiveGroup();
                        var shapes = [];
                        var names = [];
                        NxtControl.Util.enlivenObjects(clipboarddata.data, function (enlivenedObjects) {
                            enlivenedObjects.forEach(function (shape) {
                                _this._setCorners(shape);
                                var name = designer && designer.CheckName(shape.name, shape.type, names.join('|'));
                                if (name !== shape.name) {
                                    shape.name = name;
                                    names.push(name);
                                    shape.set({ 'left': shape.left + 10, 'top': shape.top + 10 });
                                }
                                _this.createDesigner(shape);
                                shapes.push(shape);
                                _this.designMode = 'paste';
                                _this.add(shape);
                                _this.setSelectDesignMode();
                            });
                        });
                        this.setSelectDesignMode();
                        if (shapes.length === 1)
                            this.setActiveObject(shapes[0]);
                        else if (shapes.length > 1)
                            this.setActiveGroup(new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, shapes));
                        this.renderAll();
                    }
                }
                catch (err) {
                    this.reportException(err);
                }
            };
            DesignCanvas.prototype.redo = function () {
                designer && designer.Redo();
            };
            DesignCanvas.prototype.undo = function () {
                designer && designer.Undo();
            };
            DesignCanvas.prototype.align = function (where) {
                var group = this.getActiveGroup();
                if (group === null)
                    return;
                if (where !== 'left' && where !== 'right' && where !== 'top' && where !== 'bottom' && where !== 'horzCenter' && where !== 'vertCenter')
                    return;
                var left = Number.MAX_VALUE, top = Number.MAX_VALUE, bottom = Number.MIN_VALUE, right = Number.MIN_VALUE;
                group.destroy();
                var shapes = [];
                group.forEachObject(function (o) {
                    shapes.splice(0, 0, o);
                    if (o.getLeft() < left)
                        left = o.getLeft();
                    if (o.getTop() < top)
                        top = o.getTop();
                    if (o.getLeft() + o.getWidth() > right)
                        right = o.getLeft() + o.getWidth();
                    if (o.getTop() + o.getHeight() > bottom)
                        bottom = o.getTop() + o.getHeight();
                });
                this.getActiveGroup().clear();
                this._discardActiveGroup();
                for (var i = 0; i < shapes.length; i++) {
                    switch (where) {
                        case 'left':
                            shapes[i].left = left;
                            break;
                        case 'top':
                            shapes[i].top = top;
                            break;
                        case 'right':
                            shapes[i].left = right - (shapes[i].width * shapes[i].scaleX);
                            break;
                        case 'bottom':
                            shapes[i].top = bottom - (shapes[i].height * shapes[i].scaleY);
                            break;
                        case 'horzCenter':
                            shapes[i].left = (left + right) / 2 - (shapes[i].width * shapes[i].scaleX) / 2;
                            break;
                        case 'vertCenter':
                            shapes[i].top = (top + bottom) / 2 - (shapes[i].height * shapes[i].scaleY) / 2;
                            break;
                    }
                    shapes[i].setCoords();
                    shapes[i].saveState();
                }
                var savedState = this._stringifyObject(shapes);
                this._setActiveGroup(new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, shapes));
                this._setCorners(this.getActiveGroup());
                designer && designer.ShapesModified(savedState);
                this.renderAll();
            };
            DesignCanvas.prototype._getShapeNames = function () {
                var shapeNames = [];
                for (var i = 0; i < this._objects.length; i++) {
                    if (true && !this._objects[i].isGrabber)
                        shapeNames.push(this._objects[i].getName());
                }
                return shapeNames;
            };
            DesignCanvas.prototype.sendSelectionToBack = function () {
                var i, before, after;
                if (this.getActiveGroup()) {
                    var group = this.getActiveGroup();
                    group.destroy();
                    var shapes = [];
                    for (i = 0; i < group.getObjects().length; i++) {
                        var o = group.getObjects()[i];
                        shapes.push(o);
                        o.saveState();
                    }
                    group.clear();
                    this._discardActiveGroup();
                    var sortedShapes = [];
                    for (i = 0; i < this.getObjects().length; i++) {
                        var s = this._objects[i];
                        if (!s)
                            continue;
                        if (shapes.indexOf(s) > -1)
                            sortedShapes.push(s);
                    }
                    shapes = sortedShapes;
                    var savedState = this._stringifyObject(shapes);
                    before = this._getShapeNames();
                    for (i = shapes.length - 1; i >= 0; i--)
                        this.sendToBack(shapes[i]);
                    after = this._getShapeNames();
                    this._setActiveGroup(new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, shapes));
                    this._setCorners(this.getActiveGroup());
                    designer && designer.ShapesReordered(this._stringifyObject(before), this._stringifyObject(after));
                    designer && designer.ShapesSelected(savedState);
                    this.renderAll();
                }
                else if (this.getActiveObject()) {
                    before = this._getShapeNames();
                    this.sendToBack(this.getActiveObject());
                    after = this._getShapeNames();
                    designer && designer.ShapesReordered(this._stringifyObject(before), this._stringifyObject(after));
                    designer && designer.ShapeSelected(this._stringifyObject(this.getActiveObject().getOriginalState()));
                }
            };
            DesignCanvas.prototype.bringSelectionToFront = function () {
                var i, before, after;
                if (this.getActiveGroup()) {
                    var group = this.getActiveGroup();
                    group.destroy();
                    var shapes = [];
                    for (i = 0; i < group.getObjects().length; i++) {
                        var o = group.getObjects()[i];
                        shapes.push(o);
                        o.saveState();
                    }
                    group.clear();
                    this._discardActiveGroup();
                    var sortedShapes = [];
                    for (i = 0; i < this.getObjects().length; i++) {
                        var s = this._objects[i];
                        if (!s)
                            continue;
                        if (shapes.indexOf(s) > -1)
                            sortedShapes.push(s);
                    }
                    shapes = sortedShapes;
                    var savedState = this._stringifyObject(shapes);
                    before = this._getShapeNames();
                    for (i = 0; i < shapes.length; i++)
                        this.bringToFront(shapes[i]);
                    after = this._getShapeNames();
                    this._setActiveGroup(new GuiFramework.Group().load({ originX: 'center', originY: 'center' }, shapes));
                    this._setCorners(this.getActiveGroup());
                    designer && designer.ShapesReordered(this._stringifyObject(before), this._stringifyObject(after));
                    designer && designer.ShapesSelected(savedState);
                    this.renderAll();
                }
                else if (this.getActiveObject()) {
                    before = this._getShapeNames();
                    this.bringToFront(this.getActiveObject());
                    after = this._getShapeNames();
                    designer && designer.ShapesReordered(this._stringifyObject(before), this._stringifyObject(after));
                    designer && designer.ShapeSelected(this._stringifyObject(this.getActiveObject().getOriginalState()));
                }
            };
            DesignCanvas.prototype._renderBackground = function (ctx) {
                if (this.backgroundColor) {
                    ctx.fillStyle = this.backgroundColor.toLive();
                    ctx.fillRect(this.backgroundColor.offsetX || 0, this.backgroundColor.offsetY || 0, this.width, this.height);
                }
                if (this.backgroundImage) {
                    this.backgroundImage.render(ctx);
                }
            };
            DesignCanvas.prototype._onKeyDown = function (event) {
                var key;
                if (window.event) {
                    key = window.event.keyCode;
                }
                else {
                    key = event.keyCode;
                }
                var ao, ag;
                switch (key) {
                    case 8:
                    case 46:
                        if (this.deleteEnabled()) {
                            event.preventDefault();
                            this.deleteActive();
                        }
                        break;
                    case 65:
                        if (event.ctrlKey) {
                            event.preventDefault();
                            designer && designer.OnSelectAll();
                        }
                        break;
                    case 67:
                        if (this.copyEnabled() && event.ctrlKey) {
                            event.preventDefault();
                            this.copy();
                        }
                        break;
                    case 83:
                        if (event.ctrlKey) {
                            event.preventDefault();
                            designer && designer.AskForSave();
                        }
                        break;
                    case 88:
                        if (this.cutEnabled() && event.ctrlKey) {
                            event.preventDefault();
                            this.cut();
                        }
                        break;
                    case 86:
                        if (this.pasteEnabled() && event.ctrlKey) {
                            event.preventDefault();
                            this.paste();
                        }
                        break;
                    case 89:
                        if (event.ctrlKey) {
                            event.preventDefault();
                            this.redo();
                        }
                        break;
                    case 90:
                        if (event.ctrlKey) {
                            event.preventDefault();
                            this.undo();
                        }
                        break;
                    case 37:
                        {
                            ao = this.getActiveObject();
                            ag = this.getActiveGroup();
                            if (ao || ag) {
                                event.preventDefault();
                                this._moveObject(ao ? ao : ag, 'left', -1);
                            }
                        }
                        break;
                    case 39:
                        {
                            ao = this.getActiveObject();
                            ag = this.getActiveGroup();
                            if (ao || ag) {
                                event.preventDefault();
                                this._moveObject(ao ? ao : ag, 'left', 1);
                            }
                        }
                        break;
                    case 38:
                        {
                            ao = this.getActiveObject();
                            ag = this.getActiveGroup();
                            if (ao || ag) {
                                event.preventDefault();
                                this._moveObject(ao ? ao : ag, 'top', -1);
                            }
                        }
                        break;
                    case 40:
                        {
                            ao = this.getActiveObject();
                            ag = this.getActiveGroup();
                            if (ao || ag) {
                                event.preventDefault();
                                this._moveObject(ao ? ao : ag, 'top', 1);
                            }
                        }
                        break;
                    default:
                        break;
                }
            };
            DesignCanvas.prototype._moveObject = function (object, prop, offset) {
                this.stateful && object.saveState();
                object.set(prop, object[prop] + offset);
                object.setCoords();
                this.renderAll();
                if (this.stateful && object.hasStateChanged()) {
                    this.modified.trigger(this, { target: object });
                    object.modified.trigger(object, NxtControl.EmptyEventArgs);
                }
            };
            DesignCanvas.BlueColor = NxtControl.Drawing.Color.fromName('Blue');
            DesignCanvas.GreenColor = NxtControl.Drawing.Color.fromName('Green');
            return DesignCanvas;
        }(GuiFramework.InteractiveCanvas));
        GuiFramework.DesignCanvas = DesignCanvas;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var NxtControl;
(function (NxtControl) {
    var GuiFramework;
    (function (GuiFramework) {
        var min = NxtControl.Util.array.min, max = NxtControl.Util.array.max;
        var EditCircle = (function (_super) {
            __extends(EditCircle, _super);
            function EditCircle() {
                _super.apply(this, arguments);
            }
            EditCircle.prototype.setupState = function () {
                this.originalState = {};
                return this;
            };
            EditCircle.prototype.hasStateChanged = function () {
                return false;
            };
            EditCircle.prototype.saveState = function (options) {
                return this;
            };
            __decorate([
                System.DefaultValue('NxtControl.GuiFramework.EditCircle')
            ], EditCircle.prototype, "type", void 0);
            return EditCircle;
        }(GuiFramework.Ellipse));
        var ShapeDesigner = (function () {
            function ShapeDesigner() {
                this.showSingleSelectBorders = true;
            }
            ShapeDesigner.prototype.initialize = function (options) {
                this.shape = options.shape;
                this.canvas = options.canvas;
                this.noEditCircles = options.noEditCircles || false;
                if (this.noEditCircles === false) {
                    var _this = this;
                    this.__objectMoving = function (sender, e) {
                        _this._objectMoving(e.target, e.e);
                    };
                    this.__mouseUp = function (sender, e) {
                        _this._mouseUp(e.target, e.e);
                    };
                    this.activeEditCircles = [];
                }
            };
            ShapeDesigner.prototype.propertyChanged = function (propertyName) {
                if (this.noEditCircles === false) {
                    if (propertyName === 'active') {
                        if (this.shape['active'] === true && this.canvas.getActiveGroup() === null) {
                            if (this.activeEditCircles.length === 0) {
                                this._addEditCircles();
                                this.canvas.moving.add(this.__objectMoving);
                                this.canvas.mouseup.add(this.__mouseUp);
                            }
                        }
                        else {
                            this._clearEditControls();
                        }
                    }
                    if (this.shape['active'] === true && !this.shape.getGroup() && (propertyName === 'left' || propertyName === 'top')) {
                        this._shapeMoved();
                    }
                }
                return this;
            };
            ShapeDesigner.prototype.hide = function () {
                if (this.noEditCircles === true)
                    return;
                for (var i = 0; i < this.activeEditCircles.length; i++)
                    this.canvas.remove(this.activeEditCircles[i]);
            };
            ShapeDesigner.prototype.show = function () {
                if (this.noEditCircles === true)
                    return;
                for (var i = 0; i < this.activeEditCircles.length; i++)
                    this.canvas.add(this.activeEditCircles[i]);
            };
            ShapeDesigner.prototype._clearEditControls = function () {
                if (this.activeEditCircles.length === 0)
                    return;
                var _this = this;
                setTimeout(function () {
                    if (_this.activeEditCircles.indexOf(_this.canvas.getActiveObject()) !== -1) {
                        return;
                    }
                    _this.canvas.moving.remove(_this.__objectMoving);
                    _this.canvas.mouseup.remove(_this.__mouseUp);
                    _this.activeEditCircles.forEach(function (item) {
                        _this.canvas.remove(item);
                    });
                    _this.activeEditCircles.length = 0;
                    _this.canvas.renderAll();
                });
            };
            ShapeDesigner.prototype._createHolderShape = function (x, y) {
                var holdershape = new EditCircle().load({
                    left: x,
                    top: y,
                    width: 8,
                    height: 8,
                    hoverCursor: 'ne-resize',
                    brush: new NxtControl.Drawing.SolidBrush({ color: 'Green' }),
                    pen: new NxtControl.Drawing.Pen({ color: 'Black', width: 2 }),
                    isGrabber: true
                });
                holdershape['hasControls'] = holdershape['hasBorders'] = false;
                return holdershape;
            };
            ShapeDesigner.prototype._addEditCircles = function () { };
            ShapeDesigner.prototype._shapeMoved = function () { };
            ShapeDesigner.prototype._objectMoving = function (target, e) { };
            ShapeDesigner.prototype._mouseUp = function (target, e) { };
            return ShapeDesigner;
        }());
        GuiFramework.ShapeDesigner = ShapeDesigner;
        var RectangleDesigner = (function (_super) {
            __extends(RectangleDesigner, _super);
            function RectangleDesigner() {
                _super.apply(this, arguments);
            }
            RectangleDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
            };
            RectangleDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return RectangleDesigner;
        }(ShapeDesigner));
        GuiFramework.RectangleDesigner = RectangleDesigner;
        var EllipseDesigner = (function (_super) {
            __extends(EllipseDesigner, _super);
            function EllipseDesigner() {
                _super.apply(this, arguments);
            }
            EllipseDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
            };
            EllipseDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return EllipseDesigner;
        }(ShapeDesigner));
        GuiFramework.EllipseDesigner = EllipseDesigner;
        var LineDesigner = (function (_super) {
            __extends(LineDesigner, _super);
            function LineDesigner() {
                _super.apply(this, arguments);
            }
            LineDesigner.prototype.initialize = function (options) {
                _super.prototype.initialize.call(this, options);
                this.showSingleSelectBorders = false;
            };
            LineDesigner.prototype.propertyChanged = function (propertyName) {
                _super.prototype.propertyChanged.call(this, propertyName);
                if (this.shape['active'] === true && !this.shape.getGroup() &&
                    (propertyName === 'start' || propertyName === 'end')) {
                    this._shapeMoved();
                }
                return this;
            };
            LineDesigner.prototype._shapeMoved = function () {
                if (!this.activeEditCircles || this.activeEditCircles.length === 0)
                    return;
                this.activeEditCircles[0].left = this.shape.getStart().x - 3;
                this.activeEditCircles[0].top = this.shape.getStart().y - 3;
                this.activeEditCircles[0].setCoords();
                this.activeEditCircles[1].left = this.shape.getEnd().x - 3;
                this.activeEditCircles[1].top = this.shape.getEnd().y - 3;
                this.activeEditCircles[1].setCoords();
            };
            LineDesigner.prototype._objectMoving = function (target, e) {
                if (typeof e.target === 'object') {
                    if (this.activeEditCircles.indexOf(target) !== -1) {
                        if (target.pointIndex === 0) {
                            this.shape.setStart({ x: target.getLeft() + 3, y: target.getTop() + 3 });
                        }
                        else {
                            this.shape.setEnd({ x: target.getLeft() + 3, y: target.getTop() + 3 });
                        }
                        this.shape.setCoords();
                    }
                }
            };
            LineDesigner.prototype._mouseUp = function (target, e) {
                if (typeof e.target === 'object') {
                    if (this.activeEditCircles.indexOf(target) !== -1) {
                        this.canvas.callDesigner(false);
                        this.canvas.setActiveObject(this.shape);
                        this.canvas.callDesigner(true);
                        this.canvas.modified.trigger(this.canvas, { target: this.shape });
                        this.shape.modified.trigger(this.shape, { e: e });
                    }
                }
            };
            LineDesigner.prototype._addEditCircles = function () {
                var holdershape = this._createHolderShape(this.shape.getStart().x - 3, this.shape.getStart().y - 3);
                holdershape.pointIndex = 0;
                this.activeEditCircles.push(holdershape);
                this.canvas.add(holdershape);
                holdershape = this._createHolderShape(this.shape.getEnd().x - 3, this.shape.getEnd().y - 3);
                holdershape.pointIndex = 1;
                this.activeEditCircles.push(holdershape);
                this.canvas.add(holdershape);
            };
            return LineDesigner;
        }(ShapeDesigner));
        GuiFramework.LineDesigner = LineDesigner;
        var FreeTextDesigner = (function (_super) {
            __extends(FreeTextDesigner, _super);
            function FreeTextDesigner() {
                _super.apply(this, arguments);
            }
            FreeTextDesigner.prototype.initialize = function (options) {
                _super.prototype.initialize.call(this, options);
                this.showSingleSelectBorders = false;
            };
            FreeTextDesigner.prototype.propertyChanged = function (propertyName) {
                _super.prototype.propertyChanged.call(this, propertyName);
                if (this.shape['active'] === true && !this.shape.getGroup() &&
                    (propertyName === 'left' || propertyName === 'top')) {
                    this._shapeMoved();
                }
                return this;
            };
            FreeTextDesigner.prototype._shapeMoved = function () {
                if (!this.activeEditCircles || this.activeEditCircles.length === 0)
                    return;
                this.activeEditCircles[0].left = this.shape.getLeft() - 3;
                this.activeEditCircles[0].top = this.shape.getTop() - 3;
                this.activeEditCircles[0].setCoords();
            };
            FreeTextDesigner.prototype._objectMoving = function (target, e) {
                if (typeof e.target === "object") {
                    if (this.activeEditCircles.indexOf(target) !== -1) {
                        this.shape.left = target.getLeft() + 3;
                        this.shape.top = target.getTop() + 3;
                        this.shape.setCoords();
                    }
                }
            };
            FreeTextDesigner.prototype._mouseUp = function (target, e) {
                if (typeof e.target === "object") {
                    if (this.activeEditCircles.indexOf(target) !== -1) {
                        this.canvas.callDesigner(false);
                        this.canvas.setActiveObject(this.shape);
                        this.canvas.callDesigner(true);
                        this.canvas.modified.trigger(this.canvas, { target: this.shape });
                        this.shape.modified.trigger(this.shape, { e: e });
                    }
                }
            };
            FreeTextDesigner.prototype._addEditCircles = function () {
                var holdershape = this._createHolderShape(this.shape.getLeft() - 3, this.shape.getTop() - 3);
                holdershape.pointIndex = 0;
                this.activeEditCircles.push(holdershape);
                this.canvas.add(holdershape);
            };
            return FreeTextDesigner;
        }(ShapeDesigner));
        GuiFramework.FreeTextDesigner = FreeTextDesigner;
        var PolygonDesigner = (function (_super) {
            __extends(PolygonDesigner, _super);
            function PolygonDesigner() {
                _super.apply(this, arguments);
            }
            PolygonDesigner.prototype.initialize = function (options) {
                _super.prototype.initialize.call(this, options);
                this.showSingleSelectBorders = false;
            };
            PolygonDesigner.prototype.propertyChanged = function (propertyName) {
                _super.prototype.propertyChanged.call(this, propertyName);
                if (this.shape['active'] === true && !this.shape.getGroup() && (propertyName === 'width' || propertyName === 'height' || propertyName === 'dpoints')) {
                    this._shapeMoved();
                }
                return this;
            };
            PolygonDesigner.prototype._shapeMoved = function () {
                if (!this.activeEditCircles || this.activeEditCircles.length === 0)
                    return;
                var _this = this;
                this.activeEditCircles.forEach(function (p) {
                    p.left = _this.shape.getLeft() + _this.shape.getWidth() / 2 + _this.shape.getPoints()[p.pointIndex].x - 3;
                    p.top = _this.shape.getTop() + _this.shape.getHeight() / 2 + _this.shape.getPoints()[p.pointIndex].y - 3;
                    p.setCoords();
                });
            };
            PolygonDesigner.prototype._objectMoving = function (target, e) {
                if (typeof e.target === "object") {
                    if (this.activeEditCircles.indexOf(target) !== -1) {
                        var center = this.shape.getCenterPoint();
                        for (var i = 0; i < this.shape.getPoints().length; i++) {
                            this.shape.getPoints()[i].x += center.x;
                            this.shape.getPoints()[i].y += center.y;
                        }
                        this.shape.getPoints()[target.pointIndex].x = target.getLeft() + 3;
                        this.shape.getPoints()[target.pointIndex].y = target.getTop() + 3;
                        var points = this.shape.getPoints();
                        var minX = min(points, 'x');
                        var maxX = max(points, 'x');
                        var minY = min(points, 'y');
                        var maxY = max(points, 'y');
                        this.shape.left = minX;
                        this.shape.top = minY;
                        this.shape.width = (maxX - minX) || 1;
                        this.shape.height = (maxY - minY) || 1;
                        center = this.shape.getCenterPoint();
                        this.shape.getPoints().forEach(function (p) {
                            p.x -= center.x;
                            p.y -= center.y;
                        }, this.shape);
                        this.shape.setCoords();
                    }
                }
            };
            PolygonDesigner.prototype._mouseUp = function (target, e) {
                var i;
                if (typeof e.target === 'object') {
                    if (this.activeEditCircles.indexOf(target) !== -1) {
                        if (e.ctrlKey) {
                            var pointIndex = target.pointIndex;
                            if (e.altKey) {
                                if (this.activeEditCircles.length > 3) {
                                    pointIndex = target.pointIndex;
                                    var center = this.shape.getCenterPoint();
                                    this.shape.getPoints().splice(pointIndex, 1);
                                    for (i = 0; i < this.shape.getPoints().length; i++) {
                                        this.shape.getPoints()[i].x += center.x;
                                        this.shape.getPoints()[i].y += center.y;
                                    }
                                    var points = this.shape.getPoints();
                                    var minX = min(points, 'x');
                                    var maxX = max(points, 'x');
                                    var minY = min(points, 'y');
                                    var maxY = max(points, 'y');
                                    this.shape.left = minX;
                                    this.shape.top = minY;
                                    this.shape.width = (maxX - minX) || 1;
                                    this.shape.height = (maxY - minY) || 1;
                                    center = this.shape.getCenterPoint();
                                    this.shape.getPoints().forEach(function (p) {
                                        p.x -= center.x;
                                        p.y -= center.y;
                                    }, this.shape);
                                    this.shape.setCoords();
                                    this.activeEditCircles.splice(pointIndex, 1);
                                    for (i = pointIndex; i < this.activeEditCircles.length; i++)
                                        this.activeEditCircles[i].pointIndex--;
                                    this.canvas.remove(target);
                                    this.canvas.renderAll();
                                }
                            }
                            else {
                                var p = { x: this.shape.getPoints()[pointIndex].x, y: this.shape.getPoints()[pointIndex].y };
                                this.shape.getPoints().splice(pointIndex, 0, p);
                                var holdershape = this._createHolderShape(this.shape.getLeft() + this.shape.getWidth() / 2 + (p.x * this.shape.getScaleX()) - 3, this.shape.getTop() + this.shape.getHeight() / 2 + (p.y * this.shape.getScaleY()) - 3);
                                holdershape.pointIndex = pointIndex;
                                this.activeEditCircles.splice(pointIndex, 0, holdershape);
                                for (i = pointIndex + 1; i < this.activeEditCircles.length; i++)
                                    this.activeEditCircles[i].pointIndex++;
                                this.canvas.add(holdershape);
                            }
                        }
                        this.canvas.callDesigner(false);
                        this.canvas.setActiveObject(this.shape);
                        this.canvas.callDesigner(true);
                        this.canvas.modified.trigger(this.canvas, { target: this.shape });
                        this.shape.modified.trigger(this.shape, { e: e });
                    }
                }
            };
            PolygonDesigner.prototype._addEditCircles = function () {
                var length = this.shape.getPoints().length;
                for (var i = 0; i < length; i++) {
                    var holdershape = this._createHolderShape(this.shape.getLeft() + this.shape.getWidth() / 2 + (this.shape.getPoints()[i].x * this.shape.getScaleX()) - 3, this.shape.getTop() + this.shape.getHeight() / 2 + (this.shape.getPoints()[i].y * this.shape.getScaleY()) - 3);
                    holdershape.pointIndex = i;
                    this.activeEditCircles.push(holdershape);
                    this.canvas.add(holdershape);
                }
            };
            return PolygonDesigner;
        }(ShapeDesigner));
        GuiFramework.PolygonDesigner = PolygonDesigner;
        var LabelDesigner = (function (_super) {
            __extends(LabelDesigner, _super);
            function LabelDesigner() {
                _super.apply(this, arguments);
            }
            LabelDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
                this.shape['lockScalingY'] = true;
                this.shape['lockScalingX'] = false;
            };
            LabelDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return LabelDesigner;
        }(ShapeDesigner));
        GuiFramework.LabelDesigner = LabelDesigner;
        var TextBoxDesigner = (function (_super) {
            __extends(TextBoxDesigner, _super);
            function TextBoxDesigner() {
                _super.apply(this, arguments);
            }
            TextBoxDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
                this.shape['lockScalingY'] = true;
                this.shape['lockScalingX'] = false;
            };
            TextBoxDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return TextBoxDesigner;
        }(ShapeDesigner));
        GuiFramework.TextBoxDesigner = TextBoxDesigner;
        var TwoStateButtonDesigner = (function (_super) {
            __extends(TwoStateButtonDesigner, _super);
            function TwoStateButtonDesigner() {
                _super.apply(this, arguments);
            }
            TwoStateButtonDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
                this.shape['lockScalingY'] = true;
                this.shape['lockScalingX'] = false;
            };
            TwoStateButtonDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return TwoStateButtonDesigner;
        }(ShapeDesigner));
        GuiFramework.TwoStateButtonDesigner = TwoStateButtonDesigner;
        var ButtonDesigner = (function (_super) {
            __extends(ButtonDesigner, _super);
            function ButtonDesigner() {
                _super.apply(this, arguments);
            }
            ButtonDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
            };
            ButtonDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return ButtonDesigner;
        }(ShapeDesigner));
        GuiFramework.ButtonDesigner = ButtonDesigner;
        var DropDownDesigner = (function (_super) {
            __extends(DropDownDesigner, _super);
            function DropDownDesigner() {
                _super.apply(this, arguments);
            }
            DropDownDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
            };
            DropDownDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return DropDownDesigner;
        }(ShapeDesigner));
        GuiFramework.DropDownDesigner = DropDownDesigner;
        var SymbolDesigner = (function (_super) {
            __extends(SymbolDesigner, _super);
            function SymbolDesigner() {
                _super.apply(this, arguments);
            }
            SymbolDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
            };
            SymbolDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return SymbolDesigner;
        }(ShapeDesigner));
        GuiFramework.SymbolDesigner = SymbolDesigner;
        var GraphicDesigner = (function (_super) {
            __extends(GraphicDesigner, _super);
            function GraphicDesigner() {
                _super.apply(this, arguments);
            }
            GraphicDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
            };
            GraphicDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return GraphicDesigner;
        }(ShapeDesigner));
        GuiFramework.GraphicDesigner = GraphicDesigner;
        var ImageDesigner = (function (_super) {
            __extends(ImageDesigner, _super);
            function ImageDesigner() {
                _super.apply(this, arguments);
            }
            ImageDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
            };
            ImageDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            return ImageDesigner;
        }(ShapeDesigner));
        GuiFramework.ImageDesigner = ImageDesigner;
        var TrendDesigner = (function (_super) {
            __extends(TrendDesigner, _super);
            function TrendDesigner() {
                _super.apply(this, arguments);
            }
            TrendDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
            };
            TrendDesigner.prototype.propertyChanged = function (propertyName) {
                return this;
            };
            TrendDesigner.prototype.addSeriesData = function (data, series) {
                if (series.data.length > 0)
                    return;
                var axis = this.shape.trend.getXAxes()[0];
                for (var t = 0; t < 5; t++) {
                    series.data.push([new Date(axis.min + t * (axis.max - axis.min) / 4).getTime(), Math.floor((Math.random() * 100) + 1)]);
                }
            };
            return TrendDesigner;
        }(ShapeDesigner));
        GuiFramework.TrendDesigner = TrendDesigner;
        var AlarmControlDesigner = (function (_super) {
            __extends(AlarmControlDesigner, _super);
            function AlarmControlDesigner() {
                _super.apply(this, arguments);
            }
            AlarmControlDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
                this.shape['lockScalingY'] = true;
            };
            AlarmControlDesigner.prototype.propertyChanged = function (propertyName) {
                if (propertyName === 'height') {
                    this.canvas.modified.trigger(this.canvas, { target: this.shape });
                    this.shape.modified.trigger(this.shape, { e: null });
                }
                return this;
            };
            return AlarmControlDesigner;
        }(ShapeDesigner));
        GuiFramework.AlarmControlDesigner = AlarmControlDesigner;
        var TrackerDesigner = (function (_super) {
            __extends(TrackerDesigner, _super);
            function TrackerDesigner() {
                _super.apply(this, arguments);
            }
            TrackerDesigner.prototype.initialize = function (options) {
                options.noEditCircles = true;
                _super.prototype.initialize.call(this, options);
                this.propertyChanged('orientation');
            };
            TrackerDesigner.prototype.propertyChanged = function (propertyName) {
                if (propertyName === 'orientation') {
                    if (this.shape.getOrientation() === NxtControl.Drawing.Orientation.Horizontal) {
                        this.shape['lockScalingY'] = true;
                        this.shape['lockScalingX'] = false;
                    }
                    else {
                        this.shape['lockScalingY'] = false;
                        this.shape['lockScalingX'] = true;
                    }
                }
                return this;
            };
            return TrackerDesigner;
        }(ShapeDesigner));
        GuiFramework.TrackerDesigner = TrackerDesigner;
    })(GuiFramework = NxtControl.GuiFramework || (NxtControl.GuiFramework = {}));
})(NxtControl || (NxtControl = {}));
//# sourceMappingURL=NxtControl.GuiFramework.Design.js.map