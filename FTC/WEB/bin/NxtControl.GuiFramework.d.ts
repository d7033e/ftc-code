declare var globalDocument: Document;
declare var globalWindow: Window;
declare module System {
    var document: Document;
    var window: Window;
    var isTouchSupported: boolean;
    var isLikelyNode: boolean;
    function DefaultValue(value: any): (target: any, key: string) => void;
    function Browsable(target: any, key: string): void;
}
declare module NxtControl.Util {
    function enlivenObjects(objects?: any[], callback?: Function, namespace?: string, reviver?: Function): void;
    function falseFunction(): boolean;
    function getById(id: string | HTMLElement): HTMLElement;
    function makeElement(tagName: string, attributes: any): HTMLElement;
    function addClass(element: any, className: any): void;
    function wrapElement(element: any, wrapper: any, attributes: any): any;
    function getScrollLeftTop(element: Node, upperCanvasEl: HTMLElement): {
        left: number;
        top: number;
    };
    function getElementOffset(element: HTMLElement): {
        left: number;
        top: number;
    };
    function getElementStyle(element: HTMLElement, attr: string): any;
    function makeElementUnselectable(element: any): any;
    function makeElementSelectable(element: any): any;
    function getScript(url: string, callback: Function): void;
    function addListener(element: EventTarget, eventName: string, handler: Function): void;
    function removeListener(element: EventTarget, eventName: string, handler: Function): void;
    function setStyle(element: HTMLElement, styles: any): any;
}
interface Array<T> {
    injectArray(idx: number, arr: any[]): any;
    unique(): any[];
}
interface String {
    lpad(padString: string, length: number): number;
}
declare module NxtControl.Util {
    function isMobile(): boolean;
    function removeFromArray(array: any[], value: any): any[];
    function populateWithProperties(source: Object, destination: Object, properties?: any[]): void;
    function createCanvasElement(canvasEl?: HTMLCanvasElement): HTMLCanvasElement;
    function getPointer(event: Event, upperCanvasEl: HTMLCanvasElement): {
        x: any;
        y: any;
    };
    function getFunctionBody(fn: any): string;
    function isTransparent(ctx: CanvasRenderingContext2D, x: number, y: number, tolerance: number): boolean;
    function drawDashedLine(ctx: CanvasRenderingContext2D, x: number, y: number, x2: number, y2: number, da: number[]): void;
    class Clip {
        static clipContext(receiver: {
            clipTo: Function;
        }, ctx: CanvasRenderingContext2D): void;
    }
    class Parser {
        static camelize(value: string): string;
        static capitalize(value: string, firstLetterOnly: boolean): string;
        static escapeXml(string: any): any;
        static resolveNamespace(nmsp: string): Object;
        static getKlass(type: string, namespace?: string): any;
    }
    class array {
        static invoke(array?: any[], method?: string, ...args: any[]): any[];
        static min(array?: any[], byProperty?: string): any;
        static max(array?: any[], byProperty?: string): any;
        private static find(array, byProperty, condition);
    }
    class object {
        static extend(destination: Object, source: Object): Object;
        static clone(object: Object): Object;
    }
    function loadImage(url: string, callback: Function, context: any, crossOrigin: string): void;
}
declare module NxtControl.Util {
    class Convert {
        static toFixed(number: string | number, fractionDigits: number): number;
        static degreesToRadians(degrees: any): number;
        static radiansToDegrees(radians: any): number;
        static tryFromStringToValueType(typeCode: NxtControl.GuiFramework.TypeCode, text: string, numberBase: NxtControl.GuiFramework.NumberBase): {
            result: boolean;
            value: any;
        };
        static tryFromStringToBoolean(s: string): {
            result: boolean;
            value: any;
        };
        static tryFromStringToDouble(text: string, numberBase: NxtControl.GuiFramework.NumberBase): {
            result: boolean;
            value: any;
        };
        static tryFromStringToSingle(text: string, numberBase: NxtControl.GuiFramework.NumberBase): {
            result: boolean;
            value: any;
        };
        static tryFromStringToInt(text: string, numberBase: NxtControl.GuiFramework.NumberBase, typeCode: NxtControl.GuiFramework.TypeCode): {
            result: boolean;
            value: any;
        };
        static fromDoubleToString(value: number, numberBase: NxtControl.GuiFramework.NumberBase): string;
        static fromIntToString(value: number, numberBase: NxtControl.GuiFramework.NumberBase): string;
        static tryFromStringToTime(text: string): {
            result: boolean;
            value: any;
        };
        static tryFromStringToTimeOfDay(text: string): {
            result: boolean;
            value: any;
        };
        static tryFromStringToDateAndTime(text: string): {
            result: boolean;
            value: any;
        };
        static tryFromStringToDate(text: string): {
            result: boolean;
            value: any;
        };
        static fromValueTypeToString(typeCode: NxtControl.GuiFramework.TypeCode, value: any, numberBase: NxtControl.GuiFramework.NumberBase): string;
        static fromTimeToString(value: any, format?: string): string;
        static fromTimeOfDayToString(value: any): string;
        static fromDateToString(value: any, format?: string): string;
        static fromDateAndTimeToString(value: any, format?: string): string;
    }
}
declare module NxtControl.Util {
    class Intersection {
        status: string;
        points: NxtControl.Drawing.Point[];
        constructor(status?: string);
        appendPoint(point: NxtControl.Drawing.Point): void;
        appendPoints(points: NxtControl.Drawing.Point[]): void;
        static intersectLineLine(a1: NxtControl.Drawing.Point, a2: NxtControl.Drawing.Point, b1: NxtControl.Drawing.Point, b2: NxtControl.Drawing.Point): any;
        static intersectLinePolygon(a1: NxtControl.Drawing.Point, a2: NxtControl.Drawing.Point, points: NxtControl.Drawing.Point[]): Intersection;
        static intersectPolygonPolygon(points1: NxtControl.Drawing.Point[], points2: NxtControl.Drawing.Point[]): Intersection;
        static intersectPolygonRectangle(points: NxtControl.Drawing.Point[], r1: NxtControl.Drawing.Point, r2: NxtControl.Drawing.Point): Intersection;
    }
}
declare module NxtControl {
    interface IEventArgs {
    }
    interface IValueEventArgs extends IEventArgs {
        value: any;
    }
    interface ISystemEventArgs extends IEventArgs {
        e?: Event;
    }
    interface IDelegate<T extends IEventArgs> {
        (sender: any, parameter: T): void;
    }
    var EmptyEventArgs: {};
    interface IBoolEventArgs extends IEventArgs {
        value: boolean;
    }
    interface ITextEventArgs extends IEventArgs {
        text: string;
    }
    interface ISelectedItemEventArgs extends ITextEventArgs {
        value: any;
    }
    interface IEvent<T extends IEventArgs> {
        add(callee: IDelegate<T>): IEvent<T>;
        remove(callee: IDelegate<T>): IEvent<T>;
    }
    interface IEventHolder<T extends IEventArgs> extends IEvent<T> {
        trigger(sender: any, parameter: T): void;
    }
    class event<T extends IEventArgs> implements IEventHolder<T> {
        private callees;
        constructor();
        trigger(sender: any, parameter: T): void;
        contains(callee: IDelegate<T>): boolean;
        add(callee: IDelegate<T>): event<T>;
        remove(callee: IDelegate<T>): event<T>;
        toDelegate(): IDelegate<T>;
    }
}
declare module NxtControl.GuiFramework {
    enum SendKey {
        ValueChanged = 0,
        History = 1,
        ArchiveAnswer = 2,
        FaceplateSubscribeData = 3,
        AlarmUpdate = 4,
        AlarmUpdateClear = 5,
        AlarmUpdateColor = 6,
        ServerUpdate = 7,
        AlarmUpdate2 = 8,
    }
    enum MaximumPosition {
        leftBottom = 0,
        rightTop = 1,
    }
    enum AnchorStyles {
        bottom = 2,
        left = 4,
        none = 0,
        right = 8,
        top = 1,
    }
    enum MouseButtonType {
        None = 0,
        Click = 1,
        DoubleClick = 2,
        MouseDownLeft = 4,
        MouseDownRight = 8,
        MouseUpLeft = 16,
        MouseUpRight = 32,
    }
    enum TypeCode {
        Boolean = 3,
        Byte = 6,
        Char = 4,
        DateTime = 16,
        DBNull = 2,
        Decimal = 15,
        Double = 14,
        Empty = 0,
        Int16 = 7,
        Int32 = 9,
        Int64 = 11,
        Object = 1,
        SByte = 5,
        Single = 13,
        String = 18,
        UInt16 = 8,
        UInt32 = 10,
        UInt64 = 12,
        Time = 2147483646,
        Date = 2147483645,
        TimeOfDay = 2147483644,
        DateAndTime = 2147483643,
    }
    module TypeCode {
        function fromTypeToTypeCode(type: string): TypeCode;
    }
    enum NumberBase {
        Decimal = 0,
        Binary = 1,
        Octal = 2,
        Hexadecimal = 3,
    }
    interface IShapeEventArgs extends ISystemEventArgs {
        target?: Shape;
    }
}
declare module NxtControl.Services {
    class ColorService {
        static instance: ColorService;
        isDesign: boolean;
        private clients;
        private needRedraw;
        private count;
        private phase;
        private inTick;
        private timer;
        startColorChange: NxtControl.IEvent<NxtControl.IEventArgs>;
        stopColorChange: NxtControl.IEvent<NxtControl.IBoolEventArgs>;
        constructor();
        getNeedRedraw(): boolean;
        attach(color: NxtControl.Drawing.BlinkColor): void;
        detach(color: NxtControl.Drawing.BlinkColor): void;
        tick(): void;
        startTicker(): void;
        stopTicker(): void;
    }
}
declare module NxtControl.Drawing {
    interface IPoint {
        x: number;
        y: number;
    }
    interface ICornerPoint extends IPoint {
        corner?: any;
    }
    class Point implements IPoint {
        x: number;
        y: number;
        constructor(x: number, y: number);
        rotatePoint(origin: any, radians: any): Point;
        add(that: Point): Point;
        addEquals(that: Point): this;
        scalarAdd(scalar: number): Point;
        scalarAddEqual(scalar: number): this;
        subtract(that: Point): Point;
        subtractEquals(that: Point): this;
        scalarSubtract(scalar: number): Point;
        scalarSubtractEquals(scalar: number): this;
        multiply(scalar: number): Point;
        multiplyEquals(scalar: number): this;
        divide(scalar: number): Point;
        divideEquals(scalar: number): this;
        eq(that: Point): boolean;
        lt(that: Point): boolean;
        lte(that: Point): boolean;
        gt(that: Point): boolean;
        gte(that: Point): boolean;
        lerp(that: Point, t: number): Point;
        distanceFrom(that: Point): number;
        midPointFrom(that: Point): Point;
        min(that: Point): Point;
        max(that: Point): Point;
        toString(): string;
        setXY(x: number, y: number): void;
        setFromPoint(that: Point): void;
        swap(that: Point): void;
    }
    enum FillDirection {
        TopToDown = 0,
        DownToTop = 1,
        LeftToRight = 2,
        RightToLeft = 3,
    }
    enum FillOrientation {
        None = 0,
        DiagonalLeftTop = 1,
        DiagonalLeftBottom = 2,
        DiagonalRightBottom = 3,
        DiagonalRightTop = 4,
        VerticalTop = 5,
        VerticalBottom = 6,
        HorizontalLeft = 7,
        HorizontalRight = 8,
        Center = 9,
        HorizontalCenter = 10,
        VerticalCenter = 11,
        InvertedHorizontalCenter = 12,
        InvertedVerticalCenter = 13,
    }
    enum Orientation {
        Horizontal = 0,
        Vertical = 1,
    }
    interface IMatrix {
        a?: number;
        b?: number;
        c?: number;
        d?: number;
        dx?: number;
        dy?: number;
    }
    class Matrix implements IMatrix {
        protected _rounder: number;
        protected _config: IMatrix;
        a: number;
        b: number;
        c: number;
        d: number;
        dx: number;
        dy: number;
        multiply(a: number, b: number, c: number, d: number, dx: number, dy: number): Matrix;
        applyCSSText(val: string): void;
        getTransformArray(val: string): number[];
        private _defaults;
        private _round(val);
        constructor(config?: IMatrix);
        scale(x: number, y: number): Matrix;
        skew(x: number, y?: number): Matrix;
        skewX(x: number): Matrix;
        skewY(y: number): Matrix;
        toCSSText(): string;
        toFilterText(): string;
        rad2deg(rad: number): number;
        deg2rad(deg: number): number;
        angle2rad(val: string | number): number;
        rotate(deg: string | number, x: number, y: number): Matrix;
        translate(x: string | number, y?: string | number): Matrix;
        translateX(x: string | number): Matrix;
        translateY(y: string | number): Matrix;
        identity(): Matrix;
        getMatrixArray(): number[][];
        getTransformedPoint(point: {
            x: number;
            y: number;
        }): {
            x: number;
            y: number;
        };
        getContentRect(width: number, height: number, x?: number, y?: number): {
            left: number;
            right: number;
            top: number;
            bottom: number;
        };
        getDeterminant(): number;
        inverse(): Matrix;
        transpose(): number[][];
        decompose(): [[string | number]];
    }
    class SutherlandHodgman {
        static clip(input: any, direction: FillDirection, fillPercent: any): any[];
    }
}
declare module NxtControl.Drawing {
    interface IColor {
        name?: string;
        currentColor(): Color;
        toLive(): any;
        toObject(): any;
        isTransparent(): boolean;
    }
    interface IColorOptions {
        name?: string;
        rgba?: number[];
        r?: number;
        g?: number;
        b?: number;
        a?: number;
        htmlString?: string;
    }
    interface IBlinkColorOptions {
        name?: string;
        color1?: IColorOptions;
        duration1?: number;
        color2?: IColorOptions;
        duration2?: number;
    }
    class Color implements IColor {
        currentColor(): Color;
        getR(): number;
        getG(): number;
        getB(): number;
        getA(): number;
        setA(alpha: number): Color;
        protected rgba: number[];
        name: string;
        constructor(color?: IColorOptions);
        isTransparent(): boolean;
        static fromName(name: string): IColor;
        static fromString(htmlString: string): IColor;
        toLive(): string;
        toObject(): any;
        toJSON(): any;
        protected setFromName(name: string): void;
        protected tryParsingColor(color: string): void;
        private rgbToHsl(r, g, b);
        getSource(): number[];
        setSource(source: number[]): void;
        toRgb(): string;
        toRgba(): string;
        toHsl(): string;
        toHsla(): string;
        toHex(): string;
        toGrayscale(): this;
        toBlackWhite(threshold?: number): this;
        lightenColor(lightenBy: any): string;
        static reRGBa: RegExp;
        static reHSLa: RegExp;
        static reHex: RegExp;
        private static hue2rgb(p, q, t);
        static fromRgb(color: string): Color;
        static sourceFromRgb(color: string): number[];
        static fromRgba: typeof Color.fromRgb;
        static fromHsl(color: any): Color;
        static fromObject(object?: any): IColor;
        static sourceFromHsl(color: any): number[];
        static fromHsla: typeof Color.fromHsl;
        static fromHex(color: any): Color;
        static sourceFromHex(color: string): number[];
        static fromSource(source: number[]): Color;
        static Transparent: Color;
        static Black: Color;
        static White: Color;
        static replaceNamedColorWithBlinkColor(name: string, options: (string | number | number[])[]): void;
        static replaceNamedColorWithColor(name: string, options: number[]): void;
        static colorNameMap: {
            [name: string]: number[];
        };
        static blinkColorNameMap: {
            [name: string]: (string | number | number[])[];
        };
    }
    class BlinkColor implements IColor {
        name: string;
        color1: Color;
        color2: Color;
        duration1: number;
        duration2: number;
        showFirstColor: boolean;
        referenceBlinkColor: BlinkColor;
        currentColor(): Color;
        constructor(options?: IBlinkColorOptions);
        protected setFromName(name: string): void;
        tick(tick: number): boolean;
        toJSON(): string;
        toObject(): any;
        isTransparent(): boolean;
        toLive(ctx?: CanvasRenderingContext2D): any;
    }
}
declare module NxtControl.Drawing {
    interface IPenOptions {
        color?: string | IColor;
        width?: number;
        dashArray?: number[];
        lineCap?: string;
        lineJoin?: string;
        miterLimit?: number;
    }
    class Pen {
        color: IColor;
        width: number;
        dashArray: number[];
        lineCap: string;
        lineJoin: string;
        miterLimit: number;
        constructor(options?: IPenOptions);
        toLive(): string;
        toObject(): any;
        toJSON(): any;
        clone(): NxtControl.Drawing.Pen;
        static fromString(text: string): NxtControl.Drawing.Pen;
        static fromObject(object?: any): NxtControl.Drawing.Pen;
        static Black: Pen;
        static White: Pen;
    }
    interface ISolidBrushOptions {
        color: string | IColor | IColorOptions | IBlinkColorOptions;
    }
    interface IGradientColorStop {
        offset: number | string;
        color: string | IColor | IColorOptions | IBlinkColorOptions;
    }
    interface IGradientCoords {
        x1: number;
        y1: number;
        x2: number;
        y2: number;
        r1?: number;
        r2?: number;
    }
    interface IGradientBrushOptions {
        type?: string;
        orient?: FillOrientation;
        coords?: IGradientCoords;
        colorStops?: IGradientColorStop[];
    }
    abstract class Brush {
        name: string;
        offsetX: number;
        offsetY: number;
        abstract attach(): void;
        abstract detach(): void;
        abstract toObject(): any;
        abstract toLive(ctx: CanvasRenderingContext2D): any;
        static fromName(name: string): Brush;
        static fromObject(object?: any): NxtControl.Drawing.Brush;
        static replaceNamedBrushWithSolidBrush(name: string, options: ISolidBrushOptions): void;
        static replaceNamedBrushWithGradientBrush(name: string, options: IGradientBrushOptions): void;
        static solidBrushNameMap: {
            [name: string]: ISolidBrushOptions;
        };
        static gradientBrushNameMap: {
            [name: string]: IGradientBrushOptions;
        };
        static NullBrush: NullBrush;
        static Black: SolidBrush;
        static White: SolidBrush;
    }
    class NullBrush extends Brush {
        attach(): void;
        detach(): void;
        toObject(): any;
        toJSON(): any;
        toLive(ctx: CanvasRenderingContext2D): any;
    }
    class SolidBrush extends Brush {
        color: IColor;
        constructor(options?: ISolidBrushOptions);
        attach(): void;
        detach(): void;
        toObject(): any;
        toJSON(): any;
        toLive(ctx: CanvasRenderingContext2D): any;
        static fromNameMap(name: string, options: ISolidBrushOptions): SolidBrush;
    }
    class GradientBrush extends Brush {
        type: string;
        orient: FillOrientation;
        coords: IGradientCoords;
        colorStops: IGradientColorStop[];
        constructor(options?: IGradientBrushOptions);
        attach(): void;
        detach(): void;
        private colorStopsToObject();
        toObject(): any;
        toJSON(): any;
        toLive(ctx: CanvasRenderingContext2D): any;
        static fromNameMap(name: string, options: IGradientBrushOptions): GradientBrush;
    }
}
declare module NxtControl.GuiFramework {
    interface ICurrentTransform {
        target: Shape;
        action: string;
        scaleX: number;
        scaleY: number;
        offsetX: number;
        offsetY: number;
        originX: string;
        originY: string;
        ex: number;
        ey: number;
        left: number;
        top: number;
        theta: number;
        width: number;
        mouseXSign: number;
        mouseYSign: number;
        original?: {
            left: number;
            top: number;
            scaleX: number;
            scaleY: number;
            originX: string;
            originY: string;
        };
        reset?: boolean;
        newScaleX?: number;
        newScaleY?: number;
    }
    interface IImageOptions {
        crossOrigin?: string;
        alignX?: string;
        alignY?: string;
        meetOrSlice?: string;
    }
    interface ICollectionObject {
        objects: any[];
    }
    interface ICanvasDimensions {
        width?: number;
        height?: number;
    }
    interface ICanvasDimensionsOptions {
        backstoreOnly?: boolean;
        cssOnly?: boolean;
    }
    interface IStaticCanvasOptions {
        allowTouchScrolling?: boolean;
        imageSmoothingEnabled?: boolean;
        preserveObjectStacking?: boolean;
        viewportTransform?: number[];
        freeDrawingColor?: string;
        freeDrawingLineWidth?: number;
        backgroundColor?: string | NxtControl.Drawing.IColor;
        backgroundImage?: Image | string;
        backgroundImageOpacity?: number;
        backgroundImageStretch?: number;
        clipTo?: (context: CanvasRenderingContext2D) => void;
        controlsAboveOverlay?: boolean;
        includeDefaultValues?: boolean;
        overlayColor?: string | NxtControl.Drawing.IColor;
        overlayImage?: Image;
        overlayImageLeft?: number;
        overlayImageTop?: number;
        renderOnAddRemove?: boolean;
        stateful?: boolean;
    }
    interface IShapeDesigner {
        initialize(options: {
            shape?: Shape;
            canvas?: InteractiveCanvas;
        }): any;
        propertyChanged(propertyName: string): IShapeDesigner;
    }
    interface IHMIObject {
        runtimeValueChanged(value: any, time: any): boolean;
        callbackdata: IHMICallback;
        callback: IHMICallback;
    }
    interface IHMIHistoryObject extends IHMIObject {
        addHistoryValues?(options: any): void;
        archiveAnswer?(options: any): void;
    }
    interface IHMICallback {
        id?: number;
        path?: string;
        deviceName?: string;
        rpath?: string;
        dataId?: number;
        dataIds?: number;
        shape?: IHMIObject;
    }
    interface IOpenFaceplate {
        fp?: string;
        mt?: MouseButtonType;
    }
    interface IListItem {
        text: string;
        value?: any;
    }
}
declare module NxtControl.GuiFramework {
    interface IShapeOptions {
        type?: string;
        top?: number;
        left?: number;
        width?: number;
        height?: number;
        scaleX?: number;
        scaleY?: number;
        flipX?: boolean;
        flipY?: boolean;
        angle?: number;
        cornerSize?: number;
        transparentCorners?: boolean;
        hoverCursor?: string;
        padding?: number;
        borderColor?: string | NxtControl.Drawing.IColor;
        cornerColor?: string | NxtControl.Drawing.IColor;
        centeredScaling?: boolean;
        centeredRotation?: boolean;
        brush?: string | NxtControl.Drawing.Brush;
        globalCompositeOperation?: string;
        pen?: NxtControl.Drawing.IPenOptions | NxtControl.Drawing.Pen;
        borderOpacityWhenMoving?: number;
        borderScaleFactor?: number;
        transformMatrix?: any[];
        minScaleLimit?: number;
        selectable?: boolean;
        evented?: boolean;
        visible?: boolean;
        hasControls?: boolean;
        hasBorders?: boolean;
        hasRotatingPoint?: boolean;
        rotatingPointOffset?: number;
        includeDefaultValues?: boolean;
        clipTo?: Function;
        lockMovementX?: boolean;
        lockMovementY?: boolean;
        lockRotation?: boolean;
        lockScalingX?: boolean;
        lockScalingY?: boolean;
        lockUniScaling?: boolean;
        lockScalingFlip?: boolean;
        name?: string;
        data?: any;
    }
}
declare module NxtControl.GuiFramework {
    abstract class Shape implements Shape {
        constructor();
        dispose(): this;
        initEvents(): void;
        disposeEvents(): void;
        static NUM_FRACTION_DIGITS: number;
        selected: IEvent<ISystemEventArgs>;
        mousedown: IEvent<ISystemEventArgs>;
        mousemove: IEvent<ISystemEventArgs>;
        mouseup: IEvent<ISystemEventArgs>;
        click: IEvent<ISystemEventArgs>;
        added: IEvent<IEventArgs>;
        modified: IEvent<IEventArgs>;
        removed: IEvent<IEventArgs>;
        rotating: IEvent<ISystemEventArgs>;
        scaling: IEvent<ISystemEventArgs>;
        moving: IEvent<ISystemEventArgs>;
        protected type: string;
        getType(): string;
        protected csClass: string;
        getCsClass(): string;
        originX: string;
        originY: string;
        protected top: number;
        getTop(): number;
        setTop(value: number): this;
        protected left: number;
        getLeft(): number;
        setLeft(value: number): this;
        protected width: number;
        getWidth(): number;
        getUnscaledWidth(): number;
        setWidth(value: number): this;
        protected height: number;
        getHeight(): number;
        getUnscaledHeight(): number;
        setHeight(value: number): this;
        protected scaleX: number;
        getScaleX(): number;
        protected scaleY: number;
        getScaleY(): number;
        protected flipX: boolean;
        getFlipX(): boolean;
        setFlipX(value: boolean): this;
        protected flipY: boolean;
        getFlipY(): boolean;
        setFlipY(value: boolean): this;
        protected angle: number;
        getAngle(): number;
        setAngle(value: number): this;
        protected anchor: AnchorStyles;
        getAnchor(): AnchorStyles;
        setAnchor(anchor: AnchorStyles): this;
        protected anchorOffsets: {
            x: number;
            y: number;
            w: number;
            h: number;
        };
        getAnchorOffsets(): {
            x: number;
            y: number;
            w: number;
            h: number;
        };
        setAnchorOffsets(ao: {
            x: number;
            y: number;
            w: number;
            h: number;
        }): this;
        openFaceplates: IOpenFaceplate[];
        private cornerSize;
        private transparentCorners;
        private hoverCursor;
        private padding;
        private borderColor;
        private cornerColor;
        private centeredScaling;
        private centeredRotation;
        protected brush: NxtControl.Drawing.Brush;
        getBrush(): NxtControl.Drawing.Brush;
        setBrush(value: NxtControl.Drawing.Brush): this;
        private globalCompositeOperation;
        protected pen: NxtControl.Drawing.Pen;
        getPen(): NxtControl.Drawing.Pen;
        setPen(value: NxtControl.Drawing.Pen): this;
        private borderOpacityWhenMoving;
        private borderScaleFactor;
        protected transformMatrix: any[];
        private minScaleLimit;
        selectable: boolean;
        evented: boolean;
        protected drawable: boolean;
        getDrawable(): boolean;
        protected visible: boolean;
        getVisible(): boolean;
        setVisible(visible: boolean): void;
        private hasControls;
        private hasBorders;
        protected hasRotatingPoint: boolean;
        protected rotatingPointOffset: number;
        protected perPixelTargetFind: boolean;
        protected includeDefaultValues: boolean;
        clipTo: Function;
        protected _events_: any[];
        private lockMovementX;
        private lockMovementY;
        private lockRotation;
        private lockScalingX;
        private lockScalingY;
        private lockUniScaling;
        protected name: string;
        getName(): string;
        setName(name: string): void;
        data: any;
        private isMoving;
        sourcePath: string;
        protected active: boolean;
        protected group: Group;
        getGroup(): Group;
        protected canvas: StaticCanvas;
        getCanvas(): StaticCanvas;
        protected originalState: any;
        getOriginalState(): any;
        protected designer: IShapeDesigner;
        setDesigner(designer: IShapeDesigner, canvas: InteractiveCanvas): void;
        protected designerType: string;
        protected oCoords: {
            tl: NxtControl.Drawing.ICornerPoint;
            tr: NxtControl.Drawing.ICornerPoint;
            bl: NxtControl.Drawing.ICornerPoint;
            br: NxtControl.Drawing.ICornerPoint;
            ml?: NxtControl.Drawing.ICornerPoint;
            mt?: NxtControl.Drawing.ICornerPoint;
            mr?: NxtControl.Drawing.ICornerPoint;
            mb?: NxtControl.Drawing.ICornerPoint;
            mtr?: NxtControl.Drawing.ICornerPoint;
        };
        protected currentWidth: number;
        protected currentHeight: number;
        protected _inInitialize: boolean;
        load(options?: any): this;
        invalidate(): void;
        invalidateImmediately(): void;
        getTransformation(): NxtControl.Drawing.Matrix;
        getInverseTransformation(): NxtControl.Drawing.Matrix;
        captureMouse(object: Shape): void;
        getCurrentWidth(): number;
        getCurrentHeight(): number;
        getBorderColor(): Drawing.IColor;
        setBorderColor(value: NxtControl.Drawing.IColor): this;
        getCornerSize(): number;
        setCornerSize(value: number): this;
        getFill(): NxtControl.Drawing.Brush;
        setFill(value: NxtControl.Drawing.Brush): this;
        setOptions(options: any): void;
        protected _getLeftTopCoords(): Drawing.Point;
        transform(ctx: CanvasRenderingContext2D, fromLeft?: boolean): void;
        protected _removeDefaultValues(object: any): any;
        toObject(propertiesToInclude?: any[]): any;
        toDatalessObject(propertiesToInclude?: any[]): any;
        get(property: string): any;
        set(key: any): this;
        set(key: string, value: any | Function): this;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        toggle(property: string): this;
        setSourcePath(value: string): this;
        render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        renderBordersAndControls(ctx: CanvasRenderingContext2D): void;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _transform(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _setStrokeStyles(ctx: CanvasRenderingContext2D): void;
        protected _setFillStyles(ctx: CanvasRenderingContext2D): void;
        _renderFill(ctx: CanvasRenderingContext2D, brush?: NxtControl.Drawing.Brush, x?: number, y?: number, w?: number, h?: number): void;
        protected _stroke(ctx: CanvasRenderingContext2D): void;
        protected _renderDashedStroke(ctx: CanvasRenderingContext2D): void;
        _renderStroke(ctx: CanvasRenderingContext2D, pen?: NxtControl.Drawing.Pen): void;
        clone(callback: Function, propertiesToInclude?: any[]): Shape;
        isType(type: string): boolean;
        toJSON(propertiesToInclude?: any[]): any;
        centerH(): this;
        centerV(): this;
        center(): this;
        remove(): Shape;
        getLocalPointer(e: Event, pointer: any): NxtControl.Drawing.IPoint;
        private getStateProperties();
        hasStateChanged(): boolean;
        saveState(options?: {
            stateProperties: any[];
        }): this;
        setupState(): this;
        protected _getAngleValueForStraighten(): number;
        straighten(): this;
        bringForward(intersecting?: boolean): this;
        bringToFront(): this;
        sendBackwards(intersecting?: boolean): this;
        sendToBack(): this;
        moveTo(index: number): this;
        translateToCenterPoint(point: NxtControl.Drawing.Point, originX: string, originY: string): NxtControl.Drawing.Point;
        translateToOriginPoint(center: NxtControl.Drawing.Point, originX: string, originY: string): NxtControl.Drawing.Point;
        getCenterPoint(): NxtControl.Drawing.Point;
        getPointByOrigin(originX: string, originY: string): NxtControl.Drawing.Point;
        toLocalPoint(point: NxtControl.Drawing.Point, originX: string, originY: string): NxtControl.Drawing.Point;
        setPositionByOrigin(pos: NxtControl.Drawing.Point, originX: string, originY: string): void;
        adjustPosition(to: string): void;
        findTargetCorner(e: Event, offset: {
            left: number;
            top: number;
        }): string;
        protected _setCornerCoords(): void;
        drawBorders(ctx: CanvasRenderingContext2D): this;
        protected _controlsVisibility: any;
        protected _getControlsVisibility(): any;
        drawControls(ctx: CanvasRenderingContext2D): this;
        protected _drawControl(control: string, ctx: CanvasRenderingContext2D, methodName: string, left: number, top: number): void;
        isControlVisible(controlName: string): boolean;
        setControlVisible(controlName: string, visible: boolean): this;
        setControlsVisibility(options?: {
            bl?: boolean;
            br?: boolean;
            mb?: boolean;
            ml?: boolean;
            mr?: boolean;
            mt?: boolean;
            tl?: boolean;
            tr?: boolean;
            mtr?: boolean;
        }): this;
        setCoords(): this;
        getBoundingRect(): {
            left: number;
            top: number;
            width: number;
            height: number;
        };
        isContainedWithinObject(other: Shape): boolean;
        isContainedWithinRect(pointTL: NxtControl.Drawing.Point, pointBR: NxtControl.Drawing.Point): boolean;
        protected _getImageLines(oCoords: {
            tl: NxtControl.Drawing.IPoint;
            tr: NxtControl.Drawing.IPoint;
            bl: NxtControl.Drawing.IPoint;
            br: NxtControl.Drawing.IPoint;
        }): {
            topline: {
                o: Drawing.IPoint;
                d: Drawing.IPoint;
            };
            rightline: {
                o: Drawing.IPoint;
                d: Drawing.IPoint;
            };
            bottomline: {
                o: Drawing.IPoint;
                d: Drawing.IPoint;
            };
            leftline: {
                o: Drawing.IPoint;
                d: Drawing.IPoint;
            };
        };
        protected _findCrossPoints(point: any, oCoords: any): number;
        containsPoint(point: NxtControl.Drawing.IPoint, touchPoints?: NxtControl.Drawing.IPoint[]): boolean;
        _constrainScale(value: number): number;
        scale(value: number): this;
        scaleToHeight(value: number): this;
        scaleToWidth(value: number): this;
        intersectsWithObject(other: Shape): boolean;
        intersectsWithRect(pointTL: any, pointBR: any): boolean;
        protected _initPen(options: any, propName?: string): NxtControl.Drawing.Pen;
        protected _initBrush(options: any, propName?: string): NxtControl.Drawing.Brush;
        protected _initClipping(options: any): void;
    }
}
declare module NxtControl.GuiFramework {
    class Group extends Shape {
        protected type: string;
        renderOnAddRemove: boolean;
        protected _objects: Shape[];
        private _originalLeft;
        private _originalTop;
        constructor();
        load(options?: any, objects?: Shape[]): this;
        clear(): void;
        add(...shapes: Shape[]): this;
        insertAt(object: Shape, index: number, nonSplicing?: boolean): this;
        remove(): Shape;
        remove(object: Shape): Shape;
        forEachObject(callback: Function, context?: Object): this;
        getObjects(type?: string): Shape[];
        item(index: number): Shape;
        isEmpty(): boolean;
        size(): number;
        contains(object: Shape): boolean;
        protected _updateObjectsCoords(): void;
        protected _updateObjectCoords(object: Shape): void;
        addWithUpdate(object: Shape): this;
        protected _setObjectActive(object: Shape): void;
        removeWithUpdate(object: Shape): this;
        protected _onObjectAdded(object: Shape): void;
        protected _onObjectRemoved(object: Shape): void;
        protected delegatedProperties: any;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        toObject(propertiesToInclude?: any[]): Object;
        render(ctx: CanvasRenderingContext2D, noTransform: boolean): void;
        renderBordersAndControls(ctx: CanvasRenderingContext2D): void;
        protected _renderObject(object: Shape, ctx: CanvasRenderingContext2D): void;
        protected _restoreObjectsState(): this;
        protected _moveFlippedObject(object: any): this;
        protected _toggleFlipping(object: Shape): void;
        protected _restoreObjectState(object: Shape): this;
        protected _setObjectPosition(object: Shape): void;
        protected _getRotatedLeftTop(object: Shape): {
            left: number;
            top: number;
        };
        destroy(): this;
        saveCoords(): this;
        hasMoved(): boolean;
        setObjectsCoords(): this;
        protected _setOpacityIfSame(): void;
        protected _calcBounds(): void;
        protected _getBounds(aX?: number[], aY?: number[]): {
            left: number;
            top: number;
            width: number;
            height: number;
        };
        get(prop: string): any;
        static fromObject(object?: any, callback?: Function): void;
    }
}
declare module NxtControl.Services {
    interface IDeviceConnectionEventArgs extends NxtControl.IEventArgs {
        target: string;
        state: boolean;
    }
    interface IValueEventArgs extends NxtControl.IEventArgs {
        id?: any;
        isInput: boolean;
        value: any;
        time: number;
    }
    interface IHistoryOptionsEventArgs extends NxtControl.IEventArgs {
        target: string;
        id: number;
        historyId: number;
        isInput: boolean;
        values?: any[];
        sec?: number;
        usec?: number;
        archiveId?: number;
    }
    interface IHistoryArchiveAnswerEventArgs extends NxtControl.IEventArgs {
        values: any;
        times: any;
        id: number;
        isInput: boolean;
        start: any;
        end: any;
        continued: boolean;
    }
    class CommunicationService {
        static instance: CommunicationService;
        connection: NxtControl.IEvent<IDeviceConnectionEventArgs>;
        history: NxtControl.IEvent<IHistoryOptionsEventArgs>;
        archiveAnswer: NxtControl.IEvent<IHistoryArchiveAnswerEventArgs>;
        valueChanged: NxtControl.IEvent<IValueEventArgs>;
        private connectors;
        constructor();
        private registerConnector(deviceNames, address);
        onValueChanged(value: IValueEventArgs): void;
        isDeviceConnected(deviceName: string): boolean;
        isConnected(): boolean;
        subscribe(isInput: boolean, deviceName: string, path: string, eventId: string, varNames: string[], varTypes: string[], dataId: number): number;
        unsubscribe(isInput: boolean, deviceName: string, path: string, id: number): number;
        subscribeAlarm(id: number): number;
        unsubscribeAlarm(): number;
        ackAlarm(alarmId: string): number;
        setValue(options: any, value: any): any;
        setValues(options: any, values: any[]): any;
        queryArchive(minimum: number, maximum: number, data: any): void;
        getFaceplateSubscribeData(faceplate: any): void;
    }
}
declare module NxtControl.Services {
    interface IAlarmUpdateOptions {
        action: string;
        index: number;
        alarmInfo: any;
    }
    interface IAlarmUpdateColorOptions {
        index: number;
        alarmInfo: any;
    }
    class AlarmService {
        static instance: AlarmService;
        constructor();
        id: number;
        alarmControls: {
            [alarmId: number]: any;
        };
        alarmCount: number;
        subscribe(alarmControl: any): number;
        unsubscribe(alarmId: number): void;
        ackAlarm(alarmId: string): void;
        alarmUpdate(options: IAlarmUpdateOptions): void;
        alarmUpdate2(alarmId: number, options: IAlarmUpdateOptions): void;
        alarmUpdateColor(options: IAlarmUpdateColorOptions): void;
        alarmUpdateClear(): void;
        private _onConnection(sender, ea);
    }
}
declare module NxtControl.GuiFramework {
    class StaticCanvas {
        constructor(el?: HTMLCanvasElement | string, options?: any);
        protected initialize(el?: string | HTMLCanvasElement, options?: any): void;
        added: IEvent<IShapeEventArgs>;
        modified: IEvent<IShapeEventArgs>;
        removed: IEvent<IShapeEventArgs>;
        beforeSelectionCleared: IEvent<IShapeEventArgs>;
        selectionCleared: IEvent<IEventArgs>;
        canvasCleared: IEvent<IEventArgs>;
        beforeRender: IEvent<IEventArgs>;
        afterRender: IEvent<IEventArgs>;
        static activeInstance: StaticCanvas;
        contextContainer: CanvasRenderingContext2D;
        contextTop: CanvasRenderingContext2D;
        selection: boolean;
        lowerCanvasEl: HTMLCanvasElement;
        upperCanvasEl: HTMLCanvasElement;
        protected cacheCanvasEl: HTMLCanvasElement;
        protected wrapperEl: HTMLElement;
        interactive: boolean;
        protected width: number;
        protected height: number;
        protected _objects: Shape[];
        protected _offset: {
            left: number;
            top: number;
        };
        protected _groupSelector: {
            ex: number;
            ey: number;
            top: number;
            left: number;
        };
        backgroundColor: NxtControl.Drawing.IColor;
        backgroundImage: any;
        overlayColor: NxtControl.Drawing.IColor;
        overlayImage: any;
        overlayImageLeft: number;
        overlayImageTop: number;
        includeDefaultValues: boolean;
        stateful: boolean;
        renderOnAddRemove: boolean;
        clipTo: Function;
        controlsAboveOverlay: boolean;
        allowTouchScrolling: boolean;
        onBeforeScaleRotate(target: Shape): void;
        getLowerCanvasElement(): HTMLCanvasElement;
        getUpperCanvasElement(): HTMLCanvasElement;
        protected _initStatic(el?: string | HTMLCanvasElement, options?: any): void;
        add(...shapes: Shape[]): this;
        insertAt(object: Shape, index: number, nonSplicing?: boolean): StaticCanvas;
        remove(object: Shape): Shape;
        find(path: string): Shape;
        protected _find(pathParts: string[], index: number): Shape;
        forEachObject(callback: Function, context?: Object): StaticCanvas;
        getObjects(type?: string): Shape[];
        item(index: any): Shape;
        isEmpty(): boolean;
        size(): number;
        contains(object: Shape): boolean;
        calcOffset(): this;
        setOverlayImage(image: any, callback: Function, options?: any): this;
        setBackgroundImage(image: any, callback: Function, options?: any): this;
        setOverlayColor(overlayColor: any, callback: any): this;
        setBackgroundColor(backgroundColor: any, callback: any): this;
        protected __setBgOverlayImage(property: any, image: any, callback: Function, options?: any): this;
        protected __setBgOverlayColor(property: any, color: any, callback: any): this;
        protected _createCanvasElement(): HTMLCanvasElement;
        protected _initCanvasElement(element: HTMLCanvasElement): void;
        protected _initOptions(options: any): void;
        protected _applyCanvasStyle(element: HTMLElement): void;
        protected _createLowerCanvas(canvasEl?: string | HTMLCanvasElement): void;
        getWidth(): number;
        getHeight(): number;
        setWidth(value: number): this;
        setHeight(value: number): this;
        setDimensions(dimensions: ICanvasDimensions, options?: ICanvasDimensionsOptions): this;
        protected _setDimension(prop: string, value: number): this;
        getElement(): HTMLCanvasElement;
        getActiveObject(): any;
        getActiveGroup(): any;
        protected _draw(ctx: CanvasRenderingContext2D, object: Shape): void;
        protected _onObjectAdded(obj: Shape): void;
        protected _discardActiveObject(): void;
        protected _onObjectRemoved(obj: Shape): void;
        clearContext(ctx: CanvasRenderingContext2D): this;
        getContext(): CanvasRenderingContext2D;
        discardActiveObject(e?: Event): StaticCanvas;
        getPointer(e: Event): NxtControl.Drawing.IPoint;
        clear(): this;
        drawControls(ctx: CanvasRenderingContext2D): void;
        renderAll(allOnTop?: boolean): this;
        private _invalidateId;
        invalidate(): void;
        protected _renderObjects(ctx: CanvasRenderingContext2D, activeGroup?: Group): void;
        protected _renderActiveGroup(ctx: CanvasRenderingContext2D, activeGroup: Group): void;
        protected _renderBackground(ctx: CanvasRenderingContext2D): void;
        protected _renderOverlay(ctx: any): void;
        protected _drawSelection(): void;
        renderTop(): this;
        getCenter(): {
            top: number;
            left: number;
        };
        centerObjectH(object: Shape): this;
        centerObjectV(object: Shape): this;
        centerObject(object: Shape): this;
        protected _centerObject(object: Shape, center: NxtControl.Drawing.Point): this;
        toDatalessJSON(propertiesToInclude: any): {
            objects: any[];
        };
        toObject(propertiesToInclude?: any[]): any;
        toDatalessObject(propertiesToInclude?: any[]): {
            objects: any[];
        };
        discardActiveGroup(e?: Event): StaticCanvas;
        setActiveGroup(group: Group, e?: Event): StaticCanvas;
        protected _toObjectMethod(methodName: string, propertiesToInclude?: any[]): {
            objects: any[];
        };
        protected _toObjects(methodName: string, propertiesToInclude?: any[]): any[];
        protected _toObject(instance: any, methodName: any, propertiesToInclude: any): any;
        protected __serializeBgOverlay(): any;
        sendToBack(object: Shape): this;
        bringToFront(object: Shape): this;
        sendBackwards(object: Shape, intersecting?: boolean): this;
        protected _findNewLowerIndex(object: Shape, idx: number, intersecting: boolean): any;
        bringForward(object: Shape, intersecting?: boolean): this;
        protected _findNewUpperIndex(object: Shape, idx: number, intersecting: boolean): any;
        moveTo(object: Shape, index: number): this;
        loadFromJSON(json?: string | Object, callback?: Function, reviver?: Function): StaticCanvas;
        protected _setBgOverlay(serialized: any, callback?: Function): void;
        protected __setBgOverlay(property: string, value: any, loaded: any, callback: Function): void;
        protected _enlivenObjects(objects?: any[], callback?: Function, reviver?: Function): void;
        removeListeners(): void;
        dispose(): this;
        toJSON: (propertiesToInclude?: any[]) => any;
        static EMPTY_JSON: string;
        static supports(methodName: string): boolean;
    }
}
declare module NxtControl.GuiFramework {
    class InteractiveCanvas extends StaticCanvas {
        constructor(el?: string | HTMLCanvasElement, options?: any);
        protected initialize(el?: string | HTMLCanvasElement, options?: any): void;
        selected: IEvent<IShapeEventArgs>;
        mousedown: IEvent<IShapeEventArgs>;
        mousemove: IEvent<IShapeEventArgs>;
        mouseup: IEvent<IShapeEventArgs>;
        click: IEvent<IShapeEventArgs>;
        rotating: IEvent<IShapeEventArgs>;
        scaling: IEvent<IShapeEventArgs>;
        moving: IEvent<IShapeEventArgs>;
        selectionCreated: IEvent<IShapeEventArgs>;
        uniScaleTransform: boolean;
        centeredScaling: boolean;
        centeredRotation: boolean;
        interactive: boolean;
        selection: boolean;
        selectionColor: NxtControl.Drawing.IColor;
        selectionDashArray: number[];
        selectionBorderColor: NxtControl.Drawing.IColor;
        selectionLineWidth: number;
        hoverCursor: string;
        moveCursor: string;
        defaultCursor: string;
        freeDrawingCursor: string;
        rotationCursor: string;
        containerClass: string;
        perPixelTargetFind: boolean;
        targetFindTolerance: number;
        skipTargetFind: boolean;
        protected _activeObject: Shape;
        protected _activeGroup: Group;
        protected _currentTransform: ICurrentTransform;
        protected contextCache: CanvasRenderingContext2D;
        protected lastRenderedObjectWithControlsAboveOverlay: Shape;
        protected relatedTarget: Shape;
        protected _previousPointer: NxtControl.Drawing.IPoint;
        protected isDrawingMode: boolean;
        protected _isCurrentlyDrawing: boolean;
        protected _previousOriginX: string;
        protected _previousOriginY: string;
        protected _captureMouseObject: Shape;
        protected _initInteractive(): void;
        protected _resetCurrentTransform(e: Event): void;
        containsPoint(e: Event, target: Shape): boolean;
        protected _normalizePointer(object: Shape, pointer: NxtControl.Drawing.IPoint): NxtControl.Drawing.IPoint;
        isTargetTransparent(target: Shape, x: number, y: number): boolean;
        protected _shouldClearSelection(e: Event, target: Shape): boolean;
        protected _shouldCenterTransform(e: Event, target: Shape): boolean;
        protected _getOriginFromCorner(target: Shape, corner: string): {
            x: string;
            y: string;
        };
        protected _getActionFromCorner(target: Shape, corner: string): string;
        protected _setupCurrentTransform(e: Event, target: Shape): void;
        protected _translateObject(x: number, y: number): void;
        protected _scaleObject(x: number, y: number, by?: string): void;
        protected _setObjectScale(localMouse: NxtControl.Drawing.IPoint, transform: ICurrentTransform, lockScalingX: boolean, lockScalingY: boolean, by?: string): void;
        protected _scaleObjectEqually(localMouse: NxtControl.Drawing.IPoint, target: Shape, transform: ICurrentTransform): void;
        protected _flipObject(transform: ICurrentTransform): void;
        protected _setLocalMouse(localMouse: NxtControl.Drawing.IPoint, t: ICurrentTransform): void;
        protected _rotateObject(x: number, y: number): void;
        protected _setCursor(value: string): void;
        protected _resetObjectTransform(target: Shape): void;
        protected _drawSelection(): void;
        protected _isLastRenderedObject(e: Event): string;
        findTarget(e: Event, skipGroup?: boolean): Shape;
        protected _searchPossibleTargets(e: Event): Shape;
        getPointer(e: Event): NxtControl.Drawing.IPoint;
        protected _createUpperCanvas(): void;
        protected _createCacheCanvas(): void;
        protected _initWrapperElement(): void;
        protected _applyCanvasStyle(element: HTMLElement): void;
        protected _copyCanvasStyle(fromEl: HTMLElement, toEl: HTMLElement): void;
        getSelectionContext(): CanvasRenderingContext2D;
        getSelectionElement(): HTMLCanvasElement;
        protected _setActiveObject(object: Shape): void;
        setActiveObject(object: Shape, e?: Event): this;
        getActiveObject(): Shape;
        protected _discardActiveObject(): void;
        discardActiveObject(e: Event): this;
        protected _setActiveGroup(group: Group): void;
        setActiveGroup(group: Group, e?: Event): this;
        getActiveGroup(): Group;
        protected _discardActiveGroup(): void;
        discardActiveGroup(e?: Event): this;
        deactivateAll(): this;
        captureMouse(shape: Shape): void;
        deactivateAllWithDispatch(e: Event): this;
        drawControls(ctx: CanvasRenderingContext2D): void;
        protected _drawGroupControls(ctx: CanvasRenderingContext2D, activeGroup: Group): void;
        protected _drawObjectsControls(ctx: CanvasRenderingContext2D): void;
        protected _drawControls(ctx: CanvasRenderingContext2D, object: Shape, klass: string): void;
        protected _initEventListeners(): void;
        protected _bindEvents(): void;
        removeListeners(): void;
        protected __onTransformGesture: (e: Event, s: any) => void;
        protected _onGesture(e: Event, s: any): void;
        protected __onDrag: (e: Event, s: any) => void;
        protected _onDrag(e: Event, s: any): void;
        protected __onMouseWheel(e: Event, s: any): void;
        protected _onMouseWheel(e: Event, s: any): void;
        protected __onOrientationChange: (e: Event, s: any) => void;
        protected _onOrientationChange(e: Event, s: any): void;
        protected __onShake: (e: Event, s: any) => void;
        protected _onShake(e: Event, s: any): void;
        protected isMultiTouch: Boolean;
        protected _onTouchDown(e: TouchEvent): void;
        protected _onMouseDown(e: MouseEvent): void;
        protected _onMouseClick(e: MouseEvent): void;
        protected _onTouchEnd(e: TouchEvent): void;
        protected _onMouseUp(e: Event): void;
        private detachFromMouseUp();
        protected _onTouchMove(e: TouchEvent): void;
        protected _onMouseMove(e: Event): void;
        protected _onResize(): void;
        protected _shouldRender(target: Shape, pointer: NxtControl.Drawing.IPoint): boolean;
        protected __onMouseUp(e: Event): any;
        protected _handleCursorAndEvent(e: Event, target: Shape): void;
        protected _finalizeCurrentTransform(): void;
        protected _restoreOriginXY(target: Shape): void;
        protected _onMouseDownInDrawingMode(e: Event): void;
        protected _onMouseMoveInDrawingMode(e: Event): void;
        protected _onMouseUpInDrawingMode(e: Event): void;
        protected __onMouseDown(e: MouseEvent): void;
        protected __onMouseClick(e: MouseEvent): void;
        protected _beforeTransform(e: Event, target: Shape): void;
        protected _clearSelection(e: Event, target: Shape, pointer: NxtControl.Drawing.IPoint): void;
        protected _setOriginToCenter(target: Shape): void;
        protected _setCenterToOrigin(target: Shape): void;
        protected __onMouseMove(e: Event): Shape;
        protected _transformObject(e: Event): void;
        protected _performTransformAction(e: Event, transform: any, pointer: NxtControl.Drawing.IPoint): void;
        protected _fire(eventName: string, target: Shape, e: Event): void;
        protected _beforeScaleTransform(e: Event, transform: any): void;
        protected _onScale(e: Event, transform: any, x: number, y: number): void;
        protected _setCursorFromEvent(e: Event, target: Shape): boolean;
        protected _setCornerCursor(corner: string, target: Shape): boolean;
        protected _getRotatedCornerCursor(corner: string, target: Shape): string;
        protected _shouldGroup(e: Event, target: Shape): boolean;
        protected _handleGrouping(e: Event, target: Shape): void;
        protected _updateActiveGroup(target: Shape, e: Event): void;
        protected _createActiveGroup(target: Shape, e: Event): void;
        protected _createGroup(target: Shape): Group;
        _groupSelectedObjects(e: Event): void;
        protected _collectObjects(): Shape[];
        protected _maybeGroupObjects(e: Event): void;
    }
}
declare module NxtControl.GuiFramework {
    class CanvasCore extends InteractiveCanvas {
        protected type: string;
        protected csClass: string;
        ignoreRender: boolean;
        protected _callbackCache: {
            [id: string]: {
                callback: IHMICallback;
                value: any;
                time: any;
            };
        };
        protected _callbackCacheHasNewValues: boolean;
        protected json: string | any;
        protected initialize(element?: HTMLCanvasElement | string, options?: any): void;
        dispose(): any;
        protected _initEventListeners(): void;
        renderAll(allOnTop?: boolean): this;
        setDimensions(dimensions: ICanvasDimensions, options?: ICanvasDimensionsOptions): this;
        protected _onMouseDownInDrawingMode(e: Event): void;
        protected _onMouseMoveInDrawingMode(e: any): void;
        protected _onMouseUpInDrawingMode(e: any): void;
        protected __onMouseDown(e: MouseEvent): Shape;
        loadFromJSON(serialized: ICollectionObject, callback?: Function, reviver?: Function): StaticCanvas;
        toObject(propertiesToInclude?: any[]): any;
    }
    class Canvas extends CanvasCore {
        protected type: string;
        protected csClass: string;
        protected db: any;
        private connectorInputs;
        private connectorOutputs;
        protected _valueChangedTimer: number;
        protected initialize(element?: HTMLCanvasElement | string, options?: any): void;
        dispose(): any;
        initializeCommunication(): void;
        private _deviceConnected(sender, options);
        private _valueChangedTick();
        private _valueChanged(sender, options);
        private _addHistoryValues(sender, options);
        private _archiveAnswer(sender, options);
        private _startColorChange(sender, ea);
        private _stopColorChange(sender, eventArgs);
        getSymbolPath(symbolPath: any): any;
        static fromObject(element?: HTMLCanvasElement | string, options?: any): Canvas;
    }
    class CanvasTopologyNavigator extends Canvas {
        protected type: string;
        protected _inInitialize: boolean;
        protected ctLogo: Image;
        initialize(element?: HTMLCanvasElement, options?: any): void;
        setNavigatorWidths(sender?: any, ea?: IEventArgs): void;
        static fromObject(element?: HTMLCanvasElement | string, options?: any): CanvasTopologyNavigator;
    }
}
declare module NxtControl.GuiFramework {
    interface ICanvasData {
        text: string;
        tooltip: string;
    }
}
declare module NxtControl.Services {
    interface ICanvasBaseInfo {
        Name: string;
        Title: string;
        Tooltip: string;
        CanvasId?: number;
        ParentCanvasId?: number;
        LeftCanvasId?: number;
        RightCanvasId?: number;
        Children?: number[];
    }
    interface ICanvasInfo extends ICanvasBaseInfo {
        Instance?: string;
        IsLink?: boolean;
    }
    interface ICanvasTopology {
        Id?: number;
        Name?: string;
        FirstCanvasId?: number;
        Parent?: ICanvasResolution;
        Canvases?: {
            [id: number]: ICanvasBaseInfo;
        };
    }
    interface ICanvasResolution {
        StartCanvasClass?: string;
        Name?: string;
        Topologies?: ICanvasTopology[];
    }
    interface ICanvasEventArgs extends NxtControl.IEventArgs {
        currentCanvas: NxtControl.GuiFramework.Canvas;
    }
    class CanvasTopologyService {
        static instance: CanvasTopologyService;
        canvasChanged: NxtControl.IEvent<ICanvasEventArgs>;
        currentCanvas: ICanvasInfo;
        currentElement: HTMLCanvasElement;
        faceplates: {
            [id: number]: NxtControl.GuiFramework.Faceplate;
        };
        firstCanvasId: number;
        private uniqueCanvasTopologyId;
        private uniqueId;
        private parentElement;
        private canvasName;
        private canvasTopologies;
        private canvases;
        private resolutions;
        private json;
        constructor();
        onLoad(element: HTMLElement, canvasName: string, resolutionName: string, topologyName: string, startCnv: string): number;
        loadFromDatabase(parent: any, firstCanvas: any): number;
        private getUniqueCanvasTopologyId();
        private getUniqueId();
        getCurrentCanvas(): ICanvasInfo;
        getStartCanvasId(startCnv: string): number;
        getTopologyId(resolutionName?: string, topologyName?: string): number;
        openCanvas(canvasId: number): void;
        getChildCanvasIds(parentCanvasId: number): number[];
        getCanvasData(canvasId: number): NxtControl.GuiFramework.ICanvasData;
        private findCanvasFromType(canvasType);
        private findCanvas(canvasId);
        getCanvasTopParentID(canvasId: number): number;
        openParentCanvas(): void;
        openLeftCanvas(): void;
        openRightCanvas(): void;
        openChildCanvas(childIndex: number): void;
        getLeftTooltip(): string;
        getRightTooltip(): string;
        getUpTooltip(): string;
        getDownTooltip(): string;
        registerFaceplate(faceplate: NxtControl.GuiFramework.Faceplate): number;
        unregisterFaceplate(id: number): void;
        getFaceplate(id: number): NxtControl.GuiFramework.Faceplate;
        closeFaceplates(): void;
    }
}
declare var Cufon: any;
declare module NxtControl.GuiFramework {
    class Rectangle extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        protected fillDirection: NxtControl.Drawing.FillDirection;
        getFillDirection(): Drawing.FillDirection;
        setFillDirection(fillDirection: NxtControl.Drawing.FillDirection): this;
        protected fillPercent: number;
        getFillPercent(): number;
        setFillPercent(fillPercent: number): this;
        protected radius: number;
        getRadius(): number;
        setRadius(radius: number): this;
        protected x: number;
        protected y: number;
        load(options?: any): this;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _renderDashedStroke(ctx: CanvasRenderingContext2D): void;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _normalizeLeftTopProperties(parsedAttributes: any): this;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): Rectangle;
    }
    class Ellipse extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        load(options?: any): this;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        toObject(propertiesToInclude?: any[]): any;
        render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        static fromObject(object?: any): Ellipse;
    }
    class Line extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        protected start: NxtControl.Drawing.IPoint;
        getStart(): Drawing.IPoint;
        setStart(start: NxtControl.Drawing.IPoint): this;
        protected end: NxtControl.Drawing.IPoint;
        getEnd(): Drawing.IPoint;
        setEnd(end: NxtControl.Drawing.IPoint): this;
        load(options?: any): this;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _renderDashedStroke(ctx: CanvasRenderingContext2D): void;
        toObject(propertiesToInclude?: any[]): any;
        setDesigner(designer: IShapeDesigner, canvas: InteractiveCanvas): void;
        static fromObject(object?: any): Line;
    }
    class Polygon extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        protected fillDirection: NxtControl.Drawing.FillDirection;
        getFillDirection(): Drawing.FillDirection;
        setFillDirection(fillDirection: NxtControl.Drawing.FillDirection): this;
        protected fillPercent: number;
        getFillPercent(): number;
        setFillPercent(fillPercent: number): this;
        protected closed: boolean;
        getClosed(): boolean;
        setClosed(closed: boolean): this;
        protected points: NxtControl.Drawing.IPoint[];
        getPoints(): Drawing.IPoint[];
        private dpoints;
        protected minX: number;
        protected maxX: number;
        protected minY: number;
        protected maxY: number;
        load(options?: any): this;
        protected _calcDimensions(skipOffset: boolean): void;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _renderDashedStroke(ctx: CanvasRenderingContext2D): void;
        get(property: string): any;
        toObject(propertiesToInclude?: any[]): any;
        setDesigner(designer: IShapeDesigner, canvas: InteractiveCanvas): void;
        static fromObject(object?: any): Polygon;
    }
    class Polyline extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        closed: boolean;
        getClosed(): boolean;
        setClosed(closed: boolean): this;
        protected points: NxtControl.Drawing.IPoint[];
        getPoints(): Drawing.IPoint[];
        private dpoints;
        protected minX: number;
        protected maxX: number;
        protected minY: number;
        protected maxY: number;
        load(options?: any): this;
        protected _calcDimensions(skipOffset: boolean): void;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        get(property: string): any;
        setDesigner(designer: IShapeDesigner, canvas: InteractiveCanvas): void;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _renderDashedStroke(ctx: CanvasRenderingContext2D): void;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): Polyline;
    }
    class FreeText extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        protected text: string;
        getText(): string;
        setText(text: string): this;
        protected fontSize: number;
        getFontSize(): number;
        setFontSize(fontSize: number): this;
        protected fontWeight: number | string;
        getFontWeight(): number | string;
        setFontWeight(fontWeight: string | number): this;
        protected fontFamily: string;
        getFontFamily(): string;
        setFontFamily(fontFamily: string): this;
        protected textDecoration: string;
        getTextDecoration(): string;
        setTextDecoration(textDecoration: string): this;
        protected textAlign: string;
        getTextAlign(): string;
        setTextAlign(textAlign: string): this;
        protected fontStyle: string;
        getFontStyle(): string;
        setFontStyle(fontStyle: string): this;
        private lineHeight;
        protected textColor: NxtControl.Drawing.IColor;
        getTextColor(): NxtControl.Drawing.IColor;
        setTextColor(clr: NxtControl.Drawing.IColor): this;
        protected textBackgroundColor: NxtControl.Drawing.IColor;
        protected path: string;
        protected useNative: boolean;
        protected pen: NxtControl.Drawing.Pen;
        protected _dimensionAffectingProps: any;
        protected _totalLineHeight: number;
        protected _boundaries: {
            left?: number;
            width?: number;
            height?: number;
        }[];
        protected skipTextAlign: any;
        protected _skipFillStrokeCheck: any;
        static _reNewline: RegExp;
        protected __skipDimension: boolean;
        load(options?: any): this;
        protected _initDimensions(): void;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        setDesigner(designer: IShapeDesigner, canvas: InteractiveCanvas): void;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _renderViaCufon(ctx: CanvasRenderingContext2D): void;
        private _initDummyElementForCufon();
        _renderViaNative(ctx: CanvasRenderingContext2D): void;
        protected _renderText(ctx: CanvasRenderingContext2D, textLines: string[]): void;
        protected _translateForTextAlign(ctx: CanvasRenderingContext2D): void;
        protected _setBoundaries(ctx: CanvasRenderingContext2D, textLines: string[]): void;
        protected _setFillStyles(ctx: CanvasRenderingContext2D): void;
        protected _setTextStyles(ctx: CanvasRenderingContext2D): void;
        protected _getTextHeight(ctx: CanvasRenderingContext2D, textLines: string[]): number;
        protected _getTextWidth(ctx: CanvasRenderingContext2D, textLines: string[]): number;
        protected _renderChars(method: string, ctx: CanvasRenderingContext2D, chars: string, left: number, top: number, lineIndex?: number): void;
        protected _renderTextLine(method: string, ctx: CanvasRenderingContext2D, line: string, left: number, top: number, lineIndex: number): void;
        protected _getLeftOffset(): number;
        protected _getTopOffset(): number;
        protected _renderTextFill(ctx: CanvasRenderingContext2D, textLines: string[]): void;
        protected _renderTextStroke(ctx: CanvasRenderingContext2D, textLines: string[]): void;
        protected _getHeightOfLine(ctx?: CanvasRenderingContext2D, index?: number, textLines?: string[]): number;
        protected _renderTextBackground(ctx: any, textLines: any): void;
        protected _renderTextBoxBackground(ctx: CanvasRenderingContext2D): void;
        protected _renderTextLinesBackground(ctx: CanvasRenderingContext2D, textLines: string[]): void;
        protected _getLineLeftOffset(lineWidth: number): number;
        protected _getLineWidth(ctx: CanvasRenderingContext2D, line: string): number;
        protected _renderTextDecoration(ctx: CanvasRenderingContext2D, textLines: string[]): void;
        protected _getFontDeclaration(): string;
        render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        toObject(propertiesToInclude?: any): any;
        static fromObject(object?: any): FreeText;
    }
}
declare module NxtControl.GuiFramework {
    class Image extends Shape {
        static CSS_CANVAS: string;
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        autoSize: boolean;
        src: string;
        protected pen: NxtControl.Drawing.Pen;
        crossOrigin: string;
        imageLoaded: IEvent<NxtControl.IEventArgs>;
        protected _element: HTMLImageElement;
        constructor();
        load(options?: any): this;
        setSrc(value: string): this;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        getElement(): HTMLImageElement;
        protected _setElement(element: HTMLImageElement): this;
        setElement(element: HTMLImageElement): this;
        setCrossOrigin(value: any): this;
        getOriginalSize(): {
            width: number;
            height: number;
        };
        render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected myStroke(ctx: CanvasRenderingContext2D): void;
        protected _renderDashedStroke(ctx: CanvasRenderingContext2D): void;
        toObject(propertiesToInclude?: any[]): Object;
        getSrc(): string;
        clone(callback: Function, propertiesToInclude?: any[]): Shape;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _resetWidthHeight(): void;
        protected _initConfig(options?: any): void;
        protected _setWidthHeight(options: any): void;
        static fromObject(object?: any): Image;
        static pngCompression: number;
    }
}
declare module NxtControl.GuiFramework {
    class TextBase extends Shape {
        protected type: string;
        protected text: string;
        getText(): string;
        setText(txt: string): this;
        protected textColor: NxtControl.Drawing.IColor;
        getTextColor(): NxtControl.Drawing.IColor;
        setTextColor(clr: NxtControl.Drawing.IColor): this;
        protected textDisabledColor: NxtControl.Drawing.IColor;
        getTextDisabledColor(): NxtControl.Drawing.IColor;
        setTextDisabledColor(clr: NxtControl.Drawing.IColor): this;
        protected textAlign: string;
        getTextAlign(): string;
        setTextAlign(textAlign: string): this;
        protected textPadding: number;
        getTextPadding(): number;
        setTextPadding(textPadding: number): this;
        protected prefixColor: NxtControl.Drawing.IColor;
        getPrefixColor(): NxtControl.Drawing.IColor;
        setPrefixColor(clr: NxtControl.Drawing.IColor): this;
        protected backColor: NxtControl.Drawing.IColor;
        getBackColor(): NxtControl.Drawing.IColor;
        setBackColor(clr: NxtControl.Drawing.IColor): this;
        protected prefix: string;
        getPrefix(): string;
        setPrefix(txt: string): this;
        protected prefixPadding: number;
        getPrefixPadding(): number;
        setPrefixPadding(prefixPadding: number): this;
        protected suffixColor: NxtControl.Drawing.IColor;
        getSuffixColor(): NxtControl.Drawing.IColor;
        setSuffixColor(clr: NxtControl.Drawing.IColor): this;
        protected suffix: string;
        getSuffix(): string;
        setSuffix(txt: string): this;
        protected suffixPadding: number;
        getSuffixPadding(): number;
        setSuffixPadding(suffixPadding: number): this;
        protected isPrefixSuffixOutside: boolean;
        getIsPrefixSuffixOutside(): boolean;
        setIsPrefixSuffixOutside(isPrefixSuffixOutside: boolean): this;
        protected fontFamily: string;
        getFontFamily(): string;
        setFontFamily(fontFamily: string): this;
        protected fontWeight: number | string;
        getFontWeight(): number | string;
        setFontWeight(fontWeight: string | number): this;
        protected fontSize: number;
        getFontSize(): number;
        setFontSize(fontSize: number): this;
        protected fontStyle: string;
        getFontStyle(): string;
        setFontStyle(fontStyle: string): this;
        protected x: number;
        protected y: number;
        textChanged: IEvent<ITextEventArgs>;
        protected _drawTextBase: boolean;
        protected __isMouseDown: boolean;
        protected _textWidth: number;
        protected _prefixTextWidth: number;
        protected _suffixTextWidth: number;
        constructor();
        load(options?: any): this;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _calculateSize(): void;
        protected _getTextWidth(text: any): number;
        protected _setTextStyles(ctx: CanvasRenderingContext2D, color: NxtControl.Drawing.IColor): void;
        protected _getFontDeclaration(): string;
        protected _renderBackground(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _getTextPos(): number;
        protected _getTextColor(): Drawing.IColor;
        toObject(propertiesToInclude?: any[]): any;
    }
}
declare module NxtControl.GuiFramework {
    class DropDown extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        valueType: string;
        protected enabled: boolean;
        getEnabled(): boolean;
        setEnabled(enabled: boolean): this;
        protected selectedItem: any;
        protected selectedText: string;
        protected items: IListItem[];
        getItems(): IListItem[];
        setItems(items: IListItem[]): this;
        protected textColor: NxtControl.Drawing.IColor;
        getTextColor(): NxtControl.Drawing.IColor;
        setTextColor(clr: NxtControl.Drawing.IColor): this;
        protected textDisabledColor: NxtControl.Drawing.IColor;
        getTextDisabledColor(): NxtControl.Drawing.IColor;
        setTextDisabledColor(clr: NxtControl.Drawing.IColor): this;
        protected textPadding: number;
        getTextPadding(): number;
        setTextPadding(textPadding: number): this;
        protected backColor: NxtControl.Drawing.IColor;
        getBackColor(): NxtControl.Drawing.IColor;
        setBackColor(clr: NxtControl.Drawing.IColor): this;
        protected fontFamily: string;
        getFontFamily(): string;
        setFontFamily(fontFamily: string): this;
        protected fontWeight: number | string;
        getFontWeight(): number | string;
        setFontWeight(fontWeight: string | number): this;
        protected fontSize: number;
        getFontSize(): number;
        setFontSize(fontSize: number): this;
        protected fontStyle: string;
        getFontStyle(): string;
        setFontStyle(fontStyle: string): this;
        protected x: number;
        protected y: number;
        itemSelected: IEvent<ISelectedItemEventArgs>;
        protected _drawDropDown: boolean;
        protected __isMouseDown: boolean;
        protected _textWidth: number;
        constructor();
        initEvents(): void;
        disposeEvents(): void;
        load(options?: any): this;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _calculateSize(): void;
        protected _getTextWidth(text: any): number;
        protected _setTextStyles(ctx: CanvasRenderingContext2D, color: NxtControl.Drawing.IColor): void;
        protected _getFontDeclaration(): string;
        protected _renderBackground(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _getTextPos(): number;
        protected _getTextColor(): Drawing.IColor;
        protected _onClick(sender: any, options: ISystemEventArgs): void;
        private static menuContainer;
        private destroyDropDown();
        private createContextMenu(e);
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object: any): DropDown;
    }
}
declare module NxtControl.GuiFramework {
    class Button extends TextBase {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        protected enabled: boolean;
        getEnabled(): boolean;
        setEnabled(enabled: boolean): this;
        protected textColor: NxtControl.Drawing.IColor;
        protected textDisabledColor: NxtControl.Drawing.IColor;
        protected brush: NxtControl.Drawing.Brush;
        protected pushedBrush: NxtControl.Drawing.Brush;
        getPushedBrush(): Drawing.Brush;
        setPushedBrush(pushedBrush: NxtControl.Drawing.Brush): this;
        protected pen: NxtControl.Drawing.Pen;
        protected image: string;
        getImage(): string;
        setImage(image: string): this;
        protected _image: HTMLImageElement;
        protected imageAutoSize: boolean;
        getImageAutoSize(): boolean;
        setImageAutoSize(imageAutoSize: boolean): this;
        protected imagePadding: number;
        getImagePadding(): number;
        setImagePAdding(imagePadding: number): this;
        protected crossOrigin: string;
        protected textAlign: string;
        load(options?: any): this;
        initEvents(): void;
        disposeEvents(): void;
        protected _calculateSize(): void;
        getImageElement(): HTMLImageElement;
        setImageElement(image: any): this;
        setCrossOrigin(value: string): this;
        getOriginalSize(): {
            width: number;
            height: number;
        };
        protected _onMouseDown(sender: any, options: ISystemEventArgs): void;
        protected _onMouseUp(sender: any, options: ISystemEventArgs): void;
        protected _onClick(sender: any, options: ISystemEventArgs): void;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _getTextPos(): number;
        protected _render(ctx: CanvasRenderingContext2D): void;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object: any): Button;
    }
}
declare module NxtControl.GuiFramework {
    class TwoStateButton extends TextBase {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        protected checked: boolean;
        getChecked(): boolean;
        setChecked(checked: boolean): this;
        protected enabled: boolean;
        getEnabled(): boolean;
        setEnabled(enabled: boolean): this;
        protected trueText: string;
        getTrueText(): string;
        setTrueText(trueText: string): this;
        protected falseText: string;
        getFalseText(): string;
        setFalseText(falseText: string): this;
        protected trueTextColor: NxtControl.Drawing.IColor;
        getTrueTextColor(): Drawing.IColor;
        setTrueTextColor(trueTextColor: NxtControl.Drawing.IColor): this;
        protected trueTextDisabledColor: NxtControl.Drawing.IColor;
        getTrueTextDisabledColor(): Drawing.IColor;
        setTrueTextDisabledColor(trueTextDisabledColor: NxtControl.Drawing.IColor): this;
        protected falseTextColor: NxtControl.Drawing.IColor;
        getFalseTextColor(): Drawing.IColor;
        setFalseTextColor(falseTextColor: NxtControl.Drawing.IColor): this;
        protected falseTextDisabledColor: NxtControl.Drawing.IColor;
        getFalseTextDisabledColor(): Drawing.IColor;
        setFalseTextDisabledColor(falseTextDisabledColor: NxtControl.Drawing.IColor): this;
        protected brush: NxtControl.Drawing.Brush;
        protected pen: NxtControl.Drawing.Pen;
        protected switchBrush: NxtControl.Drawing.Brush;
        protected innerBorderPen: NxtControl.Drawing.Pen;
        protected radius: number;
        getRadius(): number;
        setRadius(radius: number): this;
        checkedChanged: IEvent<IEventArgs>;
        protected _textWidthFalse: number;
        constructor();
        load(options?: any): this;
        initEvents(): void;
        disposeEvents(): void;
        private _onMouseDown(sender, options);
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _calculateSize(): void;
        protected _render(ctx: CanvasRenderingContext2D): void;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): TwoStateButton;
    }
}
declare module NxtControl.GuiFramework {
    class Label extends TextBase {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        protected textColor: NxtControl.Drawing.IColor;
        prefixColor: NxtControl.Drawing.IColor;
        suffixColor: NxtControl.Drawing.IColor;
        backColor: NxtControl.Drawing.IColor;
        protected pen: NxtControl.Drawing.Pen;
        decimalPlacesCount: number;
        load(options?: any): this;
        toObject(propertiesToInclude?: any[]): Object;
        static fromObject(object: any): Label;
    }
}
declare module NxtControl.GuiFramework {
    class TextBox extends TextBase {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        protected enabled: boolean;
        getEnabled(): boolean;
        setEnabled(enabled: boolean): this;
        readOnly: boolean;
        protected pen: NxtControl.Drawing.Pen;
        keyboardTextType: TypeCode;
        keyboardNumberBase: NumberBase;
        sendValueOnLostFocus: boolean;
        decimalPlacesCount: number;
        minimum: number;
        maximum: number;
        useRange: boolean;
        constructor();
        load(options?: any): this;
        initEvents(): void;
        disposeEvents(): void;
        protected _onMouseDown(sender: any, options: ISystemEventArgs): void;
        protected _getTextColor(): Drawing.IColor;
        protected _onNewValueChanged(e: any): void;
        protected _checkDoubleValue(value: number): string;
        protected _checkStringValue(text: string): string;
        protected _onReturn(value: string): void;
        protected _onBlur(value: string): void;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): TextBox;
    }
}
declare module NxtControl.GuiFramework {
    class Tracker extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        value: number;
        protected referenceValue: number;
        getReferenceValue(): number;
        setReferenceValue(referenceValue: number): this;
        protected refPercent: number;
        protected useReferenceValue: boolean;
        getUseReferenceValue(): boolean;
        setUseReferenceValue(useReferenceValue: boolean): this;
        protected orientation: NxtControl.Drawing.Orientation;
        getOrientation(): Drawing.Orientation;
        setOrientation(orientation: NxtControl.Drawing.Orientation): this;
        protected pen: NxtControl.Drawing.Pen;
        protected minimum: number;
        getMinimum(): number;
        setMinimum(minimum: number): this;
        protected maximum: number;
        getMaximum(): number;
        setMaximum(maximum: number): this;
        protected maximumPosition: MaximumPosition;
        getMaximumPosition(): MaximumPosition;
        setMaximumPosition(maximumPosition: MaximumPosition): this;
        protected minMaxDecimalPlacesCount: number;
        getMinMaxDecimalPlacesCount(): number;
        setMinMaxDecimalPlacesCount(minMaxDecimalPlacesCount: number): this;
        protected useSnap: boolean;
        getUseSnap(): boolean;
        setUseSnap(useSnap: boolean): this;
        protected snap: number;
        getSnap(): number;
        setSnap(snap: number): this;
        protected trackValueBrush: NxtControl.Drawing.Brush;
        getTrackValueBrush(): Drawing.Brush;
        setTrackValueBrush(trackValueBrush: NxtControl.Drawing.Brush): this;
        protected radius: number;
        getRadius(): number;
        setRadius(radius: number): this;
        protected readOnly: boolean;
        getReadOnly(): boolean;
        setReadOnly(readOnly: boolean): this;
        protected tickLength: number;
        getTickLength(): number;
        setTickLength(tickLength: number): this;
        protected tickOffset: number;
        getTickOffset(): number;
        setTickOffset(tickOffset: number): this;
        protected tickThickness: number;
        getTickThickness(): number;
        setTickThickness(tickThickness: number): this;
        protected tickPercent: number;
        getTickPercent(): number;
        setTickPercent(tickPercent: number): this;
        tickColor: NxtControl.Drawing.IColor;
        getTickColor(): Drawing.IColor;
        setTickColor(tickColor: NxtControl.Drawing.IColor): this;
        protected fontFamily: string;
        getFontFamily(): string;
        setFontFamily(fontFamily: string): this;
        protected fontWeight: number | string;
        getFontWeight(): number | string;
        setFontWeight(fontWeight: string | number): this;
        protected fontSize: number;
        getFontSize(): number;
        setFontSize(fontSize: number): this;
        protected fontStyle: string;
        getFontStyle(): string;
        setFontStyle(fontStyle: string): this;
        protected textColor: NxtControl.Drawing.IColor;
        getTextColor(): NxtControl.Drawing.IColor;
        setTextColor(clr: NxtControl.Drawing.IColor): this;
        valueColor: NxtControl.Drawing.IColor;
        getValueColor(): NxtControl.Drawing.IColor;
        setValueColor(clr: NxtControl.Drawing.IColor): this;
        valueDecimalPlacesCount: number;
        getValueDecimalPlacesCount(): number;
        setValueDecimalPlacesCount(valueDecimalPlacesCount: number): this;
        protected showValue: boolean;
        getShowValue(): boolean;
        setShowValue(showValue: boolean): this;
        valueSuffix: string;
        getValueSuffix(): string;
        setValueSuffix(valueSuffix: number): this;
        protected showMinMax: boolean;
        getShowMinMax(): boolean;
        setShowMinMax(showMinMax: boolean): this;
        protected showTicks: boolean;
        getShowTicks(): boolean;
        setShowTicks(showTicks: boolean): this;
        protected trackLineThickness: number;
        getTrackLineThickness(): number;
        setTrackLineThickness(trackLineThickness: number): this;
        protected trackLineBrush: NxtControl.Drawing.Brush;
        getTrackLineBrush(): Drawing.Brush;
        setTrackLineBrush(trackLineBrush: NxtControl.Drawing.Brush): this;
        protected trackHandleLength: number;
        getTrackHandleLength(): number;
        setTrackHandleLength(trackHandleLength: number): this;
        protected trackHandleBrush: NxtControl.Drawing.Brush;
        getTrackHandleBrush(): Drawing.Brush;
        setTrackHandleBrush(trackHandleBrush: NxtControl.Drawing.Brush): this;
        protected trackHandlePen: NxtControl.Drawing.Pen;
        getTrackHandlePen(): Drawing.Pen;
        setTrackHandlePen(trackHandlePen: NxtControl.Drawing.Pen): this;
        protected mouseMoveValueThreshold: number;
        getMouseMoveValueThreshold(): number;
        setMouseMoveValueThreshold(mouseMoveValueThreshold: number): this;
        valueChanged: IEvent<IEventArgs>;
        protected x: number;
        protected y: number;
        protected _valuesSetByMouseDown: boolean;
        protected __isMouseDown: boolean;
        protected _trackHandleLT: NxtControl.Drawing.IPoint;
        protected _trackHandleRB: NxtControl.Drawing.IPoint;
        protected _trackLineLT: NxtControl.Drawing.IPoint;
        protected _trackLineRB: NxtControl.Drawing.IPoint;
        protected __centerOffset: NxtControl.Drawing.IPoint;
        protected _fillPercent: number;
        protected _minimumTextWidth: number;
        protected _maximumTextWidth: number;
        protected _valueTextWidth: number;
        protected _minimumTextOffset: number;
        protected _maximumTextOffset: number;
        constructor();
        load(options?: any): this;
        dispose(): this;
        initEvents(): void;
        disposeEvents(): void;
        private _onMouseDown(sender, options);
        private _onMouseMove(sender, options);
        private snapValue(val);
        private _onMouseUp(sender, options);
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _setMouseValue(value: number): void;
        protected _calculateValues(): void;
        protected _calculateSize(): void;
        protected _getTextWidth(text: any): number;
        protected _setTextStyles(ctx: CanvasRenderingContext2D, color: NxtControl.Drawing.IColor): void;
        protected _getFontDeclaration(): string;
        protected _calculateTrackHandlePosition(): void;
        protected _render(ctx: CanvasRenderingContext2D): void;
        protected _renderTrackLine(ctx: CanvasRenderingContext2D, x: number, y: number, w: number, h: number, r: number, isInPathGroup: boolean, brush: NxtControl.Drawing.Brush, stroke: boolean): void;
        protected _renderTrackHandle(ctx: CanvasRenderingContext2D, x: number, y: number, isInPathGroup: boolean): void;
        protected _renderTicks(ctx: CanvasRenderingContext2D, x: number, y: number, w: number, h: number, isInPathGroup: boolean): void;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): Tracker;
    }
}
declare module NxtControl.GuiFramework {
    interface IDateEventArgs extends IEventArgs {
        date: Date;
    }
    class DateTimePicker extends Image {
        protected type: string;
        showTime: boolean;
        timeMode: number;
        format: string;
        protected cal: Calendar;
        dateTimeChanged: IEvent<IDateEventArgs>;
        constructor();
        load(options?: any): this;
        show(left: number, top: number): void;
        static fromObject(object?: any): DateTimePicker;
    }
    class Calendar extends Shape {
        dtp: DateTimePicker;
        date: number;
        month: number;
        year: number;
        hours: number;
        minutes: string | number;
        seconds: string | number;
        aMorPM: string;
        static monthName: string[];
        static weekDayName: string[];
        weekChar: number;
        cellWidth: number;
        dateSeparator: string;
        timeMode: number;
        showLongMonth: boolean;
        showMonthYear: boolean;
        monthYearColor: NxtControl.Drawing.IColor;
        weekHeadColor: NxtControl.Drawing.IColor;
        sundayColor: NxtControl.Drawing.IColor;
        saturdayColor: NxtControl.Drawing.IColor;
        weekDayColor: NxtControl.Drawing.IColor;
        fontColor: NxtControl.Drawing.IColor;
        todayColor: NxtControl.Drawing.IColor;
        selDateColor: NxtControl.Drawing.IColor;
        yrSelColor: NxtControl.Drawing.IColor;
        showTime: boolean;
        format: string;
        win: HTMLFormElement;
        dateTimeChanged: IEvent<IDateEventArgs>;
        closed: IEvent<IEventArgs>;
        constructor();
        load(options?: any): this;
        getMonthIndex(shortMonthName: string): number;
        incYear(): void;
        decYear(): void;
        switchMth(intMth: number): void;
        setHour(intHour: string): void;
        setMinute(intMin: string): void;
        setSecond(intSec: string): void;
        setAmPm(pvalue: string): void;
        getShowHour(): any;
        getMonthName(isLong: any): string;
        getMonDays(): number;
        isLeapYear(): boolean;
        formatDate(pDate: any): string;
        show(left: number, top: number): void;
        private _isDescendant(child);
        private _onMyClick(e);
        getMinutes(): number;
        getSeconds(): number;
        setDay(date: any): void;
        hide(): void;
        updateCal(): void;
        appendCal(): any;
        genCell(pValue?: string, pHighLight?: boolean, pColor?: NxtControl.Drawing.IColor): any;
    }
    var currentCalendar: Calendar;
}
declare module NxtControl.GuiFramework {
    class TrendSeries {
        trend: Trend;
        tagName: string;
        min: Date;
        max: Date;
        archiveId: number;
        constructor(trend: Trend, tagName: string);
        runtimeValueChanged(value: number, time: number): void;
        search(data: any, low: any, high: any, x: any): any;
        addHistoryValues(options: any): boolean;
        archiveAnswer(options: any): boolean;
    }
    class Trend extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        enabled: boolean;
        timeSpan: number;
        zoomPercent: number;
        showLegend: boolean;
        protected series: any;
        protected timeAxis: any;
        protected leftAxis: any;
        protected rightAxis: any;
        protected trendContainer: HTMLDivElement;
        protected trendHolder: HTMLDivElement;
        protected trendLegend: HTMLDivElement;
        protected toolBar: HTMLDivElement;
        protected _showLegend: HTMLImageElement;
        protected _zoomOut: HTMLImageElement;
        protected _zoomIn: HTMLImageElement;
        protected _zoomReset: HTMLImageElement;
        protected _dtp: HTMLImageElement;
        private trend;
        protected _cal: Calendar;
        private _timer;
        private updateLegendTimeout;
        protected _inMouseMove: boolean;
        protected latestPosition: {
            x: number;
            y: number;
        };
        private _needArchiveQuery;
        _historyAnswerCount: number;
        protected _objects: any[];
        protected _tipText: string;
        load(options?: any): this;
        initEvents(): void;
        dispose(): this;
        disposeEvents(): void;
        setDesigner(designer: IShapeDesigner, canvas: InteractiveCanvas): void;
        private _insertLegend();
        private _updateLegend();
        private _onHover(event, pos);
        private _onScroll(event);
        stop(): void;
        private _onShowLegend();
        zoomOut(): void;
        zoomIn(): void;
        zoomReset(): void;
        private _onZoom();
        private _onZoomReset();
        protected _getScreenCordinates(obj: HTMLElement): {
            x?: number;
            y?: number;
        };
        private _openCalendar();
        private _onCalClosed(sender, ea);
        private _onDateTimeChanged(sender, ea);
        _queryArchiveIfNecessary(): void;
        private _mergeRecursive(dst, src);
        private _timeFormatter(value, axis);
        private _addInvisibleCurve(data);
        protected _render(ctx: CanvasRenderingContext2D): void;
        get(property: string): any;
        addCurve(tagName: string, data: any, options?: any): jquery.flot.dataSeries[];
        private _loadImage(img, url);
        protected _find(pathParts: string[], index: number): any;
        private _startTrend();
        private _interval();
        private _setTimeSpan(timeSpan?);
        private _setTrendBounds(left, top, width, height);
        protected _set(key: string, value: any, invalidate?: boolean): this;
        private _setYAxisOptions(options, value);
        private _getTagNames();
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): Trend;
    }
}
declare module NxtControl.GuiFramework {
    class Symbol extends Group {
        protected type: string;
        protected designerType: string;
        protected oneShape: boolean;
        json: string | any;
        protected _left: number;
        protected _top: number;
        protected src: any;
        constructor();
        load(options?: any): this;
        dispose(): this;
        initEvents(): void;
        disposeEvents(): void;
        protected _onMouseDown(sender: any, options: ISystemEventArgs): void;
        protected _onMouseMove(sender: any, options: ISystemEventArgs): void;
        protected _onMouseUp(sender: any, options: ISystemEventArgs): void;
        protected _onClick(sender: any, options: ISystemEventArgs): void;
        protected _doOpenFaceplates(target: Shape, options: any, mouseDown: boolean): void;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        setSymbolLeftTop(left?: number, top?: number): this;
        protected _resetBounds(key: string, value: any): void;
        protected _loadFromSrc(callback?: Function): boolean;
        loadFromJSON(json: any, callback?: Function): Symbol;
        _enlivenObjects(objects: any[], callback?: Function, reviver?: Function): void;
        getSrc(): any;
        setSrc(value: any): void;
        find(path: string): Shape;
        protected _find(pathParts: string[], index: number): Shape;
        protected _findTarget(pointer: any): Shape;
        getSymbolPath(symbolPath?: string): any;
        protected delegatedProperties: any;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): Symbol;
    }
}
declare module NxtControl.GuiFramework {
    class Graphic extends Group {
        protected type: string;
        protected designerType: string;
        protected oneShape: boolean;
        json: string | any;
        protected _left: number;
        protected _top: number;
        protected src: any;
        constructor();
        load(options?: any): this;
        dispose(): this;
        initEvents(): void;
        disposeEvents(): void;
        protected _onMouseDown(sender: any, options: ISystemEventArgs): void;
        protected _onMouseMove(sender: any, options: ISystemEventArgs): void;
        protected _onMouseUp(sender: any, options: ISystemEventArgs): void;
        protected _onClick(sender: any, options: ISystemEventArgs): void;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _resetBounds(key: string, value: any): void;
        protected _loadFromSrc(callback?: Function): boolean;
        loadFromJSON(json: any, callback?: Function): Graphic;
        _enlivenObjects(objects: any[], callback?: Function, reviver?: Function): void;
        getSrc(): any;
        setSrc(value: any): void;
        find(path: string): Shape;
        protected _find(pathParts: string[], index: number): Shape;
        protected _findTarget(pointer: any): Shape;
        protected delegatedProperties: any;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): Graphic;
    }
}
declare module NxtControl.GuiFramework {
    class RuntimeSymbol extends Symbol {
        protected type: string;
        tagName: string;
        protected csClass: string;
        private _faceplates;
        constructor();
        _doOpenFaceplates(target: Shape, options: any, isDown: any): void;
        _onCloseDialog(sender: any, options: any): void;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object: any): RuntimeSymbol;
    }
}
declare module NxtControl.GuiFramework {
    class RuntimeBaseSymbol extends Symbol implements IHMIObject {
        protected type: string;
        protected csClass: string;
        tagName: string;
        valueType: string;
        callbackdata: IHMICallback;
        callback: IHMICallback;
        value: any;
        onValueChanged(value: any, time: any): boolean;
        runtimeValueChanged(value: any, time: any): boolean;
        static getValue(ranges: any, propertyName: any, value: any): any;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): RuntimeBaseSymbol;
    }
}
declare module NxtControl.GuiFramework {
    interface IAlarmInfo {
        Id?: number;
        TableId?: number;
        State?: string;
        DeviceName?: string;
        Path?: string;
        Origin?: string;
        Value?: string;
        PairIndex?: number;
        Present?: boolean;
        Color?: string;
        Shortcut?: string;
        Ack?: string;
        Alias?: string;
        Text?: string;
        Interval?: string;
        CameTime?: number;
        GoneTime?: number;
        CameAckTime?: number;
        GoneAckTime?: number;
        CameAckUser?: string;
        GoneAckUser?: string;
        InfoValue?: string;
        AlarmClassName?: string;
    }
    class AlarmControl extends Shape {
        protected type: string;
        protected csClass: string;
        protected designerType: string;
        alarmType: string;
        alarmColumns: {
            Column?: string;
            Visible?: boolean;
            HeaderText?: string;
            Width?: number;
        }[];
        alarmId: number;
        alarmInfos: any;
        alarmColors: {
            [name: string]: {
                color: NxtControl.Drawing.BlinkColor;
                count: number;
            };
        };
        pagerSize: number;
        protected alarmControl: HTMLDivElement;
        protected table: JQuery;
        constructor();
        load(options?: any): this;
        dispose(): this;
        private hasClass(element, cls);
        initEvents(): void;
        protected _render(ctx: CanvasRenderingContext2D, noTransform?: boolean): void;
        protected _set(key: string, value: any, invalidate?: boolean): this;
        protected _setAlarmBounds(left?: number, top?: number, width?: number, height?: number): void;
        protected _insertAlarmTable(): void;
        protected _onAckClick(e: any): void;
        setDesigner(designer: IShapeDesigner, canvas: InteractiveCanvas): void;
        alarmUpdate(action: string, index: number, alarmInfo: any): void;
        protected _detachColor(color: string): void;
        protected _defineRow(row: any, alarmInfo: IAlarmInfo, oldColor: string): any[];
        private _startColorChange();
        private _stopColorChange(sender, eventArgs);
        alarmUpdateColor(index: number, alarmInfo: IAlarmInfo): void;
        alarmUpdateClear(): void;
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): AlarmControl;
    }
}
declare module NxtControl.GuiFramework {
    interface IFaceplateEventArgs extends IEventArgs {
        faceplate: string;
    }
    class Dialog {
        protected type: string;
        private faceplateName;
        private faceplateInstance;
        private divContainer;
        private _onMouseDownPosition;
        private __isMouseDown;
        onclose: IEvent<IFaceplateEventArgs>;
        private canvas;
        constructor(faceplateName: string, faceplateClass: string, symbolPath: any);
        close(): void;
        bringToFront(): void;
        _rectCloseDialog_mouseDown(): void;
        _fpRectMove_mouseDown(sender: any, e: any): void;
        protected _onMouseMove(sender: any, e: IShapeEventArgs): void;
        protected _onMouseUp(sender: any, e: IShapeEventArgs): void;
    }
}
declare module NxtControl.GuiFramework {
    class Faceplate extends CanvasCore {
        protected type: string;
        tagName: string;
        protected csClass: string;
        title: string;
        protected db: {
            input: any;
            output: any;
        };
        protected id: number;
        protected symbolPath: string;
        protected dialog: Dialog;
        private connectorInputs;
        private connectorOutputs;
        private _faceplates;
        protected _valueChangedTimer: number;
        initialize(options?: any): void;
        dispose(): void;
        initEvents(): void;
        disposeEvents(): void;
        initializeCommunication(symbolPath: string): void;
        _doOpenFaceplates(target: Shape, options: any, isDown: any): void;
        _onCloseDialog(sender: any, options: any): void;
        _onObjectMouseDown(sender: any, options: any): void;
        _onObjectMouseUp(sender: any, options: any): void;
        _onObjectClick(sender: any, options: any): void;
        setFaceplateSubscribeData(subscribeData: any): void;
        private _deviceConnected(sender, options);
        _valueChangedTick(): void;
        _valueChanged(sender: any, options: NxtControl.Services.IValueEventArgs): void;
        _addHistoryValues(sender: any, options: any): void;
        _archiveAnswer(sender: any, options: any): void;
        close(): void;
        getSymbolPath(symbolPath?: string): any;
        toObject(propertiesToInclude?: any[]): Object;
        static fromObject(): Faceplate;
    }
}
declare module NxtControl.GuiFramework {
    class CTRoseRectangle extends Rectangle {
        protected type: string;
        setTooltip(tooltip: string): void;
        protected _render(ctx: CanvasRenderingContext2D): void;
        static fromObject(object?: any): CTRoseRectangle;
    }
    class CanvasTopologyRose extends Symbol {
        protected type: string;
        private siblingLeft;
        private siblingRight;
        private levelUp;
        private levelDown;
        protected home: Shape;
        load(options?: any): this;
        initEvents(): void;
        disposeEvents(): void;
        protected _canvasChanged(): void;
        protected _homeClick(): void;
        protected _levelUpClick(): void;
        protected _siblingLeftClick(): void;
        protected _siblingRightClick(): void;
        protected _levelDownClick(): void;
        static fromObject(object: any): CanvasTopologyRose;
    }
}
declare module NxtControl.GuiFramework {
    enum CanvasTopologyType {
        Top = 0,
        Sibling = 1,
        Child = 2,
    }
    class CanvasTopologyPanel extends Symbol {
        protected type: string;
        topologyType: CanvasTopologyType;
        protected inCreateButtons: boolean;
        protected btnCountToShow: number;
        protected btnColor: NxtControl.Drawing.IColor;
        protected currentBtnColor: NxtControl.Drawing.IColor;
        btnWidth: number;
        btnHeight: number;
        startIndex: number;
        protected currentParentCanvasId: number;
        realBtnCountToShow: number;
        protected toLeft: CTRoseRectangle;
        protected toRight: CTRoseRectangle;
        private _buttons;
        private _inCreateButtons;
        load(options?: any): this;
        initEvents(): void;
        disposeEvents(): void;
        _createButtons(): void;
        private _manage(data, currentCanvasId);
        _canvasChanged(sender: any, ea: IEventArgs): void;
        private _btnClick(sender, e);
        private _toLeftClick(sender, e);
        private _toRightClick(sender, e);
        toObject(propertiesToInclude?: any[]): any;
        static fromObject(object?: any): CanvasTopologyPanel;
    }
}
