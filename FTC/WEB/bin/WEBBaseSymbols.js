var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                (function (LedShape) {
                    LedShape[LedShape["Ellipse"] = 0] = "Ellipse";
                    LedShape[LedShape["Rectangle"] = 1] = "Rectangle";
                })(Base.LedShape || (Base.LedShape = {}));
                var LedShape = Base.LedShape;
                /**
                 * System.WEB.Symbols.Base.Led class
                 * @class System.WEB.Symbols.Base.Led
                 * @extends NxtControl.GuiFramework.Symbol
                 */
                var Led = (function (_super) {
                    __extends(Led, _super);
                    function Led() {
                        _super.apply(this, arguments);
                    }
                    /**
                     * @param {Object} [options] Options object
                     * @return {Object} thisArg
                     */
                    Led.prototype.load = function (options) {
                        options = options || {};
                        options.valueType = options.valueType || 'bool';
                        _super.prototype.load.call(this, options);
                        this.ledR = this.find('ledR');
                        this.ledFrameR = this.find('ledFrameR');
                        this.ledE = this.find('ledE');
                        this.ledFrameE = this.find('ledFrameE');
                        this._set('ledShape', this.ledShape);
                        this._set('colorFrame', this.colorFrame);
                        this._set('frameSize', this.frameSize);
                        if (!this.hasOwnProperty('ranges'))
                            this._onInitialized();
                        this._inInitialize = false;
                        return this;
                    };
                    Led.prototype._onInitialized = function () {
                        if (!this.hasOwnProperty('ranges')) {
                            this.ranges = {};
                            this.ranges.range = [];
                            if (this.valueType === 'bool') {
                                this.ranges.defaultProps = { color: NxtControl.Drawing.Color.fromObject('LedDefaultColor') };
                                this.ranges.range[0] = { value: false, props: { color: NxtControl.Drawing.Color.fromObject('LedFalseColor') } };
                                this.ranges.range[1] = { value: true, props: { color: NxtControl.Drawing.Color.fromObject('LedTrueColor') } };
                            }
                            else if (this.valueType === 'float' || this.valueType === 'double') {
                                this.ranges.defaultProps = { color: NxtControl.Drawing.Color.fromObject('LedDefaultColor') };
                                this.ranges.range[0] = { incMin: true, incMax: true, min: null, max: null, props: { color: NxtControl.Drawing.Color.fromObject('LedDefaultColor') } };
                            }
                            else {
                                this.ranges.defaultProps = { color: NxtControl.Drawing.Color.fromObject('LedDefaultColor') };
                            }
                        }
                    };
                    Led.prototype._set = function (key, value, invalidate) {
                        if (invalidate === void 0) { invalidate = false; }
                        if (key == 'ledShape') {
                            this.ledShape = value;
                            if (this.ledR) {
                                this.ledR.setVisible(value !== LedShape.Ellipse);
                                this.ledFrameR.setVisible(value !== LedShape.Ellipse);
                                this.ledE.setVisible(value === LedShape.Ellipse);
                                this.ledFrameE.setVisible(value === LedShape.Ellipse);
                            }
                        }
                        else if (key === 'colorFrame') {
                            this.colorFrame = NxtControl.Drawing.Color.fromObject(value);
                            if (this.ledFrameE) {
                                this.ledFrameE.setPen(new NxtControl.Drawing.Pen({ color: this.colorFrame, width: this.ledFrameE.getPen().width }));
                                this.ledFrameR.setPen(new NxtControl.Drawing.Pen({ color: this.colorFrame, width: this.ledFrameR.getPen().width }));
                                this.ledE.setPen(new NxtControl.Drawing.Pen({ color: this.colorFrame, width: this.ledE.getPen().width }));
                                this.ledR.setPen(new NxtControl.Drawing.Pen({ color: this.colorFrame, width: this.ledR.getPen().width }));
                            }
                        }
                        else if (key == 'frameSize') {
                            if (value < 0)
                                value = 0;
                            if (value > 100)
                                value = 100;
                            this.frameSize = value;
                            if (value != 0 && value != 100) {
                                var pw = 6 * ((value) / 100);
                                if (this.ledFrameE) {
                                    this.ledFrameE.setPen(new NxtControl.Drawing.Pen({ color: this.ledFrameE.getPen().color, width: pw }));
                                    this.ledFrameR.setPen(new NxtControl.Drawing.Pen({ color: this.ledFrameR.getPen().color, width: pw }));
                                    this.ledE.setPen(new NxtControl.Drawing.Pen({ color: this.ledE.getPen().color, width: pw }));
                                    this.ledR.setPen(new NxtControl.Drawing.Pen({ color: this.ledR.getPen().color, width: pw }));
                                }
                            }
                        }
                        else {
                            _super.prototype._set.call(this, key, value, invalidate);
                        }
                        return this;
                    };
                    Led.prototype.onValueChanged = function (value, time) {
                        if (this.value === value)
                            return false;
                        this.value = value;
                        var color;
                        if (this.ranges) {
                            color = NxtControl.GuiFramework.RuntimeBaseSymbol.getValue(this.ranges, 'color', value);
                        }
                        else {
                            color = NxtControl.Drawing.Color.fromObject(value === true ? NxtControl.Drawing.Color.fromName('LedTrueColor') : NxtControl.Drawing.Color.fromName('LedTrueColor'));
                        }
                        this.ledFrameE.setBrush(new NxtControl.Drawing.SolidBrush({ color: color }));
                        this.ledFrameR.setBrush(new NxtControl.Drawing.SolidBrush({ color: color }));
                        return true;
                    };
                    /**
                     * Returns object representation of an instance
                     * @param {Array} propertiesToInclude
                     * @return {Object} object representation of an instance
                     */
                    Led.prototype.toObject = function (propertiesToInclude) {
                        return NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                            ranges: this.ranges,
                            ledShape: this.ledShape,
                            colorFrame: this.colorFrame,
                            frameSize: this.frameSize
                        });
                    };
                    /**
                     * Returns {@link System.WEB.Symbols.Base.Led} instance from an object representation
                     * @static
                     * @param {Object} object Object to create a symbol from
                     * @return {System.WEB.Symbols.Base.Led} An instance of System.WEB.Symbols.Base.Led
                     */
                    Led.fromObject = function (object) {
                        return new Led().load(object);
                    };
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.Led')
                    ], Led.prototype, "type", void 0);
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.Led<ValueType>')
                    ], Led.prototype, "csClass", void 0);
                    __decorate([
                        System.Browsable
                    ], Led.prototype, "ranges", void 0);
                    __decorate([
                        System.DefaultValue(LedShape.Ellipse),
                        System.Browsable
                    ], Led.prototype, "ledShape", void 0);
                    __decorate([
                        System.DefaultValue(NxtControl.Drawing.Color.fromName('LedFrameColor')),
                        System.Browsable
                    ], Led.prototype, "colorFrame", void 0);
                    __decorate([
                        System.DefaultValue(33),
                        System.Browsable
                    ], Led.prototype, "frameSize", void 0);
                    return Led;
                })(NxtControl.GuiFramework.RuntimeBaseSymbol);
                Base.Led = Led;
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                Base.Led.prototype.json =
                    //Start JSON/////////////////////////////////////////////
                    '{"objects":[{"type":"NxtControl.GuiFramework.Ellipse","name":"ledFrameE","width":12,"height":12,"brush":{"color":"LedFalseColor"},"pen":{"color":"LedFrameColor","width":2}},{"type":"NxtControl.GuiFramework.Ellipse","name":"ledE","width":12,"height":12,"brush":{"orient":9,"colorStops":[{"color":"Transparent","offset":0},{"color":"Transparent","offset":0.19},{"color":"Black","offset":1}]},"pen":{"color":"LedFrameColor","width":2}},{"type":"NxtControl.GuiFramework.Rectangle","name":"ledFrameR","width":12,"height":12,"brush":{"color":"LedFalseColor"},"pen":{"color":"LedFrameColor","width":2}},{"type":"NxtControl.GuiFramework.Rectangle","name":"ledR","width":12,"height":12,"brush":{"orient":9,"colorStops":[{"color":"Transparent","offset":0},{"color":"Transparent","offset":0.19},{"color":"Black","offset":1}]},"pen":{"color":"LedFrameColor","width":2}}],"background":[204,204,204,1],"overlay":"Transparent"}';
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                /**
                 * System.WEB.Symbols.Base.Execute class
                 * @extends NxtControl.GuiFramework.RuntimeBaseSymbol
                 */
                var Execute = (function (_super) {
                    __extends(Execute, _super);
                    function Execute() {
                        _super.apply(this, arguments);
                    }
                    /**
                     * @param {Object} [options] Options object
                     * @return {Object} thisArg
                     */
                    Execute.prototype.load = function (options) {
                        options = options || {};
                        this.valueChanged = new NxtControl.event();
                        _super.prototype.load.call(this, options);
                        return this;
                    };
                    /**
                     * Returns object representation of an instance
                     * @param {Array} propertiesToInclude
                     * @return {Object} object representation of an instance
                     */
                    Execute.prototype.toObject = function (propertiesToInclude) {
                        var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                            text: this.text
                        });
                        if (!this.includeDefaultValues) {
                            this._removeDefaultValues(object);
                        }
                        return object;
                    };
                    Execute.prototype.onValueChanged = function (value) {
                        if (this.value === value)
                            return false;
                        this.value = value;
                        this.valueChanged.trigger(this, { value: value });
                        return true;
                    };
                    /**
                     * Returns {@link System.WEB.Symbols.Base.Execute} instance from an object representation
                     * @static
                     * @param {Object} object Object to create a symbol from
                     * @return {System.WEB.Symbols.Base.Execute} An instance of System.WEB.Symbols.Base.Execute
                     */
                    Execute.fromObject = function (object) {
                        return new Execute().load(object);
                    };
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.Execute')
                    ], Execute.prototype, "type", void 0);
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.Execute')
                    ], Execute.prototype, "csClass", void 0);
                    __decorate([
                        System.DefaultValue(false)
                    ], Execute.prototype, "drawable", void 0);
                    __decorate([
                        System.DefaultValue('Execute'),
                        System.Browsable
                    ], Execute.prototype, "text", void 0);
                    return Execute;
                })(NxtControl.GuiFramework.RuntimeBaseSymbol);
                Base.Execute = Execute;
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                Base.Execute.prototype.json =
                    //Start JSON/////////////////////////////////////////////
                    '{"objects":[],"background":[204,204,204,1],"overlay":"Transparent"}';
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                /**
                 * System.WEB.Symbols.Base.Image class
                 * @class System.WEB.Symbols.Base.Image
                 * @extends NxtControl.GuiFramework.Symbol
                 */
                var Image = (function (_super) {
                    __extends(Image, _super);
                    function Image() {
                        _super.apply(this, arguments);
                        this.isAsync = true;
                    }
                    /**
                     * @param {Object} [options] Options object
                     * @return {Object} thisArg
                     */
                    Image.prototype.load = function (options) {
                        options = options || {};
                        options.valueType = options.valueType || 'bool';
                        options.autoSize = options.autoSize || this.autoSize;
                        this._inInitialize = true;
                        _super.prototype.load.call(this, options);
                        this.imageShape = this.find('ImageShape');
                        if (!this.hasOwnProperty('ranges')) {
                            this._onImageLoaded = this._onImageLoaded.bind(this);
                            this.imageShape.imageLoaded.add(this._onImageLoaded);
                        }
                        else {
                            this.imageShape.set('src', this.ranges.defaultProps.image);
                        }
                        this._inInitialize = false;
                        return this;
                    };
                    Image.prototype._onImageLoaded = function () {
                        this.imageShape.imageLoaded.remove(this._onImageLoaded);
                        this._onInitialized();
                        this.onCreated && this.onCreated();
                    };
                    Image.prototype._onInitialized = function () {
                        if (!this.hasOwnProperty('ranges')) {
                            var src = this.imageShape.getSrc();
                            this.ranges = {};
                            this.ranges.defaultProps = { image: src };
                            this.ranges.range = [];
                            if (this.valueType === 'bool') {
                                this.ranges.range[0] = { value: false, props: { image: src } };
                                this.ranges.range[1] = { value: true, props: { image: src } };
                            }
                            else if (this.valueType === 'float' || this.valueType === 'double') {
                                this.ranges.range[0] = { incMin: true, incMax: true, min: null, max: null, props: { image: src } };
                            }
                        }
                    };
                    Image.prototype._set = function (key, value, invalidate) {
                        if (invalidate === void 0) { invalidate = false; }
                        if (key === 'value' && this.designer !== null) {
                            this.onValueChanged(value, null);
                            return this;
                        }
                        if (key == 'ranges') {
                            this.ranges = value;
                            this.imageShape.set('src', this.ranges.defaultProps.image);
                            invalidate && this.invalidate();
                        }
                        else if (key == 'autoSize') {
                            this.autoSize = value;
                            this.imageShape.set(key, value);
                            invalidate && this.invalidate();
                        }
                        else
                            _super.prototype._set.call(this, key, value, invalidate);
                        return this;
                    };
                    /**
                     * Returns object representation of an instance
                     * @param {Array} propertiesToInclude
                     * @return {Object} object representation of an instance
                     */
                    Image.prototype.toObject = function (propertiesToInclude) {
                        return NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                            ranges: this.ranges,
                            autoSize: this.autoSize
                        });
                    };
                    Image.prototype.onValueChanged = function (value, time) {
                        if (this.value === value)
                            return false;
                        this.value = value;
                        if (this.ranges) {
                            var img = NxtControl.GuiFramework.RuntimeBaseSymbol.getValue(this.ranges, 'image', value);
                            this.imageShape.set('src', img);
                        }
                        return true;
                    };
                    /**
                     * Returns {@link System.WEB.Symbols.Base.Image} instance from an object representation
                     * @static
                     * @memberOf NxtControl.GuiHTMLFramework.IECSymbol
                     * @param {Object} object Object to create a symbol from
                     * @return {System.WEB.Symbols.Base.Image} An instance of System.WEB.Symbols.Base.Image
                     */
                    Image.fromObject = function (object) {
                        return new Image().load(object);
                    };
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.Image')
                    ], Image.prototype, "type", void 0);
                    __decorate([
                        System.DefaultValue('NxtControl.GuiFramework.ImageDesigner')
                    ], Image.prototype, "designerType", void 0);
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.Image<ValueType>')
                    ], Image.prototype, "csClass", void 0);
                    __decorate([
                        System.DefaultValue(true),
                        System.Browsable
                    ], Image.prototype, "autoSize", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], Image.prototype, "src", void 0);
                    __decorate([
                        System.Browsable
                    ], Image.prototype, "ranges", void 0);
                    return Image;
                })(NxtControl.GuiFramework.RuntimeBaseSymbol);
                Base.Image = Image;
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                Base.Image.prototype.json =
                    //Start JSON/////////////////////////////////////////////
                    '{"objects":[{"type":"NxtControl.GuiFramework.Image","name":"ImageShape","width":80,"height":80,"src":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAACHDwAAjA8AAP1SAACBQAAAfXkAAOmLAAA85QAAGcxzPIV3AAAKNWlDQ1BzUkdCIElFQzYxOTY2LTIuMQAASMedlndUVNcWh8+9d3qhzTDSGXqTLjCA9C4gHQRRGGYGGMoAwwxNbIioQEQREQFFkKCAAaOhSKyIYiEoqGAPSBBQYjCKqKhkRtZKfHl57+Xl98e939pn73P32XuftS4AJE8fLi8FlgIgmSfgB3o401eFR9Cx/QAGeIABpgAwWempvkHuwUAkLzcXerrICfyL3gwBSPy+ZejpT6eD/0/SrFS+AADIX8TmbE46S8T5Ik7KFKSK7TMipsYkihlGiZkvSlDEcmKOW+Sln30W2VHM7GQeW8TinFPZyWwx94h4e4aQI2LER8QFGVxOpohvi1gzSZjMFfFbcWwyh5kOAIoktgs4rHgRm4iYxA8OdBHxcgBwpLgvOOYLFnCyBOJDuaSkZvO5cfECui5Lj25qbc2ge3IykzgCgaE/k5XI5LPpLinJqUxeNgCLZ/4sGXFt6aIiW5paW1oamhmZflGo/7r4NyXu7SK9CvjcM4jW94ftr/xS6gBgzIpqs+sPW8x+ADq2AiB3/w+b5iEAJEV9a7/xxXlo4nmJFwhSbYyNMzMzjbgclpG4oL/rfzr8DX3xPSPxdr+Xh+7KiWUKkwR0cd1YKUkpQj49PZXJ4tAN/zzE/zjwr/NYGsiJ5fA5PFFEqGjKuLw4Ubt5bK6Am8Kjc3n/qYn/MOxPWpxrkSj1nwA1yghI3aAC5Oc+gKIQARJ5UNz13/vmgw8F4psXpjqxOPefBf37rnCJ+JHOjfsc5xIYTGcJ+RmLa+JrCdCAACQBFcgDFaABdIEhMANWwBY4AjewAviBYBAO1gIWiAfJgA8yQS7YDApAEdgF9oJKUAPqQSNoASdABzgNLoDL4Dq4Ce6AB2AEjIPnYAa8AfMQBGEhMkSB5CFVSAsygMwgBmQPuUE+UCAUDkVDcRAPEkK50BaoCCqFKqFaqBH6FjoFXYCuQgPQPWgUmoJ+hd7DCEyCqbAyrA0bwwzYCfaGg+E1cBycBufA+fBOuAKug4/B7fAF+Dp8Bx6Bn8OzCECICA1RQwwRBuKC+CERSCzCRzYghUg5Uoe0IF1IL3ILGUGmkXcoDIqCoqMMUbYoT1QIioVKQ21AFaMqUUdR7age1C3UKGoG9QlNRiuhDdA2aC/0KnQcOhNdgC5HN6Db0JfQd9Dj6DcYDIaG0cFYYTwx4ZgEzDpMMeYAphVzHjOAGcPMYrFYeawB1g7rh2ViBdgC7H7sMew57CB2HPsWR8Sp4sxw7rgIHA+XhyvHNeHO4gZxE7h5vBReC2+D98Oz8dn4Enw9vgt/Az+OnydIE3QIdoRgQgJhM6GC0EK4RHhIeEUkEtWJ1sQAIpe4iVhBPE68QhwlviPJkPRJLqRIkpC0k3SEdJ50j/SKTCZrkx3JEWQBeSe5kXyR/Jj8VoIiYSThJcGW2ChRJdEuMSjxQhIvqSXpJLlWMkeyXPKk5A3JaSm8lLaUixRTaoNUldQpqWGpWWmKtKm0n3SydLF0k/RV6UkZrIy2jJsMWyZf5rDMRZkxCkLRoLhQWJQtlHrKJco4FUPVoXpRE6hF1G+o/dQZWRnZZbKhslmyVbJnZEdoCE2b5kVLopXQTtCGaO+XKC9xWsJZsmNJy5LBJXNyinKOchy5QrlWuTty7+Xp8m7yifK75TvkHymgFPQVAhQyFQ4qXFKYVqQq2iqyFAsVTyjeV4KV9JUCldYpHVbqU5pVVlH2UE5V3q98UXlahabiqJKgUqZyVmVKlaJqr8pVLVM9p/qMLkt3oifRK+g99Bk1JTVPNaFarVq/2ry6jnqIep56q/ojDYIGQyNWo0yjW2NGU1XTVzNXs1nzvhZei6EVr7VPq1drTltHO0x7m3aH9qSOnI6XTo5Os85DXbKug26abp3ubT2MHkMvUe+A3k19WN9CP16/Sv+GAWxgacA1OGAwsBS91Hopb2nd0mFDkqGTYYZhs+GoEc3IxyjPqMPohbGmcYTxbuNe408mFiZJJvUmD0xlTFeY5pl2mf5qpm/GMqsyu21ONnc332jeaf5ymcEyzrKDy+5aUCx8LbZZdFt8tLSy5Fu2WE5ZaVpFW1VbDTOoDH9GMeOKNdra2Xqj9WnrdzaWNgKbEza/2BraJto22U4u11nOWV6/fMxO3Y5pV2s3Yk+3j7Y/ZD/ioObAdKhzeOKo4ch2bHCccNJzSnA65vTC2cSZ79zmPOdi47Le5bwr4urhWuja7ybjFuJW6fbYXd09zr3ZfcbDwmOdx3lPtKe3527PYS9lL5ZXo9fMCqsV61f0eJO8g7wrvZ/46Pvwfbp8Yd8Vvnt8H67UWslb2eEH/Lz89vg98tfxT/P/PgAT4B9QFfA00DQwN7A3iBIUFdQU9CbYObgk+EGIbogwpDtUMjQytDF0Lsw1rDRsZJXxqvWrrocrhHPDOyOwEaERDRGzq91W7109HmkRWRA5tEZnTdaaq2sV1iatPRMlGcWMOhmNjg6Lbor+wPRj1jFnY7xiqmNmWC6sfaznbEd2GXuKY8cp5UzE2sWWxk7G2cXtiZuKd4gvj5/munAruS8TPBNqEuYS/RKPJC4khSW1JuOSo5NP8WR4ibyeFJWUrJSBVIPUgtSRNJu0vWkzfG9+QzqUvia9U0AV/Uz1CXWFW4WjGfYZVRlvM0MzT2ZJZ/Gy+rL1s3dkT+S453y9DrWOta47Vy13c+7oeqf1tRugDTEbujdqbMzfOL7JY9PRzYTNiZt/yDPJK817vSVsS1e+cv6m/LGtHlubCyQK+AXD22y31WxHbedu799hvmP/jk+F7MJrRSZF5UUfilnF174y/ariq4WdsTv7SyxLDu7C7OLtGtrtsPtoqXRpTunYHt897WX0ssKy13uj9l4tX1Zes4+wT7hvpMKnonO/5v5d+z9UxlfeqXKuaq1Wqt5RPXeAfWDwoOPBlhrlmqKa94e4h+7WetS212nXlR/GHM44/LQ+tL73a8bXjQ0KDUUNH4/wjowcDTza02jV2Nik1FTSDDcLm6eORR67+Y3rN50thi21rbTWouPguPD4s2+jvx064X2i+yTjZMt3Wt9Vt1HaCtuh9uz2mY74jpHO8M6BUytOdXfZdrV9b/T9kdNqp6vOyJ4pOUs4m3924VzOudnzqeenL8RdGOuO6n5wcdXF2z0BPf2XvC9duex++WKvU++5K3ZXTl+1uXrqGuNax3XL6+19Fn1tP1j80NZv2d9+w+pG503rm10DywfODjoMXrjleuvyba/b1++svDMwFDJ0dzhyeOQu++7kvaR7L+9n3J9/sOkh+mHhI6lH5Y+VHtf9qPdj64jlyJlR19G+J0FPHoyxxp7/lP7Th/H8p+Sn5ROqE42TZpOnp9ynbj5b/Wz8eerz+emCn6V/rn6h++K7Xxx/6ZtZNTP+kv9y4dfiV/Kvjrxe9rp71n/28ZvkN/NzhW/l3x59x3jX+z7s/cR85gfsh4qPeh+7Pnl/eriQvLDwG/eE8/s6uL5TAAAACXBIWXMAAAsSAAALEgHS3X78AAAIJUlEQVR4Xu2dO6xVRRSGecn7URBAo7wTIKECAiQ00JAABWADhEdFQ6QAg4iFJnaQGKMJFihQYALaWJhogYqFiXY2FihITHwGfMRHhKig1/8jZ5M5m/+cu2fP7HPPvVp8gTt75l9r5p6798yaNfuMGhgYGDKujR41W2wRT4mz4n3xqbghborbLW61yj4Tl8QZ8YSg7Syn3StsYVOos5PFdvGK+FwMZOKaeFk8KiY7201hC3OiDo0Rm8R58btwA5ATPrmvCWyOcT7lxBbmQM5PEYcFnw7X0V6A7cfFVOdjDmxhCnJ2knhS/Chcp4aCHwQ+TXQ+p2AL6yIHd4mvhOtEP/Cl2OF8r4stjEVOzRUXW04OB94R811fYrGFMciR3eLXlmPDiV/EXtenGGxhFWR8gmA64pwbTpwWE1wfq2ALB0MGmQB/2HJgJPCRmOP6Ohi2sBsytFjknAT3C0x5Frs+d8MWdkIGlopvWgZHIt+KJa7vnbCFDgkvEt+1DI1k+IAscmPgsIVlJMg9byhXFL3mqpjtxqKMLQyREE/bkfTAqAp9HvTpbAtDJELoyBn4L3DajUmILSyQwL6SYFNw3yGCQvCBKMoyMUMQ/hovpouFYoN4TLwqvhZOKzd73NgU2EJQw/miyRXGz+JFsU6Mdj50gzZilXheNBm4YAzmOR/AFoIasV50gqkQWSbENM3ZrYO0iAA9K5y9HFx0dsEWqsHOkkAO/hInxHRnMwVpPiQ+Ec5uLnY62/cVqCK/zdwhqStiTdlWDqS7QPRiZcSYTCrbb/sBVOlY0CgHb4tsf64h0uVh06uHCRwt+9D2gypMFURvXeM6nBJjQxu5kO4K8X3LTq9gbKaEfpSd4ubuGtaBMFH007UK0uXJzVPc2W2aw6EvoVPsnuVarr0pGtkRk+5G0YvdvU4wRvf6FjrGBNY1iIWN8exPWpDuNvFHy85QsqnwKXTuQqlSHcgiWFlo5kS6e1v6zm6vOV/4VTjHkinHn8WJQjgn0j0g/g7sDDWM1d0MiMJB0i1cxRhYYWTfwJYm06p/Wjb6ia34VzjJdMNViuFQ2PEcSPN4yUY/cQofC0cJILpKVWFKkS2pR1rMCF5qaTfBW+KnUlksV/AVZ2eVLtThZHkQ6iKtcYJwlbOTAx6WD4gXgrK6zMZhcuzcxRjWusGIRTpEv98IdHPDreruHE7/EgpzdWLYjBBJN+5iVXh4JE+apcFMoMn0kOfEvZUR/xepO4zHECIz1F2syr05UV2kQfT5g0AzN093sHuuVC+Ws4i8VyqM5Yhzripqzz3440AvJ0x/Os4OdO1gULcOlxC5XCqMZaNzrgpq+4hItd+JO2K/s1ug6+yxuLZVuYzI9VJhLEudc4OhdmzUfxHo5ORPMWgeoOqwUeXaV+UGImTAu4tVmemc64baLBekUTi9VOjPFme3jOpNC9rV4SYiqQv0qICp6q8WTe2i/SbWO7sO1R0btK3DHUS4V7iLVak8gKq7XtBJp5MKv5TVzm4nVD91AG8jwrEAd7EqlfY7VI8Je+rtohPcDpY7u91Qm5mBRh1uIcJE2F2sykLnXIjq7BDc2F37VHgQVc6mClE70vWcZlWuI5I6jdjgnCvQ9f0i9TbRCXx/2NmtgtqyPeB0q3J3GsPZM3exKgedc6Brh0RTsTwm30nn5NT+SKBXh3cRSV3Knevg3DOlejlh2TfD2Y1BGhw/c/pVubuUS91IZ2O7vEhn4e7q5oCAQ3LsURrEHFPv/0cR2lwqrMOqllNMC5o8+kCoa3x5MOognbWBbl22IET6rrsYA8FJgpQ5dvY6QZB1nBuMOkjrZKBdl1mFGMk/rkJVCI8TJnfXckB4P9tGvbSIPaZmNlxFqxDMsanUFMfDzudAmswOnK0Y2jaVtpYu9gNMf46FHc+BNEmgSn14wHb0CtFcG+u5YBP9QNjxXEiXJE9nM4b2jfWWcOqcKBdEh5JPUTqku7Kl7+zGcKHQDMVzJRelQOLQtsKnnEiXTH8Sn5zdWGxyUc70tjrwZ1F7e6Ab0qVvpNw5u7H49LaWIc5puEZNw5RiXehLLqTLyohkT2e3Dj7BEnSRN23kTPGtAmm6K0I/ciFdVkY5p2iMTVsCVZtBUIXUjfYYWEcvK/uQA+my30GCu7Nbl/umVW0/gCo1cczBwdGEBWX7OZDuGpG6uipT7ZgDqGITB21COBTzoLOdgjR50jLP41CPs5tCtYM2BWrQZJ4Kx7Lu+23WRVr8uXLCIMcKwxF31AvUaJ5o8rAhu2gcFCRLqu5hQ447cGCxySMPjEHHd8zYwgI13BMINQkPE8JVHGUl3YKMAf4cOerKMpPkI04lMdlnqsXR2F69u2GfG5sCWxgigZxzqOHGGTcmIbYwRCL/H/nvgi0sIyGi1ql51MMJlmt5XjpRIEGyqUbyO2MKeLVL3teeFEh4iWgqq6of4AMSla5nC7shA7z6aSijNk3BLSo6RcQWDoYMzRG8sMs5MhzhgVHpnlfGFlZBBnk6j4QpDvvYvX39XYiMc4qSlxk65/oZVhi7XZ9isIWxyBHeMdPUa1KagHX+XNeXWGxhXeQUeYC86NU53Q8QktrlfK+LLUxBDk4UBGV7HdnuBoELfMoWASqwhTmQs2xgE2IayikPtgk+tL1pIye2MCdynh0xoiivi9R87Cqwu8ceNzaz5dN0whY2hTpEaIovDOCLA3J+MtkeQJMUlZH1ZQTdUGeLr8Pgqy14TyHpxnzlBZFlMvrJIgA+uZSxMc5XZpBVy1do0LbWBDgPA6P+BdGw4A2lQqpfAAAAAElFTkSuQmCC","crossOrigin":"","autoSize":true}],"background":"CanvasBackColor","overlay":"Transparent"}';
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                /**
                 * System.WEB.Symbols.Base.Label class
                 * @extends NxtControl.GuiFramework.RuntimeBaseSymbol
                 */
                var Label = (function (_super) {
                    __extends(Label, _super);
                    function Label() {
                        _super.apply(this, arguments);
                    }
                    /**
                     * Constructor
                     * @param {Object} [options] Options object
                     * @return {Object} thisArg
                     */
                    Label.prototype.load = function (options) {
                        options = options || {};
                        options.width = options.width || 150;
                        options.valueType = options.valueType || 'float';
                        options.text = options.text || '${Value}';
                        this._inInitialize = true;
                        _super.prototype.load.call(this, options);
                        this._onInitialized();
                        this._inInitialize = false;
                        if (this.valueType !== 'string')
                            this.csClass = 'System.WEB.Symbols.Base.Label<ValueType>';
                        else
                            this.csClass = 'System.WEB.Symbols.Base.Label';
                        this._textPattern = this.text;
                        return this;
                    };
                    Label.prototype._onInitialized = function () {
                        if (!this.hasOwnProperty('ranges') && this.valueType !== 'string') {
                            this.ranges = {};
                            this.ranges.defaultProps = { text: '${Value}', pen: this.pen, backColor: this.backColor, textColor: this.textColor };
                            this.ranges.range = [];
                            if (this.valueType === 'bool') {
                                this.ranges.range[0] = { value: false, props: { text: '${Value}' } };
                                this.ranges.range[1] = { value: true, props: { text: '${Value}' } };
                            }
                            else if (this.valueType === 'float' || this.valueType === 'double') {
                                this.ranges.range[0] = { incMin: true, incMax: true, min: null, max: null, props: { text: '${Value}' } };
                            }
                        }
                    };
                    Label.prototype._set = function (key, value, invalidate) {
                        if (invalidate === void 0) { invalidate = false; }
                        if (key === 'ranges') {
                            this.ranges = value;
                        }
                        else
                            _super.prototype._set.call(this, key, value, invalidate);
                        if (this.designer !== null) {
                            if ((key === 'ranges' || key === 'backColor' || key == 'text' || key === 'textColor' || key === 'pen') && this.hasOwnProperty('ranges')) {
                                if (this.ranges.defaultProps.text !== this.text) {
                                    if (key === 'ranges')
                                        _super.prototype._set.call(this, 'text', this.ranges.defaultProps.text);
                                    else
                                        this.ranges.defaultProps.text = this.text;
                                }
                                if (this.ranges.defaultProps.backColor !== this.backColor) {
                                    if (key === 'ranges')
                                        _super.prototype._set.call(this, 'backColor', this.ranges.defaultProps.backColor);
                                    else
                                        this.ranges.defaultProps.backColor = this.backColor;
                                }
                                if (this.ranges.defaultProps.textColor !== this.textColor) {
                                    if (key === 'ranges')
                                        _super.prototype._set.call(this, 'textColor', this.ranges.defaultProps.textColor);
                                    else
                                        this.ranges.defaultProps.textColor = this.textColor;
                                }
                                if (this.ranges.defaultProps.pen !== this.pen) {
                                    if (key === 'ranges')
                                        _super.prototype._set.call(this, 'pen', this.ranges.defaultProps.pen);
                                    else
                                        this.ranges.defaultProps.pen = this.pen;
                                }
                            }
                        }
                        return this;
                    };
                    /**
                     * Returns object representation of an instance
                     * @param {Array} propertiesToInclude
                     * @return {Object} object representation of an instance
                     */
                    Label.prototype.toObject = function (propertiesToInclude) {
                        var ext = { tagName: this.tagName, valueType: this.valueType, numberBase: this.numberBase };
                        if (this.hasOwnProperty('ranges'))
                            ext.ranges = this.ranges;
                        var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), ext);
                        if (!this.includeDefaultValues) {
                            this._removeDefaultValues(object);
                        }
                        return object;
                    };
                    Label.prototype.runtimeValueChanged = function (value, time) {
                        if (this.value === value)
                            return false;
                        this.value = value;
                        this._setProperties(value);
                        return true;
                    };
                    Label.prototype._setProperties = function (value) {
                        var pattern = this._textPattern || '${Value}';
                        if (this.valueType === 'float' || this.valueType === 'double')
                            this._valueChangedText = value.toFixed(this.decimalPlacesCount);
                        else if (this.numberBase != NxtControl.GuiFramework.NumberBase.Decimal)
                            this._valueChangedText = NxtControl.Util.Convert.fromValueTypeToString(NxtControl.GuiFramework.TypeCode.fromTypeToTypeCode(this.valueType), value, this.numberBase);
                        else
                            this._valueChangedText = value.toString();
                        if (this.ranges) {
                            pattern = NxtControl.GuiFramework.RuntimeBaseSymbol.getValue(this.ranges, 'text', value).toString();
                            var backColor = NxtControl.GuiFramework.RuntimeBaseSymbol.getValue(this.ranges, 'backColor', value);
                            backColor = NxtControl.Drawing.Color.fromObject(backColor);
                            this._set('backColor', backColor, true);
                            var textColor = NxtControl.GuiFramework.RuntimeBaseSymbol.getValue(this.ranges, 'textColor', value);
                            textColor = NxtControl.Drawing.Color.fromObject(textColor);
                            this._set('textColor', textColor, true);
                            var pen = NxtControl.GuiFramework.RuntimeBaseSymbol.getValue(this.ranges, 'stroke', value);
                            this._set('pen', pen);
                        }
                        this._valueChangedText = pattern.replace('${Value}', this._valueChangedText);
                        this._set('text', this._valueChangedText);
                    };
                    /**
                     * Returns {@link System.WEB.Symbols.Base.Label} instance from an object representation
                     * @static
                     * @param {Object} object Object to create a symbol from
                     * @return {System.WEB.Symbols.Base.Label} An instance of System.WEB.Symbols.Base.Label
                     */
                    Label.fromObject = function (object) {
                        return new Label().load(object);
                    };
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.Label')
                    ], Label.prototype, "type", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], Label.prototype, "tagName", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], Label.prototype, "valueType", void 0);
                    __decorate([
                        System.Browsable
                    ], Label.prototype, "ranges", void 0);
                    __decorate([
                        System.DefaultValue(NxtControl.GuiFramework.NumberBase.Decimal)
                    ], Label.prototype, "numberBase", void 0);
                    return Label;
                })(NxtControl.GuiFramework.Label);
                Base.Label = Label;
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                /**
                 * System.WEB.Symbols.Base.TextBox class
                 * @extends NxtControl.GuiFramework.TextBox
                 */
                var TextBox = (function (_super) {
                    __extends(TextBox, _super);
                    function TextBox() {
                        _super.apply(this, arguments);
                    }
                    /**
                     * @param {Object} [options] Options object
                     * @return {Object} thisArg
                     */
                    TextBox.prototype.load = function (options) {
                        options = options || {};
                        options.width = options.width || 150;
                        options.valueType = options.valueType || 'float';
                        this._inInitialize = true;
                        this._valueChangedText = null;
                        _super.prototype.load.call(this, options);
                        this._inInitialize = false;
                        this._onTextChanged = this._onTextChanged.bind(this);
                        this.textChanged.add(this._onTextChanged);
                        return this;
                    };
                    /**
                     * Dispose event handlers
                     */
                    TextBox.prototype.disposeEvents = function () {
                        _super.prototype.disposeEvents.call(this);
                        this.textChanged.remove(this._onTextChanged);
                    };
                    TextBox.prototype._set = function (key, value, invalidate) {
                        if (invalidate === void 0) { invalidate = false; }
                        if (key === 'numberBase') {
                            if (this.keyboardTextType === NxtControl.GuiFramework.TypeCode.String ||
                                this.keyboardTextType === NxtControl.GuiFramework.TypeCode.Single ||
                                this.keyboardTextType === NxtControl.GuiFramework.TypeCode.Double)
                                return this;
                            if (value === this.numberBase)
                                return this;
                            this._set('numberBase', value, invalidate);
                            if (!this._inInitialize) {
                                if (this._valueChangedText != null) {
                                    this._setText(this.value);
                                }
                            }
                            invalidate && this.invalidate();
                        }
                        else {
                            _super.prototype._set.call(this, key, value, invalidate);
                        }
                        return this;
                    };
                    TextBox.prototype.runtimeValueChanged = function (value, time) {
                        if (this.value === value)
                            return false;
                        this.value = value;
                        this._setText(value);
                        return true;
                    };
                    TextBox.prototype._onTextChanged = function (sender, ea) {
                        var commService = NxtControl.Services.CommunicationService.instance || null;
                        if (commService == null)
                            return;
                        if (this.text != this._valueChangedText) {
                            var result = NxtControl.Util.Convert.tryFromStringToValueType(NxtControl.GuiFramework.TypeCode.fromTypeToTypeCode(this.valueType), this.text, this.numberBase);
                            if (result.result) {
                                this.value = result.value;
                                if (this.callbackdata)
                                    commService.setValue(this.callbackdata, this.value);
                                this._setText(this.value);
                            }
                            else {
                                this.textChanged.remove(this._onTextChanged);
                                this._set('text', this._valueChangedText, true);
                                this.textChanged.add(this._onTextChanged);
                            }
                        }
                    };
                    TextBox.prototype._setText = function (value) {
                        if (this.valueType === 'float' || this.valueType === 'double')
                            this._valueChangedText = value.toFixed(this.decimalPlacesCount);
                        else if (this.numberBase != NxtControl.GuiFramework.NumberBase.Decimal)
                            this._valueChangedText = NxtControl.Util.Convert.fromValueTypeToString(NxtControl.GuiFramework.TypeCode.fromTypeToTypeCode(this.valueType), value, this.numberBase);
                        else
                            this._valueChangedText = value.toString();
                        this.textChanged.remove(this._onTextChanged);
                        this._set('text', this._valueChangedText, true);
                        this.textChanged.add(this._onTextChanged);
                    };
                    /**
                     * Returns object representation of an instance
                     * @param {Array} propertiesToInclude
                     * @return {Object} object representation of an instance
                     */
                    TextBox.prototype.toObject = function (propertiesToInclude) {
                        var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), { tagName: this.tagName, valueType: this.valueType });
                        if (!this.includeDefaultValues) {
                            this._removeDefaultValues(object);
                        }
                        return object;
                    };
                    /**
                     * Returns {@link System.WEB.Symbols.Base.TextBox} instance from an object representation
                     * @static
                     * @param {Object} object Object to create a symbol from
                     * @return {System.WEB.Symbols.Base.TextBox} An instance of System.WEB.Symbols.Base.TextBox
                     */
                    TextBox.fromObject = function (object) {
                        return new TextBox().load(object);
                    };
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.TextBox')
                    ], TextBox.prototype, "type", void 0);
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.TextBox')
                    ], TextBox.prototype, "csClass", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], TextBox.prototype, "tagName", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], TextBox.prototype, "valueType", void 0);
                    __decorate([
                        System.DefaultValue(NxtControl.GuiFramework.NumberBase.Decimal)
                    ], TextBox.prototype, "numberBase", void 0);
                    return TextBox;
                })(NxtControl.GuiFramework.TextBox);
                Base.TextBox = TextBox;
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                /**
                 * System.WEB.Symbols.Base.FreeText class
                 * @extends NxtControl.GuiFramework.FreeText
                 */
                var FreeText = (function (_super) {
                    __extends(FreeText, _super);
                    function FreeText() {
                        _super.apply(this, arguments);
                    }
                    /**
                     * @param {Object} [options] Options object
                     * @return {Object} thisArg
                     */
                    FreeText.prototype.load = function (options) {
                        options = options || {};
                        options.width = options.width || 150;
                        options.valueType = options.valueType || 'float';
                        this._inInitialize = true;
                        this._valueChangedText = null;
                        _super.prototype.load.call(this, options);
                        this._inInitialize = false;
                        return this;
                    };
                    FreeText.prototype._set = function (key, value, invalidate) {
                        if (invalidate === void 0) { invalidate = false; }
                        if (key === 'numberBase') {
                            if (value === this.numberBase)
                                return this;
                            this.numberBase = value;
                            if (!this._inInitialize) {
                                if (this._valueChangedText != null) {
                                    this._setText(this.value);
                                }
                            }
                            invalidate && this.invalidate();
                        }
                        else {
                            _super.prototype._set.call(this, key, value, invalidate);
                        }
                        return this;
                    };
                    FreeText.prototype.runtimeValueChanged = function (value, time) {
                        if (this.value === value)
                            return false;
                        this.value = value;
                        this._setText(value);
                        return true;
                    };
                    FreeText.prototype._setText = function (value) {
                        if (this.valueType === 'float' || this.valueType === 'double')
                            this._valueChangedText = value.toFixed(this.decimalPlacesCount);
                        else if (this.numberBase != NxtControl.GuiFramework.NumberBase.Decimal)
                            this._valueChangedText = NxtControl.Util.Convert.fromValueTypeToString(NxtControl.GuiFramework.TypeCode.fromTypeToTypeCode(this.valueType), value, this.numberBase);
                        else
                            this._valueChangedText = value.toString();
                        this._set('text', this._valueChangedText, true);
                    };
                    /**
                     * Returns object representation of an instance
                     * @param {Array} propertiesToInclude
                     * @return {Object} object representation of an instance
                     */
                    FreeText.prototype.toObject = function (propertiesToInclude) {
                        var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), { tagName: this.tagName,
                            valueType: this.valueType,
                            numberBase: this.numberBase,
                            decimalPlacesCount: this.decimalPlacesCount });
                        if (!this.includeDefaultValues) {
                            this._removeDefaultValues(object);
                        }
                        return object;
                    };
                    /**
                     * Returns {@link System.WEB.Symbols.Base.FreeText} instance from an object representation
                     * @static
                     * @param {Object} object Object to create a symbol from
                     * @return {System.WEB.Symbols.Base.FreeText} An instance of System.WEB.Symbols.Base.FreeText
                     */
                    FreeText.fromObject = function (object) {
                        return new FreeText().load(object);
                    };
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.FreeText')
                    ], FreeText.prototype, "type", void 0);
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.FreeText')
                    ], FreeText.prototype, "csClass", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], FreeText.prototype, "tagName", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], FreeText.prototype, "valueType", void 0);
                    __decorate([
                        System.DefaultValue(NxtControl.GuiFramework.NumberBase.Decimal)
                    ], FreeText.prototype, "numberBase", void 0);
                    __decorate([
                        System.DefaultValue(2),
                        System.Browsable
                    ], FreeText.prototype, "decimalPlacesCount", void 0);
                    return FreeText;
                })(NxtControl.GuiFramework.FreeText);
                Base.FreeText = FreeText;
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                /**
                 * System.WEB.Symbols.Base.SwitchButton class
                 * @extends NxtControl.GuiFramework.TwoStateButton
                 */
                var SwitchButton = (function (_super) {
                    __extends(SwitchButton, _super);
                    function SwitchButton() {
                        _super.apply(this, arguments);
                    }
                    /**
                     * @param {Object} [options] Options object
                     * @return {Object} thisArg
                     */
                    SwitchButton.prototype.load = function (options) {
                        options = options || {};
                        options.width = options.width || 150;
                        options.valueType = options.valueType || 'bool';
                        this._inInitialize = true;
                        _super.prototype.load.call(this, options);
                        this._inInitialize = false;
                        this._onCheckedChanged = this._onCheckedChanged.bind(this);
                        this.checkedChanged.add(this._onCheckedChanged);
                        return this;
                    };
                    /**
                     * Returns {@link System.WEB.Symbols.Base.SwitchButton} instance from an object representation
                     * @static
                     * @param {Object} object Object to create a symbol from
                     * @return {System.WEB.Symbols.Base.SwitchButton} An instance of System.WEB.Symbols.Base.SwitchButton
                     */
                    SwitchButton.fromObject = function (object) {
                        return new SwitchButton().load(object);
                    };
                    /**
                     * Dispose event handlers
                     */
                    SwitchButton.prototype.disposeEvents = function () {
                        _super.prototype.disposeEvents.call(this);
                        this.checkedChanged.remove(this._onCheckedChanged);
                    };
                    SwitchButton.prototype.runtimeValueChanged = function (value, time) {
                        if (this.value === value)
                            return false;
                        this.value = value;
                        this.checkedChanged.remove(this._onCheckedChanged);
                        this._set('checked', value ? true : false, true);
                        this.checkedChanged.add(this._onCheckedChanged);
                        return true;
                    };
                    SwitchButton.prototype._onCheckedChanged = function (sender, ea) {
                        var commService = NxtControl.Services.CommunicationService.instance || null;
                        if (commService == null)
                            return;
                        if (this.value != this.checked) {
                            this.value = this.checked;
                            if (this.callbackdata)
                                commService.setValue(this.callbackdata, this.value);
                        }
                    };
                    /**
                     * Returns object representation of an instance
                     * @param {Array} propertiesToInclude
                     * @return {Object} object representation of an instance
                     */
                    SwitchButton.prototype.toObject = function (propertiesToInclude) {
                        var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                            tagName: this.tagName, valueType: this.valueType
                        });
                        if (!this.includeDefaultValues) {
                            this._removeDefaultValues(object);
                        }
                        return object;
                    };
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.SwitchButton')
                    ], SwitchButton.prototype, "type", void 0);
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.SwitchButton')
                    ], SwitchButton.prototype, "csClass", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], SwitchButton.prototype, "tagName", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], SwitchButton.prototype, "valueType", void 0);
                    return SwitchButton;
                })(NxtControl.GuiFramework.TwoStateButton);
                Base.SwitchButton = SwitchButton;
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
/*
 * Created by nxtSTUDIO.
 * User: kovaivo
 * Date: 12/6/2013
 * Time: 3:16 PM
 *
 */
var System;
(function (System) {
    var WEB;
    (function (WEB) {
        var Symbols;
        (function (Symbols) {
            var Base;
            (function (Base) {
                /**
                 * System.WEB.Symbols.Base.Button class
                 * @extends NxtControl.GuiFramework.Button
                 */
                var Button = (function (_super) {
                    __extends(Button, _super);
                    function Button() {
                        _super.apply(this, arguments);
                    }
                    /**
                     * Returns {@link System.WEB.Symbols.Base.Button} instance from an object representation
                     * @static
                     * @param {Object} object Object to create a symbol from
                     * @return {System.WEB.Symbols.Base.Button} An instance of System.WEB.Symbols.Base.Button
                     */
                    Button.fromObject = function (object) {
                        return new Button().load(object);
                    };
                    /**
                     * @param {Object} [options] Options object
                     * @return {Object} thisArg
                     */
                    Button.prototype.load = function (options) {
                        options = options || {};
                        options.width = options.width || 150;
                        options.valueType = options.valueType || 'bool';
                        this._inInitialize = true;
                        _super.prototype.load.call(this, options);
                        this._inInitialize = false;
                        this._onMyMouseDown = this._onMyMouseDown.bind(this);
                        this._onMyMouseUp = this._onMyMouseUp.bind(this);
                        return this;
                    };
                    /**
                      * Initializes event handlers
                      */
                    Button.prototype.initEvents = function () {
                        _super.prototype.initEvents.call(this);
                        this.mousedown.add(this._onMyMouseDown);
                        this.mouseup.add(this._onMyMouseUp);
                    };
                    /**
                     * Dispose event handlers
                     */
                    Button.prototype.disposeEvents = function () {
                        _super.prototype.disposeEvents.call(this);
                        this.mousedown.remove(this._onMyMouseDown);
                        this.mouseup.remove(this._onMyMouseUp);
                    };
                    Button.prototype.runtimeValueChanged = function (value, time) {
                        return false;
                    };
                    Button.prototype._onMyMouseDown = function (sender, e) {
                        if (this.enabled == false || this.valueMouseDown == null)
                            return;
                        var commService = NxtControl.Services.CommunicationService.instance || null;
                        if (commService == null)
                            return;
                        if (this.callbackdata)
                            commService.setValue(this.callbackdata, this.valueMouseDown);
                    };
                    Button.prototype._onMyMouseUp = function (sender, e) {
                        if (this.enabled == false || this.valueMouseUp == null)
                            return;
                        var commService = NxtControl.Services.CommunicationService.instance || null;
                        if (commService == null)
                            return;
                        if (this.callbackdata)
                            commService.setValue(this.callbackdata, this.valueMouseUp);
                    };
                    /**
                     * Returns object representation of an instance
                     * @param {Array} propertiesToInclude
                     * @return {Object} object representation of an instance
                     */
                    Button.prototype.toObject = function (propertiesToInclude) {
                        var object = NxtControl.Util.object.extend(_super.prototype.toObject.call(this, propertiesToInclude), {
                            tagName: this.tagName, valueType: this.valueType, valueMouseDown: this.valueMouseDown, valueMouseUp: this.valueMouseUp
                        });
                        if (!this.includeDefaultValues) {
                            this._removeDefaultValues(object);
                        }
                        return object;
                    };
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.Button')
                    ], Button.prototype, "type", void 0);
                    __decorate([
                        System.DefaultValue('System.WEB.Symbols.Base.Button<ValueType>')
                    ], Button.prototype, "csClass", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], Button.prototype, "tagName", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], Button.prototype, "valueType", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], Button.prototype, "valueMouseDown", void 0);
                    __decorate([
                        System.DefaultValue(null),
                        System.Browsable
                    ], Button.prototype, "valueMouseUp", void 0);
                    return Button;
                })(NxtControl.GuiFramework.Button);
                Base.Button = Button;
            })(Base = Symbols.Base || (Symbols.Base = {}));
        })(Symbols = WEB.Symbols || (WEB.Symbols = {}));
    })(WEB = System.WEB || (System.WEB = {}));
})(System || (System = {}));
