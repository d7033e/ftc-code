declare var designer: any;
declare module NxtControl.GuiFramework {
    class DesignCanvas extends InteractiveCanvas {
        constructor(element?: string | HTMLCanvasElement, options?: any);
        callDesigner(value: boolean): void;
        protected static BlueColor: NxtControl.Drawing.IColor;
        protected static GreenColor: NxtControl.Drawing.IColor;
        protected _callDesigner: boolean;
        protected _hasLoadingError: boolean;
        protected __onObjectModified: boolean;
        protected _dragDropKeywords: string[];
        protected _initEventListeners(): void;
        protected _stringifyObject(object: any, space?: string): string;
        cache: any[];
        protected _stringifyReplacer(key: string, value: any): any;
        protected reportException(err: Error): void;
        protected _selectionChanged(sender: any, ev: IShapeEventArgs): void;
        protected __onMouseDown(e: MouseEvent): void;
        protected __onMouseMove(e: Event): Shape;
        protected __onMouseUp(e: Event): any;
        protected __onMouseWheel(e: Event, s: any): void;
        protected _handleDblClick(e: Event): boolean;
        private _checkDragDrop(e);
        _handleDragEnter(e: DragEvent): void;
        _handleDragOver(e: DragEvent): boolean;
        _handleDragLeave(e: DragEvent): void;
        _handleDrop(e: DragEvent): boolean;
        _onFocus(): void;
        designMode: string;
        setDesignMode(mode: string): void;
        setSelectDesignMode(): void;
        isSelectDesignMode(): boolean;
        lastPoints: NxtControl.Drawing.IPoint[];
        firstPos: NxtControl.Drawing.IPoint;
        currentDesignShape: Shape;
        designNewShapeMouseDown(e: Event): void;
        designNewShapeMouseMove(e: Event): void;
        createDesigner(shape: Shape): void;
        designNewShapeMouseUp(e: any): void;
        designNewShapeDblClick(e: Event): void;
        private loadingState;
        protected designerType: string;
        protected title: string;
        protected openFaceplates: IOpenFaceplate[];
        load(json: string | Object, designerType: string, dragDropKeywords: string): void;
        protected _onJSONLoaded(): void;
        save(callback?: Function): void;
        protected __serializeBgOverlay(): any;
        protected _setBgOverlay(serialized: any, callback?: Function): void;
        _setCorners(shape: Shape): void;
        shapeToCreate(type: string, creationOptions: string, callBack: boolean): void;
        shapesToCreate(json: any, callBack: any): void;
        trendAddCurve(trendName: any, tagName: any): void;
        selectedShapesToDelete(): void;
        deleteShapes(shapeNames: any): void;
        rootChanged(propertyName: any, json: any): void;
        shapeChanged(shapeName: any, propertyName: any, json: any): void;
        shapesSelected(selection: any, callBack: any): void;
        setProperties(shapeNames: any, properties: any): void;
        protected menuItems: any[];
        reorderShapes(newOrder: any): void;
        deleteEnabled(): boolean;
        deleteActive(): void;
        cutEnabled(): boolean;
        cut(): void;
        copyEnabled(): boolean;
        copy(): void;
        pasteEnabled(): boolean;
        paste(): void;
        redo(): void;
        undo(): void;
        align(where: any): void;
        _getShapeNames(): any[];
        sendSelectionToBack(): void;
        bringSelectionToFront(): void;
        _renderBackground(ctx: any): void;
        _onKeyDown(event: KeyboardEvent): void;
        _moveObject(object: Shape, prop: string, offset: number): void;
    }
}
declare module NxtControl.GuiFramework {
    class ShapeDesigner implements IShapeDesigner {
        shape: Shape;
        canvas: DesignCanvas;
        noEditCircles: boolean;
        __objectMoving: (sender: any, e: IShapeEventArgs) => void;
        __mouseUp: (sender: any, e: IShapeEventArgs) => void;
        protected activeEditCircles: Shape[];
        showSingleSelectBorders: boolean;
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
        hide(): void;
        show(): void;
        _clearEditControls(): void;
        protected _createHolderShape(x: number, y: number): Shape;
        _addEditCircles(): void;
        _shapeMoved(): void;
        _objectMoving(target: Shape, e: Event): void;
        _mouseUp(target: Shape, e: MouseEvent): void;
    }
    class RectangleDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class EllipseDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class LineDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
        _shapeMoved(): void;
        _objectMoving(target: Shape, e: Event): void;
        _mouseUp(target: Shape, e: MouseEvent): void;
        _addEditCircles(): void;
    }
    class FreeTextDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
        _shapeMoved(): void;
        _objectMoving(target: Shape, e: Event): void;
        _mouseUp(target: Shape, e: MouseEvent): void;
        _addEditCircles(): void;
    }
    class PolygonDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
        _shapeMoved(): void;
        _objectMoving(target: Shape, e: Event): void;
        _mouseUp(target: Shape, e: MouseEvent): void;
        _addEditCircles(): void;
    }
    class LabelDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class TextBoxDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class TwoStateButtonDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class ButtonDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class DropDownDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class SymbolDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class GraphicDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class ImageDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class TrendDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
        addSeriesData(data: any, series: any): void;
    }
    class AlarmControlDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
    class TrackerDesigner extends ShapeDesigner {
        initialize(options: {
            shape: Shape;
            canvas: InteractiveCanvas;
            noEditCircles?: boolean;
        }): void;
        propertyChanged(propertyName: string): this;
    }
}
