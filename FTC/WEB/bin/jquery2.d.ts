﻿/// <reference path="jquery.d.ts" />
/// <reference path="jquery.flot.d.ts" />

interface HTMLImageElement {
  _src?: string;
}

interface Event {
  localPointer?: NxtControl.Drawing.IPoint;
  localTarget?: NxtControl.GuiFramework.Shape;
}

interface JQuery {
  /**
   * Attach an event handler function for one or more events to the selected elements.
   *
   * @param events One or more space-separated event types and optional namespaces, such as "click" or "keydown.myPlugin".
   * @param handler A function to execute when the event is triggered. The value false is also allowed as a shorthand for a function that simply does return false. Rest parameter args is for optional parameters passed to jQuery.trigger(). Note that the actual parameters on the event handler function must be marked as optional (? syntax).
   */
  off(events: string, handler: (eventObject: JQueryEventObject, ...args: any[]) => any): JQuery;
}

declare module jquery.flot {
  interface axisOptions {
    mode?: string;
    timezone?: string;
    panRange?: boolean;
    zoomRange?: boolean;
  }

  interface plotOptions {
    canvas?: boolean;
    crosshair?: { mode: string, lineWidth: number };
    zoom?: { interactive: boolean };
    pan?: { interactive: boolean };
  }

  interface seriesOptions {
    id?: any;
    tagName?: string;
    stairs?: boolean;
    scaleName?: string;
  }

  interface plot {
    destroy();

    processData(): void;
    axes?: any;
  }

  interface plotStatic {
    (placeholder: string, data: dataSeries[], options?: plotOptions): plot;
  }

} 