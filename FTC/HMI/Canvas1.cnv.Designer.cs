﻿/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/4/2019
 * Time: 2:31 PM
 * 
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for Canvas1.
	/// </summary>
	partial class Canvas1
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.NEW_TEST_CONVEYOR = new HMI.Main.Symbols.ConveyorH.sDefault();
			this.Spawn1 = new HMI.Main.Symbols.SpawnZone.sDefault();
			this.Bag = new HMI.Main.Symbols.Workpiece.sDefault();
			this.NEW_TEST_CONVEYOR_1 = new HMI.Main.Symbols.ConveyorH.sDefault();
			this.sDefault1 = new HMI.Main.Symbols.ConveyorH.sDefault();
			// 
			// NEW_TEST_CONVEYOR
			// 
			this.NEW_TEST_CONVEYOR.BeginInit();
			this.NEW_TEST_CONVEYOR.AngleIgnore = false;
			this.NEW_TEST_CONVEYOR.Name = "NEW_TEST_CONVEYOR";
			this.NEW_TEST_CONVEYOR.SecurityToken = ((uint)(4294967295u));
			this.NEW_TEST_CONVEYOR.TagName = "DC1643B3FF0AA59E";
			this.NEW_TEST_CONVEYOR.EndInit();
			// 
			// Spawn1
			// 
			this.Spawn1.BeginInit();
			this.Spawn1.AngleIgnore = false;
			this.Spawn1.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 0D, 25D);
			this.Spawn1.Name = "Spawn1";
			this.Spawn1.SecurityToken = ((uint)(4294967295u));
			this.Spawn1.TagName = "7579707B7AFB0720";
			this.Spawn1.EndInit();
			// 
			// Bag
			// 
			this.Bag.BeginInit();
			this.Bag.AngleIgnore = false;
			this.Bag.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 0D, 25D);
			this.Bag.Name = "Bag";
			this.Bag.SecurityToken = ((uint)(4294967295u));
			this.Bag.TagName = "18CFE8304FA6060E";
			this.Bag.EndInit();
			// 
			// NEW_TEST_CONVEYOR_1
			// 
			this.NEW_TEST_CONVEYOR_1.BeginInit();
			this.NEW_TEST_CONVEYOR_1.AngleIgnore = false;
			this.NEW_TEST_CONVEYOR_1.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 300D, 0D);
			this.NEW_TEST_CONVEYOR_1.Name = "NEW_TEST_CONVEYOR_1";
			this.NEW_TEST_CONVEYOR_1.SecurityToken = ((uint)(4294967295u));
			this.NEW_TEST_CONVEYOR_1.TagName = "DC1643B3FF0AA59E";
			this.NEW_TEST_CONVEYOR_1.EndInit();
			// 
			// sDefault1
			// 
			this.sDefault1.BeginInit();
			this.sDefault1.AngleIgnore = false;
			this.sDefault1.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 600D, 0D);
			this.sDefault1.Name = "sDefault1";
			this.sDefault1.SecurityToken = ((uint)(4294967295u));
			this.sDefault1.TagName = "C6E28B82B92A9B69";
			this.sDefault1.EndInit();
			// 
			// Canvas1
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0D)), ((float)(0D)), ((float)(1280D)), ((float)(900D)));
			this.Brush = new NxtControl.Drawing.Brush("CanvasBrush");
			this.Name = "Canvas1";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.sDefault1,
									this.NEW_TEST_CONVEYOR,
									this.Spawn1,
									this.NEW_TEST_CONVEYOR_1,
									this.Bag});
			this.Size = new System.Drawing.Size(1280, 900);
		}
		private HMI.Main.Symbols.ConveyorH.sDefault sDefault1;
		private HMI.Main.Symbols.ConveyorH.sDefault NEW_TEST_CONVEYOR_1;
		private HMI.Main.Symbols.Workpiece.sDefault Bag;
		private HMI.Main.Symbols.SpawnZone.sDefault Spawn1;
		private HMI.Main.Symbols.ConveyorH.sDefault NEW_TEST_CONVEYOR;
		#endregion
	}
}
