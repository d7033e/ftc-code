﻿/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/10/2019
 * Time: 4:26 PM
 * 
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.SpawnZone
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SpawnZoneBorder = new NxtControl.GuiFramework.Rectangle();
			this.SpawnZoneMiddle = new NxtControl.GuiFramework.Ellipse();
			// 
			// SpawnZoneBorder
			// 
			this.SpawnZoneBorder.Bounds = new NxtControl.Drawing.RectF(((float)(100D)), ((float)(100D)), ((float)(50D)), ((float)(50D)));
			this.SpawnZoneBorder.Brush = new NxtControl.Drawing.Brush();
			this.SpawnZoneBorder.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.SpawnZoneBorder.Name = "SpawnZoneBorder";
			this.SpawnZoneBorder.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 5F, NxtControl.Drawing.DashStyle.Solid);
			this.SpawnZoneBorder.Tooltip = "A bag can spawn here.";
			// 
			// SpawnZoneMiddle
			// 
			this.SpawnZoneMiddle.Bounds = new NxtControl.Drawing.RectF(((float)(120D)), ((float)(120D)), ((float)(10D)), ((float)(10D)));
			this.SpawnZoneMiddle.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18))));
			this.SpawnZoneMiddle.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.SpawnZoneMiddle.Name = "SpawnZoneMiddle";
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.SpawnZoneBorder,
									this.SpawnZoneMiddle});
			this.SymbolSize = new System.Drawing.Size(600, 400);
		}
		private NxtControl.GuiFramework.Ellipse SpawnZoneMiddle;
		private NxtControl.GuiFramework.Rectangle SpawnZoneBorder;
		#endregion
	}
}
