﻿/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/8/2019
 * Time: 1:53 PM
 * 
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.Workpiece
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
		public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
      this.REQ_Fired += REQ_Fired_EventHandler;
      
		}
		
		void REQ_Fired_EventHandler(object sender, HMI.Main.Symbols.Workpiece.REQEventArgs ea)
		{
			NxtControl.Drawing.PointF newPos = Workpiece.Location;
			newPos.X = (double)ea.position_HMI_X;
			newPos.Y = (double)ea.position_HMI_Y;
			Workpiece.Location = newPos;
		}
		
		void Position_HMI_XValueChanged(object sender, ValueChangedEventArgs e)
		{
			NxtControl.Drawing.PointF newPos = Workpiece.Location;
			newPos.X = (double)(float)e.Value;
			Workpiece.Location = newPos;
		}
		
		void Position_HMI_YValueChanged(object sender, ValueChangedEventArgs e)
		{
			NxtControl.Drawing.PointF newPos = Workpiece.Location;
			newPos.Y = (double)(float)e.Value;
			Workpiece.Location = newPos;
		}
	}
}
