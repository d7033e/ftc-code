/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/8/2019
 * Time: 1:53 PM
 * 
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.Workpiece
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Workpiece = new NxtControl.GuiFramework.Ellipse();
			this.position_HMI_X = new System.HMI.Symbols.Base.Execute<float>();
			this.position_HMI_Y = new System.HMI.Symbols.Base.Execute<float>();
			// 
			// Workpiece
			// 
			this.Workpiece.Bounds = new NxtControl.Drawing.RectF(((float)(0D)), ((float)(0D)), ((float)(50D)), ((float)(50D)));
			this.Workpiece.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(114)), ((byte)(114)), ((byte)(114)))}, new float[] {
															0F,
															1F})));
			this.Workpiece.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.Workpiece.Name = "Workpiece";
			this.Workpiece.Tooltip = "A black workpiece.";
			// 
			// position_HMI_X
			// 
			this.position_HMI_X.BeginInit();
			this.position_HMI_X.AngleIgnore = false;
			this.position_HMI_X.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 71D, 194D);
			this.position_HMI_X.IsOnlyInput = true;
			this.position_HMI_X.Location = new NxtControl.Drawing.PointF(double.NaN, double.NaN);
			this.position_HMI_X.Name = "position_HMI_X";
			this.position_HMI_X.Size = new NxtControl.Drawing.SizeF(1D, 1D);
			this.position_HMI_X.TagName = "position_HMI_X";
			this.position_HMI_X.Value = 0F;
			this.position_HMI_X.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.Position_HMI_XValueChanged);
			this.position_HMI_X.EndInit();
			// 
			// position_HMI_Y
			// 
			this.position_HMI_Y.BeginInit();
			this.position_HMI_Y.AngleIgnore = false;
			this.position_HMI_Y.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 82D, 211D);
			this.position_HMI_Y.IsOnlyInput = true;
			this.position_HMI_Y.Location = new NxtControl.Drawing.PointF(double.NaN, double.NaN);
			this.position_HMI_Y.Name = "position_HMI_Y";
			this.position_HMI_Y.Size = new NxtControl.Drawing.SizeF(1D, 1D);
			this.position_HMI_Y.TagName = "";
			this.position_HMI_Y.Value = 0F;
			this.position_HMI_Y.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.Position_HMI_YValueChanged);
			this.position_HMI_Y.EndInit();
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.Workpiece,
									this.position_HMI_X,
									this.position_HMI_Y});
			this.SymbolSize = new System.Drawing.Size(600, 400);
		}
		private System.HMI.Symbols.Base.Execute<float> position_HMI_Y;
		private System.HMI.Symbols.Base.Execute<float> position_HMI_X;
		private NxtControl.GuiFramework.Ellipse Workpiece;
		#endregion
	}
}
