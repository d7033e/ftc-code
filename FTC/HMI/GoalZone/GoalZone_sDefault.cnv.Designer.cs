/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/10/2019
 * Time: 4:28 PM
 * 
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.GoalZone
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      			this.Name = "sDefault";
		}
		#endregion
	}
}
