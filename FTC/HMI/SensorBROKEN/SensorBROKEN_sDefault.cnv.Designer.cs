/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/3/2019
 * Time: 1:55 PM
 * 
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.SensorBROKEN
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SensorBase = new NxtControl.GuiFramework.Rectangle();
			this.lightON = new NxtControl.GuiFramework.Rectangle();
			this.lightOFF = new NxtControl.GuiFramework.Rectangle();
			this.Sensor = new NxtControl.GuiFramework.Group();
			this.SensorPos = new System.HMI.Symbols.Base.Execute<float>();
			// 
			// SensorBase
			// 
			this.SensorBase.Bounds = new NxtControl.Drawing.RectF(((float)(100D)), ((float)(100D)), ((float)(20D)), ((float)(50D)));
			this.SensorBase.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.DiagonalLeftTop));
			this.SensorBase.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.SensorBase.Name = "SensorBase";
			this.SensorBase.Tooltip = "The base of the sensor.";
			// 
			// lightON
			// 
			this.lightON.Bounds = new NxtControl.Drawing.RectF(((float)(105D)), ((float)(105D)), ((float)(10D)), ((float)(10D)));
			this.lightON.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))),
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255)))}, new float[] {
															0F,
															0.5F,
															1F})));
			this.lightON.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.lightON.Name = "lightON";
			this.lightON.Tooltip = "The light is on!";
			// 
			// lightOFF
			// 
			this.lightOFF.Bounds = new NxtControl.Drawing.RectF(((float)(105D)), ((float)(105D)), ((float)(10D)), ((float)(10D)));
			this.lightOFF.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(26)), ((byte)(62)), ((byte)(114))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(134)), ((byte)(50)), ((byte)(46)))}, new float[] {
															0F,
															1F})));
			this.lightOFF.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.lightOFF.Name = "lightOFF";
			this.lightOFF.Tooltip = "The light is off!";
			// 
			// Sensor
			// 
			this.Sensor.BeginInit();
			this.Sensor.Name = "Sensor";
			this.Sensor.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.SensorBase,
									this.lightON,
									this.lightOFF});
			this.Sensor.EndInit();
			// 
			// SensorPos
			// 
			this.SensorPos.BeginInit();
			this.SensorPos.AngleIgnore = false;
			this.SensorPos.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 70D, 205D);
			this.SensorPos.IsOnlyInput = true;
			this.SensorPos.Location = new NxtControl.Drawing.PointF(0D, 0D);
			this.SensorPos.Name = "SensorPos";
			this.SensorPos.Size = new NxtControl.Drawing.SizeF(1D, 1D);
			this.SensorPos.TagName = "SensorPos";
			this.SensorPos.Value = 0F;
			this.SensorPos.EndInit();
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.Sensor,
									this.SensorPos});
			this.SymbolSize = new System.Drawing.Size(600, 400);
		}
		private System.HMI.Symbols.Base.Execute<float> SensorPos;
		private NxtControl.GuiFramework.Group Sensor;
		private NxtControl.GuiFramework.Rectangle lightOFF;
		private NxtControl.GuiFramework.Rectangle lightON;
		private NxtControl.GuiFramework.Rectangle SensorBase;
		#endregion
	}
}
