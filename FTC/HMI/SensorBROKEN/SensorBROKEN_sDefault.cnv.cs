/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/3/2019
 * Time: 1:55 PM
 * 
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.SensorBROKEN
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
		public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
      this.REQ_Fired += REQ_Fired_EventHandler;
      this.lightOFF.Visible = true;
      SensorPos.Value = 9;
		}
		
		void REQ_Fired_EventHandler(object sender, HMI.Main.Symbols.SensorBROKEN.REQEventArgs ea)
		{
			this.lightOFF.Visible = !this.lightOFF.Visible;
		}
		
		
	}
}
