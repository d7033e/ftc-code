/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/3/2019
 * Time: 1:37 PM
 * 
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.ConveyorV
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sDefault));
			this.ConveyorBase = new NxtControl.GuiFramework.Rectangle();
			this.ConveyorMover = new NxtControl.GuiFramework.Rectangle();
			this.Sensor_1 = new HMI.Main.Symbols.SensorH.sDefault();
			this.Sensor_2 = new HMI.Main.Symbols.SensorH.sDefault();
			this.conveyor_position_x = new System.HMI.Symbols.Base.Execute<float>();
			// 
			// ConveyorBase
			// 
			this.ConveyorBase.Bounds = new NxtControl.Drawing.RectF(((float)(0D)), ((float)(0D)), ((float)(300D)), ((float)(100D)));
			this.ConveyorBase.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(186)), ((byte)(70)), ((byte)(66))));
			this.ConveyorBase.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.ConveyorBase.Name = "ConveyorBase";
			this.ConveyorBase.Tooltip = "The base of the conveyor.";
			// 
			// ConveyorMover
			// 
			this.ConveyorMover.Bounds = new NxtControl.Drawing.RectF(((float)(0D)), ((float)(25D)), ((float)(300D)), ((float)(50D)));
			this.ConveyorMover.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.ConveyorMover.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("ConveyorMover.ImageStream")));
			this.ConveyorMover.Name = "ConveyorMover";
			// 
			// Sensor_1
			// 
			this.Sensor_1.BeginInit();
			this.Sensor_1.AngleIgnore = false;
			this.Sensor_1.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 25D, 75D);
			this.Sensor_1.Name = "Sensor_1";
			this.Sensor_1.SecurityToken = ((uint)(4294967295u));
			this.Sensor_1.TagName = "Sensor_1";
			this.Sensor_1.EndInit();
			// 
			// Sensor_2
			// 
			this.Sensor_2.BeginInit();
			this.Sensor_2.AngleIgnore = false;
			this.Sensor_2.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 255D, 75D);
			this.Sensor_2.Name = "Sensor_2";
			this.Sensor_2.SecurityToken = ((uint)(4294967295u));
			this.Sensor_2.TagName = "Sensor_2";
			this.Sensor_2.EndInit();
			// 
			// conveyor_position_x
			// 
			this.conveyor_position_x.BeginInit();
			this.conveyor_position_x.AngleIgnore = false;
			this.conveyor_position_x.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 163D, 259D);
			this.conveyor_position_x.Location = new NxtControl.Drawing.PointF(double.NaN, double.NaN);
			this.conveyor_position_x.Name = "conveyor_position_x";
			this.conveyor_position_x.Size = new NxtControl.Drawing.SizeF(1D, 1D);
			this.conveyor_position_x.TagName = "conveyor_position_x";
			this.conveyor_position_x.Value = 0F;
			this.conveyor_position_x.EndInit();
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.ConveyorBase,
									this.ConveyorMover,
									this.Sensor_1,
									this.Sensor_2,
									this.conveyor_position_x,
			                     });
			this.SymbolSize = new System.Drawing.Size(600, 400);
		}
		private NxtControl.GuiFramework.Rectangle ConveyorMover;
		private System.HMI.Symbols.Base.Execute<float> conveyor_position_x;
		private HMI.Main.Symbols.SensorH.sDefault Sensor_2;
		private HMI.Main.Symbols.SensorH.sDefault Sensor_1;
		private NxtControl.GuiFramework.Rectangle ConveyorBase;
		#endregion
	}
}
