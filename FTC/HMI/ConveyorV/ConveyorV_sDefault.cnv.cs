/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/3/2019
 * Time: 1:37 PM
 * 
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.ConveyorV
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
		public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
      this.REQ_Fired += REQ_Fired_EventHandler;
		}
		
		void ConveyorBaseValueChanged(object sender, EventArgs e)
		{
			
		}
		
		void REQ_Fired_EventHandler(object sender, HMI.Main.Symbols.ConveyorV.REQEventArgs ea)
		{
		    
		  if (this.ConveyorMover.Bounds.Contains((float)ea.Workpiece_Position_x, (float)ea.Workpiece_position_y))
      {
        Console.WriteLine("The variable is set to true.");
      }
      else
      {
        Console.WriteLine("The variable is set to false.");
      }
		}
	}
}
