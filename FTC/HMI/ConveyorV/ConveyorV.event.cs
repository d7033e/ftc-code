/*
 * Created by nxtSTUDIO.
 * User: A1203-admin
 * Date: 10/3/2019
 * Time: 1:37 PM
 * 
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #ConveyorV_HMI;

namespace HMI.Main.Symbols.ConveyorV
{

  public class REQEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public REQEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_S1_O(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,0, ref var);
      if (ret) value = (System.Boolean) var;
      return ret;
    }

    public System.Boolean? S1_O
    { get {
      if (accessorService == null)
        return null;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,0, ref var);
      if (!ret) return null;
      return (System.Boolean) var;
    }  }

    public bool Get_S2_O(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,1, ref var);
      if (ret) value = (System.Boolean) var;
      return ret;
    }

    public System.Boolean? S2_O
    { get {
      if (accessorService == null)
        return null;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,1, ref var);
      if (!ret) return null;
      return (System.Boolean) var;
    }  }

    public bool Get_Workpiece_Position_x(ref System.Single value)
    {
      if (accessorService == null)
        return false;
      float var = 0;
      bool ret = accessorService.GetFloatValue(channelId, cookie, eventIndex, true,2, ref var);
      if (ret) value = (System.Single) var;
      return ret;
    }

    public System.Single? Workpiece_Position_x
    { get {
      if (accessorService == null)
        return null;
      float var = 0;
      bool ret = accessorService.GetFloatValue(channelId, cookie, eventIndex, true,2, ref var);
      if (!ret) return null;
      return (System.Single) var;
    }  }

    public bool Get_Workpiece_position_y(ref System.Single value)
    {
      if (accessorService == null)
        return false;
      float var = 0;
      bool ret = accessorService.GetFloatValue(channelId, cookie, eventIndex, true,3, ref var);
      if (ret) value = (System.Single) var;
      return ret;
    }

    public System.Single? Workpiece_position_y
    { get {
      if (accessorService == null)
        return null;
      float var = 0;
      bool ret = accessorService.GetFloatValue(channelId, cookie, eventIndex, true,3, ref var);
      if (!ret) return null;
      return (System.Single) var;
    }  }


  }

}

namespace HMI.Main.Symbols.ConveyorV
{

  public class CNFEventArgs : System.EventArgs
  {
    public CNFEventArgs()
    {
    }
    private System.Single? conveyor_position_x_field = null;
    public System.Single? conveyor_position_x
    {
       get { return conveyor_position_x_field; }
       set { conveyor_position_x_field = value; }
    }
    private System.Single? conveyor_position_y_field = null;
    public System.Single? conveyor_position_y
    {
       get { return conveyor_position_y_field; }
       set { conveyor_position_y_field = value; }
    }

  }

}

namespace HMI.Main.Symbols.ConveyorV
{
  partial class sDefault
  {

    private event EventHandler<HMI.Main.Symbols.ConveyorV.REQEventArgs> REQ_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
          {
            try
            {
              REQ_Fired(this, new HMI.Main.Symbols.ConveyorV.REQEventArgs(channelId, cookie, eventIndex));
            }
            catch (System.Exception e)
            {
              NxtControl.Services.LoggingService.ErrorFormatted(@"In Event Callback for event:'{0}' Type:'{1}' CAT:'{2}' came exception:{3}
stack Trace:
{4}","REQ_Fired", this.GetType().Name, this.CATName, e.Message, e.StackTrace);
            }
          }
        break; 

      }
    }
    public bool FireEvent_CNF(System.Single conveyor_position_x, System.Single conveyor_position_y)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {conveyor_position_x, conveyor_position_y});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.ConveyorV.CNFEventArgs ea)
    {
      object[] _values_ = new object[2];
      if (ea.conveyor_position_x.HasValue) _values_[0] = ea.conveyor_position_x.Value;
      if (ea.conveyor_position_y.HasValue) _values_[1] = ea.conveyor_position_y.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Single conveyor_position_x, bool ignore_conveyor_position_x, System.Single conveyor_position_y, bool ignore_conveyor_position_y)
    {
      object[] _values_ = new object[2];
      if (!ignore_conveyor_position_x) _values_[0] = conveyor_position_x;
      if (!ignore_conveyor_position_y) _values_[1] = conveyor_position_y;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }

  }
}
#endregion #ConveyorV_HMI;

#endregion Definitions;
