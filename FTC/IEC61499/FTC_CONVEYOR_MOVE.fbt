<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE FBType SYSTEM "../LibraryElement.dtd">
<FBType GUID="C9C9E0F69B35C28D" Name="FTC_CONVEYOR_MOVE" Comment="Basic Function Block Type" Namespace="Main">
  <Attribute Name="Configuration.FB.IDCounter" Value="0" />
  <Identification Standard="61499-2" />
  <VersionInfo Organization="nxtControl GmbH" Version="0.0" Author="A1203-admin" Date="10/4/2019" Remarks="Template" />
  <InterfaceList>
    <EventInputs>
      <Event Name="INIT" Comment="Initialization Request">
        <With Var="speed" />
        <With Var="position" />
      </Event>
      <Event Name="C_CYCLE" Comment="Clock cycle event.">
        <With Var="workpiece_position" />
      </Event>
      <Event Name="SENS_UPD" Comment="Sensor update event.">
        <With Var="fwd" />
        <With Var="bwd" />
      </Event>
      <Event Name="OP_UPD" Comment="Operator update event.">
        <With Var="speed" />
        <With Var="fwd" />
        <With Var="bwd" />
      </Event>
    </EventInputs>
    <EventOutputs>
      <Event Name="INITO" Comment="Initialization Confirm">
        <With Var="sensor_1_pos" />
        <With Var="sensor_2_pos" />
        <With Var="position_offset" />
        <With Var="pv1" />
        <With Var="pv2" />
      </Event>
      <Event Name="SENS_CHG" Comment="Sensor change execution confirmation.">
        <With Var="pv1" />
        <With Var="pv2" />
        <With Var="sensor_1_pos" />
        <With Var="sensor_2_pos" />
        <With Var="contains_workpiece" />
      </Event>
      <Event Name="OP_CHG" Comment="Operator change execution confirmation.">
        <With Var="pv1" />
        <With Var="pv2" />
      </Event>
    </EventOutputs>
    <InputVars>
      <VarDeclaration Name="fwd" Type="REAL" InitialValue="0" Comment="Forwards max speed." />
      <VarDeclaration Name="bwd" Type="REAL" InitialValue="0" Comment="Backwards max speed." />
      <VarDeclaration Name="speed" Type="REAL" InitialValue="0" Comment="Speed assigned to conveyor." />
      <VarDeclaration Name="position" Type="REAL" ArraySize="2" InitialValue="[2(0)]" Comment="Position of conveyor in x-axis and y-axis." />
      <VarDeclaration Name="workpiece_position" Type="REAL" ArraySize="2" InitialValue="[2(0)]" />
    </InputVars>
    <OutputVars>
      <VarDeclaration Name="pv1" Type="REAL" InitialValue="0" Comment="Fwd multiplied by speed." />
      <VarDeclaration Name="pv2" Type="REAL" InitialValue="0" Comment="Bwd multiplied by speed." />
      <VarDeclaration Name="sensor_1_pos" Type="REAL" ArraySize="2" InitialValue="[2(0)]" Comment="Put sensor 1 into position." />
      <VarDeclaration Name="sensor_2_pos" Type="REAL" ArraySize="2" InitialValue="[2(0)]" Comment="Put sensor 2 into position." />
      <VarDeclaration Name="position_offset" Type="REAL" ArraySize="2" InitialValue="[2(0)]" Comment="The offset from origin in x-axis and y-axis." />
      <VarDeclaration Name="contains_workpiece" Type="BOOL" InitialValue="FALSE" Comment="Number of workpieces on conveyor." />
    </OutputVars>
  </InterfaceList>
  <BasicFB>
    <Attribute Name="FBType.Basic.Algorithm.Order" Value="INIT,SENS_UPD,HIT_TEST" />
    <ECC>
      <ECState Name="START" Comment="Initial State" x="552.9412" y="429.4117" />
      <ECState Name="INIT" Comment="Initialization" x="923.5294" y="141.1765">
        <ECAction Algorithm="INIT" Output="INITO" />
      </ECState>
      <ECState Name="SENS_UPD" Comment="Normal execution" x="481.647" y="904.9412">
        <ECAction Algorithm="SENS_UPD" Output="SENS_CHG" />
      </ECState>
      <ECState Name="HIT_TEST" x="1496" y="596">
        <ECAction Algorithm="HIT_TEST" />
      </ECState>
      <ECTransition Source="START" Destination="INIT" Condition="INIT" x="731.1476" y="204.8375" />
      <ECTransition Source="INIT" Destination="START" Condition="1" x="797.5924" y="348.3232" />
      <ECTransition Source="START" Destination="SENS_UPD" Condition="SENS_UPD" x="599.018" y="678.2965">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="156.3658,155.0356,151.7286,185.966" />
      </ECTransition>
      <ECTransition Source="SENS_UPD" Destination="START" Condition="1" x="463.0322" y="591.7875">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="94,157,131.6422,126.3289" />
      </ECTransition>
      <ECTransition Source="START" Destination="HIT_TEST" Condition="C_CYCLE" x="1024.932" y="428.1029">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="228.7555,97.956,282.5746,107.463" />
      </ECTransition>
      <ECTransition Source="HIT_TEST" Destination="START" Condition="1" x="1003.063" y="593.4543">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="278.2258,157.0818,224.4067,147.5748" />
      </ECTransition>
    </ECC>
    <Algorithm Name="INIT" Comment="Initialization algorithm">
      <ST Text="sensor_1_pos[0] := position[0] + 25;&#xD;&#xA;sensor_1_pos[1] := position[1] + 75;&#xD;&#xA;sensor_2_pos[0] := position[0] + 255;&#xD;&#xA;sensor_2_pos[1] := position[1] + 75;&#xD;&#xA;position_offset[0] := position[0] + 300;&#xD;&#xA;position_offset[1] := position[1] + 50;" />
    </Algorithm>
    <Algorithm Name="SENS_UPD" Comment="Normally executed algorithm">
      <ST Text="IF contains_workpiece THEN&#xD;&#xA;	pv1 := fwd * speed;&#xD;&#xA;	pv2 := bwd * speed;&#xD;&#xA;ELSE&#xD;&#xA;	pv1 := 0;&#xD;&#xA;	pv2 := 0;&#xD;&#xA;END_IF;" />
    </Algorithm>
    <Algorithm Name="HIT_TEST" Comment="  A simple hit test on the x-axis to see if the conveyor has a workpiece.">
      <ST Text="IF (workpiece_position[0] &gt;= position[0]) AND (workpiece_position[0] &lt; position_offset[0]) THEN;&#xD;&#xA;	contains_workpiece := TRUE;&#xD;&#xA;ELSE&#xD;&#xA;	contains_workpiece := FALSE;&#xD;&#xA;END_IF;" />
    </Algorithm>
  </BasicFB>
</FBType>