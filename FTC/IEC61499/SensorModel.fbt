<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE FBType SYSTEM "../LibraryElement.dtd">
<FBType GUID="66F5854AAFE4EFCD" Name="SensorModel" Comment="Basic Function Block Type" Namespace="Main">
  <Attribute Name="Configuration.FB.IDCounter" Value="0" />
  <Identification Standard="61499-2" />
  <VersionInfo Organization="nxtControl GmbH" Version="0.0" Author="A1203-admin" Date="10/3/2019" Remarks="Template" />
  <InterfaceList>
    <EventInputs>
      <Event Name="INIT" Comment="Initialization Request">
        <With Var="target_pos" />
        <With Var="range" />
        <With Var="sensor_pos" />
      </Event>
      <Event Name="REQ" Comment="Normal Execution Request">
        <With Var="target_pos" />
      </Event>
    </EventInputs>
    <EventOutputs>
      <Event Name="INITO" Comment="Initialization Confirm">
        <With Var="result" />
      </Event>
      <Event Name="CHG" Comment="Execution Confirmation">
        <With Var="result" />
      </Event>
    </EventOutputs>
    <InputVars>
      <VarDeclaration Name="target_pos" Type="REAL" ArraySize="2" InitialValue="[2(0)]" Comment="Sensed item x coordinate." />
      <VarDeclaration Name="range" Type="REAL" ArraySize="2" InitialValue="[2(0)]" Comment="Range of sensor." />
      <VarDeclaration Name="sensor_pos" Type="REAL" ArraySize="2" InitialValue="[2(0)]" Comment="Position of sensor in x-axis and y-axis." />
    </InputVars>
    <OutputVars>
      <VarDeclaration Name="result" Type="BOOL" Comment="Output event qualifier" />
    </OutputVars>
  </InterfaceList>
  <BasicFB>
    <Attribute Name="FBType.Basic.Algorithm.Order" Value="INIT,Compare,FILTER" />
    <InternalVars>
      <VarDeclaration Name="sensor_tripped" Type="BOOL" InitialValue="FALSE" Comment="Set sensor tripped." />
    </InternalVars>
    <ECC>
      <ECState Name="START" Comment="Initial State" x="552.9412" y="405.4117" />
      <ECState Name="INIT" Comment="Initialization" x="923.5294" y="141.1765">
        <ECAction Algorithm="INIT" Output="INITO" />
      </ECState>
      <ECState Name="COMP" x="840" y="1384">
        <ECAction Algorithm="FILTER" />
      </ECState>
      <ECState Name="FILTER" x="956" y="808.0001">
        <ECAction Algorithm="Compare" Output="CHG" />
      </ECState>
      <ECTransition Source="START" Destination="INIT" Condition="INIT" x="731.3856" y="192.1985">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="175.2164,49.9848,193.9012,36.66225" />
      </ECTransition>
      <ECTransition Source="INIT" Destination="START" Condition="1" x="794.8165" y="338.9948">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="208.4151,82.01781,189.7303,95.34035" />
      </ECTransition>
      <ECTransition Source="FILTER" Destination="START" Condition="sensor_tripped = result" x="615.4134" y="641.8035">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="160.2153,175.4422,135.6942,150.721" />
      </ECTransition>
      <ECTransition Source="START" Destination="FILTER" Condition="REQ" x="821.2697" y="552.9979">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="193.8445,121.5625,218.7255,146.4145" />
      </ECTransition>
      <ECTransition Source="COMP" Destination="START" Condition="1" x="357.9656" y="961.6765">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="97.31624,290,38.31624,202" />
      </ECTransition>
      <ECTransition Source="FILTER" Destination="COMP" Condition="sensor_tripped &lt;&gt; result" x="977.9361" y="1110.807">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="252.9655,259.2841,245.0504,298.5871" />
      </ECTransition>
    </ECC>
    <Algorithm Name="INIT" Comment="Initialization algorithm">
      <ST Text=";" />
    </Algorithm>
    <Algorithm Name="FILTER" Comment="Normally executed algorithm">
      <ST Text="sensor_tripped := result;" />
    </Algorithm>
    <Algorithm Name="Compare" Comment="Compares wether the sensor target is inside of the range of the sensor.">
      <ST Text="IF (target_pos[0] &gt;= sensor_pos[0] + range[0]  ) AND (target_pos[0] &lt;= sensor_pos[0] + range[1]) THEN&#xD;&#xA;	result := TRUE;&#xD;&#xA;ELSE;&#xD;&#xA;	result := FALSE;&#xD;&#xA;END_IF;&#xD;&#xA;" />
    </Algorithm>
  </BasicFB>
</FBType>